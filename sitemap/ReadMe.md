for ref see DEV-1336

a command line script to generate a sitemap.xml file suitable for google

uses 2 data sources to list all the valid URLs for the site which we want Google to crawl:

  1. a list of the php files (with a few excluded)
  2. a list of all the cylinder numbers (derived from a directory listing) from special/Cylinders on the network drive.

the generator also looks up the "last modified" date for each cylinder URL. This date is derived from the MARC 005 field. This data is fetched from an Exlibris Alma server via the SRU API. The process of fetching this data is rather slow.  For example, creating a 2.5 mg sitemap.xml file for 14,844 cylinder records took about 8 hours.


## Prerequisites:

1. PHP command line interface must be installed and available

1. In the same directory as the generate_sitemap.php file you must have a file `listing_of_PHP_files.txt`
This file should have one file listed per line.  Make sure you are using Unix Line Feed &lt;LF&gt; endings not Windows "CRLF" line endings. 

1. Any files you would like excluded from the sitemap should be listed in the file `sitemap_exclude.php`

1. In the same directory as the generate_sitemap.php file you must have a file `listing_of_cylinder_numbers.txt`
In this file list one cylinder number per line.  Make sure you are using Unix Line Feed &lt;LF&gt; endings not Windows "CRLF" line endings. 

It is up to the operator to generate a list of cylinder numbers but one way to do so would be to list all the directories where the cylinders are stored on the network share drive.

for example:

```
cut -d / -f 2 $(ls -1 path/to/cylinders/share/directory)
```

to generate a sitemap run the generator like this:

```
php -f generate_sitemap.php > sitemap.xml
```

where `sitemap.xml` is your desired output file name. 
