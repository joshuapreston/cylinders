<?php
/*
See the ReadMe.md for more info on this program

*/
date_default_timezone_set ( "America/Los_Angeles" );
$site_base_url = "http://cylinders.library.ucsb.edu/";


$docroot = "/Users/ilessing/Documents/Issues/ALMA-42-Cylinders-Alma/ubuntu16.4/html/"; // path to the website's document root.

require_once("../config/main.php");
require_once("sitemap_exclude.php");
require_once("../functions.php");
// ========================= execution =================================


// Output the XML header
echo "<?xml version=\"1.0\" encoding=\"utf-8\"?><urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">";
echo "\n";


// Output the XML for the files that make up the static content of the site.
$files = file("listing_of_PHP_files.txt", FILE_IGNORE_NEW_LINES);

foreach($files as $file_line){
  $file = trim($file_line);
  if(in_array($file, $sitemap_exclude)){
    continue;
  }
  $fullpath = $docroot . $file;
  $lastmod = get_file_mod_time($fullpath);
  echo gen_file_url_node($fullpath, $lastmod);
}


// Output the XML for the Cylinders detail page URLs

$cylinder_numbers = file("listing_of_cylinder_numbers.txt", FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
foreach ($cylinder_numbers as $key => $cylinder) {
  // echo "Cylinder: $cylinder\n";//debug
// $alma_query_url = "https://ucsb.alma.exlibrisgroup.com/view/sru/01UCSB_INST?version=1.2&operation=searchRetrieve&schema=marcxml&maximumRecords=6&query=alma.PermanentCallNumber%3D".$cylinder."+and+alma.mms_tagSuppressed%3Dfalse+and+alma.elocation%3DCylinder%29+sortBy+alma.title%2Fsort.ascending";

 $record_mod_time = get_record_mod_time($cylinder);
 echo gen_file_url_node("http://cylinders.library.ucsb.edu/search.php?query_type=call_number&query=cylinder".$cylinder."&nq=1", $record_mod_time);

}

// Output the closing XML tag
echo "</urlset>\n";
// ================= functions =========================================

function get_record_mod_time($cylinder_number){
/*
pass in a cylinder number string and get back the last modified time (string) of the alma record.
Uses Alma's SRU API to retrieve MARC 005 field of the record.  This is the 'time of last transaction' which roughly translates to the last update of the holding level of the record.
*/
  $alma_query_url = build_sru_query('call_number', 'Cylinder'.$cylinder);
  // echo $alma_query_url ."\n"; //debug
  $SRU_query_response = fetch_from_sru($alma_query_url);
  $SRU_query_response_obj = simplexml_load_string($SRU_query_response);
  foreach($SRU_query_response_obj->records->record->recordData->record->controlfield as $record){
    $record_array[] = (string) $record[1];
  }
  //  example value of $record_array[1] 20161203003009.0
  $mod_date = date('c',strtotime(substr($record_array[1], 0,8)));
  return $mod_date;
}


function get_file_mod_time($filename){
//  pass in a filename and get back a modification time in ISO 8601 format
//  example:  2017-06-08T14:52:06-07:00
  if(file_exists($filename)){
    $file_mod_time = filemtime($filename);
  }else{
    echo "\nError:". __LINE__ . " filename:" . $filename ." not found\n";
  }
  $file_mod_time = date('c', $file_mod_time);
  return $file_mod_time;
}

function gen_file_url_node($filepath, $lastmod){
/*
  pass in a filename path (string) and get back an XML node string.
  example:  <url><loc>http://cylinders.library.ucsb.edu/wwi.php</loc><lastmod>2017-06-08T14:52:06-07:00</lastmod></url>
*/
  global $site_base_url;
  $path_parts = pathinfo($filepath);
  $filename = $path_parts['basename'];

  $my_node = <<<EOT
<url>
  <loc>$site_base_url$filename</loc>
  <lastmod>$lastmod</lastmod>
</url>

EOT;
  return $my_node;
}


