<?php
require_once('config/main.php');
require_once('config/smarty.php');
require_once('functions.php');

$mms_id = sanitize($_GET['query'], 'url');

$cached_record = fetch_cylinder_record_from_memcache(sanitize($_GET['query'], 'url'));

if($cached_record === false){
// somehow there is nothing in memcache for this mms_id
  $SRUquery = '&query=' . urlencode("alma.mms_tagSuppressed=false and alma.elocation=Cylinder and alma.mms_id=$mms_id)");

  /** having built an SRU query we get query results either from cache or SRUserver **/
  $SRU_query_response = fetch_sru_results($SRUquery);

  $searchRetrieveResponse = simplexml_load_string($SRU_query_response);
// now to loop over searchRetrieveResponse->records->record extracting and caching the data we're interested in
  $recordings = get_recordings($searchRetrieveResponse);
  foreach($searchRetrieveResponse->records->record as $record){
    $currentRecordNumber++;
    $recordPosition = (integer)$record->recordPosition; // which record of all the records found in the search.
    $pagination[] = $recordPosition;
    $current_record = extract_record_info($record);
    $current_record['recordings'] = $recordings;
    $mms_id = $current_record['mms_id'];

  // Cache each record individually
    $memcache = new Memcached();
    $cacheAvailable = $memcache->addServer(MEMCACHED_HOST, MEMCACHED_PORT);
    if($cacheAvailable){
      // memcache service is available

      if (!$results = $memcache->get($mms_id)) {
        // this cylinder record has not yet been stored in memcache
        // so we'll cache it using the MMS_ID as the key
        $memcache->set($mms_id, $current_record, CACHE_LIFE_SECONDS);
        if(DEVELOPMENT === true){error_log(" ".__FILE__." ".__LINE__." Caching results from Alma memcachekey: ".$mms_id );} //debug
      }
    }else{
      // memcache service is NOT available so we fall back to SRU
      error_log('memcache service not available. record not cached');
    }
    $record_array[$currentRecordNumber] = $current_record;
  } // end foreach record returned from SRU

// now that we've put the record into the cache we get it out again and pass it to template engine
  $cached_record = fetch_cylinder_record_from_memcache(sanitize($_GET['query'],'url'));
  if(DEVELOPMENT === true){error_log(" ". __FILE__." ".__LINE__." fetching very recently stored results from Memcache");} //debug
}  // end if cached_record === false



$result_set = $_SESSION['result_set'];

if(isset($_SESSION['result_set'])){
  $current_record_array = array_keys($result_set, $mms_id);
  $current_record = $current_record_array[0];
  $previous_record = $current_record - 1  ;
  $next_record = $current_record + 1;

  if(next_mms_id($mms_id) === false){
    // $show_next_cylinder_link is used in detail_pagination.tpl
    $show_next_cylinder_link = false;
  }else{
    $show_next_cylinder_link = true;
  }

  if(previous_mms_id($mms_id) === false){
    // $show_previous_cylinder_link is used in detail_pagination.tpl
    $show_previous_cylinder_link = false;
  }else{
    $show_previous_cylinder_link = true;
  }

}

/* commenting out the chunk below as we'll rely on the function next_mms_id() instead */
/*
  if(array_key_exists($next_record, $result_set)){
    $next_record_mms_id = $result_set[$next_record];
    $next_page_link = "<a href=\"?query_type=mms_id&query=".$next_record_mms_id    ."\">Next</a>";
    $smarty->assign('next_page_link', $next_page_link);
  }else{
    $next_not_loaded = true;

    // $next_page_link = '_SESSION[search_results_count]:' . $_SESSION['search_results_count'];//debug
    // $smarty->assign('next_page_link', $next_page_link);//debug

    if($_SESSION['search_results_count'] > $_SESSION['nextRecordPosition']){
      $SRUquery = build_sru_query('next',$_SESSION['query_type'],$_SESSION['query_term']);
      $SRU_query_response = fetch_sru_results($SRUquery);
      $temp  = cache_individual_cylinder_records($searchRetrieveResponse);

      error_log(" ".__FILE__." ".__LINE__." _SESSION['nextRecordPosition']".$_SESSION['nextRecordPosition']);//debug
    }
  }
*/

if(isset($_SESSION['results_set'])){
  if(array_key_exists($previous_record, $result_set)){
    $previous_record_mms_id = $result_set[$previous_record];
    $prev_cylinder_link = "<a href=\"?query_type=mms_id&query=".$previous_record_mms_id."\">Previous</a>";
    $smarty->assign('prev_cylinder_link', $prev_cylinder_link);
  }else{
    $previous_not_loaded = true;
  }
}

$record = $cached_record;

$smarty->assign('show_next_cylinder_link', $show_next_cylinder_link);
$smarty->assign('show_previous_cylinder_link', $show_previous_cylinder_link);
$smarty->assign('record', $cached_record);
$smarty->assign('session', $_SESSION);
$smarty->assign('hits', $_SESSION['search_results_count']);
$smarty->display('detail.tpl');

//default landing page for search.php
if(!$_SERVER['QUERY_STRING']){
  $smarty->assign('page_title', 'Search');
  $smarty->assign('session', $_SESSION);
  $smarty->display('search.tpl');
}






/*  ------------------------------- debug code below ------------------------------------------- */
if(DEVELOPMENT === true){

echo "<pre>\n";
foreach ($_SESSION['result_set'] as $key => $value) {
  echo "[".$key."] => ". $value;
  if($value == $mms_id){ echo "<--- current record being displayed "; }
  echo "\n";
}
echo "</pre>";
//echo "<h3>url_fetched:</h3>"; echo("<textarea cols=\"180\">".$_SESSION['url_fetched']."</textarea>"); echo "";
//echo "<h3>current_record:</h3><pre>"; var_dump($current_record); echo "</pre><hr>";
echo "<h3>previous mms_id:</h3><pre>"; var_dump(previous_mms_id($mms_id)); echo "</pre>";
echo "<h3>current mms_id:</h3><pre> $mms_id </pre>";
echo "<h3>next mms_id:</h3><pre>"; var_dump( next_mms_id($mms_id)); echo "</pre><hr>";

echo "show_next_cylinder_link: "; var_dump($show_next_cylinder_link);


//echo "<h3>previous_record:</h3><pre>"; var_dump($previous_record); echo "</pre><hr>";
//echo "<h3>next_record:</h3><pre>"; var_dump($next_record); echo "</pre><hr>";
//echo "<br>hits:<pre>"; print_r($hits); echo "</pre><hr>";
//echo "_GET:<pre>"; print_r($_GET); echo "</pre><hr>";
//echo "results_set:<pre>"; print_r($results_set); echo "</pre>";

// echo "<h3>pagination:</h3><pre>"; print_r($pagination); echo "</pre><hr>";
// echo "<div>min record number:". min($pagination). " </div> ";
// echo "<div>max record number:". max($pagination). " </div><hr> ";

//echo "<h2>LinesReached</h2><pre>"; print_r($LinesReached); echo "</pre><hr>";
//echo "SERVER:<pre>"; print_r($_SERVER); echo "</pre><hr>";
//echo "<h2> result_set:</h2><pre>"; print_r($result_set); echo "</pre><hr>";
//echo "<h3>record_array</h3><pre>"; print_r($record_array); echo "</pre><hr>";
echo "<h3>temp:</h3><pre>"; print_r($temp); echo "</pre>";
echo "<h3>SRUquery:</h3><pre>"; print_r($SRUquery); echo "</pre><hr>";
echo "<h3>_Session:</h3><pre>"; print_r($_SESSION); echo "</pre><hr>";
}