<?php
require_once('config/main.php');
require_once('config/smarty.php');
require_once('functions.php');

if(isset($_GET['r'])){
  $r = intval($_GET['r']);  // this variable is the 'record' number that user requests.  answers question: Which record of the results set do I want to see.
  }

if(isset($_GET['nq']) AND $_GET['nq'] == '1'){
  // nq = new query
  // this indicates that the user submitted a form with a new query rather than just clicking on a link
  // When we have a new query we can overwrite any query results in the session
  $new_query = true;
  unset($_GET['nq']);
  unset($_SESSION['orig_query']);
  unset($_SESSION['query_type']);
  unset($_SESSION['query_term']);

  $query_types = array('keyword','author','title','subject','year','call_number', 'mms_id');
  // passing query_type selector state from page to page see: navigation.tpl
  foreach ($query_types as $value) {
    if($_GET['query_type'] == $value){
        $query_type = $value;
        $_SESSION['query_type'] = $query_type;
        $smarty->assign('session', $_SESSION);// it might be more appropriate to do this later in the script execution than line 27.
      }
    }
  if(isset($_GET['query'])){
    // query term is used to populate an input field on the search form on the search results page
    $query = sanitize($_GET['query']);  
    $_SESSION['query_term'] = $query;
    }
}else{
  $new_query = false;
}


// handle someone passing "cylinder=2345"  in the URL
if(isset($_GET['cylinder'])){
  $getCylinderNumber = sanitize($_GET['cylinder'], 'url');
  // Problem:  non-digitized cylinders don't have string: 'Cylinder' in the elocation field
  $SRUquery = '&query=' . urlencode("alma.mms_tagSuppressed=false and alma.elocation=Cylinder$getCylinderNumber)");
}

// handle the URL scheme of the cylinder site as it was before 2017-09 when it used Aleph & Z39.50 as data store & query engine
// example:  http://cylinders.library.ucsb.edu/search.php?queryType=@attr+1=1020&num=1&start=1&query=cylinder8401
if(isset($_GET['queryType'])){
  // echo "_GET<pre>"; print_r($_GET); echo "</pre>"; //debug
  // echo "_SERVER[HTTP_REFERER]<pre>"; print_r($_SERVER['HTTP_REFERER']); echo "</pre>"; //debug

  error_log($_SERVER['HTTP_X_FORWARDED_FOR'].' requested an old style URL. HTTP_REFERER: '. $_SERVER['HTTP_REFERER']);
  $old_style_url = TRUE;

  $query_term = str_replace('cylinder', '', sanitize($_GET['query'],'url'));
  $SRUquery = build_sru_query('OBJID', $query_term);
  // echo "SRUquery: ". $SRUquery; //debug
  // exit;
}

if(!isset($SRUquery)){
  // if the SRUquery has not already been set by code above do so now.
  $SRUquery = build_sru_query($query_type, $query, NULL, 'title', 'ascending');
  }

$query_results = fetch_sru_results($SRUquery);

//default landing page for search.php when no query params are in the request
if(!$_SERVER['QUERY_STRING']){
  $smarty->assign('page_title', 'Search');
  $smarty->assign('session', $_SESSION);
  $smarty->display('search.tpl');
  exit;
}


/*  =====================================================================
              Results processing - process XML returned from Alma
  =====================================================================
*/

// push XML string into an simpleXML object which can be queried via Xpath 

$query_results = str_replace('xmlns=', 'ns=', $query_results);// fix for Jira issue DEV-1261
$searchRetrieveResponse = simplexml_load_string($query_results);
$searchRetrieveResponse->registerXPathNamespace('record', 'http://www.loc.gov/MARC21/slim'); // this is probably not strictly necessary as our XPath queries don't ref a namespace

$_SESSION['nextRecordPosition'] = (integer)$searchRetrieveResponse->nextRecordPosition;

$temp = cache_individual_cylinder_records($searchRetrieveResponse);
$record_array = array_pop($temp);
$pagination   = array_pop($temp);
/*
$pagination looks like:
Array
(   [0] => 7
    [1] => 8
    [2] => 9
    [3] => 10
    [4] => 11
    [5] => 12   )
*/

/* $hits = number of records in the search result.  
Default for Alma SRU is 10 but could be as high as 50 depending on the maximumRecords 
param passed to SRU server in SRUPATH constant. $hits will often be higher than the $results_per_page
*/
  $hits = (string)$searchRetrieveResponse->numberOfRecords;
  $hits = intval($hits);
  
  if ($hits == 0) {
    // in this condition there are No SRU query results or no SRU query was performed which happens when someone clicks an individual record link in the search results
    // now that detail page is displayed using detail.php this condition shouldn't happen.
    if($old_style_url !== TRUE){
      error_log(__FILE__." ".__LINE__." zero hits. REQUEST_URI:".$_SERVER['REQUEST_URI']." HTTP_REFERER: ".$_SERVER['HTTP_REFERER'] ." HTTP_X_FORWARDED_FOR: ". $_SERVER['HTTP_X_FORWARDED_FOR']);
      }
    if($_GET['query_type'] == 'mms_id'){
      // assume user clicked a link in search results. Show detail page. 
          $singlerecord = $_SESSION['record_array'][$r];
          $recordPosition = $singlerecord['record_position'];
          if(isset($_GET['r'])){   // 'r' here is short for record
            $recordPosition = intval($_GET['r']);
          }
          $smarty->assign('record', $singlerecord);
          $smarty->assign('hits', count($_SESSION['result_set']));
          $smarty->assign('session', $_SESSION);
          $smarty->display('detail.tpl');


      }else{
        $smarty->assign('page_title', "No Results Found");
        $smarty->assign('session', $_SESSION);
        $smarty->display('empty.tpl');
      }
    }


  if ($hits == 1) {
    // only 1 record in the result set ---  show Detail Page
    $singlerecord = array_shift($record_array);
    $recordPosition = $singlerecord['record_position'];
    if(isset($_GET['r'])){   // 'r' here is short for record
      $recordPosition = intval($_GET['r']);
    }
    $smarty->assign('session', $_SESSION);
    $smarty->assign('record', $singlerecord);
    $smarty->assign('hits', $hits);
    $smarty->display('detail.tpl');


  }elseif($hits > 1){

    // this is the most common condition for the number of hits
    // there are multiple records in the result set  -- show Search Results list page
    $_SESSION['search_results_count'] = $hits;
    $_SESSION['result_set'] = get_all_mms_ids($searchRetrieveResponse);

    if($hits > $results_per_page){
      // there are more query results to display than the number configured to be shown on each page
  
      // here we calculate next and previous page link params
      if(!in_array($hits, $pagination)){
        $next = intval(max($pagination)) + 1;
        $smarty->assign('next_page_link', 'true');
      }
      
      if(min($pagination) != 1){
        // not currently displaying the first record of the result set so we need to calculate the previous page
        $previous = intval(min($pagination)) - 1 - $results_per_page;
        if($previous < 0){$previous = 0;}
        $smarty->assign('prev_page_link', '<a href="'.query_string_without_start($_GET).'&start='. $previous .'&nq=1">Previous</a>');
      }
    }

    $smarty->assign('record_array', $record_array);
    $_SESSION['record_array'] = $record_array;
//    $_SESSION['orig_query'] = $_SERVER['REQUEST_URI'];  // Store the search query for use on a subsequent detail page

    $smarty->assign('hits', $hits);
    $smarty->assign('page_title', 'Search_results');
    $smarty->assign('session', $_SESSION);
    $smarty->assign('results_per_page', $results_per_page);
    $smarty->display('search_results.tpl');
  }

/*  ------------------------------- debug code below ------------------------------------------- */
if(DEVELOPMENT === true){
//echo "hits: <pre>"; var_dump($hits); echo "</pre><hr>";
echo "hits:<pre>"; print_r($hits); echo "</pre><hr>"; 
//echo "_GET:<pre>"; print_r($_GET); echo "</pre><hr>"; 
echo "<h3>url_fetched:</h3>"; echo("<textarea cols=\"180\">".$_SESSION['url_fetched']."</textarea>"); echo ""; 

echo "<h3>next:</h3><pre>"; print_r($next); echo "</pre><hr>"; 
echo "<h3>pagination:</h3><pre>"; print_r($pagination); echo "</pre><hr>"; 
echo "<div>min pagination number:". min($pagination). " </div> ";
echo "<div>max pagination number:". max($pagination). " </div><hr> ";


//echo "SERVER:<pre>"; print_r($_SERVER); echo "</pre><hr>"; 
echo "<h3>_Session:</h3><pre>"; print_r($_SESSION); echo "</pre><hr>"; 
//echo "<h3>record_array</h3><pre>"; print_r($record_array); echo "</pre><hr>"; 
//echo "<h3>smarty:</h3><pre>"; print_r($smarty); echo "</pre>"; 
//echo "<h3>DEVELOPMENT:</h3><pre>"; var_dump(DEVELOPMENT); echo "</pre>";
}
