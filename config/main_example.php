<?php
//filename:config/main.php
define('DEVELOPMENT', false);  // true triggers debug output.  to be TRUE in development environment only.

//set the default timezone for use by all php functions
date_default_timezone_set('America/Los_Angeles');

//   when setting error_reporting() see documentation at:
//   http://php.net/manual/en/function.error-reporting.php  AND http://php.net/manual/en/errorfunc.constants.php
// "E_ALL & ~E_WARNING & ~E_NOTICE" means All errors except warnings and notices

if(DEVELOPMENT == true){
  // for development purposes we want to see most errors in the browser
  ini_set('display_errors', '1');
  error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE);
}else{
  ini_set('display_errors', '0');
  error_reporting(E_ERROR | E_PARSE);
}

// pass a cookie param in the HTTP header which tells browsers to disallow use of Javascript to alter session cookie
ini_set('session.cookie_httponly', '1');
session_start();

if(DEVELOPMENT == true){
  $results_per_page = 6;
  }else{
  $results_per_page = 25;  //PROD
}
define('RESULTS_PER_PAGE', $results_per_page);

$SRUserver = "https://ASDFG.alma.exlibrisgroup.com";
define('SRU_HOST', $SRUserver);

$SRUserver_path = '/view/sru/03ASDFG_INST?version=1.2&operation=searchRetrieve&schema=marcxml&maximumRecords='.$results_per_page ;
define('SRU_PATH', $SRUserver_path);

// Assets_root is the directory where the Web Server can access the MP3 files.  It is used especially when the user clicks the 'download' button. make sure there is a trailing slash.   See ALMA-522  STAGE is different from PROD!
// as of 2018-05-25 the MP3 files are not mounted to the STAGE server so no case for STAGE is configured.

if(DEVELOPMENT == true){
  define('ASSETS_ROOT', '/var/www/htdocs/assets/');// we don't have direct access to assets in the DEV environment unless SVMWindows is mounted
  }elseif($_SERVER["HTTP_HOST"]  == 'cylinders.library.ucsb.edu'){
  define('ASSETS_ROOT', '/www/cylinders.library.ucsb.edu/assets/cylinders/');//PROD
  }

//define constants for MEMCACHED_HOST and MEMCACHED_PORT
if(DEVELOPMENT == true){
  define('MEMCACHED_HOST', 'localhost');
}else{
  define('MEMCACHED_HOST', 'memcached1.library.ucsb.edu');  // prod
}

define('MEMCACHED_PORT', '11211');

// 300 seconds is 5 minutes. 1800 seconds is 30 minutes. 14400 seconds is 4 hours. 28800 seconds is 8 hours
if($_SERVER["HTTP_HOST"]  == 'cylinders.library.ucsb.edu'){
  define('CACHE_LIFE_SECONDS', '28800');  //PROD
}else{
  define('CACHE_LIFE_SECONDS', '1800');
}

if( $_SERVER['HTTP_HOST'] == "cylinders-stage.library.ucsb.edu" OR $_SERVER['HTTP_HOST'] == "cylinders.library.ucsb.edu"){
  define('USEPROXY', true);
  }else{
  define('USEPROXY', true);
  }

// time in milliseconds that CURL will wait for an HTTP response from remote ExLibris ALMA API server.
//  1 second = 1000 milliseconds
define("CURLCONNECTTIMEOUT", 60000);
