<?php
//filename:config/main.php
define('DEVELOPMENT', ($_ENV["CYLINDERS_ENV"] == "development"));  // true triggers debug output.  to be TRUE in development environment only.

if(isset($_ENV["DOWNLOADS_BASE_URL"])){
  define('DOWNLOADS_BASE_URL', $_ENV["DOWNLOADS_BASE_URL"]);
}else{
  // assume prod if the env isn't set. this should mean we're deploying with/to legacy infrastructure
  define('DOWNLOADS_BASE_URL', 'http://cylinders.library.ucsb.edu/mp3s');
}

//set the default timezone for use by all php functions
date_default_timezone_set('America/Los_Angeles');

//   when setting error_reporting() see documentation at:
//   http://php.net/manual/en/function.error-reporting.php  AND http://php.net/manual/en/errorfunc.constants.php
// "E_ALL & ~E_WARNING & ~E_NOTICE" means All errors except warnings and notices

if(DEVELOPMENT == true){
  // for development purposes we want to see most errors in the browser
  ini_set('display_errors', '1');
  error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE);
}else{
  ini_set('display_errors', '0');
  error_reporting(E_ERROR | E_PARSE);
}

//define constants for MEMCACHED_HOST and MEMCACHED_PORT
if(isset($_ENV["MEMCACHED_HOST"])){
  define('MEMCACHED_HOST', $_ENV["MEMCACHED_HOST"]);
}else{
  define('MEMCACHED_HOST', 'memcached1.library.ucsb.edu'); // legacy prod
};

if(isset($_ENV["MEMCACHED_PORT"])){
  define('MEMCACHED_PORT', $_ENV["MEMCACHED_PORT"]);
}else{
  define('MEMCACHED_PORT', '11211'); // default port
};

// use memcached for sessions
ini_set('session.save_handler', 'memcached');
ini_set('session.save_path', sprintf("%s:%s", MEMCACHED_HOST, MEMCACHED_PORT));

// pass a cookie param in the HTTP header which tells browsers to disallow use of Javascript to alter session cookie
ini_set('session.cookie_httponly', '1');
session_start();

if(DEVELOPMENT == true){
  $results_per_page = 6;
  }else{
  $results_per_page = 25;  //PROD
}
define('RESULTS_PER_PAGE', $results_per_page);


if(isset($_ENV["SRU_HOST"])){
  define('SRU_HOST', $_ENV["SRU_HOST"]);
}else{
  define('SRU_HOST', 'ucsb.alma.exlibrisgroup.com'); // legacy prod
};

if(isset($_ENV["SRU_PROTOCOL"])){
  define('SRU_PROTOCOL', $_ENV["SRU_PROTOCOL"]);
}else{
  define('SRU_PROTOCOL', 'http');  // See Jira: DEV-1365 & ALMA-394 for issues connecting with Exlibris servers over TLS
};

$SRUserver_path = '/view/sru/01UCSB_INST?version=1.2&operation=searchRetrieve&schema=marcxml&maximumRecords='.$results_per_page ;
define('SRU_PATH', $SRUserver_path);

//"https://ucsb.alma.exlibrisgroup.com/view/sru/01UCSB_INST?version=1.2&operation=explain"

// server from which we make the sitemap requets
//define('SITEMAP_API_SERVER', '10.3.1.171'); // Aleph Libcat-dev Not in use as of 2017-11
//define('SITEMAP_API_KEY', "02c39976-6dda-11e5-8f09-10ddb1dbc601");

// Assets_root is the directory where the Web Server can access the MP3 files.  It is used especially when the user clicks the 'download' button. make sure there is a trailing slash.   See ALMA-522  STAGE is different from PROD!
// as of 2018-05-25 the MP3 files are not mounted to the STAGE server so no case for STAGE is configured.

if(DEVELOPMENT == true){
  define('ASSETS_ROOT', '/www/cylinders.library.ucsb.edu/assets/cylinders/');// we don't have direct access to assets in the DEV environment unless SVMWindows is mounted
  }elseif($_SERVER["HTTP_HOST"]  == 'cylinders.library.ucsb.edu'){
  define('ASSETS_ROOT', '/www/cylinders.library.ucsb.edu/assets/cylinders/');//PROD
  }

// 300 seconds is 5 minutes. 1800 seconds is 30 minutes. 14400 seconds is 4 hours. 28800 seconds is 8 hours
if($_SERVER["HTTP_HOST"]  == 'cylinders.library.ucsb.edu'){
  define('CACHE_LIFE_SECONDS', '28800');  //PROD
}else{
  define('CACHE_LIFE_SECONDS', '1800');
}

// time in milliseconds that CURL will wait for an HTTP response from remote ExLibris ALMA API server.
//  1 second = 1000 milliseconds
define("CURLCONNECTTIMEOUT", 60000);
