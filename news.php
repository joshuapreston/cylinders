<?php
include('config/smarty.php');

$news_file = file("data/NEWS.html");
foreach ($news_file as $news_item) {
  $news_item = trim($news_item);
  if ($news_item !== ""){
    $news_items[] = $news_item;
  }
}
$smarty->assign('news_items', $news_items);

$smarty->assign('page_title', 'News');
$smarty->display('news.tpl');
?>
