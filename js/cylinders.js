(function() {
  "use strict";

  /*
   * classList.js: Cross-browser full element.classList implementation.
   * 1.1.20150312
   *
   * By Eli Grey, http://eligrey.com
   * License: Dedicated to the public domain.
   *   See https://github.com/eligrey/classList.js/blob/master/LICENSE.md
   */

  /*global self, document, DOMException */

  /*! @source http://purl.eligrey.com/github/classList.js/blob/master/classList.js */
  if ("document" in self) {
    // Full polyfill for browsers with no classList support
    // Including IE < Edge missing SVGElement.classList
    if (!("classList" in document.createElement("_")) ||
        document.createElementNS && !("classList" in document.createElementNS("http://www.w3.org/2000/svg","g"))) {

      (function (view) {
        if (!('Element' in view)) return;

        var classListProp = "classList",
            protoProp = "prototype",
            elemCtrProto = view.Element[protoProp],
            objCtr = Object,
            strTrim = String[protoProp].trim || function () {
              return this.replace(/^\s+|\s+$/g, "");
            },
            arrIndexOf = Array[protoProp].indexOf || function (item) {
              var i = 0,
                  len = this.length;
              for (; i < len; i++) {
                if (i in this && this[i] === item) {
                  return i;
                }
              }
              return -1;
            },
            // Vendors: please allow content code to instantiate DOMExceptions
            DOMEx = function (type, message) {
              this.name = type;
              this.code = DOMException[type];
              this.message = message;
            },
            checkTokenAndGetIndex = function (classList, token) {
              if (token === "") {
                throw new DOMEx(
                  "SYNTAX_ERR", "An invalid or illegal string was specified"
                );
              }
              if (/\s/.test(token)) {
                throw new DOMEx(
                  "INVALID_CHARACTER_ERR", "String contains an invalid character"
                );
              }
              return arrIndexOf.call(classList, token);
            },
            ClassList = function (elem) {
              var trimmedClasses = strTrim.call(elem.getAttribute("class") || ""),
                  classes = trimmedClasses ? trimmedClasses.split(/\s+/) : [],
                  i = 0,
                  len = classes.length;
              for (; i < len; i++) {
                this.push(classes[i]);
              }
              this._updateClassName = function () {
                elem.setAttribute("class", this.toString());
              };
            },
            classListProto = ClassList[protoProp] = [],
            classListGetter = function () {
              return new ClassList(this);
            };
        // Most DOMException implementations don't allow calling DOMException's toString()
        // on non-DOMExceptions. Error's toString() is sufficient here.
        DOMEx[protoProp] = Error[protoProp];
        classListProto.item = function (i) {
          return this[i] || null;
        };
        classListProto.contains = function (token) {
          token += "";
          return checkTokenAndGetIndex(this, token) !== -1;
        };
        classListProto.add = function () {
          var tokens = arguments,
              i = 0,
              l = tokens.length,
              token,
              updated = false;
          do {
            token = tokens[i] + "";
            if (checkTokenAndGetIndex(this, token) === -1) {
              this.push(token);
              updated = true;
            }
          }
          while (++i < l);

          if (updated) {
            this._updateClassName();
          }
        };
        classListProto.remove = function () {
          var tokens = arguments,
              i = 0,
              l = tokens.length,
              token,
              updated = false,
              index;
          do {
            token = tokens[i] + "";
            index = checkTokenAndGetIndex(this, token);
            while (index !== -1) {
              this.splice(index, 1);
              updated = true;
              index = checkTokenAndGetIndex(this, token);
            }
          }
          while (++i < l);

          if (updated) {
            this._updateClassName();
          }
        };
        classListProto.toggle = function (token, force) {
          token += "";

          var result = this.contains(token),
              method = result ? force !== true && "remove" : force !== false && "add";

          if (method) {
            this[method](token);
          }

          if (force === true || force === false) {
            return force;
          } else {
            return !result;
          }
        };
        classListProto.toString = function () {
          return this.join(" ");
        };

        if (objCtr.defineProperty) {
          var classListPropDesc = {
            get: classListGetter,
            enumerable: true,
            configurable: true
          };
          try {
            objCtr.defineProperty(elemCtrProto, classListProp, classListPropDesc);
          } catch (ex) { // IE 8 doesn't support enumerable:true
            if (ex.number === -0x7FF5EC54) {
              classListPropDesc.enumerable = false;
              objCtr.defineProperty(elemCtrProto, classListProp, classListPropDesc);
            }
          }
        } else if (objCtr[protoProp].__defineGetter__) {
          elemCtrProto.__defineGetter__(classListProp, classListGetter);
        }

      }(self));

    } else {
      // There is full or partial native classList support, so just check if we need
      // to normalize the add/remove and toggle APIs.

      (function () {
        var testElement = document.createElement("_");

        testElement.classList.add("c1", "c2");

        // Polyfill for IE 10/11 and Firefox <26, where classList.add and
        // classList.remove exist but support only one argument at a time.
        if (!testElement.classList.contains("c2")) {
          var createMethod = function(method) {
            var original = DOMTokenList.prototype[method];

            DOMTokenList.prototype[method] = function(token) {
              var i, len = arguments.length;

              for (i = 0; i < len; i++) {
                token = arguments[i];
                original.call(this, token);
              }
            };
          };
          createMethod('add');
          createMethod('remove');
        }

        testElement.classList.toggle("c3", false);

        // Polyfill for IE 10 and Firefox <24, where classList.toggle does not
        // support the second argument.
        if (testElement.classList.contains("c3")) {
          var _toggle = DOMTokenList.prototype.toggle;

          DOMTokenList.prototype.toggle = function(token, force) {
            if (1 in arguments && !this.contains(token) === !force) {
              return force;
            } else {
              return _toggle.call(this, token);
            }
          };
        }
        testElement = null;
      }());
    }
  }

  var wrappers = document.getElementsByClassName('audio-wrapper');
  // bail early if there are no audio players on the page:
  if (!wrappers.length) return;

  // IE 9 form elements don't have .checkValidity() method
  var notIE9 = wrappers[0].getElementsByClassName('volumeSlider')[0].checkValidity;

  var n = wrappers.length;
  while (n--) {
    wrappers[n].getElementsByTagName('audio')[0]
      .addEventListener('loadedmetadata', setDurationDisplay, false);

    wrappers[n].getElementsByClassName('playButton')[0]
      .addEventListener('click', playPause, false);

    wrappers[n].getElementsByClassName('muteButton')[0]
      .addEventListener('click', muteUnmute, false);

    wrappers[n].getElementsByClassName('bar')[0]
      .addEventListener('click', jump, false);

    wrappers[n].getElementsByClassName('download')[0].children[0]
      .addEventListener('click', gaDownload, false);

    var vol = wrappers[n].getElementsByClassName('volumeSlider')[0];
    vol.addEventListener('mousemove', setVolume, false);
    if (notIE9) vol.style.height = '10px';
  }

  function controller(className, audio) {
    return audio.parentNode.getElementsByClassName(className)[0];
  }

  function audioElement(node) {
    var wrapperElement = node.parentNode;
    while (!wrapperElement.classList.contains("audio-wrapper")) {
      wrapperElement = wrapperElement.parentNode;
    }
    return wrapperElement.getElementsByTagName('audio')[0];
  }

  //// LISTENER FUNCTIONS
  ///
  function setDurationDisplay(e) {
    var track = e.target;
    var minutes = parseInt(track.duration/60);
    var seconds = parseInt(track.duration%60);
    var addzero = (seconds < 10) ? "0" : "";
    controller('durationDisplay', track).innerHTML = minutes + ':' + addzero + seconds;
  }

  function playPause(e) {
    var track = audioElement(e.target);

    // interval to update the currentTime and barProgress
    var updateTime;

    if(!track.paused && !track.ended){
      gaPause();

      track.pause();
      controller('playButton', track).innerHTML = '<i class="fa fa-play-circle fa-lg"></i>';
      window.clearInterval(updateTime);

    } else {
      gaPlay();

      track.play();
      controller('playButton', track).innerHTML = '<i class="fa fa-pause fa-lg"></i>';

      updateTime = setInterval(function (){
        var curTime = controller('currentTime', track);

        if(!track.ended){
          var playedMinutes = parseInt(track.currentTime/60);
          var playedSeconds = parseInt(track.currentTime%60);
          var addzero = (playedSeconds < 10) ? "0" : "";
          curTime.innerHTML = playedMinutes + ':' + addzero + playedSeconds;

          var size = parseInt(track.currentTime*100/track.duration);
          controller('barProgress', track).style.width = size + "px";

        } else if (track.ended){
          curTime.innerHTML = "0:00";
          this.innerHTML = '<i class="fa fa-play fa-lg"></i>';

          controller('barProgress', track).style.width = "0px";
          window.clearInterval(updateTime);
        }
        // milliseconds for setInterval
      }, 100);
    }
  }

  function muteUnmute(e) {
    var track = audioElement(e.target);
    if (track.muted === true){
      track.muted = false;
      controller('muteButton', track).innerHTML = '<i class="fa fa-volume-up fa-lg"></i>';
    } else {
      track.muted = true;
      controller('muteButton', track).innerHTML = '<i class="fa fa-volume-off fa-lg"></i>';
    }
  }

  function jump(e) {
    var track = audioElement(e.target);

    // offset is the number of pixels between the left edge of the
    // element from which the event originates (`this`, or <div
    // class="bar">) and the mouse at the time of the click.
    //
    // every browser should have offsetX, but fallback to layerX just in case
    //
    // https://developer.mozilla.org/en-US/docs/Web/API/MouseEvent/offsetX
    // https://developer.mozilla.org/en-US/docs/Web/API/UIEvent/layerX
    var offset = e.offsetX || e.layerX;

    // <div class="bar"> is 100 pixels wide, so if offset is 50, that
    // means the user clicked halfway along the bar.  This allows us
    // to calculate where to jump to in the audio.
    //
    // For example, if a track is 200 seconds, track.duration/100 will
    // be 2.  Multiply that by 50, and we get 100 seconds, or halfway
    // through the track.
    var newtime = offset * track.duration / 100;
    track.currentTime = newtime;

    // Match this change visually by updating the width of <div class="barProgress">
    controller('barProgress', track).style.width = offset + 'px';
  }

  function setVolume(e) {
    var track = audioElement(e.target);
    track.volume = controller('volumeSlider', track).value / 100;
  }

  function gaPlay() {
    gaSend("play");
  }

  function gaPause() {
    gaSend("pause");
  }

  function gaDownload() {
    gaSend("download");
  }

  function gaSend(action) {
    // Bail unless Google Analytics is loaded
    if (!ga) return;
    ga("send", {
      hitType: "event",
      eventCategory: "Audio player",
      eventAction: action,
      eventLabel: window.location.href
    });
  }
})();

// Events tracking for Adopt-a-Cylinder
(function() {
  "use strict";

  // Bail unless Google Analytics is loaded
  if (!ga) return;

  var href = document.getElementById("adopt-btn");
  if (!href) return;

  href.addEventListener('click', gaTrack, false);

  function gaTrack(e) {
    ga("send", {
      hitType: "event",
      eventCategory: 'Adoption',
      eventAction: 'Aeon',
      eventLabel: window.location.href
    });
  }
})();
