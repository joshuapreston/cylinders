var host = "http://cylinders" +
    (casper.cli.options.subdomain || "") +
    ".library.ucsb.edu";

casper.test.begin("Record with multiple cylinders", 4, function suite(test){
  casper.start(host, function(){
    this.fill(".search form", { queryType: "@attr 1=1020",
                                query: "cylinder0126" }, true);
  });

  casper.then(function(){
    var queryTypeValue = this.evaluate(function(){
      return document.getElementById("querytype").value;
    });
    test.assertEqual(queryTypeValue, "@attr 1=1020", "Query type is remembered");

    var queryStringValue = this.evaluate(function(){
      return document.getElementsByTagName("input")[0].value;
    });
    test.assertEqual(queryStringValue, "cylinder0126", "Query string is remembered");

    var resultsCount = this.evaluate(function(){
      return document.getElementsByClassName("result-row").length;
    });
    test.assertEqual(resultsCount, 1, "One result found");

    var resultHref = this.evaluate(function(){
      return document.getElementsByClassName("result-row")[0].getElementsByTagName("a")[0].getAttribute("href");
    });
    this.open(host + "/search.php" + resultHref);
  });

  casper.then(function(){
    var audioPlayerCount = this.evaluate(function(){
      return document.getElementsByClassName("audio-wrapper").length;
    });
    test.assertEqual(audioPlayerCount, 2, "Two audio players on page");
  });

  casper.run(function() {
    test.done();
  });
});
