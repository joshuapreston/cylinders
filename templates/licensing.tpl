{include file="header.tpl"}
{include file="navigation.tpl"}
<!-- File: licensing.tpl -->
<div class="content text">
  <h1>Copyright and Licensing</h1>
  <p align="left">
    MP3 files  of the cylinders available for download are &copy;2005-2015 by the Regents of the University of California. They are licensed for non-commercial  use under a <a href="http://creativecommons.org/licenses/by-nc/2.5/">Creative Commons Attribution-Noncommercial 2.5 License</a>. Acknowledgments for reuse of the transfers should read &quot;University of California, Santa Barbara Library.&quot;
  </p>
  <p>
    Original wav files (either unedited or restored) can be provided upon request for commercial or non-commercial use such as CD reissues, film/TVsynchronization, or use on websites or in exhibits. The University of California makes no claims or warranties as to the copyright status of the original recordings and charges a use fee  for the use of the transfers. Please contact <a href="mailto:seubert@ucsb.edu">David Seubert</a> for information on licensing cylinder transfers.  </p>

  <h4>Audio Use Fees*</h4>
  <table class="grid">
    <thead>
      <tr>
        <th>Test</th>
        <th>Minimum</th>
        <th>Commercial Use</th>
        <th>Non-Profit Use</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Music</td>
        <td>$500</td>
        <td>$5 per second</td>
        <td>$2.50 per second</td>
      </tr>
      <tr>
        <td>Spoken Word</td>
        <td>$250</td>
        <td>$2 per second</td>
        <td>$1 per second</td>
      </tr>
    </tbody>
  </table>

  <p>
    *Single use, non-exclusive, worldwide market. Multiple use, larger quantities and limited markets negotiable.
    Contact staff for assistance  or for more information.
  </p>
</div><!-- end .content -->
<!-- End of file: licensing.tpl -->
{include file="footer.tpl"}
