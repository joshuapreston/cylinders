<!-- File: content-header.tpl -->  
<div id="main">
    <div class="content-header">
      <h1>Cylinder Preservation and Digitization Project</h1>
      <h2>Department of Special Collections</h2>
      <img class="sound-wave-img pure-img" src="/images/wave_image.png" alt="waveform">
    </div>
<!-- End of file: content-header.tpl -->  