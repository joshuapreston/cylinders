{include file="header.tpl"}
{include file="navigation.tpl"}
<!-- File: moran.tpl -->
<div class="content text">
  <h1>Operatic Cylinders from the William R. Moran Collection</h1>
  <p>
    <a href="thematic_playlists.xml"><img src="/images/rss-podcast.png" alt="Podcast"></a>
    &nbsp;</p>
  <p>
    Collector and discographer William R. Moran was not interested
    in cylinders in particular, but because of his interest in opera he assembled
    a fine collection of operatic cylinders, many of which are quite rare
    and have never been issued in any other format. Some, including the rare
    <a href="search.php?nq=1&query=Mary+Garden&query_type=keyword">Mary Garden cylinders</a>,
     are believed to have been from the collection of
    the brother of singer Jean P&eacute;rier (Debussy's first Pell&eacute;as)
    and found their way to Mr. Moran's collection through William Hogarth
    of Australia. A selection of 35 operatic cylinders from <a href="search.php?nq=1&query=moran&query_type=keyword">Mr. Moran's collection</a>
    are gathered together for this playlist, featuring
    some of the most well known voices of the day and giving a broad representation
    of the typical operatic repertoire of 100 years ago, which remains largely
    the same today.  </p>
  <p>
    These cylinders date primarily from 1906 to 1913, a brief period from
    shortly before the four-minute cylinder was introduced (which was much
    more suitable for opera) until the gradual decline of cylinders in the
    teens as discs became more popular. While opera singers that recorded
    primarily on cylinder are much more obscure than their counterparts who
    recorded on disc, some do not deserve this status. Singers who also recorded
    on disc, such as <a href="search.php?nq=1&query=slezak&query_type=keyword">Leo Slezak</a> and <a href="search.php?nq=1&query=scotti&query_type=keyword">Antonio Scotti</a>, are still well known today.
  </p>
  <p>
    <em>&mdash;David Seubert, UC Santa Barbara, Curator/Performing Arts </em>
  </p>
  <ul>
    <li><span class="small-text">P&ecirc;cheurs de perles. Je crois entendre encore
      / Georges Bizet. Aristodemo Giorgini, tenor. <a href="search.php?nq=1&query_type=call_number&query=0048">Edison Amberol: 30032</a>. 1911.</span></li>
    <li><span class="small-text">Elisir d'amore. Furtiva lagrima / Gaetano Donizetti.
      Karl Jorn, tenor. <a href="search.php?nq=1&query_type=call_number&query=0543">Edison Blue Amberol: 28217</a>. 1915</span></li>
    <li><span class="small-text">Contes d'Hoffmann. Belle nuit / Jacques Offenbach.
      Marie Rappold, soprano, Thomas Chalmers, baritone. <a href="search.php?nq=1&query_type=call_number&query=1515">Edison Blue Amberol: 28101</a>. 1912. </span></li>
    <li><span class="small-text">Trovatore. Miserere / Giuseppe Verdi. Anna Case, soprano,
      Paul Althouse, tenor and male chorus. <a href="search.php?nq=1&query_type=call_number&query=1524">Edison Blue Amberol: 28197</a>. 1914. </span></li>
    <li><span class="small-text">Robin Hood. Oh, promise me / Reginald De Koven. Marie
      Rappold, soprano. <a href="search.php?nq=1&query_type=call_number&query=1527">Edison Blue Amberol: 28165</a>. 1913. </span></li>
    <li><span class="small-text">Tosca. E lucevan le stelle / Giacomo Puccini. Leo
      Slezak, tenor. <a href="search.php?nq=1&query_type=call_number&query=1530">Edison Blue Amberol: 28146</a>. 1913.</span></li>
    <li><span class="small-text">Forza del destino. O tu che in seno agli angeli /
      Giuseppe Verdi. Carlo Albani, tenor. <a href="search.php?nq=1&query_type=call_number&query=1534">Edison Blue Amberol: 28141</a>. 1913.</span></li>
    <li><span class="small-text"> Pagliacci. Prologo / Ruggiero Leoncavallo. Carlo
      Galeffi, baritone. <a href="search.php?nq=1&query_type=call_number&query=1535">Edison Blue Amberol: 28134</a>. 1913.</span></li>
    <li><span class="small-text">Otello. Ave Maria / Giuseppe Verdi. Maria Farneti,
      soprano. <a href="search.php?nq=1&query_type=call_number&query=1536">Edison Blue Amberol: 28139</a>. 1913</span></li>
    <li><span class="small-text">Rigoletto. Caro nome / Giuseppe Verdi. Selma Kurz,
      soprano. <a href="search.php?nq=1&query_type=call_number&query=1537">Edison Blue Amberol: 28133</a>. 1913. </span></li>
    <li><span class="small-text">Rigoletto. Questa o quella / Verdi. Alessandro Bonci,
      tenor. <a href="search.php?nq=1&query_type=call_number&query=1541">Edison Blue Amberol: 29001</a>. 1913. </span></li>
    <li><span class="small-text">Luisa Miller. Quando le sere al placido / Giuseppe Verdi. Alessandro Bonci, tenor. <a href="search.php?nq=1&query_type=call_number&query=1545">Edison Royal Purple Amberol: 29005</a>. 1918 </span></li>
    <li><span class="small-text">Freisch&uuml;tz. Durch die W&auml;lder, durch die
      Auen / Carl Maria von Weber. Ernst Kraus, tenor. <a href="search.php?nq=1&query_type=call_number&query=2231">Edison Amberol: 15178</a>. 1911.</span></li>
    <li><span class="small-text">Juive. Rachel! quand du Seigneur la gr&acirc;ce tut&eacute;laire
      / F. Hal&eacute;vy. Paul Dangely, tenor. <a href="search.php?nq=1&query_type=call_number&query=2246">Edison Amberol: 17100</a>. 1911.</span></li>
    <li><span class="small-text"> Boh&egrave;me. Je suis po&egrave;te / Giacomo Puccini.
      Gaston Dubois, tenor. <a href="search.php?nq=1&query_type=call_number&query=2252">Edison Amberol: 17157</a>. 1912.</span></li>
    <li><span class="small-text">Cavalleria rusticana. Tu qui Santuzza? / Pietro Mascagni.
      Maria Avezza, soprano, Francesco Daddi tenor. <a href="search.php?nq=1&query_type=call_number&query=2278">Edison Amberol: 5008</a>. 1909.</span></li>
    <li><span class="small-text"> Elisir d'amore. Furtiva lagrima / Donizetti, Gaetano.
      Francesco Daddi, tenor. <a href="search.php?nq=1&query_type=call_number&query=2280">Edison Amberol: 5011</a>. 1909.</span></li>
    <li><span class="small-text">Ernani. Oh de' verd'anni miei / Giuseppe Verdi. Giovanni
      Baratto, baritone. <a href="search.php?nq=1&query_type=call_number&query=2289">Edison Amberol: 7525</a>. 1911.</span></li>
    <li><span class="small-text"> Jour et la nuit. La Manola / Charles Lecocq. Blanche
      Arral, soprano. <a href="search.php?nq=1&query_type=call_number&query=2301">Edison Amberol: 35006</a>. 1910.</span></li>
    <li><span class="small-text"> Favorite. L&eacute;onor, viens / Gaetano Donizetti.
      Louis Nucelly, baritone. <a href="search.php?nq=1&query_type=call_number&query=2329">Edison Amberol: B176</a>. 1909.</span></li>
    <li><span class="small-text">Boh&egrave;me. S&igrave;, mi chiamano Mim&igrave;
      / Giacomo Puccini. Lucrezia Bori, soprano. <a href="search.php?nq=1&query_type=call_number&query=2313">Edison Amberol: 40036</a>. 1910.</span></li>
    <li><span class="small-text">Otello. Niun mi tema / Giuseppe Verdi. Leo Slezak,
      tenor. <a href="search.php?nq=1&query_type=call_number&query=2319">Edison Amberol: B153</a>. 1909.</span></li>
    <li><span class="small-text">Huguenots. Conjuration et B&eacute;n&eacute;diction
      des poignards / Giacomo Meyerbeer. Ren&eacute; Fournets, bass. <a href="search.php?nq=1&query_type=call_number&query=4497">Cylindres Edison Moul&eacute;s Sur Or: 17203</a>. 1907.</span></li>
    <li><span class="small-text"> Otello. Air des adieux. / Giuseppe Verdi. M. Gluck,
      tenor. <a href="search.php?nq=1&query_type=call_number&query=4509">Cylindres Edison Moul&eacute;s Sur Or: 17305</a>. between 1907 and 1909.</span></li>
    <li><span class="small-text"> Rigoletto. Donna &egrave; mobile / Giuseppe Verdi.
      Florencio Constantino, tenor. <a href="search.php?nq=1&query_type=call_number&query=4535">Edison Gold Moulded Record: B.4</a>. 1906. </span></li>
    <li><span class="small-text">Traviata. Duetto / Giuseppe Verdi. Gustavo Bernal-Resky,
      baritone Scarphy Resky, soprano. <a href="search.php?nq=1&query_type=call_number&query=4581">Edison Gold Moulded Record: B. 14</a>. 1906. </span></li>
    <li><span class="small-text"> Faust. Je ris de me voir si belle / Charles Gounod.
      Bessie Abott, soprano. <a href="search.php?nq=1&query_type=call_number&query=4586">Edison Gold Moulded Record: B. 23</a>. 1906.</span></li>
    <li><span class="small-text">A&iuml;da. Sortita d'Amonasro / Giuseppe Verdi. Antonio
      Scotti, baritone. <a href="search.php?nq=1&query_type=call_number&query=4591">Edison Gold Moulded Record: B. 32</a>. 1907.</span></li>
    <li><span class="small-text">Ring des Nibelungen. Walk&uuml;re. Winterst&uuml;rme
      wichen dem Wonnemond Heinrich Knote, tenor. <a href="search.php?nq=1&query_type=call_number&query=4596">Edison Gold Moulded Record: B. 38</a>. 1906</span></li>
    <li><span class="small-text"> Don Pasquale. Bella siccome un angelo / Gaetano Donizetti.
      Antonio Scotti, baritone. <a href="search.php?nq=1&query_type=call_number&query=4598">Edison Gold Moulded Record: B. 44</a>. 1907</span></li>
    <li><span class="small-text"> Manon Lescaut. In quelle trine morbide / Giacomo
      Puccini. Lina Garavaglia, soprano. <a href="search.php?nq=1&query_type=call_number&query=4606">Edison Gold Moulded Record: B. 53</a>. 1907.</span></li>
    <li><span class="small-text"> Faust. Veau d'or est toujours debout / Charles Gounod.
      Robert Blass, bass. <a href="search.php?nq=1&query_type=call_number&query=4609">Edison Gold Moulded Record: B. 62</a>. 1908</span></li>
    <li><span class="small-text"> Carmen. Entr'acte / Georges Bizet. Lucien Muratore.
      <a href="search.php?nq=1&query_type=call_number&query=4641">Cylindres Edison Moul&eacute;s Sur Or: 17583</a>. 1907.</span></li>
    <li><span class="small-text"> Faust. Alerte! alerte! / Gaetano Donizetti. Ida Vaud&eacute;re,
      soprano; M. Gluck, tenor; M. Ragneau, baritone. <a href="search.php?nq=1&query_type=call_number&query=4647">Cylindres Edison Moul&eacute;s Sur Or: 17802</a>. ca 1906.</span></li>
    <li><span class="small-text"> Carmen. Fleur que tu m'avais jet&eacute;e / Georges
      Bizet. Francisco Nuibo, tenor solo. <a href="search.php?nq=1&query_type=call_number&query=4765">Columbia Phonograph Co.: 32716</a>. 1905. </span></li>
  </ul>
</div><!-- end .content -->
<!-- End of file: moran.tpl -->
{include file="footer.tpl"}
