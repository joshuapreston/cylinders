{include file="header.tpl"}
{include file="navigation.tpl"}
<!-- File: newadditions-march2007.tpl -->
<div class="content text">
	<h2>New Cylinders, June 2008</h2>
  <p>
  	Meredith Bak, a graduate student here at UCSB in Film and Media Studies has been digitizing newly added cylinders over the past few months. We are delighted to be able to add another 350 cylinders to the online collection.  A complete list of the newly added cylinders can is below.
  </p>
  <p>Kentucky Babe / Invincible Quartet. Invincible Quartet. <a href="search.php?nq=1&query_type=call_number&query=7945">Columbia Phonograph Co.: 9054</a>. between 1896 and 1900.</p>
  <p>Brown wax home recording (180 rpm). Performer not given. <a href="search.php?nq=1&query_type=call_number&query=7170">Brown wax home recording: </a>. between 1890 and 1929.</p>
  <p>Let her rip: quadrille. Columbia Orchestra. <a href="search.php?nq=1&query_type=call_number&query=7952">Columbia Phonograph Co.: 15121</a>. 1896-1900.</p>
  <p>My black bird. Len Spencer. <a href="search.php?nq=1&query_type=call_number&query=7950">Columbia Phonograph Co.: 7474</a>. 1896-1900.</p>
  <p>Florodora. In the shade of the palm (160 rpm) / Leslie Stuart. John W. Myers. <a href="search.php?nq=1&query_type=call_number&query=7147">Columbia Phonograph Co.: 37620</a>. 1896-1900.</p>
	<p>Whisper your mothers name / Stanley Carter. George J. Gaskin. <a href="search.php?nq=1&query_type=call_number&query=7482">Columbia Phonograph Co.: 4127</a>. 1896-1900.</p>
	<p>Sleep, baby, sleep. Pete LeMaire. <a href="search.php?nq=1&query_type=call_number&query=7483">Columbia Phonograph Co.: 8002</a>. 1897.</p>
	<p>Leonora waltz. Charles P. Lowe. <a href="search.php?nq=1&query_type=call_number&query=7489">Edison Record: 3405</a>. 1897 or 1898.</p>
	<p>Come where lilies bloom / Will L. Thompson. Edison Brass Quartet. <a href="search.php?nq=1&query_type=call_number&query=7488">Edison Record: 3405</a>. 1897 or 1898.</p>
	<p>Kentucky babe / Adam Geibel ; Rev. Henry Buck. S. H. Dudley. <a href="search.php?nq=1&query_type=call_number&query=7491">Edison Record: 1055</a>. 1897- 1898.</p>
	<p>Schubert's serenade / Franz Schubert. William Paris Chambers. <a href="search.php?nq=1&query_type=call_number&query=7959">Columbia Phonograph Co.: 3700</a>. 1897- 1898.</p>
	<p>Dinah's jubilee. Edison Brass Quartet. <a href="search.php?nq=1&query_type=call_number&query=7485">Edison Record: 3411</a>. 1898.</p>
	<p>Dancing in the sunlight. Charles P. Lowe. <a href="search.php?nq=1&query_type=call_number&query=7484">Edison Record: 3005</a>. 1898.</p>
	<p>Ye ancients / Ernest Reeves. Marine Corps Fife and Drum Corps. <a href="search.php?nq=1&query_type=call_number&query=7486">Edison Record: 5303</a>. 1898.</p>
	<p>On the Midway. Columbia Orchestra. <a href="search.php?nq=1&query_type=call_number&query=7941">Columbia Phonograph Co.: 15143</a>. 1898.</p>
	<p>Chiming bells. . <a href="search.php?nq=1&query_type=call_number&query=7955">Columbia Phonograph Co.: 12516</a>. 1898- 1899.</p>
	<p>Whistling Rufus / Kerry Mills. Peerless Orchestra. <a href="search.php?nq=1&query_type=call_number&query=7487">Edison Record: 704</a>. 1899.</p>
	<p>Uncle Josh in a Chinese laundry. Cal Stewart. <a href="search.php?nq=1&query_type=call_number&query=7397">Columbia Phonograph Co.: 14028</a>. 190-.</p>
	<p>Calligan, call again / Harry Lauder. Harry Lauder. <a href="search.php?nq=1&query_type=call_number&query=7458">Edison Gold Moulded Record: 13759</a>. 190-?.</p>
	<p>The Bouncer at the Blazing Rag. Len Spencer. <a href="search.php?nq=1&query_type=call_number&query=7417>"Columbia Phonograph Co.: 11024</a>. 190-?.</p>
	<p>Mr. Thomas Cat. Gilmore's Band. <a href="search.php?nq=1&query_type=call_number&query=7946">Columbia Phonograph Co.: 31538</a>. 1901.</p>
	<p>La bonita waltz. Samuel Siegel. <a href="search.php?nq=1&query_type=call_number&query=7937">Columbia Phonograph Co.: 31388</a>. 1901.</p>
	<p>The Marseillaise / Rouget de Lisle. Columbia Band. <a href="search.php?nq=1&query_type=call_number&query=7468">Columbia Phonograph Co.: 1526</a>. 1901-1909.</p>
	<p>Medley of Irish airs / Dan Godfrey. Columbia Band. <a href="search.php?nq=1&query_type=call_number&query=7472">Columbia Phonograph Co.: 1579</a>. 1901-1909.</p>
	<p>Sleighride party. Edison Male Quartette. <a href="search.php?nq=1&query_type=call_number&query=7190">Edison Gold Moulded Record: 2218</a>. 1902.</p>
	<p>Belle of New York march. Edison Grand Concert Band. <a href="search.php?nq=1&query_type=call_number&query=7193">Edison Gold Moulded Record: 10</a>. 1902.</p>
	<p>Martha selection / Friedrich von Flotow. Edison Grand Concert Band. <a href="search.php?nq=1&query_type=call_number&query=7196">Edison Gold Moulded Record: 79</a>. 1902.</p>
	<p>Meeting of school-house directors. Cal Stewart. <a href="search.php?nq=1&query_type=call_number&query=7197">Edison Gold Moulded Record: 3891</a>. 1902.</p>
	<p>Down where the Wurzburger flows. Arthur Collins and Byron G. Harlan. <a href="search.php?nq=1&query_type=call_number&query=7210">Edison Gold Moulded Record: 8238</a>. 1902.</p>
	<p>Mister Dooley. Dan W. Quinn. <a href="search.php?nq=1&query_type=call_number&query=7212">Columbia Phonograph Co.: 31779</a>. 1902.</p>
	<p>Auction sale of a bird and animal store. Len Spencer and Gilbert Girard. <a href="search.php?nq=1&query_type=call_number&query=7215">Columbia Phonograph Co.: 31836</a>. 1902.</p>
	<p>On board the Oregon. Invincible Quartet. <a href="search.php?nq=1&query_type=call_number&query=7221">Edison Gold Moulded Record: 8042</a>. 1902.</p>
	<p>Dixie / N/A. Edison Concert Band. <a href="search.php?nq=1&query_type=call_number&query=7238">Edison Gold Moulded Record: 8133</a>. 1902.</p>
	<p>Down in the depths. Frank C. Stanley. <a href="search.php?nq=1&query_type=call_number&query=7256">Edison Gold Moulded Record: 8178</a>. 1902.</p>
	<p>Miss Helen Hunt / Harry Connor. Will F. Denny. <a href="search.php?nq=1&query_type=call_number&query=7445">Edison Gold Moulded Record: 7123</a>. 1902.</p>
	<p>Russian hymn. Edison Concert Band. <a href="search.php?nq=1&query_type=call_number&query=7396">Edison Gold Moulded Record: 63</a>. 1902.</p>
	<p>Daybreak at Calamity Farm. Len Spencer ; Gilbert Girard. <a href="search.php?nq=1&query_type=call_number&query=7414">Edison Gold Moulded Record: 8034</a>. 1902.</p>
	<p>Nigger in a fit. Vess L. Ossman. <a href="search.php?nq=1&query_type=call_number&query=7450">Edison Gold Moulded Record: 2625</a>. 1902.</p>
	<p>I'll be your sweetheart / Harry Dacre. John J. Fisher or Arthur Clifford. <a href="search.php?nq=1&query_type=call_number&query=7457">Edison Gold Moulded Record: 7735</a>. 1902.</p>
	<p>Way down in old Indiana. J.W. Myers. <a href="search.php?nq=1&query_type=call_number&query=7431">Columbia Phonograph Co.: 31749</a>. 1902.</p>
	<p>Independence bell. Len Spencer. <a href="search.php?nq=1&query_type=call_number&query=7502">Edison Gold Moulded Record: 8158</a>. 1902.</p>
	<p>Temptation schottische. Peerless orchestra. <a href="search.php?nq=1&query_type=call_number&query=7508">Edison Gold Moulded Record: 8055</a>. 1902.</p>
	<p>Love's dreamland waltz / Otto Roeder. Edison Symphony Orchestra. <a href="search.php?nq=1&query_type=call_number&query=7873">Edison Gold Moulded Record: 574</a>. 1902.</p>
	<p>Jolly fellows waltz / Robert Vollstedt. Peerless Orchestra. <a href="search.php?nq=1&query_type=call_number&query=7132">Edison Gold Moulded Record: 7341</a>. 1902?.</p>
	<p>Sleighride party. Edison Male Quartet. <a href="search.php?nq=1&query_type=call_number&query=7152">Edison Gold Moulded Record: 2218</a>. 1902?.</p>
	<p>Tell us, pretty ladies. Arthur Collins and Joe Natus. <a href="search.php?nq=1&query_type=call_number&query=7229">Edison Gold Moulded Record: 7905</a>. 1902?.</p>
	<p>Auction sale of household goods. Len Spencer ; Gilbert Girard. <a href="search.php?nq=1&query_type=call_number&query=7434">Edison Gold Moulded Record: 8089</a>. 1902?.</p>
	<p>Du du. William Tuson. <a href="search.php?nq=1&query_type=call_number&query=7438">Edison Gold Moulded Record: 7987</a>. 1902?.</p>
	<p>I couldn't. Will F. Denny. <a href="search.php?nq=1&query_type=call_number&query=7446">Edison Gold Moulded Record: 7107</a>. 1902?.</p>
	<p>Pat Malone forgot that he was dead. Dan W. Quinn. <a href="search.php?nq=1&query_type=call_number&query=7448">Edison Gold Moulded Record: 7358</a>. 1902?.</p>
	<p>Sleighride party / Edison Male Quartet. Edison Male Quartette. <a href="search.php?nq=1&query_type=call_number&query=7410">Edison Gold Moulded Record: 2218</a>. 1902?.</p>
	<p>Irish Reel / Frank S. Mazziotta. Frank S. Mazziotta. <a href="search.php?nq=1&query_type=call_number&query=7435">Edison Gold Moulded Record: 2806</a>. 1902?.</p>
	<p>Peggy from Paris / William Loraine. Edison Symphony Orchestra. <a href="search.php?nq=1&query_type=call_number&query=7153">Edison Gold Moulded Record: 8442</a>. 1903.</p>
	<p>I'm a Jonah man. Arthur Collins. <a href="search.php?nq=1&query_type=call_number&query=7195">Edison Gold Moulded Record: 8440</a>. 1903.</p>
	<p>In old Alabama / 
	. Peerless Orchestra. <a href="search.php?nq=1&query_type=call_number&query=7510">Edison Gold Moulded Record: 8392</a>. 1903.</p>
	<p>The old brigade / Odoardo Barri. British Military Band. <a href="search.php?nq=1&query_type=call_number&query=7467">Edison Gold Moulded Record: 12917</a>. 1903.</p>
	<p>Anona. Edison Concert Band. <a href="search.php?nq=1&query_type=call_number&query=7428">Edison Gold Moulded Record: 8474</a>. 1903.</p>
	<p>Life's dream is o'er. Miss Chapell ; George M. Stricklett. <a href="search.php?nq=1&query_type=call_number&query=7443">Edison Gold Moulded Record: 8395</a>. 1903.</p>
	<p>I'm so tired of livin', I don't care when I die. Arthur Collins. <a href="search.php?nq=1&query_type=call_number&query=7440">Edison Gold Moulded Record: 8309</a>. 1903.</p>
	<p>My own United States / Julian Edwards. J.W. Myers. <a href="search.php?nq=1&query_type=call_number&query=7430">Columbia Phonograph Co.: 32055</a>. 1903.</p>
	<p>Because you were an old sweetheart of mine / Harry T. Robinson. J.W. Myers.. <a href="search.php?nq=1&query_type=call_number&query=7452">Columbia Phonograph Co.: 32287</a>. 1903.</p>
	<p>The banquet in misery hall. Byron G. Harlan. <a href="search.php?nq=1&query_type=call_number&query=7495">Edison Gold Moulded Record: 8397</a>. 1903.</p>
	<p>The Piccolo player’s reward / Len Spencer. Len Spencer. <a href="search.php?nq=1&query_type=call_number&query=7135">Columbia Phonograph Co.: 32424</a>. 1904.</p>
	<p>In Zanzibar. Harry MacDonough. <a href="search.php?nq=1&query_type=call_number&query=7151">Edison Gold Moulded Record: 8651</a>. 1904.</p>
	<p>In Zanzibar / Myer, John W. Harry MacDonough. <a href="search.php?nq=1&query_type=call_number&query=7201">Edison Gold Moulded Record: 8651</a>. 1904.</p>
	<p>Yale college life march / N/A. Edison Military Band. <a href="search.php?nq=1&query_type=call_number&query=7218">Edison Gold Moulded Record: 8795</a>. 1904.</p>
	<p>Old folks at home / Stephen Collins Foster. W. H. Thompson. <a href="search.php?nq=1&query_type=call_number&query=7224">Edison Gold Moulded Record: 8781</a>. 1904.</p>
	<p>One heart mind / Johann Strauss. Albert Benzler. <a href="search.php?nq=1&query_type=call_number&query=7227">Edison Gold Moulded Record: 8803</a>. 1904.</p>
	<p>The gondolier. Edison Military Band. <a href="search.php?nq=1&query_type=call_number&query=7183">Edison Gold Moulded Record: 8624</a>. 1904.</p>
	<p>The lass o' Killicrankie / Harry Lauder. Harry Lauder. <a href="search.php?nq=1&query_type=call_number&query=7264">Pathé: 60452</a>. 1904.</p>
	<p>Gimme the leavin's / Bob Cole. Arthur Collins. <a href="search.php?nq=1&query_type=call_number&query=7401">Edison Gold Moulded Record: 8917</a>. 1904.</p>
	<p>Cocoanut dance / Andrew Hermann. Vess L. Ossman. <a href="search.php?nq=1&query_type=call_number&query=7470">Edison Gold Moulded Record: 2604</a>. 1904.</p>
	<p>Here's my friend / Ted Snyder. Bob Roberts. <a href="search.php?nq=1&query_type=call_number&query=7411">Edison Gold Moulded Record: 8824</a>. 1904.</p>
	<p>My little Irish canary. Arthur Collins ; Byron G. Harlan. <a href="search.php?nq=1&query_type=call_number&query=7451">Edison Gold Moulded Record: 8647</a>. 1904.</p>
	<p>Scissors to grind / Thos. S. Allen. Arthur Collins. <a "href=search.php?nq=1&query_type=call_number&query=7425">Edison Gold Moulded Record: 8794</a>. 1904.</p>
	<p>You're as welcome as the flowers in May. Byron G. Harlan ; Frank C. Stanley. <a href="search.php?nq=1&query_type=call_number&query=7408">Edison Gold Moulded Record: 8767</a>. 1904.</p>
	<p>Barney medley. Edison Military Band. <a href="search.php?nq=1&query_type=call_number&query=7444">Edison Gold Moulded Record: 8648</a>. 1904.</p>
	<p>Belle of the west. Edison Military Band. <a href="search.php?nq=1&query_type=call_number&query=7449">Edison Gold Moulded Record: 8754</a>. 1904.</p>
	<p>Rock of ages / Thomas Hastings. Henry Burr. <a href="search.php?nq=1&query_type=call_number&query=7456">Columbia Phonograph Co.: 32497</a>. 1904.</p>
	<p>Uncle Sammy march / Abe Holzmann. Columbia Orchestra. <a href="search.php?nq=1&query_type=call_number&query=7442">Columbia Phonograph Co.: 32390</a>. 1904.</p>
	<p>Eine feine Familie / Martin Bendix. Martin Bendix. <a href="search.php?nq=1&query_type=call_number&query=7466">Edison Goldguss Walze: 15016</a>. 1904.</p>
	<p>Just a gleam of heaven in her eyes. Byron G. Harlan. <a href="search.php?nq=1&query_type=call_number&query=7427">Edison Gold Moulded Record: 8733</a>. 1904.</p>
	<p>The man in the overalls. Byron G. Harlan. <a href="search.php?nq=1&query_type=call_number&query=7504">Edison Gold Moulded Record: 8683</a>. 1904.</p>
	<p>It's the band. Byron G. Harlan. <a href="search.php?nq=1&query_type=call_number&query=7507">Edison Gold Moulded Record: 8718</a>. 1904.</p>
	<p>My old New Hampshire home. Albert C. Campbell ; James F. Harrison. <a href="search.php?nq=1&query_type=call_number&query=7511">Edison Gold Moulded Record: 8734</a>. 1904.</p>
	<p>Down in the vale of Shenandoah / Charles K. Harris. Harry MacDonough. <a href="search.php?nq=1&query_type=call_number&query=7514">Edison Gold Moulded Record: 8788</a>. 1904.</p>
	<p>Forsaken. Columbia Quartett. <a href="search.php?nq=1&query_type=call_number&query=7136">Columbia Phonograph Co.: 32759</a>. 1905.</p>
	<p>Around the camp fire in the Philippines. Columbia Quintette. <a href="search.php?nq=1&query_type=call_number&query=7186">Edison Gold Moulded Record: 32635</a>. 1905.</p>
	<p>"Coax me" medley. Prince's Military Band. <a href="search.php?nq=1&query_type=call_number&query=7188">Edison Gold Moulded Record: 32650</a>. 1905.</p>
	<p>Oh, oh, Sallie / Eddie Leonard. Arthur Collins and Byron G. Harlan. <a href="search.php?nq=1&query_type=call_number&query=7189">Edison Gold Moulded Record: 8935</a>. 1905.</p>
	<p>The holy city / Stephen Adams. Irving Gillette [i.e. Henry Burr]. <a href="search.php?nq=1&query_type=call_number&query=7192">Edison Gold Moulded Record: 2018</a>. 1905.</p>
	<p>Come take a trip in my air-ship medley. Albert Benzler. <a href="search.php?nq=1&query_type=call_number&query=7198">Edison Gold Moulded Record: 8931</a>. 1905.</p>
	<p>You must think I'm Santa Claus / Maxwell Silver. Tascott. <a href="search.php?nq=1&query_type=call_number&query=7200">Edison Gold Moulded Record: 9091</a>. 1905.</p>
	<p>She waits by the deep blue sea / Theodore F. Morse. Henry Burr. <a href="search.php?nq=1&query_type=call_number&query=7230">Edison Gold Moulded Record: 9023</a>. 1905.</p>
	<p>Mildred. Albert Benzler. <a href="search.php?nq=1&query_type=call_number&query=7239">Edison Gold Moulded Record: 8949</a>. 1905.</p>
	<p>Through battle to victory march [Durch Kampf zum Sieg] / Franz von Blon. Edison Concert Band. <a href="search.php?nq=1&query_type=call_number&query=7257">Edison Gold Moulded Record: 9089</a>. 1905.</p>
	<p>The sweetest flower that blows / C. B. Hawley. Marie Narelle. <a href="search.php?nq=1&query_type=call_number&query=7436">Edison Gold Moulded Record: 9138</a>. 1905.</p>
	<p>My Carolina lady / George Hamilton. Ada Jones. <a href="search.php?nq=1&query_type=call_number&query=7403">Edison Gold Moulded Record: 8948</a>. 1905.</p>
	<p>And the world goes on / Harry O. Sutton. Bob Roberts. <a href="search.php?nq=1&query_type=call_number&query=7407">Edison Gold Moulded Record: 9131</a>. 1905.</p>
	<p>Port Arthur march / Roland Forrest Seitz. Edison Military Band. <a href="search.php?nq=1&query_type=call_number&query=7501">Edison Gold Moulded Record: 8927</a>. 1905.</p>
	<p>My little canoe / Leslie Stuart. Grace Nelson. <a href="search.php?nq=1&query_type=call_number&query=7503">Edison Gold Moulded Record: 8858</a>. 1905.</p>
	<p>Mississippi minstrels. minstrels. <a href="search.php?nq=1&query_type=call_number&query=7505">Edison Gold Moulded Record: 9072</a>. 1905.</p>
	<p>Just across the bridge of gold / Harry von Tilzer. Byron G. Harlan. <a href="search.php?nq=1&query_type=call_number&query=7509">Edison Gold Moulded Record: 9066</a>. 1905.</p>
	<p>Jasper, don't you hear me calling you / Henry Frantzen. Arthur Collins ; Byron G. Harlan. <a href="search.php?nq=1&query_type=call_number&query=7512">Edison Gold Moulded Record: 8955</a>. 1905.</p>
	<p>Jim Bludsoe / John Hay. Edgar L. Davenport. <a href="search.php?nq=1&query_type=call_number&query=7513">Edison Gold Moulded Record: 9053</a>. 1905.</p>
	<p>Robin Adair / Leo Zimmerman. Herbert L. Clarke and Leo Zimmerman. <a href="search.php?nq=1&query_type=call_number&query=7419">Edison Gold Moulded Record: 9101</a>. 1905.</p>
	<p>My Maryland march / W. S. Mygrant. Edison Military Band. <a href="search.php?nq=1&query_type=call_number&query=7795">Edison Gold Moulded Record: 9121</a>. 1905
	1905.</p>
	<p>You're my heart's desire, I love you, Nellie Dean / Harry Armstrong. Byron G. Harlan ; Frank C. Stanley. <a href="search.php?nq=1&query_type=call_number&query=7416">Edison Gold Moulded Record: 9013</a>. 1905 .</p>
	<p>The Irish girl I love / Max Hoffmann. Edward Barrow. <a href="search.php?nq=1&query_type=call_number&query=7421">Edison Gold Moulded Record: 9140</a>. 1905- 1906.</p>
	<p>Where the River Shannon flows / James L. Russell. Harry MacDonough. <a href="search.php?nq=1&query_type=call_number&query=7159">Edison Gold Moulded Record: 9344</a>. 1906.</p>
	<p>Shall we meet beyond the river / Elihu S. Rice. Harry Anthony and James F. Harrison. <a href="search.php?nq=1&query_type=call_number&query=7181">Edison Gold Moulded Record: 9356</a>. 1906.</p>
	<p>Imperial life guard march / Robert Browne Hall. Edison Military Band. <a href="search.php?nq=1&query_type=call_number&query=7194">Edison Gold Moulded Record: 9274</a>. 1906.</p>
	<p>Waltz of the roses. Edison Symphony Orchestra. <a href="search.php?nq=1&query_type=call_number&query=7422">Edison Gold Moulded Record: 9237</a>. 1906.</p>
	<p>Why don't you write when you don't need money / Gus Edwards. Bob Roberts. <a href="search.php?nq=1&query_type=call_number&query=7406">Edison Gold Moulded Record: 9376</a>. 1906.</p>
	<p>I lost my heart 'way down in Alabama / H. W. Petrie. Harry MacDonough. <a href="search.php?nq=1&query_type=call_number&query=7400">Edison Gold Moulded Record: 9230</a>. 1906.</p>
	<p>It's a good world, after all / Vincent Bryan. Bob Roberts. <a href="search.php?nq=1&query_type=call_number&query=7418">Edison Gold Moulded Record: 9200</a>. 1906.</p>
	<p>Punch and Judy. Len Spencer ; Al S. Holt. <a href="search.php?nq=1&query_type=call_number&query=7409">Columbia Phonograph Co.: 33001</a>. 1906.</p>
	<p>Uncle Josh at the roller skating rink / Cal Stewart. Cal Stewart. <a href="search.php?nq=1&query_type=call_number&query=7473">Columbia Phonograph Co.: 33024</a>. 1906.</p>
	<p>Vogelfänger bin ich ja /  Wolfgang Amadeus Mozart. Robert Leonhardt. <a href="search.php?nq=1&query_type=call_number&query=7345">Edison Blue Amberol: 12236</a>. 1906- 1908.</p>
	<p>I get dippy when I do that two-step dance / Bert Fitzgibbon. Arthur Collins. <a href="search.php?nq=1&query_type=call_number&query=7172">Edison Gold Moulded Record: 9708</a>. 1907.</p>
	<p>That's what the rose said to me / Leo Edwards. Louise Le Baron. <a href="search.php?nq=1&query_type=call_number&query=7182">Edison Gold Moulded Record: 9518</a>. 1907.</p>
	<p>The dream of the rarebit fiend / T. W. Thruban. Edison Military Band. <a href="search.php?nq=1&query_type=call_number&query=7187">Edison Gold Moulded Record: 9585</a>. 1907.</p>
	<p>I’ve told his missus all about him / Jas Tate. Helen Trix. <a href="search.php?nq=1&query_type=call_number&query=7232">Edison Gold Moulded Record: 9534</a>. 1907.</p>
	<p>Wiener Ländler. Lanner Quartett. <a href="search.php?nq=1&query_type=call_number&query=7464">Edison Goldguss Walze: 15720</a>. 1907.</p>
	<p>Two blue eyes / Theodore F. Morse. Byron G. Harlan. <a href="search.php?nq=1&query_type=call_number&query=7331">Indestructible Record: 626</a>. 1907.</p>
	<p>When the flowers bloom in spring-time / Harry Von Tilzer. Harry MacDonough. <a href="search.php?nq=1&query_type=call_number&query=7399">Edison Gold Moulded Record: 9438</a>. 1907.</p>
	<p>No wedding bells for me / Seymour Furth. Bob Roberts. <a href="search.php?nq=1&query_type=call_number&query=7453">Edison Gold Moulded Record: 9538</a>. 1907.</p>
	<p>Ain't you coming back to old New Hampshire, Molly? /  J. Fred Helf. Columbia Quartette. <a href="search.php?nq=1&query_type=call_number&query=7404">Columbia Phonograph Co.: 33048</a>. 1907.</p>
	<p>Turkey in the straw. Billy Golden. <a href="search.php?nq=1&query_type=call_number&query=7423">Columbia Phonograph Co.: 33065</a>. 1907.</p>
	<p>Arrah Wanna / Jack Drislane ; Theodore F. Morse. Arthur Collins ; Byron G. Harlan. <a href="search.php?nq=1&query_type=call_number&query=7469">Columbia Phonograph Co.: 33050</a>. 1907.</p>
	<p>Believe me if all those endearing young charms / Sir John Stevenson. Albert Benzler. <a href="search.php?nq=1&query_type=call_number&query=7506">Edison Gold Moulded Record: 9437</a>. 1907.</p>
	<p>Rescue the perishing / W. Howard Doane ; Fanny J. Crosby. Harry Anthony ; James F. Harrison. <a href="search.php?nq=1&query_type=call_number&query=7869">Edison Gold Moulded Record: 9711</a>. 1907.</p>
	<p>Girls study your cookery book / Maurice Scott ; Joe Burley. Florrie Forde. <a href="search.php?nq=1&query_type=call_number&query=7876">Edison Gold Moulded Record: 13715</a>. 1907.</p>
	<p>When the snow birds cross the valley / Alfred Solman, Harry Macdonough. Harry MacDonough. <a href="search.php?nq=1&query_type=call_number&query=7412">Edison Gold Moulded Record: 9459</a>. 1907.</p>
	<p>Some day when dreams come true / Phil Staats; Henry Burr. Irving Gilletta. <a href="search.php?nq=1&query_type=call_number&query=7437">Edison Gold Moulded Record: 9702</a>. 1907.</p>
	<p>Red wing / Henry Burr. Frank Stanley and Henry Burr. <a href="search.php?nq=1&query_type=call_number&query=7439">Columbia Record: 33163</a>. 1907.</p>
	<p>There never was a girl like you / Egbert Van Alstyne. Byron G. Harlan. <a href="search.php?nq=1&query_type=call_number&query=7148">Edison Gold Moulded Record: 9795</a>. 1908.</p>
	<p>Moolbarri / Junie McCree. James Brockman. <a href="search.php?nq=1&query_type=call_number&query=7176">Edison Gold Moulded Record: 9776</a>. 1908.</p>
	<p>Society swing two-step / Henry Frantzen. Edison Military Base. <a href="search.php?nq=1&query_type=call_number&query=7199">Edison Gold Moulded Record: 9935</a>. 1908.</p>
	<p>It’s never late ’till morning. Bob Roberts. <a href="search.php?nq=1&query_type=call_number&query=7203">Indestructible Record: 937</a>. 1908.</p>
	<p>In the morning. Steve Porter. <a href="search.php?nq=1&query_type=call_number&query=7204">Indestructible Record: 904</a>. 1908.</p>
	<p>Larboard watch / Thomas Williams. Frank C. Stanley and Byron G. Harlan. <a href="search.php?nq=1&query_type=call_number&query=7206">Indestructible Record: 936</a>. 1908.</p>
	<p>Christmas at Clancy's. Steve Porter. <a href="search.php?nq=1&query_type=call_number&query=7209">Indestructible Record: 948</a>. 1908.</p>
	<p>The country constable. Empire Vaudeville Company. <a href="search.php?nq=1&query_type=call_number&query=7234">Edison Gold Moulded Record: 9839</a>. 1908.</p>
	<p>The home over there / T. C. O'Kane. Edison Mixed Quartet.. <a href="search.php?nq=1&query_type=call_number&query=7405">Edison Gold Moulded Record: 9899</a>. 1908.</p>
	<p>I lost my heart when I saw your eyes / J. Fred Helf. Manuel Romain. <a href="search.php?nq=1&query_type=call_number&query=7424">Edison Gold Moulded Record: 9954</a>. 1908.</p>
	<p>Rob Roy Mackintosh / Harry Lauder. Harry Lauder. <a href="search.php?nq=1&query_type=call_number&query=7454">Edison Gold Moulded Record: 19173</a>. 1908.</p>
	<p>Harrigan medley. Prince's Military Band. <a href="search.php?nq=1&query_type=call_number&query=7398">Columbia Phonograph Co.: 33238</a>. 1908.</p>
	<p>Waltz melodies / Franz Lehár ; Agnes Morgan. Prince's Orchestra. <a href="search.php?nq=1&query_type=call_number&query=7471">Columbia Phonograph Co.: 33288</a>. 1908.</p>
	<p>Christ, the Lord, is risen today / Frederick W. Ecke. Edison Concert Band. <a href="search.php?nq=1&query_type=call_number&query=7426">Edison Standard Record: 10099</a>. 1908.</p>
	<p>What a friend we have in Jesus / Charles C. Converse. James F. Harrison. <a href="search.php?nq=1&query_type=call_number&query=7337">Indestructible Record: 871</a>. 1908.</p>
	<p>Medley of bucks and reels. John J. Kimmel. <a href="search.php?nq=1&query_type=call_number&query=7338">Indestructible Record: 813</a>. 1908.</p>
	<p>Beulah land. Frank C. Stanley. <a href="search.php?nq=1&query_type=call_number&query=7299">U.S. Everlasting Record: 232</a>. 1908.</p>
	<p>On the first dark night next week. Ada Jones ; Walter Van Brunt. <a href="search.php?nq=1&query_type=call_number&query=7290">U.S. Everlasting Record: 394</a>. 1908.</p>
	<p>Abide with me / William Henry Monk. Frank C. Stanley. <a href="search.php?nq=1&query_type=call_number&query=7284">U.S. Everlasting Record: 233</a>. 1908.</p>
	<p>Old folks at home / Stephen Collins Foster. Florence Ethel Smith. <a href="search.php?nq=1&query_type=call_number&query=7294">U.S. Everlasting Record: 269</a>. 1908.</p>
	<p>Battle hymn of the Republic / Julia Ward Howe. Elise C. Stevenson ; Frank C. Stanley. <a href="search.php?nq=1&query_type=call_number&query=7867">Edison Amberol: 79</a>. 1908.</p>
	<p>Dr. Whackem's Academy. Four Komical Kards. <a href="search.php?nq=1&query_type=call_number&query=7875">Edison Bell Record: 20098</a>. 1908.</p>
	<p>Hoch Hapsburg: march / Johann Nepomuk Král. Alexander Prince. <a href="search.php?nq=1&query_type=call_number&query=7877">Edison Record: 13791</a>. 1908.</p>
	<p>When the summer days are gone / Peerless Quartet. Peerless Quartette. <a href="search.php?nq=1&query_type=call_number&query=7319">Indestructible Record: 900</a>. 1908.</p>
	<p>Girls of America - March and Two-step / J. Mahlon Duganne. Albert Benzler. <a href="search.php?nq=1&query_type=call_number&query=7793">Edison Gold Moulded Record: 9753</a>. 1908
	1908.</p>
	<p>On the levee / L. P. Laurendeau. Albert Benzler. <a href="search.php?nq=1&query_type=call_number&query=7185">Edison Gold Moulded Record: 10092</a>. 1909.</p>
	<p>On the levee / L. P. Laurendeau. Albert Benzler. <a href="search.php?nq=1&query_type=call_number&query=7191">Edison Gold Moulded Record: 10092</a>. 1909.</p>
	<p>I can’t say you’re the only one / Jerome Kern. Ada Jones and Billy Murray. <a href="search.php?nq=1&query_type=call_number&query=7205">Indestructible Record: 983</a>. 1909.</p>
	<p>Crown diamonds (overture) / Daniel François Esprit Auber. National Military Band (London, England). <a href="search.php?nq=1&query_type=call_number&query=7211">Edison Amberol: 253</a>. 1909.</p>
	<p>One sweetly solemn thought / Phoebe Cary. John Alexander. <a href="search.php?nq=1&query_type=call_number&query=7341">Indestructible Record: 982</a>. 1909.</p>
	<p>My wife's gone to the country: Hurrah!  Hurrah! / Ted Snyder. Bob Roberts. <a href="search.php?nq=1&query_type=call_number&query=7335">Indestructible Record: 1145</a>. 1909.</p>
	<p>Steamboat leaving the wharf at New Orleans. Peerless Quartet. <a href="search.php?nq=1&query_type=call_number&query=7333">Indestructible Record: 1080</a>. 1909.</p>
	<p>Das Erntefest / Hans Blädel. Hans Blädel. <a href="search.php?nq=1&query_type=call_number&query=7463">Edison Goldguss Walze: 16127</a>. 1909.</p>
	<p>Call 'round any old time. Dorothy Kingsley. <a href="search.php?nq=1&query_type=call_number&query=7318">Indestructible Record: 985</a>. 1909.</p>
	<p>Immer an der Wand lang. F. Traube. <a href="search.php?nq=1&query_type=call_number&query=7322">Indestructible Record: 1135</a>. 1909.</p>
	<p>Let me love thee / Luigi Arditi. Alan Turner. <a href="search.php?nq=1&query_type=call_number&query=7309">Indestructible Record: 1218</a>. 1909.</p>
	<p>Down among the sugar cane / Cecil Mack ; Chris Smith. Arthur Collins ; Byron G. Harlan. <a href="search.php?nq=1&query_type=call_number&query=7334">Indestructible Record: 1015</a>. 1909.</p>
	<p>I never knew I loved you till you said good-bye / Penso. H. Ellis. <a href="search.php?nq=1&query_type=call_number&query=7420">Indestructible Record: 1191</a>. 1909.</p>
	<p>I'll be with you, Honey, in honeysuckle time. Harvey Hindermyer. <a href="search.php?nq=1&query_type=call_number&query=7296">U.S. Everlasting Record: 1314</a>. 1909.</p>
	<p>Violets / Ellen Wright. J. Louis Von der Mehden, Jr. and His Orchestra. <a href="search.php?nq=1&query_type=call_number&query=7415">U.S. Everlasting Record: 1476</a>. 1909.</p>
	<p>Hi-le-hi-lo medley. George P. Watson. <a href="search.php?nq=1&query_type=call_number&query=7295">U.S. Everlasting Record: 1002</a>. 1909.</p>
	<p>Pretty pick's chat to me. U.S. Military Band. <a href="search.php?nq=1&query_type=call_number&query=7300">U.S. Everlasting Record: 1237</a>. 1909.</p>
	<p>Oh! You circus day. Arthur Collins ; Byron G. Harlan. <a href="search.php?nq=1&query_type=call_number&query=7283">U.S. Everlasting Record: 1545</a>. 1909.</p>
	<p>Six musical hits. U.S. Military Band. <a href="search.php?nq=1&query_type=call_number&query=7288">U.S. Everlasting Record: 1479</a>. 1909.</p>
	<p>Sweet spirit, hear my prayer / William Vincent Wallace. U.S. Instrumental Trio. <a href="search.php?nq=1&query_type=call_number&query=7297">U.S. Everlasting Record: 1284</a>. 1909.</p>
	<p>When Sunday rolls around. Ada Jones ; Walter Van Brunt. <a href="search.php?nq=1&query_type=call_number&query=7286">U.S. Everlasting Record: 1224</a>. 1909.</p>
	<p>The butterfly. . <a href="search.php?nq=1&query_type=call_number&query=7282">U.S. Everlasting Record: 1204</a>. 1909.</p>
	<p>In Jaytown. Steve Porter ; Byron G. Harlan. <a href="search.php?nq=1&query_type=call_number&query=7291">U.S. Everlasting Record: 1488</a>. 1909.</p>
	<p>Till the sands of the desert grow cold. Elsie Baker. <a href="search.php?nq=1&query_type=call_number&query=7285">U.S. Everlasting Record: 1591</a>. 1909.</p>
	<p>American fantasia. U.S. Concert Band. <a href="search.php?nq=1&query_type=call_number&query=7298">U.S. Everlasting Record: 1074</a>. 1909.</p>
	<p>Sweet spirit, hear my prayer / William Vincent Wallace. U.S. instrumental trio. <a href="search.php?nq=1&query_type=call_number&query=7497">U.S. Everlasting Record: 1284</a>. 1909.</p>
	<p>Forgotten melodies. Alexander Prince. <a href="search.php?nq=1&query_type=call_number&query=7884">Edison Blue Amberol: 23128</a>. 1909.</p>
	<p>Queen of the earth / Ciro Pinsuti. Peter Dawson. <a href="search.php?nq=1&query_type=call_number&query=7882">Edison Blue Amberol: 23127</a>. 1909.</p>
	<p>Irish vaudeville / Ada Jones and Steve Porter. Ada Jones and Steve Porter. <a href="search.php?nq=1&query_type=call_number&query=7292">U.S. Everlasting Record: 1526</a>. 1909.</p>
	<p>The waltz lesson. Florence Ethel Smith ; Harvey Hindermyer. <a href="search.php?nq=1&query_type=call_number&query=7293">U.S. Everlasting Record: 1390</a>. 1909 .</p>
	<p>Revival meeting at Pumpkin Center / Cal Stewart. Cal Stewart. <a href="search.php?nq=1&query_type=call_number&query=7429">U.S. Everlasting Record: 1349</a>. 1909- 1913.</p>
	<p>Come under my new camp. Billy Whitlock. <a href="search.php?nq=1&query_type=call_number&query=7476">Indestructible Record: 7050</a>. 191-.</p>
	<p>Summer reminds me of you. Walter Van Brunt. <a href="search.php?nq=1&query_type=call_number&query=7122">Indestructible Record: 3032</a>. 1910.</p>
	<p>The girl of my dreams / Karl Hoschna and Otto Hauerbach. Harry Anthony. <a href="search.php?nq=1&query_type=call_number&query=7133">Edison Gold Moulded Record: 594</a>. 1910.</p>
	<p>Has anybody here seen Kelly / C. W. Murphy. Ada Jones. <a href="search.php?nq=1&query_type=call_number&query=7142">Indestructible Record: 1248</a>. 1910.</p>
	<p>Put on your old grey bonnet / Percy Wenrich. Byron G. Harlan and chorus. <a href="search.php?nq=1&query_type=call_number&query=7143">Edison Blue Amberol: 1303</a>. 1910.</p>
	<p>I’d like to be a soldier boy in blue. Byron G. Harlan. <a href="search.php?nq=1&query_type=call_number&query=7207">Indestructible Record: 1295</a>. 1910.</p>
	<p>The morning after the night before / J. Fred Helf. Billy Murray. <a href="search.php?nq=1&query_type=call_number&query=7236">Edison Amberol: 488</a>. 1910.</p>
	<p>Infantry calls, No. 1. Fletcher. <a href="search.php?nq=1&query_type=call_number&query=7258">Indestructible Record: 1308</a>. 1910.</p>
	<p>Ebenezer Julius Caesar Washington Gray. Arthur Collins. <a href="search.php?nq=1&query_type=call_number&query=7340">Indestructible Record: 1242</a>. 1910.</p>
	<p>What makes the world go round / Harry Williams ; Egbert Van Alstyne. Elise Stevenson ; Frank Stanley. <a href="search.php?nq=1&query_type=call_number&query=7336">Indestructible Record: 3019</a>. 1910.</p>
	<p>Emmaline / William Jerome ; Jean Schwartz. Ada Jones ; Walter Van Brunt. <a href="search.php?nq=1&query_type=call_number&query=7320">Indestructible Record: 1317</a>. 1910.</p>
	<p>Grossmutterchen. . <a href="search.php?nq=1&query_type=call_number&query=7312">Indestructible Record: 1427</a>. 1910.</p>
	<p>Where the sunset turns the ocean's blue to gold / H. W. Petrie. Byron G. Harlan. <a href="search.php?nq=1&query_type=call_number&query=7313">Indestructible Record: 3014</a>. 1910.</p>
	<p>Dinah dear / John Larkins ; Chris Smith. Arthur Collins ; Byron G. Harlan. <a href="search.php?nq=1&query_type=call_number&query=7315">Indestructible Record: 1247</a>. 1910.</p>
	<p>Come along my Mandy / Nora Bayes. Ada Jones ; Walter Van Brunt. <a href="search.php?nq=1&query_type=call_number&query=7306">Indestructible Record: 3107</a>. 1910.</p>
	<p>The watermelon fete. Indestructible Symphony Orchestra. <a href="search.php?nq=1&query_type=call_number&query=7342">Indestructible Record: 3157</a>. 1910.</p>
	<p>To thee. Indestructible Military Band. <a href="search.php?nq=1&query_type=call_number&query=7326">Indestructible Record: 3111</a>. 1910.</p>
	<p>Amoretten tanze. Indestructible Record Band. <a href="search.php?nq=1&query_type=call_number&query=7343">Indestructible Record: 3154</a>. 1910.</p>
	<p>Serenata / Gaetano Braga. Charles Schuetze. <a href="search.php?nq=1&query_type=call_number&query=7316">Indestructible Record: 3179</a>. 1910.</p>
	<p>The butterfly / Theo Bendix. Eugene Rose ; George Rubel. <a href="search.php?nq=1&query_type=call_number&query=7344">Indestructible Record: 3025</a>. 1910.</p>
	<p>Cocoanut dance / Andrew Hermann. Indestructible Military Band. <a href="search.php?nq=1&query_type=call_number&query=7302">Indestructible Record: 1301</a>. 1910.</p>
	<p>The rube and the country doctor. Frank Stanley ; Byron G. Harlan. <a href="search.php?nq=1&query_type=call_number&query=7303">Indestructible Record: 3035</a>. 1910.</p>
	<p>I must go home tonight / William Hargreaves. Billy Williams. <a href="search.php?nq=1&query_type=call_number&query=7868">Edison Amberol: 12056</a>. 1910.</p>
	<p>The green isle of Erin / Joseph L. Roeckel. Ernest Pike. <a href="search.php?nq=1&query_type=call_number&query=7870">Edison Amberol: 12231</a>. 1910.</p>
	<p>H.M.S. Pinafore: Selection / Arthur Sullivan ; William S. Gilbert. National Military Band. <a href="search.php?nq=1&query_type=call_number&query=7879">Edison Amberol: 12170</a>. 1910.</p>
	<p>Garden of dreams. Florence Ethel Smith ; John Young [i.e. Harry Anthony]. <a href="search.php?nq=1&query_type=call_number&query=7479">U.S. Everlasting Record: 1158</a>. 1910?.</p>
	<p>Do you remember the last waltz / Bennett Scott ; Arthur J. Mills. Ernest Pike. <a href="search.php?nq=1&query_type=call_number&query=7402">Edison Standard Record: 14088</a>. 1911.</p>
	<p>Pfeif lied. Guido Dialdini. <a href="search.php?nq=1&query_type=call_number&query=7329">Indestructible Record: 1510</a>. 1911.</p>
	<p>Girl from Saskatchewan. Peerless Quartet. <a href="search.php?nq=1&query_type=call_number&query=7324">Indestructible Record: 3236</a>. 1911.</p>
	<p>Heaven is my home. James F. Harrison. <a href="search.php?nq=1&query_type=call_number&query=7327">Indestructible Record: 1477</a>. 1911.</p>
	<p>The dawn of love / Percy Elliott. Indestructible Record Band. <a href="search.php?nq=1&query_type=call_number&query=7330">Indestructible Record: 3182</a>. 1911.</p>
	<p>Whistling Pete. Billy Golden ; Joe Hughes. <a href="search.php?nq=1&query_type=call_number&query=7481">U.S. Everlasting Record: 1351</a>. 1911.</p>
	<p>Those songs my mother used to sing / H. Wakefield Smith. Elizabeth Spencer. <a href="search.php?nq=1&query_type=call_number&query=7871">Edison Amberol: 625</a>. 1911.</p>
	<p>All alone medley / Harry Von Tilzer. New York Military Band. <a href="search.php?nq=1&query_type=call_number&query=7872">Edison Amberol: 814</a>. 1911.</p>
	<p>Selection from 'The Gondoliers' / Arthur Sullivan ; William S. Gilbert. National Military Band. <a href="search.php?nq=1&query_type=call_number&query=7885">Edison Blue Amberol: 23170</a>. 1911.</p>
	<p>Luella Lee / Theodore F. Morse. Albert Campbell and Henry Burr. <a href="search.php?nq=1&query_type=call_number&query=7163">Edison Blue Amberol: 1582</a>. 1912.</p>
	<p>When I waltz with you. Charles W. Harrison. <a href="search.php?nq=1&query_type=call_number&query=7177">Edison Blue Amberol: 1556</a>. 1912.</p>
	<p>Roses, roses everywhere / H.(Henry) Trotère. Harvey Hindermyer. <a href="search.php?nq=1&query_type=call_number&query=7231">Edison Blue Amberol: 1554</a>. 1912.</p>
	<p>Where the edelweiss is blooming: "Hanky panky" / Alfred Baldwin Sloane. Elizabeth Spencer and Henry Burr. <a href="search.php?nq=1&query_type=call_number&query=7277">Edison Blue Amberol: 1599</a>. 1912.</p>
	<p>One sweetly solemn thought / R. S. Ambrose. Thomas Chalmers. <a href="search.php?nq=1&query_type=call_number&query=7378">Edison Blue Amberol: 28104</a>. 1912.</p>
	<p>On Moonlight Bay / Edward Madden ; Percy Wenrich. Helen Clark ; Walter Van Brunt. <a href="search.php?nq=1&query_type=call_number&query=7305">Indestructible Record: 3264</a>. 1912.</p>
	<p>In the golden afterwhile / Frank Stanley Grinsted. Peerless Quartette. <a href="search.php?nq=1&query_type=call_number&query=7874">Edison Amberol: 947</a>. 1912.</p>
	<p>Venus waltz - "Oh! Oh! Delphine" / Ivan Caryll. American Standard Orchestra. <a href="search.php?nq=1&query_type=call_number&query=7124">Edison Blue Amberol: 1620</a>. 1913.</p>
	<p>When was there ever a night like this - "The passing show of 1912" / Louis A. Hirsch. Charles W. Harrison. <a href="search.php?nq=1&query_type=call_number&query=7128">Edison Blue Amberol: 1541</a>. 1913.</p>
	<p>I wish that you belonged to me / Raymond A. Browne. Ada Jones and Billy Murray. <a href="search.php?nq=1&query_type=call_number&query=7145">Edison Blue Amberol: 2069</a>. 1913.</p>
	<p>You made me love you / James V. Monaco. Anna Chandler. <a href="search.php?nq=1&query_type=call_number&query=7178">Edison Blue Amberol: 1931</a>. 1913.</p>
	<p>If you only knew what I know says the moon / Robert D. Sharp. Ada Jones. <a href="search.php?nq=1&query_type=call_number&query=7180">Edison Blue Amberol: 2122</a>. 1913.</p>
	<p>My beautiful lady: The pink lady  / Ivan Caryll. Elizabeth Spencer. <a href="search.php?nq=1&query_type=call_number&query=7213">Edison Blue Amberol: 1992</a>. 1913.</p>
	<p>Down at Finnegan’s jamboree. Charles D’Almaine. <a href="search.php?nq=1&query_type=call_number&query=7216">Edison Blue Amberol: 1763</a>. 1913.</p>
	<p>La bella Cubanera / Mayhew Lake. National Promenade Band. <a href="search.php?nq=1&query_type=call_number&query=7217">Edison Blue Amberol: 21612161</a>. 1913.</p>
	<p>Good-bye boys / Harry Von Tilzer. Billy Murray. <a href="search.php?nq=1&query_type=call_number&query=7226">Edison Blue Amberol: 1749</a>. 1913.</p>
	<p>I’m looking for a nice young fellow who is looking for a nice young girl / S.R. Henry. Ada Jones. <a href="search.php?nq=1&query_type=call_number&query=7248">Edison Blue Amberol: 1853</a>. 1913.</p>
	<p>On the Mississippi: Hanky panky / Harry Carroll. Billy Murray. <a href="search.php?nq=1&query_type=call_number&query=7278">Edison Blue Amberol: 1637</a>. 1913.</p>
	<p>Old folks at home; arr. / Stephen Collins Foster. André Benoist. <a href="search.php?nq=1&query_type=call_number&query=7279">Edison Blue Amberol: 1908</a>. 1913.</p>
	<p>Uncle Josh's rheumatism / Cal Stewart. Cal Stewart. <a href="search.php?nq=1&query_type=call_number&query=7280">Edison Blue Amberol: 1986</a>. 1913.</p>
	<p>The golden wedding. Ada Jones ; Len Spencer. <a href="search.php?nq=1&query_type=call_number&query=7494">Edison Blue Amberol: 1871</a>. 1913.</p>
	<p>The old time street fakir. Steve Porter ; Byron G. Harlan. <a href="search.php?nq=1&query_type=call_number&query=7395">Edison Blue Amberol: 1999</a>. 1913.</p>
	<p>Way back home / Theodore F. Morse. Peerless Quartet. <a href="search.php?nq=1&query_type=call_number&query=7386">Edison Blue Amberol: 1930</a>. 1913.</p>
	<p>Till the sands of the desert grow cold / Ernest R. Ball. George Baker. <a href="search.php?nq=1&query_type=call_number&query=7314">Indestructible Record: 3308</a>. 1913.</p>
	<p>The matrimonial handicap / Robert P. Weston ; Fred J. Barnes. Jack Charman. <a href="search.php?nq=1&query_type=call_number&query=7878">Edison Blue Amberol: 23158</a>. 1913.</p>
	<p>Shipmates o' mine / Wilfrid E. Sanderson ; Edward Teschemacher. T. F. Kinniburgh. <a href="search.php?nq=1&query_type=call_number&query=7883">Edison Blue Amberol: 23120</a>. 1913.</p>
	<p>I'll love you forevermore / Henry Frantzen. Harry Anthony. <a href="search.php?nq=1&query_type=call_number&query=7889">Edison Blue Amberol: 1629</a>. 1913.</p>
	<p>She does like a little bit of scotch / Fred Godfrey ; Billy Williams. Billy Williams. <a href="search.php?nq=1&query_type=call_number&query=7881">Edison Blue Amberol: 23113</a>. 1913- 1914.</p>
	<p>Going back to Arkansas / Billy Golden and Joe Hughes. Billy Golden and Joe Hughes. <a href="search.php?nq=1&query_type=call_number&query=7127">Edison Blue Amberol: 2308</a>. 1914.</p>
	<p>In the evening by the moonlight, dear Louise / Harry Von Tilzer. Harvey Hindermyer. <a href="search.php?nq=1&query_type=call_number&query=7240">Edison Blue Amberol: 2457</a>. 1914.</p>
	<p>In the town where I was born / Alfred C. Harriman. Owen J. McCormack. <a href="search.php?nq=1&query_type=call_number&query=7247">Edison Blue Amberol: 2304</a>. 1914.</p>
	<p>When the green leaves turn to gold (take 3) / Raymond White. Elizabeth Spencer and Walter Van Brunt. <a href="search.php?nq=1&query_type=call_number&query=7276">Edison Blue Amberol: 2491</a>. 1914.</p>
	<p>Andreas Hofer / Julius Mosen. Harvey Hindermeyer. <a href="search.php?nq=1&query_type=call_number&query=7365">Edison Blue Amberol: 26127</a>. 1914.</p>
	<p>Highland fling. National Promenade Band. <a href="search.php?nq=1&query_type=call_number&query=7375">Edison Blue Amberol: 2244</a>. 1914.</p>
	<p>Sing me the rosary / F. Henri Klickmann. Henry Burr. <a href="search.php?nq=1&query_type=call_number&query=7310">Indestructible Record: 3340</a>. 1914.</p>
	<p>Seaside trippers. Will Terry. <a href="search.php?nq=1&query_type=call_number&query=7880">Edison Blue Amberol: 23262</a>. 1914.</p>
	<p>What time tomorrow night? / Fred Godfrey ; Billy Williams. Billy Williams. <a href="search.php?nq=1&query_type=call_number&query=7864">Edison Blue Amberol: 23279</a>. 1914.</p>
	<p>Laughing eyes: Intermezzo / Herman Finck. National Military Band. <a href="search.php?nq=1&query_type=call_number&query=7886">Edison Blue Amberol: 23290</a>. 1914.</p>
	<p>The violin my great grand-daddy made / Ernie Erdman. George Wilton Ballard. <a href="search.php?nq=1&query_type=call_number&query=7126">Edison Blue Amberol: 2613</a>. 1915.</p>
	<p>The three bears. Edna Bailey. <a href="search.php?nq=1&query_type=call_number&query=7214">Edison Blue Amberol: 2697</a>. 1915.</p>
	<p>Doodle-oodle dee / Theodore F. Morse. Arthur Collins and Byron G. Harlan. <a href="search.php?nq=1&query_type=call_number&query=7220">Edison Amberol: 2576</a>. 1915.</p>
	<p>Desperate Desmond (Rehearsing the orchestra) / N/A. Fred Duprez. <a href="search.php?nq=1&query_type=call_number&query=7249">Edison Blue Amberol: 2636</a>. 1915.</p>
	<p>Garden of roses waltz / Ellis Brooks. New York Military Band. <a href="search.php?nq=1&query_type=call_number&query=7384">Edison Blue Amberol: 2730</a>. 1915.</p>
	<p>My own Iona / Anatol Friedland. Albert Campbell ; Henry Burr. <a href="search.php?nq=1&query_type=call_number&query=7323">Indestructible Record: 3391</a>. 1915?.</p>
	<p>Gentle spring is here againOld Silas does the turkey trot / Glenn C. Leap. Byron G. Harlan ; Arthur Collins. <a href="search.php?nq=1&query_type=call_number&query=7332">Indestructible Record: 3336</a>. 1915?.</p>
	<p>Praise ye: Attila  / Giuseppe Verdi. Sodero’s Band. <a href="search.php?nq=1&query_type=call_number&query=7164">Edison Blue Amberol: 2848 </a>. 1916.</p>
	<p>The girl who smiles / Jean Briquet. Jaudas’ Society Orchestra. <a href="search.php?nq=1&query_type=call_number&query=7165">Edison Blue Amberol: 2850 </a>. 1916.</p>
	<p>Mari, Mari! / Eduardo Di Capua. Thomas Chalmers. <a href="search.php?nq=1&query_type=call_number&query=7179">Edison Blue Amberol: 3016</a>. 1916.</p>
	<p>What a wonderful mother you’d make / Al Piantadosi. Henry Burr. <a href="search.php?nq=1&query_type=call_number&query=7208">Indestructible Record: 1530</a>. 1916.</p>
	<p>By the sad Luana shore. Step this way / E. Ray Goetz. Elizabeth Spencer and George Wilton Ballard. <a href="search.php?nq=1&query_type=call_number&query=7281">Edison Blue Amberol: 3000</a>. 1916.</p>
	<p>Melody in F / Anton Rubinstein. Isidore Moskowitz. <a href="search.php?nq=1&query_type=call_number&query=7353">Edison Blue Amberol: 2818</a>. 1916.</p>
	<p>Honey, I wants yer now ; Jerusalem morning / Collin Coe. Criterion Quartet. <a href="search.php?nq=1&query_type=call_number&query=7367">Edison Blue Amberol: 2862</a>. 1916.</p>
	<p>There's a little lane without a turning "on the way to home sweet home" / George W. Meyer. George Wilton Ballard. <a href="search.php?nq=1&query_type=call_number&query=7379">Edison Blue Amberol: 2811</a>. 1916.</p>
	<p>Merry whirl - one-step / Vess L. Ossman's Banjo Orchestra. Julius Lenzberg. <a href="search.php?nq=1&query_type=call_number&query=7389">Edison Blue Amberol: 2858</a>. 1916.</p>
	<p>Pretty baby / Gus Kahn. Pete Murray. <a href="search.php?nq=1&query_type=call_number&query=7263">Edison Blue Amberol: 3374</a>. 1916.</p>
	<p>By the sad Luana shore / E. Ray Goetz. Elizabeth Spencer and George Wilton Ballard. <a href="search.php?nq=1&query_type=call_number&query=7499">Edison Blue Amberol: 3000</a>. 1916.</p>
	<p>Medley of American patriotic airs. New York Military Band. <a href="search.php?nq=1&query_type=call_number&query=7129">Edison Blue Amberol: 3203</a>. 1917.</p>
	<p>Bella figlia dell’ amore [Rigoletto. Quartetto; arr.]?? / Giuseppe Verdi. Alice Verlet, Merle Alcock, Arthur Middleton and Guido Ciccolini. <a href="search.php?nq=1&query_type=call_number&query=7202">Edison Royal Purple Amberol: 29006</a>. 1917.</p>
	<p>Joy to the world, our Lord is born today / I.H. Meredith. Metropolitan Quartet . <a href="search.php?nq=1&query_type=call_number&query=7356">Edison Blue Amberol: 3345</a>. 1917.</p>
	<p>Honest Injun - one-step / Harry  Von Tilzer. Lou Chiha "Frisco". <a href="search.php?nq=1&query_type=call_number&query=7361">Edison Blue Amberol: 3356</a>. 1917.</p>
	<p>Annie Laurie / Lady John Douglas Scott. Anna Case. <a href="search.php?nq=1&query_type=call_number&query=7363">Edison Blue Amberol: 28261</a>. 1917.</p>
	<p>La confession valse. Jaudas' Society Orchestra. <a href="search.php?nq=1&query_type=call_number&query=7366">Edison Blue Amberol: 3047</a>. 1917.</p>
	<p>Gentle Annie /  Stephen Collins Foster . Apollo Quartet of Boston. <a href="search.php?nq=1&query_type=call_number&query=7390">Edison Blue Amberol: 3289</a>. 1917.</p>
	<p>Dreams / Anton Strelezki. Carolina Lazzari. <a href="search.php?nq=1&query_type=call_number&query=7385">Edison Blue Amberol: 3208</a>. 1917.</p>
	<p>Hawaiian butterfly / Billy Baskette ; Joseph H. Santly. Helen Louise ; Frank Ferera. <a href="search.php?nq=1&query_type=call_number&query=7328">Indestructible Record: 1563</a>. 1917.</p>
	<p>Wailana waltz. Helen Louise ; Frank Ferera. <a href="search.php?nq=1&query_type=call_number&query=7304">Indestructible Record: 3409</a>. 1917.</p>
	<p>Smiles / Lee S. Roberts and J. Will Callahan. The Harmony Four. <a href="search.php?nq=1&query_type=call_number&query=7123">Edison Blue Amberol: 3613</a>. 1918.</p>
	<p>Bonnie Kate / John J. Kimmel. John J. Kimmel ;  Joe Linder. <a href="search.php?nq=1&query_type=call_number&query=7377">Edison Blue Amberol: 3577</a>. 1918.</p>
	<p>Go down, Moses (let my people go!) / H.T. Burleigh. Reed Miller. <a href="search.php?nq=1&query_type=call_number&query=7380">Edison Blue Amberol: 3574</a>. 1918.</p>
	<p>Paddle-addle - fox trot / Ted Snyder. Jaudas' Society Orchestra. <a href="search.php?nq=1&query_type=call_number&query=7383">Edison Blue Amberol: 3389</a>. 1918.</p>
	<p>Hawaiian breezes waltz / J. A. MacMeekin. Waikiki Hawaiian Orchestra. <a href="search.php?nq=1&query_type=call_number&query=7289">Edison Blue Amberol: 3616</a>. 1918.</p>
	<p>Oh! What a pal was Mary / Pete Wendling. Edward Allen. <a href="search.php?nq=1&query_type=call_number&query=7131">Edison Blue Amberol: 3872</a>. 1919.</p>
	<p>I'm forever blowing bubbles / Jaan Kenbrovin. Helen Clark and George Wilton Ballard. <a href="search.php?nq=1&query_type=call_number&query=7144">Edison Blue Amberol: 3798</a>. 1919.</p>
	<p>Wait and see: (you’ll want me back) / Carey Morgan. Charles Hart. <a href="search.php?nq=1&query_type=call_number&query=7255">Edison Blue Amberol: 3814</a>. 1919.</p>
	<p>In heavenly love abiding /  Albert J. Holden. Metropolitan Quartet. <a href="search.php?nq=1&query_type=call_number&query=7493">Edison Blue Amberol: 3813</a>. 1919.</p>
	<p>Love in idleness - serenade / Allan Macbeth. Sodero's Band. <a href="search.php?nq=1&query_type=call_number&query=7388">Edison Blue Amberol: 3711</a>. 1919.</p>
	<p>I’m in heaven when I’m in my mother’s arms / Milton Ager. Helen Clark. <a href="search.php?nq=1&query_type=call_number&query=7253">Edison Blue Amberol: 4124</a>. 1920.</p>
	<p>'Twas an old-fashioned song he was singing / Fred Godfrey. Lewis James. <a href="search.php?nq=1&query_type=call_number&query=7492">Edison Blue Amberol: 3906</a>. 1920.</p>
	<p>When you write, sweet Marie (send your heart to me) / Jack Mahoney. Vernon Dalhart. <a href="search.php?nq=1&query_type=call_number&query=7359">Edison Blue Amberol: 4016</a>. 1920.</p>
	<p>Flanagan's night off. Ada Jones ; Steve Porter. <a href="search.php?nq=1&query_type=call_number&query=7351">Edison Blue Amberol: 3992</a>. 1920.</p>
	<p>Flanagan's real estate deal. Steve Porter. <a href="search.php?nq=1&query_type=call_number&query=7355">Edison Blue Amberol: 3936</a>. 1920.</p>
	<p>Back home on the farm. Billy Golden ; Joe Hughes. <a href="search.php?nq=1&query_type=call_number&query=7350">Edison Blue Amberol: 3912</a>. 1920.</p>
	<p>Fancy little Nancy / William Baines. Wheeler Wadsworth. <a href="search.php?nq=1&query_type=call_number&query=7349">Edison Blue Amberol: 3903</a>. 1920.</p>
	<p>I'd love to fall asleep and wake up in my mammy's arms / Fred E. Albert. Reese Jones. <a href="search.php?nq=1&query_type=call_number&query=7387">Edison Blue Amberol: 4070</a>. 1920.</p>
	<p>I'm a dancin' fool / Al Bernard. Al Bernard. <a href="search.php?nq=1&query_type=call_number&query=7382">Edison Blue Amberol: 3913</a>. 1920.</p>
	<p>Chili bean / Albert Von Tilzer. Billy Murray. <a href="search.php?nq=1&query_type=call_number&query=7393">Edison Blue Amberol: 4103</a>. 1920.</p>
	<p>I'm a dancin' fool / J. Russel Robinson. Al Bernard. <a href="search.php?nq=1&query_type=call_number&query=7496">Edison Blue Amberol: 3913</a>. 1920.</p>
	<p>Saxema. Rudy Wiedoeft. <a href="search.php?nq=1&query_type=call_number&query=7798">Edison Blue Amberol: 4005</a>. 1920.</p>
	<p>Was there ever a pal like you? / Irving Berlin. George Wilton Ballard. <a href="search.php?nq=1&query_type=call_number&query=7953">Edison Blue Amberol: 3971</a>. 1920.</p>
	<p>I come from get-it-land / Al Bernard and Ernest Hare. Al Bernard and Ernest Hare. <a href="search.php?nq=1&query_type=call_number&query=7800">Edison Amberol: 3980 </a>. 1920.</p>
	<p>The Broadway Blues / Carey Morgan. Al Bernard. <a href="search.php?nq=1&query_type=call_number&query=7246">Edison Blue Amberol: 4181</a>. 1921.</p>
	<p>Gee willikens / Al Wilson. Byron G. Harlan. <a href="search.php?nq=1&query_type=call_number&query=7474">Edison Blue Amberol: 4397</a>. 1921.</p>
	<p>I wonder if you still care for me / Ted Snyder.  Walter Van Brunt. <a href="search.php?nq=1&query_type=call_number&query=7364">Edison Blue Amberol: 4381</a>. 1921.</p>
	<p>Nobody's rose. George Wilton Ballard . <a href="search.php?nq=1&query_type=call_number&query=7352">Edison Blue Amberol: 4321</a>. 1921.</p>
	<p>Ma! / Sidney Clare ; Con Conrad. Harry Raderman's Jazz Orchestra. <a href="search.php?nq=1&query_type=call_number&query=7394">Edison Blue Amberol: 4380</a>. 1921.</p>
	<p>Down at the old swimming hole / Al Wilson ; James A. Brennan. Billy Jones ; Ernest Hare. <a href="search.php?nq=1&query_type=call_number&query=7372">Edison Blue Amberol: 4391</a>. 1921.</p>
	<p>The sinner and the song / Will L. Thompson. Fred East. <a href="search.php?nq=1&query_type=call_number&query=7498">Edison Blue Amberol: 4360</a>. 1921.</p>
	<p>Nobody's rose / Maritn Fried and John White. George Wilton Ballard and female chorus. <a href="search.php?nq=1&query_type=call_number&query=7500">Edison Blue Amberol: 4321</a>. 1921.</p>
	<p>Virginia blues / Fred Meinken. Broadway Dance Orchestra. <a href="search.php?nq=1&query_type=call_number&query=7134">Edison Blue Amberol: 4533</a>. 1922.</p>
	<p>Down old Virginia way / Abe Olman. Helen Clark and Walter Van Brunt. <a href="search.php?nq=1&query_type=call_number&query=7157">Edison Blue Amberol: 4664</a>. 1922.</p>
	<p>Swaying - waltz / Edward Stephen Chenette. Ernest L. Stevens. <a href="search.php?nq=1&query_type=call_number&query=7173">Edison Blue Amberol: 4575</a>. 1922.</p>
	<p>Serenade / Moritz Moszkowski. Creatore and his band. <a href="search.php?nq=1&query_type=call_number&query=7184">Edison Blue Amberol: 4606</a>. 1922.</p>
	<p>Closer / Leon Du Parc. Walter Van Brunt. <a href="search.php?nq=1&query_type=call_number&query=7374">Edison Blue Amberol: 4573</a>. 1922.</p>
	<p>Mindin’ my business / Gus Kahn. Ernest Hare. <a href="search.php?nq=1&query_type=call_number&query=7150">Edison Blue Amberol: 4865</a>. 1924.</p>
	<p>The Boston burglar. Dalhart Vernon. <a href="search.php?nq=1&query_type=call_number&query=7348">Edison Blue Amberol: 5129</a>. 1925.</p>
	<p>Christofo Columbo (thought the world was round-o) / "Speed" Langworthy. Billy Jones. <a href="search.php?nq=1&query_type=call_number&query=7347">Edison Blue Amberol: 5008</a>. 1925.</p>
	<p>I'll make the pies like mother made (if you'll make the dough like dad) / Harry Von Tilzer . Billy Jones. <a href="search.php?nq=1&query_type=call_number&query=7358">Edison Blue Amberol: 5001</a>. 1925.</p>
	<p>Oh, how I miss you to-night / Benny Davis ; Joe Burke ; Mark Fisher. James Doherty. <a href="search.php?nq=1&query_type=call_number&query=7346">Edison Blue Amberol: 5045</a>. 1925.</p>
	<p>I'll take you home again, pal o' mine / Harold Dixon ; Claude Sacre. Walter Scanlan [i.e. Walter van Brunt] ; Helen Clark. <a href="search.php?nq=1&query_type=call_number&query=7392">Edison Blue Amberol: 4984</a>. 1925.</p>
	<p>The death of Floyd Collins / Irene Spain ; Andrew Jenkins. Vernon Dalhart and Company. <a href="search.php?nq=1&query_type=call_number&query=7370">Edison Blue Amberol: 5049</a>. 1925.</p>
	<p>The prisoner's song / Guy S. Massey. Vernon Dalhart. <a href="search.php?nq=1&query_type=call_number&query=7371">Edison Blue Amberol: 4954</a>. 1925.</p>
	<p>Where they never say goodbye / A.H. Ackley. Homer A. Rodeheaver. <a href="search.php?nq=1&query_type=call_number&query=7475">Edison Blue Amberol: 5173</a>. 1926.</p>
	<p>The smiler / Percy Wenrich. Fred Van Eps ; John F. Burckhardt. <a href="search.php?nq=1&query_type=call_number&query=7381">Edison Blue Amberol: 5182</a>. 1926.</p>
	<p>Wild Bill Jones. Ernest V. Stoneman. <a href="search.php?nq=1&query_type=call_number&query=7376">Edison Blue Amberol: 5196</a>. 1926.</p>
	<p>Drifting and dreaming / Egbert Van Alstyne. Waikiki Hawaiian Orchestra. <a href="search.php?nq=1&query_type=call_number&query=7373">Edison Blue Amberol: 5227</a>. 1926.</p>
	<p>The freight wreck at Altoona / Carson Robison. Vernon Dalhart. <a href="search.php?nq=1&query_type=call_number&query=7360">Edison Blue Amberol: 5122</a>. 1926.</p>
	<p>Hand me down my walking cane / James A. Bland. Ernest V. Stoneman and his Dixie Mountaineers. <a href="search.php?nq=1&query_type=call_number&query=7362">Edison Blue Amberol: 5297</a>. 1927.</p>
	<p>The Mississippi flood / Carson Robison. Vernon Dalhart. <a href="search.php?nq=1&query_type=call_number&query=7357">Edison Blue Amberol: 5395</a>. 1927.</p>
	<p>Naughty Marietta selections. Victor Herbert and his orchestra. <a href="search.php?nq=1&query_type=call_number&query=7146">Edison Blue Amberol: 5487</a>. 1928.</p>
	<p>
		The old maid and the burglar. Ernest V. Stoneman and his Dixie Mountaineers. <a href="search.php?nq=1&query_type=call_number&query=7354">Edison Blue Amberol: 5531</a>. 1928.
	</p>
</div><!-- end .content -->
<!-- End of file: newadditions-june2008.tpl -->   
{include file="footer.tpl"}