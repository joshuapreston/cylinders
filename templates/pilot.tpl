{include file="header.tpl"}
{include file="navigation.tpl"}
<!-- File: pilot.tpl -->
<div class="content text">
  <h1>Cylinder Preservation and Digitization Pilot Project</h1>
    <p>(These  pages are modified versions of the original pilot project pages put online in 2002)</p>
  <div class="pure-g">
    <div class="pure-u-1 pure-u-md-1-1 pure-u-lg-1-2">
      <div class="center-img-horiz">
        <img src="/images/cylinders.jpg" ALT="Cylinder Player">
      </div>
    </div>
    <div class="pure-u-1 pure-u-md-1-1 pure-u-lg-1-2">
      <h3><a href="pilotintro.php">Introduction</a></h3>
        Introduction to the scope and purpose of the pilot project
      <h3><a href="pilottech.php">Technical Details</a></h3>
        Details on the analog to digital conversion process, the compression used for web access, and  cataloging
    </div>
  </div>
</div><!-- end .content -->
<!-- End of file: pilot.tpl -->   
{include file="footer.tpl"}