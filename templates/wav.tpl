{include file="header.tpl"}
{include file="navigation.tpl"}
<!-- File: wav.tpl -->
<div class="content text">
	<h1>Why can I no longer download wav files?</h1>
  <p>
  	As per a change in policy after June 2009, the library will no longer be providing
  	downloadable wav files free of charge. The UCSB library remains committed to maintaining
  	this site as a free resouce and mp3 files can still be downloaded free of charge. Comercial use of mp3 files is prohibited.
  </p>
  <p>
  	In an effort to develop a sustainable model for continuing the ongoing digitization of new cylinders,
  	the library charges a use for for the high-resolution unrestored transfers and high-resolution
  	restored versions on wav files for commercial and non commercial use. For use fees  see our
  	<a href="/licensing.php">rate sheet</a>. Please direct any questions to the
  	project director David  Seubert at 805-893-5444 or at <a href="mailto:seubert@ucsb.edu">seubert@ucsb.edu</a>.  </p>
</div><!-- end .content -->
<!-- End of file: wav.tpl -->
{include file="footer.tpl"}
