{include file="header.tpl"}
{include file="navigation.tpl"}
<!-- File: relaunch.tpl -->
<div class="content text">
    <h1 class="center-text">UCSB Cylinder Audio Archive Launched&nbsp;</h1>
    <p>Nearly ten years after its launch in November, 2005, the Cylinder Preservation and Digitization project has been relaunched as the UCSB Cylinder Audio Archive. With new content, enhanced compatibility with mobile devices, and a more user-friendly interface, the site can now make this unique content available to a new generation of users on a wider variety of platforms.</p>
  <p>Please contact staff with any questions or comments.</p>
  <p><em>&mdash;<a href="mailto:seubert@ucsb.edu">David Seubert</a>, UC Santa Barbara Library</em></p>

</div><!-- end .content -->
<!-- End of file: relaunch.tpl -->
{include file="footer.tpl"}
