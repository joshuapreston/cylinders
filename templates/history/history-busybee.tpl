{include file="header.tpl"}
{include file="navigation.tpl"}
<!-- File: history-busybee.tpl -->
<div class="content text">
	<h2>Busy-Bee Cylinders</h2>

  <figure>
    <img src="/images/busybee.jpg" title="Busy Bee Cylinder" alt="Uncle Josh at the dentist&#39;s / Cal Stewart and Len Spencer. Busy-Bee Record: 437. 190-." /><figcaption>Uncle Josh at the dentist's / Cal Stewart and Len Spencer. <a href="/search.php?nq=1&query_type=call_number&query=6659">Busy-Bee Record: 437</a>. 190-.</figcaption>
  </figure>

  <p>
  	<a href="search.php?nq=1&query=busy+bee&query_type=keyword">Busy-Bee cylinders</a>
	were manufactured by the O'Neill-James Co. of Chicago, which issued both cylinders and discs,
	often with content licensed from other companies&mdash;such as this recording originally issued as a Columbia cylinder.</p>
  <div class="pagination">
  	<span class="previous">Previous: <a href="history-columbia.php" title="Previous">Columbia  Cylinders</a></span>
    <i class="small-screen fa fa-ellipsis-h"></i>
  	<span class="next">Next: <a href="history-indestructible.php" title="Next">Indestructible  Cylinders</a></span>
  </div>
</div><!-- end .content -->
<!-- End of file: history-busybee.tpl -->
{include file="footer.tpl"}
