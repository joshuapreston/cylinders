{include file="header.tpl"}
{include file="navigation.tpl"}
<!-- File: history.tpl -->
<div class="content text">
  <h1>Cylinder Recordings: A Primer</h1>
    <p>
      From the first recordings made on tinfoil in 1877 to the last produced on
      celluloid in 1929, cylinders spanned a half-century of technological development
      in sound recording. As documents of American cultural history and musical
      style, cylinders serve as an audible witness to the sounds and songs through
      which typical audiences first encountered the recorded human voice. And
      for those living at the turn of the 20th century, the most likely source
      of recorded sound on cylinders would have been Thomas Alva Edison's crowning
      achievement, the phonograph. Edison wasn't the only one in the sound recording
      business in the first decades of the 20th century; several companies with
      a great number of recording artists, in addition to the purveyors of the
      burgeoning disc format, all competed in the nascent musical marketplace.
      Still, more than any other figure of his time, Edison and the phonograph
      became synonymous with the cylinder medium.&nbsp;Because of the overwhelming
      preponderance of cylinder recordings bearing his name in UCSB's collection,
      the following history is, we admit, Edison-centric. Nonetheless, Edison's
      story is heavily dependent on the stories of numerous musical figures and
      sound recording technological developments emblematic of the period, and
      it is our hope that we have fairly represented them here.&nbsp;Herein, a
      humble primer.
    </p>
    <h3>Early Cylinder Recordings&mdash;The Beginnings of Recorded Sound</h3>
      <ul>
        <li><a href="history-early.php">Precursors to Edison's Phonograph</a></li>
        <li><a href="history-tinfoil.php">Tinfoil Recordings</a></li>
        <li><a href="history-wax.php">The Earliest Wax Cylinders</a></li>
        <li><a href="history-lioret.php">Lioret Cylinders</a></li>
        <li><a href="history-brownwax.php">Brown Wax Cylinders </a></li>
        <li><a href="history-concerts.php">Brown Wax Concert Cylinders</a></li>
      </ul>
    <h3>Cylinders in the New Century&mdash;Edison Phonograph Cylinders </h3>
      <ul>
        <li><a href="history-goldmoulded.php">Edison Gold-Moulded Cylinders</a></li>
        <li><a href="history-amberol.php">Edison Amberol Cylinders </a></li>
        <li><a href="history-grandopera.php">Edison Grand Opera Cylinders</a></li>
        <li><a href="history-blueamberol.php">Edison Blue Amberol Cylinders</a></li>
        <li><a href="history-edisonforeign.php">Edison Foreign Releases&mdash;Cylindres Edison Moul&eacute;s Sur Or and Edison Goldguss Walze</a></li>
        <li><a href="history-chinese.php">Edison Chinese Cylinders</a></li>
      </ul>
    <h3>Edison's Competitors</h3>
      <ul>
        <li><a href="history-columbia.php">Columbia Cylinders</a></li>
        <li><a href="history-busybee.php">Busy Bee Cylinders</a></li>
        <li><a href="history-indestructible.php">Indestructible Cylinders</a></li>
        <li><a href="history-everlasting.php">U.S. Everlasting Cylinders</a></li>
        <li><a href="history-lambert.php">Lambert Cylinders</a></li>
        <li><a href="history-pathe.php">Path&eacute; Cylinders</a></li>
        <li><a href="history-edisonbell.php">Edison Bell Cylinders</a></li>
        <li><a href="history-sterling.php">Sterling Cylinders</a>  </li>
      </ul>

      <h3>Further Reading</h3>
      <ul>
        <li><a href="history-bibliography.php">Bibliography</a></li>
      </ul>

  <div class="pagination">
  	<span class="next">Next: <a href="history-early.php" title="Next">Precursors to Edison's Phonograph</a></span>
  </div>

</div><!-- end .content -->
<!-- End of file: history.tpl -->
{include file="footer.tpl"}
