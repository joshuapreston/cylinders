{include file="header.tpl"}
{include file="navigation.tpl"}
<!-- File: history-edisonforeign.tpl -->
<div class="content text">
  <h2>Edison Foreign Releases—Cylindres Edison Moul&eacute;s Sur Or and Edison Goldguss Walze</h2>

  <figure>
    <img src="/images/edisonfrench.jpg" title="Cylindres Edison Moulés Sur Or" alt="Traviata. De&#39; miei bollenti spiriti / Verdi. Lucien Muratore, tenor. Cylindres Edison Moulés Sur Or: 17523. between 1907 and 1909." /><figcaption>Traviata. De' miei bollenti spiriti / Verdi. Lucien Muratore, tenor. <a href="search.php?nq=1&query_type=call_number&query=4524">Cylindres Edison Moulés Sur Or: 17523</a>. between 1907 and 1909.</figcaption>
  </figure>

  <p>
    In addition to the Gold-Moulded recordings, which were recorded and manufactured
    in the United States, Edison recorded and released cylinders from offices
    in London, Paris, and Berlin. While many foreign cylinders were issued
    in the United States in series such as Edison's 
    <a href="search.php?nq=1&query=british+series&query_type=keyword">British</a>,
    <a href="search.php?nq=1&query=bohemian+series&query_type=keyword">Bohemian</a>,
    <a href="search.php?nq=1&query=foreign+french+series&query_type=keyword">French</a>,
    <a href="search.php?nq=1&query=foreign+german+series&query_type=keyword">German</a>,
    <a href="search.php?nq=1&query_type=keyword+&query=foreign+swedish">Swedish</a>,
    and 
    <a href="search.php?nq=1&query=norwegian+series&query_type=keyword">Norwegian</a>
    series, cylinders were also recorded and manufactured overseas in several
    markets, including France and Germany. Much less is known about Edison's
    overseas operations and cylinders than the U.S. cylinders.
  </p>

  <p>
    Our first foreign series is a group of releases recorded in Paris from
    approximately 1904 to 1909 under the name "
    <a href="search.php?nq=1&query=Cylindres+Edison+Moules+Sur+Or&query_type=keyword">Cylindres Edison Moul&eacute;s Sur Or</a>." Although some of these recordings were
    popular commercial tunes sung in French, the vast majority were operatic
    arias and short classical works, such as the selection above, sung by
    the famous French tenor
    <a href="search.php?nq=1&query_type=keyword+&query=Lucien+Muratore"> Lucien Muratore</a>, which is accompanied by its original box—presumably
    what one would have found in a turn-of-the-century Parisian music store.
    There are approximately 100 French releases in the UCSB collection.
  </p>

  <figure>
    <img src="/images/edisongoldguss.jpg" title="Edison Goldguss Walze" alt="Lohengrin. Mein lieber Schwan / Wagner. Ernst Kraus, tenor. Edison Goldguss Walze: 12258.1907." /><figcaption>Lohengrin. Mein lieber Schwan / Wagner. Ernst Kraus, tenor. 
    <a href="/search.php?nq=1&query_type=call_number&query=2513">Edison Goldguss Walze: 12258</a>.1907.</figcaption>
  </figure>

  <p>
    Berlin supplied the Edison Company with dozens of German-language cylinder
    recordings from around 1902 to 1912. In addition to popular songs of the
    day, the "<a href="search.php?nq=1&query_type=keyword+&query=Edison+Goldguss+Walze">Edison Goldguss Walze</a>" cylinders, like their French counterparts, showcased operatic arias performed by European singers. Interestingly, the Goldguss
    Walze series also includes numerous German comedy sketches, which are
    featured in the cylinder radio show <a href="deutschekomische.php">Deutsche komische Zylinder</a>.
  </p>  <div class="pagination">
  	<span class="previous">Previous: <a href="history-blueamberol.php" title="Previous">Edison Blue Amberol Cylinders</a></span>
    <i class="small-screen fa fa-ellipsis-h"></i>
  	<span class="next">Next: <a href="history-chinese.php" title="Next">Edison Chinese Cylinders</a></span>
  </div>
</div><!-- end .content -->
<!-- End of file: history-edisonforeign.tpl -->
{include file="footer.tpl"}
