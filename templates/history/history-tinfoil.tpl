{include file="header.tpl"}
{include file="navigation.tpl"}
<!-- File: history-tinfoil.tpl -->
<div class="content text">
  <h2>Tinfoil Recordings</h2>

  <figure>
    <img src="/images/tinfoil.jpg" alt="Original Edison tinfoil phonograph. (Photo courtesy of U.S. Department of the Interior, National Park Service, Edison National Historic Site.)" /><figcaption>Original Edison tinfoil phonograph. (Photo courtesy of U.S. Department of the Interior, National Park Service, Edison National Historic Site.)</figcaption>
  </figure>

  <p>
    In 1877, Edison put all previous ideas and experiments with sound to the
    ultimate test in the invention of tinfoil recordings. By singing or playing
    in front of a large horn at a predetermined distance, a performer would
    produce sound that could be captured via a diaphragm attached to a stylus.
    When the diaphragm was stimulated by the vibration of sound waves, the
    resulting motion caused the stylus to come down on a piece of tinfoil
    wrapped around a cylindrical mandrel, which, with the help of a human-powered
    crank, would then create a series of indentations&mdash;in essence, a tinfoil
    record.&nbsp;Despite the tinny quality of these recordings (pun intended!),
    the records and the phonograph on which they played flourished briefly
    in the late 1870s as an entertainment device. They were novel enough,
    in fact, that Edison purportedly was granted an audience with President
    Rutherford B. Hayes to demonstrate his newfangled contraption. Yet as any user of ordinary
    kitchen foil knows, the medium has serious longevity problems. The recordings
    could be played only a few times before the indentations made by the sound
    on the tinfoil were so flattened down as to be unplayable.&nbsp;Alas,
    none of the few existing tinfoil recordings have been successfully transferred
    to modern media.&nbsp;Nonetheless, the basic design of Edison's tinfoil
    phonograph changed little over time.&nbsp;It was changes in the physical
    medium itself that would ultimately exert the most long-lasting effects
    on the evolution of American sound recording history.
  </p>
  <div class="pagination">
  	<span class="previous">Previous: <a href="history-early.php" title="Previous">Precursors to Edison's Phonograph</a></span>
    <i class="small-screen fa fa-ellipsis-h"></i>
  	<span class="next">Next: <a href="history-wax.php" title="Next">The Earliest Wax Cylinders</a></span>
  </div>
</div><!-- end .content -->
<!-- End of file: history-tinfoil.tpl -->
{include file="footer.tpl"}
