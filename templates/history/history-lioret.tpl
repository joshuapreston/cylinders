{include file="header.tpl"}
{include file="navigation.tpl"}
<!-- File: history-lioret.tpl -->
<div class="content text">
  <h2>Lioret Cylinders (1893&ndash;1900)</h2>

  <figure>
    <img src="/images/lioret.jpg" title="Lioret Cylinders" alt="Several examples of Lioret cylinders, clockwise from bottom left, No. 1, No. 2, No. 3, brown wax, and No. 4 varieties." /><figcaption>Several examples of Lioret cylinders, clockwise from bottom left, <a href="/search.php?nq=1&query_type=call_number&query=11694">No. 1</a>, <a href="/search.php?nq=1&query_type=call_number&query=11694">No. 2</a>, <a href="/search.php?nq=1&query_type=call_number&query=11695">No. 3</a>, <a href="/search.php?nq=1&query_type=call_number&query=5307">brown wax</a>, and <a href="/search.php?nq=1&query_type=call_number&query=11692">No. 4</a> varieties.</figcaption>
  </figure>

  <p>
    French clockmaker and inventor Henri Lioret first developed a talking doll in 1893 using a small celluloid cylinder, and by 1897
    he had developed a completely original type of musical cylinder unlike that of any other manufacturer.
    <a href="/search.php?nq=1&query=lioret&query_type=keyword">Lioret cylinders</a>
    were unique in their construction, comprising a brass tube with spokes covered by a molded celluloid sleeve carrying the grooves.
    He was the first to use the durable celluloid for cylinders (which Edison later adopted in 1912) and was also the first to develop
    a method of duplicating cylinders by molding. In the final years of the 1890s he was recording and manufacturing several sizes of
    musical cylinders to be played on his clockwork phonographs, commonly known as Lioretgraphs
    (though the different models had specific names). They played at 100 or 120 rpm and, depending on the type,  they contained anywhere
    from 30 seconds (No. 1) to 4 minutes (Eureka No. 4) of music.
  </p>
  <p>
    The color of the label color initially indicated the type of music, with blue labels for songs, orange for instrumental solos,  red  for fanfares,
    green for harmonies, and gray for hymns. By 1899, Lioret's catalog listed nearly 1400 titles. Performers are not named or
    announced and catalog numbers are almost never listed on the cylinder, making precise identification and  dating difficult.  </p>
  <p>
    Lioret later issued  convential brown wax cylinders   as illustrated in the photograph above. For more information,
    see Julien Anton's biography, <a href="http://worldcatlibraries.org/wcpa/oclc/74666929">Henri Lioret: Clockmaker and Phonograph Pioneer</a>
  </p>
  <p>
    UCSB's small but significant collection of Lioret cylinders was originally owned by collector Ray Phillips
    and supplemented by collector John Levin.
  </p>  <div class="pagination">
  	<span class="previous">Previous: <a href="history-wax.php" title="Previous">The Earliest Wax Cylinders</a></span>
    <i class="small-screen fa fa-ellipsis-h"></i>
  	<span class="next">Next: <a href="history-brownwax.php" title="Next">Brown Wax Cylinders</a></span>
  </div>
</div><!-- end .content -->
<!-- End of file: history-lioret.tpl -->
{include file="footer.tpl"}
