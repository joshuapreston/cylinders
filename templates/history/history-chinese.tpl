{include file="header.tpl"}
{include file="navigation.tpl"}
<!-- File: history-chinese.tpl -->
<div class="content text">
  <h2>Edison Chinese Cylinders</h2>

  <figure>
    <img src="/images/edisonchinese.jpg" title="Edison Chinese Cylinder" alt="An old valet carries a letter / Performer unknown. Edison Records in Chinese: 12769. 1902." /><figcaption>An old valet carries a letter / Performer unknown. <a href="search.php?nq=1&query_type=call_number&query=6174">Edison Records in Chinese: 12769</a>. 1902.</figcaption>
  </figure>

  <p>
    In 1902, Edison sent recordist Walter Miller to San Francisco
    to record 46 cylinders of Chinese content for sale  to Chinese speakers. It is not known if they were also sold in  China as well as to the Chinese community in the United States.
    The records stayed in the catalog for a number of years and included 
    <a href="search.php?nq=1&query=songs%2C+Chinese&query_type=keyword">songs</a>,
    <a href="search.php?nq=1&query_type=call_number&query=6223">Chinese opera,</a> and, naturally, comic recitations and band music. Many of the pieces were too long to fit on one cylinder and were spread over multiple cylinders, which was an unusual practice for the time period.
  </p>

  <figure>
    <img src="/images/chinesecatalog.jpg" alt="Chinese selections in the 1905 Edison catalog of British, European, Canadian, Mexican, and Asiatic records. (Courtesy of Wang Gang, Zhengzhou, PRC.)" /><figcaption>Chinese selections in the 1905 Edison catalog of British, European, Canadian, Mexican, and Asiatic records. (Courtesy of Wang Gang, Zhengzhou, PRC.)</figcaption>
  </figure>  <div class="pagination">
  	<span class="previous">Previous: <a href="history-edisonforeign.php" title="Previous">Edison Foreign Releases—Cylindres Edison Moul&eacute;s Sur Or and Edison Goldguss Walze</a></span>
    <i class="small-screen fa fa-ellipsis-h"></i>
  	<span class="next">Next: <a href="history-columbia.php" title="Next">Columbia Cylinders</a></span>
  </div>
</div><!-- end .content -->
<!-- End of file: history-chinese.tpl -->
{include file="footer.tpl"}
