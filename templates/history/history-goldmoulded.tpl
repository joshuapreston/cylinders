{include file="header.tpl"}
{include file="navigation.tpl"}
<!-- File: history-goldmoulded.tpl -->
<div class="content text">
  <h2>Edison Gold-Moulded Cylinders (1902&ndash;1912)</h2>

  <figure>
    <img src="/images/edisongoldmoulded.jpg" title="Edison Gold Moulded Record" alt="The Man Behind / Collins and Harlan. Edison Gold-Moulded: 8650. 1904." /><figcaption>The Man Behind / Collins and Harlan. <a href="search.php?nq=1&query_type=call_number&query=2733">Edison Gold-Moulded: 8650</a>. 1904.</figcaption>
  </figure>

  <p>
    By the end of the 19th century, as the limitations of brown wax cylinders
    became increasingly evident, Edison returned to the laboratory in an attempt
    to make refinements to his pioneering developments in sound recording.
    At that time, the standard procedure for recording brown wax cylinders
    was mass repetition. A performer would typically sing or play in front
    of a small number of horns hour after hour, with each performance yielding
    a small batch of cylinders. Subsequent developments in the brown wax duplication
    process made churning out these early cylinders easier: the acoustic dubbing
    method and its technologically advanced successor, the pantographic method,
    mitigated the necessity of the all-day performances required by those
    instrumentalists or singers whose tunes were in high demand. Still, the
    means of recording remained primitive and the quality from cylinder to
    cylinder was often inconsistent, with the results dependent on the number
    of duplication sessions involving the master cylinder, which degraded
    with use, and also on the recording decisions made by the engineer for
    any given performance.
  </p>
  <p>
    The &quot;Gold-Moulded&quot; process, developed in 1902, significantly
    ameliorated these limitations. The process involved creating a metal mold
    from a wax master; a brown wax blank could then be put inside the resulting
    mold and subjected to a preestablished and precisely calibrated level
    of heat. As the blank expanded, the grooves would be pressed into the
    blank, and after cooling, the newly molded cylinder could be removed
    from the mold. The "gold" from its namesake is derived from the trace
    levels of the metal that were applied as a conductive agent in creating
    the initial mold from the wax master.
  </p>
  <p>
    With Edison Gold-Moulded cylinders, playback speed was standardized at
    160 revolutions per minute (RPM). The number of grooves on gold-molded
    cylinders remained the same as for brown wax cylinders, at 100 TPI, or
    threads per inch. (At 160 RPM, a cylinder of 100 TPI will play for
    about 1.5 to 2.5 minutes&mdash;hence the &quot;two-minute&quot; label commonly
    given to Edison Gold-Moulded recordings.) Thanks to improved efficiency
    in the duplication of cylinders as well as the burgeoning commercial success
    of the industry overall, Edison's cylinder recordings began selling in
    greater numbers from 1902 on.
  </p>
  <p>
    The popular songs, instrumental pieces, and vaudeville routines of the
    day formed the bulk of the Edison Gold-Moulded series. Between 1902
    and 1912, when these cylinders were discontinued, thousands of titles
    were recorded and issued. As a mature technology, with remarkable fidelity
    compared to the discs of the day, the cylinders preserve an entire era
    of popular entertainment. 
    Singers of popular songs like <a href="search.php?nq=1&query=billy+murray&query_type=keyword">Billy Murray</a>
    , instrumentalists like banjo player <a href="search.php?nq=1&query_type=keyword+&query=vess+ossman">Vess Ossman</a>
    , and such bands as the <a href="search.php?nq=1&query=New+York+Military+Band&query_type=author">New York Military Band</a>
     recorded hundreds of popular cylinders for Edison and other companies. 
     Other performers&mdash;<a href="search.php?nq=1&query=Len+Spencer&query_type=author">Len Spencer</a> 
     and <a href="search.php?nq=1&query=Arthur+Collins&query_type=author">Arthur Collins</a>
     , for example&mdash;presented vaudeville routines and ethnic comedy that were staples 
     of that time but have been largely forgotten today.  </p>
  <div class="pagination">
  	<span class="previous">Previous: <a href="history-concerts.php" title="Previous">Brown Wax Concert Cylinders</a></span>
    <i class="small-screen fa fa-ellipsis-h"></i>
  	<span class="next">Next: <a href="history-amberol.php" title="Next">Edison Amberol Cylinders</a></span>
  </div>
</div><!-- end .content -->
<!-- End of file: history-goldmoulded.tpl -->
{include file="footer.tpl"}
