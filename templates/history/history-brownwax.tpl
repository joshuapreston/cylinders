{include file="header.tpl"}
{include file="navigation.tpl"}
<!-- File: history-brownwax.tpl -->
<div class="content text">
  <h2>Brown Wax Cylinders (1895&ndash;1901)</h2>

  <figure>
    <img src="/images/brownwax.jpg" title="Edison Brown Wax Cylinder" alt="Three minutes with the minstrels / Arthur Collins, S. H. Dudley and Ancient City. Edison Record: 4705." /><figcaption>Three minutes with the minstrels / Arthur Collins, S. H. Dudley and Ancient City. <a href="search.php?nq=1&query_type=call_number&query=5222">Edison Record: 4705</a>. 1899.</figcaption>
  </figure>

  <p>
    Brown wax cylinders were the first sound recordings produced
    on a widespread commercial scale. Physically, these recordings are closest
    to the commonly understood idea of &quot;wax&quot;&mdash;that is, something
    waxy in a tactile sense, like a candle. In fact, the earliest cylinders,
    the so-called "white wax" cylinders, were originally derived from a blend
    of plant and animal waxes. The variability of the wax quality from these
    sources, however, caused Edison to abandon them in favor of a standardized
    metallic soap composite, which created a cylinder that felt waxy, even
    if it wasn't technically a wax.
  </p>
  <p>
    During their heyday at the turn of the 20th century, brown
    wax cylinders were sold with an accompanying slip of paper to identify
    the recording. Since only a release number was inscribed on the circumference
    of most cylinders, the paper was the sole visual evidence of the contents
    of the recording. Most of these original slips have been lost, so determining
    exactly what is on a given brown wax cylinder can be challenging. Fortunately,
    virtually all of the commercial brown wax cylinders contained spoken introductions
    identifying at least the song title and performer. Factors inhibiting
    identification of these cylinders include their unfortunate tendency to
    develop mold growth and the variable rate at which these early cylinders
    were recorded&mdash;ranging generally between 120 and 160 RPM.  </p>
  <p>
    Besides the commercially released brown wax cylinders, the
    UCSB collection also contains a small series of home recordings. These
    cylinders were made from wax "blanks," which Edison claimed could be reused
    up to 100 times by literally shaving off the old grooves. In this
    way, brown wax blanks could perhaps be considered an early rewritable
    medium, akin to a CD-RW today.&nbsp;For these home recordings, discerning
    identification information as well as playback speed is often impossible.
    Yet hearing them can be a fascinating, even otherworldly experience: a
    crying baby who cannot be pacified, say, or a drunken caterwauler flailing
    through a song, their identities forever lost to time.
  </p>
  <div class="pagination">
  	<span class="previous">Previous: <a href="history-lioret.php" title="Previous">Lioret Cylinders</a></span>
    <i class="small-screen fa fa-ellipsis-h"></i>
  	<span class="next">Next: <a href="history-concerts.php" title="Next">Brown Wax Concert Cylinders</a></span>
  </div>
</div><!-- end .content -->
<!-- End of file: history-brownwax.tpl -->
{include file="footer.tpl"}
