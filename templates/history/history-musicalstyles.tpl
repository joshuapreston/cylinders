{include file="header.tpl"}
{include file="navigation.tpl"}
<!-- File: history-musicalstyles.tpl -->
<div class="content text">  
    <h2 >Musical Styles of the Cylinder Era</h2>
    <p>
        The music of the cylinder era spanned a wide variety of genres. Comedy 
        songs, hymns, marches, and opera were all popular styles of the time and 
        there wasn't the distinction between high art and low art that there is 
        today. Small ensembles of musicians or solo acts were best suited for 
        the phonograph because of its frequency response and limited dynamic range 
        and the need for all musicians to be in close proximity to the recording 
        horn. Brass instruments, banjos, and xylophones were commonly recorded because
        their sound was more easily captured than the sound of a guitar, piano 
        or violin. Drums were usually avoided because if played loudly they would 
        cause the record to skip on playback. Military bands recorded well, and
        recordings of bands playing everything from opera to popular songs to 
        marches had wide appeal. Vaudeville was also a popular style of live entertainment
        around the turn of the century, with vaudeville acts usually consisting of a
        variety show of song and dance. The &quot;coon&quot; song was a popular
        form that made fun of black stereotypes, and such songs were often performed by
        white performers who would paint their faces black. The short length of a typical
        vaudeville act was especially well suited for the two- or four-minute cylinder.
    </p>
</div><!-- end .content -->
<!-- End of file: history-brownwax.tpl -->    
{include file="footer.tpl"}