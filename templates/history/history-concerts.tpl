{include file="header.tpl"}
{include file="navigation.tpl"}
<!-- File: history-concerts.tpl -->
<div class="content text">
  <h2>Concert Cylinders of the Brown Wax Era</h2>

  <figure>
    <img src="/images/edisonconcert.jpg" title="Edison Concert Cylinder" alt="Beau ideal / Edison Concert Band. Edison Concert Record: 8. 1898 or 1899." /><figcaption>Beau ideal / Edison Concert Band. <a href="search.php?nq=1&query_type=call_number&query=5328">Edison Concert Record: 8</a>. 1898 or 1899.</figcaption>
  </figure>

  <p>
    Concert cylinders were large diameter brown-wax records sold by  Columbia,
     Edison  and others beginning in late 1898. Columbia called them Graphophone
    Grand records, while Edison named them simply Concert Records. Concert
    cylinders were five inches in diameter&mdash;more than twice the size of standard
    cylinders&mdash;and could deliver a louder recording, suitable for public performance.
    Five-inch cylinders were first recorded at 120 rpm, then 144 rpm in late 1899, and finally 160 rpm in early 1902.  </p>
  <p>
    Early concert cylinder recordings also delivered better sound quality
    than standard cylinders, and the price of these concerts was correspondingly
    high&mdash;$4 to $5, compared with 35 to 50 cents for a standard cylinder.
    In addition, they were very large, took up a lot of space, and were infamously
    fragile. As a result, they did not sell well, and relatively few survive
    today. Nearly 173,000 Edison concert cylinders were sold in 1901-02 compared with nearly 2 million Edison two-minute wax cylinders.
  </p>
  <p>
    Few early concert cylinders were original recordings. Most early  concert cylinders
    were pantographic copies of molded five-inch master  recordings. The pantographic process
    produced commercial copies with  excellent audio response. Audio quality continued to improve
    when two-minute Gold Moulded cylinders began to be used as pantograph masters in  1901.
    Continued improvements in two-minute wax recordings were reflected  in concert cylinders as the decade went on.
  </p>
  <p>
    As concert sales continued, prices were halved and halved again, until  by 1904
    the cylinders were selling for 75 cents each.. Although standard cylinders
    were by then being made by a molding process using black wax, concert
    cylinders were never molded (except by Lambert Records, which manufactured
    an &quot;indestructible&quot; celluloid five-inch record).
  </p>
  <p>
    Ever faithful to his customers, Edison produced Concert cylinders until late 1911. Edison discontinued
    producing wax entertainment cylinders altogether in January 1914.However, the Edison Co. manufactured
    brown wax blank cylinders for office dictation use well into the 1950s.
  </p>
  <p>
    There are about hundreds of <a href="search.php?nq=1&query_type=keyword+&query=graphophone+grand">concert cylinder</a> recordings on the cylinder site, but with electrical reproduction
    it is almost impossible to tell that a recording is a concert cylinder.  </p>
  <div class="pagination">
  	<span class="previous">Previous: <a href="history-brownwax.php" title="Previous">Brown Wax Cylinder Recordings</a></span>
    <i class="small-screen fa fa-ellipsis-h"></i>
  	<span class="next">Next: <a href="history-goldmoulded.php" title="Next">Edison Gold-Moulded Cylinders</a></span>
  </div>
</div><!-- end .content -->
<!-- End of file: history-concerts.tpl -->
{include file="footer.tpl"}
