{include file="header.tpl"}
{include file="navigation.tpl"}
<!-- File: history-blueamberol.tpl -->
<div class="content text">
  <h2>Edison Blue Amberol Cylinders  (1912&ndash;1929)</h2>

  <figure>
    <img src="/images/blueamberol.jpg" title="Edison Blue Amberol" alt="The Right of the People to Rule / Teddy Roosevelt. Edison Blue Amberol: 3707. 1919." /><figcaption>The Right of the People to Rule / Teddy Roosevelt. <a href="search.php?nq=1&query_type=call_number&query=2683">Edison Blue Amberol: 3707</a>. 1919.</figcaption>
  </figure>

  <p>
    The rapid rate of technological development in the cylinder
    era culminated in a shift to a new cylinder medium—celluloid. Albany
    had begun selling celluloid-based Indestructible cylinders as early
    as 1907 and the Lambert Co. in Chicago had sold them as early as 1901.
    Nonetheless, for Edison, the switch was more complete and long-lasting
    than it was for his rival companies: the Blue Amberol, introduced in
    1912, would be the last incarnation of the cylinder line for the Edison
    Company. Like the Amberols, the Edison Blue Amberols had a playing
    time of around four minutes (200 TPI). When they were first introduced,
    durability was seen as the chief virtue of celluloid media; a misstep
    with a celluloid cylinder, unlike the fragile wax recordings, wouldn't
    cause it to shatter. While resistance to breakage was rightly considered
    to be progress in 1912, today it is viewed as the celluloid cylinder's
    only redeeming quality. Shrinkage and deformation over time have rendered
    the cylinders difficult to play, and their sound today often is worse
    than that of the earlier wax cylinders.
  </p>
  <p>
    Nevertheless, Blue Amberol cylinders fought the emerging
    dominance of the disc format, introduced by Emile Berliner with the
    Gramophone in 1888. By 1912, Edison's fiercest competitors were the
    Victor Talking Machine Company and the Columbia Phonograph Co. (which
    had stopped making cylinders in 1909). By 1915, following years of resistance,
    the Edison Company began manufacturing its own disc format, the "Diamond
    Disc." After July 1914, most Edison Blue Amberols were dubbed from disc
    masters, and selections from then on were usually issued in both disc
    and cylinder format. The decline of cylinders was long and slow, and
    they continued to be produced until Edison left the record business
    entirely in October 1929 (several days before the stock market crash),
    ending a remarkable run in the development of sound recording. Very
    late Blue Amberols, which by this time were being produced in tiny quantities,
    were actually dubbed from electrically recorded discs and sound like
    the more familiar electrical recordings of the 1920s. An electrically
    recorded cylinder by the <a href="search.php?nq=1&query_type=call_number&query=1404">Waikiki Hawaiian Orchestra</a> is an example.
  </p>
  <p>
    The main domestic series of Blue Amberol Records, running
    from number 1500 to 5719, were issued between 1912 and 1929 and featured
    everything from popular music and band selections to light classics.
    There were other series for concert and operatic music, as well as series for ethnic and foreign markets. These series include  the <a href="search.php?nq=1&query_type=keyword&query=blue+amberol+mexican">22000 Mexican series</a>, the <a href="search.php?query=concert%20blue%20amberol%20&query_type=keyword&sortBy=cnum&sortOrder=ia">28000 Concert series</a>, the <a href="search.php?query=british+blue+amberol&query_type=keyword">23000 British series</a>, and the <a href="search.php?nq=1&query_type=keyword+&query=royal+purple">Royal Purple Amberol series</a>, pictured below.
  </p>

  <figure>
    <img src="/images/edisonroyalpurple.jpg" title="Edison Royal Purple Amberol" alt="Träumerei [Kinderscenen. Träumerei] / Schumann. Albert Spalding. Edison Royal Purple Amberol Record: 29050. 1920. (Dubbed from Diamond Disc matrix 5749.)" /><figcaption>Träumerei [Kinderscenen. Träumerei] / Schumann. Albert Spalding. <a href="/search.php?nq=1&query_type=call_number&query=6480&quot;">Edison Royal Purple Amberol Record: 29050</a>. 1920. (Dubbed from Diamond Disc matrix 5749.)</figcaption>
  </figure>

  <p>
    Essentially celluloid Blue Amberols of a different hue,
    these cylinders were dyed purple and were marketed as a series of higher-end
    recordings,  featuring operatic and classical selections.
  </p>
  <p>
    Duane Deakins, an early discographer of cylinder records, lists the following numerical
    series of Blue Amberol records issued by Edison:
  </p>

  <table class="stack">
    <tr>
      <td>Popular, Domestic</td>
      <td>1501-5719</td>
    </tr>
    <tr>
      <td>Popular, Domestic (Specials) </td>
      <td>A-K</td>
    </tr>
    <tr>
      <td>Norwegian</td>
      <td>9225-9251</td>
    </tr>
    <tr>
      <td>Swedish and Danish</td>
      <td>9425-9462</td>
    </tr>
    <tr>
      <td>Holland-Dutch</td>
      <td>9650-9669</td>
    </tr>
    <tr>
      <td>Bohemian</td>
      <td>9850-9867</td>
    </tr>
    <tr>
      <td>Hebrew</td>
      <td>10050-10066</td>
    </tr>
    <tr>
      <td>Polish</td>
      <td>10750-10760</td>
    </tr>
    <tr>
      <td>Hungarian</td>
      <td>11025-11029</td>
    </tr>
    <tr>
      <td>Russian</td>
      <td>11225-11231</td>
    </tr>
    <tr>
      <td>Finnish</td>
      <td>11700-11712</td>
    </tr>
    <tr>
      <td><a href="search.php?nq=1&query=blue+amberol+greek&query_type=keyword">Greek</a></td>
      <td>11800-11805</td>
    </tr>
    <tr>
      <td><a href="search.php?nq=1&query=blue+amberol+welsh&query_type=keyword">Welsh</a></td>
      <td>14201-14212</td>
    </tr>
    <tr>
      <td>Mexican, Spanish, Cuban, Portuguese, and    Argentine</td>
      <td>
        <p>
          22001-22045<br>
          22479-22510<br>
          22545-22552
        </p>
      </td>
    </tr>
    <tr>
      <td>Italian</td>
      <td>22046-22478</td>
    </tr>
    <tr>
      <td>Puerto Rican</td>
      <td>22511-22534</td>
    </tr>
    <tr>
      <td>Hawaiian</td>
      <td>22535-22539</td>
    </tr>
    <tr>
      <td>Australian</td>
      <td>22540-22544</td>
    </tr>
    <tr>
      <td>British</td>
      <td>23001-23403</td>
    </tr>
    <tr>
      <td>German</td>
      <td>26050-26196</td>
    </tr>
    <tr>
      <td>French</td>
      <td>27001-27195</td>
    </tr>
    <tr>
      <td>Concert and Grand Opera</td>
      <td>28101-28290</td>
    </tr>
    <tr>
      <td>Special Grand Opera</td>
      <td>29001-29006</td>
    </tr>
    <tr>
      <td>Royal Purple</td>
      <td>29007-29080</td>
    </tr>
  </table>

  <div class="pagination">
  	<span class="previous">Previous: <a href="history-amberol.php" title="Previous">Edison Amberol Cylinders</a></span>
    <i class="small-screen fa fa-ellipsis-h"></i>
  	<span class="next">Next: <a href="history-edisonforeign.php" title="Next">Edison Foreign Cylinders—Cylindres Edison Moul&eacute;s Sur Or and Edison Goldguss Walze</a></span>
  </div>

<!-- End of file: history-blueamberol.tpl -->
</div><!-- end .content -->

{include file="footer.tpl"}
