{include file="header.tpl"}
{include file="navigation.tpl"}
<!-- File: history-edisonbell.tpl -->
<div class="content text">
 	<h2>Edison Bell Cylinders</h2>

  <figure>
    <img src="/images/edisonbell.jpg" title="Edison Bell Cylinder" alt="What does it matter to me? / Whit Cunliffe. Edison Bell: 6901. 1906." /><figcaption>What does it matter to me? / Whit Cunliffe. <a href="search.php?nq=1&query_type=call_number&query=0063">Edison Bell: 6901</a>. 1906.</figcaption>
  </figure>

  <p>
  	The largest cylinder manufacturer in  England was
  	<a href="search.php?nq=1&query=edison+bell+truesound&query_type=keyword">Edison Bell</a>, which produced
	several thousand titles in the first two decades of the 20th century. From the early 1890s until 1903,
  	the cylinder record  industry in England had been ruled by the Edison Bell Consolidated Phonograph  Company,
  	as their patents  gave them control over  virtually every aspect of manufacturing and selling  of
  	cylinder records and phonographic &ldquo;talking machines&rdquo; in England. Upon the expiration of their patents,
  	the industry was opened to competition.
  </p>  <div class="pagination">
  	<span class="previous">Previous: <a href="history-pathe.php" title="Previous">Path&eacute; Cylinders</a></span>
    <i class="small-screen fa fa-ellipsis-h"></i>
  	<span class="next">Next: <a href="history-sterling.php" title="Next">Sterling Cylinders</a></span>
  </div>
</div><!-- end .content -->
<!-- End of file: history-edisonbell.tpl -->
{include file="footer.tpl"}
