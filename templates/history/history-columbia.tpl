{include file="header.tpl"}
{include file="navigation.tpl"}
<!-- File: history-columbia.tpl -->
<div class="content text">
  <h2>Columbia Phonograph Co. Cylinders<br></h2>

  <figure>
    <img src="/images/columbia.jpg" title="Columbia Cylinder" alt="Take me back to New York town / Billy Murray. Columbia Phonograph Co: 33196. 1907." /><figcaption>Take me back to New York town / Billy Murray. 
    <a href="search.php?nq=1&query_type=call_number&query=4848">Columbia Phonograph Co: 33196</a>. 1907.</figcaption>
  </figure>

  <p>
    The history of the Columbia Phonograph Company can be traced to 1890,
    when Columbia began releasing a series of brown wax recordings consisting
    mainly of whistling solos and band marches. Thomas Edison, who would shortly
    begin selling commercial brown wax recordings through the North American
    Phonograph Company, seems to have been beaten to the punch. From what
    can be gleaned from the company&#146;s cylinder recording catalogs, between
    1890 and 1899 all releases were sold to the public in the context of a
    block system. In other words, band pieces blared by Sousa or banjo tunes
    strummed by Vess L. Ossman would be segregated, with each artist having
    a series of cylinder releases. (Dan W. Quinn was an extremely prolific
    performer, recording more than 300 cylinders for the company.) In each
    successive catalog, Columbia culled the less popular titles and replaced
    them in the given block with a series of new releases&mdash;thereby creating
    discographical headaches for modern researchers. By 1899, the tangle of
    numbers was partially mitigated by Columbia&#146;s introduction of a consecutive
    numbering system, starting at number 31300. Thus Hebrew vaudeville would
    be lumped with barbershop quartets, or trumpet solos with &quot;coon&quot;
    songs, for instance.
  </p>
  <p>
    By 1902, the gold-moulding process that had begun to revolutionize the
    production of cylinders (see <a href="history-goldmoulded.php">Edison Gold-Moulded cylinders</a>) was adopted by Columbia. In fact, it is unclear
    which company was the first to employ the practice of creating a metal
    master and using it to spin off a much greater number of duplicate cylinders
    than was possible previously. In any case, Columbia&#146;s switch to the
    gold-moulded process, unlike Edison's, was not unequivocal. For some years
    after 1902, both brown wax and &#147;black wax&#148; (moulded) cylinders
    were released simultaneously, the latter far outliving their brown wax
    counterparts. The 31300 consecutive numbering series therefore encompasses
    both brown and black wax cylinders from 1899 on; these are cumulatively
    known as the 30000 XP series.
  </p>
  <p>
    The repertoire of Columbia cylinders is similar to that of Edison cylinders,
    and many artists recorded for both companies; the &quot;exclusive contract&quot;
    concept clearly was not yet the standard that it is in today's music business.
    Columbia continued to issue cylinders until 1909, when it stopped releasing
    new titles and turned to the more successful disc format. The company
    did continue to distribute cylinders, including Indestructible records,
    for other companies, but no longer recorded any new pieces.
  </p>

  <figure>
    <img src="/images/columbia20thcentury.jpg" title="Columbia Twentieth Century Cylinder" alt="Lanciers from Miss Dolly dollars / Figure 5 Orchestra. Twentieth Century Talking Machine Record: 85051. Between 1905 and 1908" /><figcaption>Lanciers from Miss Dolly dollars / Figure 5 Orchestra. <a href="search.php?nq=1&query_type=call_number&query=6013">Twentieth Century Talking Machine Record: 85051</a>. Between 1905 and 1908</figcaption>
  </figure>

  <p>
    Starting in 1905 the Columbia Phonograph Co. released a series of six-inch long
    <a href="search.php?nq=1&query=Twentieth+Century&query_type=keyword">Twentieth Century Talking Machine Record</a>
    cylinders that played for three minutes rather than the usual two minutes. The series was short-lived and
    appromimately  189 titles were issued. These cylinders are uncommon today.
  </p>
  <div class="pagination">
  	<span class="previous">Previous: <a href="history-chinese.php" title="Previous">Edison Chinese Cylinders</a></span>
    <i class="small-screen fa fa-ellipsis-h"></i>
  	<span class="next">Next: <a href="history-busybee.php" title="Next">Busy-Bee Cylinders</a></span>
  </div>
</div><!-- end .content -->
<!-- End of file: history-columbia.tpl -->
{include file="footer.tpl"}
