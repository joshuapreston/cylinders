{include file="header.tpl"}
{include file="navigation.tpl"}
<!-- File: detail.tpl -->
<div class="content">

  {include file="detail_pagination.tpl"}

  <div class="record-detail">

    <div class="detail-container">

      <h1 id="uniform_title245bpn240ak130a">
        {$record.titles.245a}.
        {$record.titles.245b}
        {$record.titles.245p}
        {$record.titles.245n}

{if $record.uniform_titles.display_brackets}[{/if}{$record.uniform_titles.imploded}{if $record.uniform_titles.display_brackets}]{/if}
      </h1>

      {if $record.performer neq ""}
        <div class="pure-g detail-row">
          <div class="pure-u-1-3"><div class=" data-label">Performers: </div></div>
          <div class="pure-u-2-3"><div class="data-entry">{$record.performer}</div></div>
        </div>
      {/if}

      {if $record.issue_numbers neq ""}
        <div class="pure-g detail-row">
          <div class="pure-u-1-3"><div class="data-label">Issue Number: </div></div>
          <div class="pure-u-2-3">
            {foreach $record.issue_numbers as $issue_number}
            <div class="data-entry">{$issue_number}</div>
            {/foreach}
          </div>
        </div>
        {/if}

      {if $record.release_years neq ""}
        <div class="pure-g detail-row">
          <div class="pure-u-1-3"><div class="data-label">Release year: </div></div>
          <div class="pure-u-2-3">
            {foreach $record.release_years as $release_year}
            <div class="data-entry"><a href="/search.php?nq=1&query_type=year&query={$release_year}">{$release_year}</a></div>
            {/foreach}
          </div>
        </div>
      {/if}


      {if not $record.is_digitized}
      {* the above condition checks for FALSE as the value of is_digitized *}
        <div class="pure-g detail-row">
          <div class="pure-u-1-3"><div class="data-label">Audio:</div></div>
          <div class="pure-u-2-3"><div class="data-entry">This cylinder has not been digitized for online access. Learn more about our <a href="/adopt.php" title="Cylinder adoption program">“adopt-a-cylinder” program</a> and to adopt this cylinder, use our <a id="adopt-btn" href="https://giving.ucsb.edu/Funds/Give?id=233" class="pure-button button-adopt">credit card payment option</a> or <a href="/adopt.php">donate by check</a>.
          </div></div>
        </div>
      {else}
        {foreach $record['recordings'] as $key => $recording}
          <div class="detail-audio audio-wrapper">
            <h3>Cylinder {$recording.number}</h3>
            <audio controls src="{$smarty.const.DOWNLOADS_BASE_URL}/{$recording.subdirectory}/{$recording.number}/cusb-cyl{$recording.number}d.mp3" type="audio/m4a"></audio>
            <div class="playback-controls">
              <button type="button" class="playButton">
                <i class="fa fa-play-circle fa-lg"></i><span class="hidden">Play</span></button>
              <div class="currentTime">0:00</div>
              <div class="bar"><div class="barProgress"></div></div>
              <div class="durationDisplay">0:00</div>
            </div>
            <!-- volume controller doesn't work on mobile -->
            <div class="volume-controls large-screen">
              <button type="button" class="muteButton">
                <i class="fa fa-volume-up fa-lg"></i><span class="hidden">Mute</span></button>
              <input class="volumeSlider" type="range" min="0" max="100" value="100" step="1">
            </div>
            <div class="download">
              <a href="{$smarty.const.DOWNLOADS_BASE_URL}/{$recording.subdirectory}/{$recording.number}/cusb-cyl{$recording.number}d.mp3">
                <i class="fa fa-cloud-download fa-lg"></i>Download</a></div>
            <div class="share">Share:
            <ul>
            <li><a href="https://twitter.com/intent/tweet?text=Listen%20to%20“{$title}”%20on%20the%20%23UCSB%20Cylinder%20Audio%20Archive%0A&url=https://www.library.ucsb.edu/OBJID/Cylinder{$recording.number}" title="Tweet about it!">
              <i class="fa fa-twitter-square fa-lg"></i><span class="large-screen">Twitter</span></a></li>
            <li><a href="https://facebook.com/sharer.php?u=https://www.library.ucsb.edu/OBJID/Cylinder{$recording.number}" title="Post to Facebook">
              <i class="fa fa-facebook-square fa-lg"></i><span class="large-screen">Facebook</span></a></li>
            </ul>
          </div>
        </div>
        {/foreach}

      {/if}
      {*else*}
      {*  <p>Audio not available.</p> *}
      {*/else*}

      <div class="detail-container">

        {if $record.alternate_title neq ""}
          <div class="pure-g detail-row">
            <div class="pure-u-1-3"><div class="data-label">Alternate Title: </div></div>
            <div class="pure-u-2-3"><div class="data-entry">{$record.alternate_title}</div></div>
          </div>
        {/if}

        {if $record.other_titles neq ""}
          <div class="pure-g detail-row">
            <div class="pure-u-1-3"><div class="data-label">Other Title: </div></div>
            <div class="pure-u-2-3">
              {foreach $record.other_titles as $other_title}
                <div class="data-entry">{$other_title}</div>
              {/foreach}
              <div class="data-entry">
                  {$record.other_title2} {$record.other_title3} {$record.other_title4} {$record.other_title5} {$record.other_title6} {$record.other_title7} {$record.other_title8}
                </div>
            </div>
          </div>
        {/if}

        {if isset($record.notes)}
          <div class="pure-g detail-row">
            <div class="pure-u-1-3"><div class="data-label">Notes: </div></div>
            <div class="pure-u-2-3">
              {foreach $record.notes as $note}
                <div class="data-entry">{$note}</div>
              {/foreach}
            </div>
          </div>
        {/if}

        {if ($record.language neq '')}
          <div class="pure-g detail-row">
            <div class="pure-u-1-3"><div class="data-label">Language: </div></div>
            <div class="pure-u-2-3"><div class="data-entry">{$record.language}</div></div>
          </div>
        {/if}

        {if isset($record.collections)}
          <div class="pure-g detail-row">
            <div class="pure-u-1-3"><div class="data-label">Collection: </div></div>
            <div class="pure-u-2-3">
              {foreach $record.collections as $collection }
                <div class="data-entry">{$collection}</div>
              {/foreach}
            </div>
          </div>
        {/if}

        {if isset($record.personal_names)}
          <div class="pure-g detail-row">
            <div class="pure-u-1-3"><div class="data-label">Personal Name: </div></div>
            <div class="pure-u-2-3">
              {foreach $record.personal_names as $personal_name}
              <div class="data-entry">
                  <a href="{$personal_name.href}">{$personal_name.name}{if $personal_name.dates neq ''}, {$personal_name.dates}{/if}.</a>
                </div>
                  {/foreach}

            </div>
          </div>
        {/if}

        {if ($record.group_name neq "")}
          <div class="pure-g detail-row">
            <div class="pure-u-1-3"><div class=" data-label">Group Name: </div></div>
            <div class="pure-u-2-3">
              <div class="data-entry"><a href="/search.php?nq=1&query_type=keyword&query={$record.group_name}">{$record.group_name}</a></div>
            </div>
          </div>
        {/if}

        {if count($record.subjects) > 0}
          <div class="pure-g detail-row">
            <div class="pure-u-1-3"><div class="data-label">Subjects: </div></div>
            <div class="pure-u-2-3">
              {foreach $record.subjects as $subject}
                <div class="data-entry"><a href="/search.php?nq=1&query_type=subject&query={$subject}">{$subject}</a></div>
              {/foreach}
            </div>
          </div>
        {/if}

        {if ($record.genres neq '')}
          <div class="pure-g detail-row" id="genre655a">
            <div class="pure-u-1-3"><div class="data-label">Genre:</div></div>
            <div class="pure-u-2-3">
              {foreach $record.genres as $genre}
              <div class="data-entry"><a href="/search.php?nq=1&query_type=keyword&query={$genre}">{$genre}</a></div>
              {/foreach}
            </div>
          </div>
        {/if}

        {if $record.geographic_subject neq ''}
          <div class="pure-g detail-row">
            <div class="pure-u-1-3"><div class="data-label">Geographic Subject: </div></div>
            <div class="pure-u-2-3"><div class="data-entry">
              <a href="/search.php?nq=1&query_type=subject&query={$record.geographic_subject}{if $record.geographic_subject_v neq ''}+{$record.geographic_subject_v}{/if}">
                {$record.geographic_subject}{if $record.geographic_subject_v neq ''} - {$record.geographic_subject_v}{/if}
              </a></div></div>
          </div>
        {/if}

        {if $record.original_item_info neq ''}
          <div class="pure-g detail-row">
            <div class="pure-u-1-3"><div class="data-label">Original Item Information: </div></div>
            <div class="pure-u-2-3">
                <div class="data-entry">
{foreach from=$record.original_item_info key=k item=v name=marc999s}
{$v.j}{foreach from=$v.z key=kz item=vz name=marc999z}{if $smarty.foreach.marc999z.first}: {else}, {/if}{$vz}{if $smarty.foreach.marc999z.last}{else}{/if}{/foreach}{if $smarty.foreach.marc999s.last}{else}; {/if}{/foreach}
                </div>
            </div>
          </div>
        {/if}

        {if $record.marc_856U neq ''}
          <div class="pure-g detail-row">
            <div class="pure-u-1-3"><div class="data-label">Durable URL: </div></div>
            <div class="pure-u-2-3">
              <div class="data-entry"><a href="{$record.marc_856U[0]}">{$record.marc_856U[0]}</a></div>
            </div>
          </div>
        {elseif $record.base_cylinder_number neq ''}
          <div class="pure-g detail-row">
            <div class="pure-u-1-3"><div class="data-label">Durable URL: </div></div>
            <div class="pure-u-2-3">
              <div class="data-entry"><a href="https://www.library.ucsb.edu/OBJID/Cylinder{$record.base_cylinder_number[0]}">https://www.library.ucsb.edu/OBJID/Cylinder{$record.base_cylinder_number[0]}</a></div>
            </div>
          </div>
        {/if}

      </div>
    </div>

  </div> <!-- <div class="record-detail"> -->

  {include file="detail_pagination.tpl"}

</div> <!-- <div class="content"> -->
{include file="footer.tpl"}
<script>
// load links to next/previous cylinder in search results set
$(document).ready(function(e) {
   $('#next1').load('next-cylinder.php?query={$record.mms_id}');
   $('#next2').load('next-cylinder.php?query={$record.mms_id}');
   $('#prev1').load('previous-cylinder.php?query={$record.mms_id}');
   $('#prev2').load('previous-cylinder.php?query={$record.mms_id}');
});
</script>
<!-- End of file: detail.tpl -->
