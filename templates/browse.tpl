{include file="header.tpl"}
{include file="navigation.tpl"}
<!-- File: browse.tpl -->
<div class="content text">
<h1>Browse Collection</h1>
    <p>Links below facilitate browsing through the entire collection, or by  genre, instrument, topical subject and
      ethnic group and countries. The collection can also be explored through <a href="playlists.php">thematic playlists</a>.</p>

      <h3><a href="search.php?nq=1&query_type=keyword&query=">Browse entire collection</a></h3>
      <div class="pure-g">
        <div class="pure-u-1 pure-u-md-1-2 pure-u-lg-1-2">
        <h3>Browse by  genre</h3>
        <div class="browse-list">
            <li><a href="search.php?nq=1&query=band+music&query_type=keyword">Band music</a></li>
            <li><a href="search.php?nq=1&query=cakewalk&query_type=keyword">Cakewalks</a></li>
            <li><a href="search.php?nq=1&query=carols&query_type=keyword">Carols</a></li>
            <li><a href="search.php?nq=1&query=christmas&query_type=keyword">Christmas music and stories </a></li>
            <li><a href="search.php?nq=1&query=Humorous+songs&query_type=keyword">Comic songs</a></li>
            <li><a href="search.php?nq=1&query=Corridos&query_type=keyword">Corridos</a></li>
            <li><a href="search.php?nq=1&query_type=keyword&amp;query=countertenor">Countertenor solos</a> </li>
            <li><a href="search.php?nq=1&query=old+time&query_type=keyword">Country/Old-time music</a></li>
            <li><a href="search.php?nq=1&query=dance+orchestra+music&query_type=keyword">Dance bands</a> </li>
            <li>Ethnic humor
              <ul>
                <li><a href="search.php?nq=1&query=german+wit+and+humor&query_type=keyword">German</a></li>
                <li><a href="search.php?nq=1&query=irish+wit+and+humor&query_type=keyword">Irish</a></li>
                <li><a href="search.php?nq=1&query=italian+wit+and+humor&query_type=keyword">Italian</a></li>
                <li><a href="search.php?nq=1&query=scottish+wit+and+humor&query_type=keyword">Scottish</a></li>
                <li><a href="search.php?nq=1&query=jewish+wit+and+humor&query_type=keyword">Jewish</a></li>
              </ul>
            </li>
            <li><a href="search.php?nq=1&query=fiddle&query_type=keyword">Fiddle tunes</a></li>
            <li><a href="search.php?nq=1&query=hawaii&query_type=keyword">Hawaiian music</a></li>
            <li><a href="search.php?nq=1&query=personal+recordings&query_type=keyword">Home recordings</a></li>
            <li><a href="search.php?nq=1&query=Humorous+recitations&query_type=keyword">Humorous recitations</a></li>
            <li><a href="search.php?nq=1&query=hymns&query_type=keyword">Hymns</a></li>
            <li><a href="search.php?nq=1&query=jazz&query_type=keyword">Jazz</a></li>
            <li><a href="search.php?nq=1&query=language+instruction&query_type=keyword">Language instruction</a> 
              (
                <a href="search.php?nq=1&query=english+lesson&query_type=title">English</a>, 
                <a href="search.php?nq=1&query=french+language+Self-instruction&query_type=keyword">French</a>,
                <a href="search.php?nq=1&query=italian+language+Self-instruction&query_type=keyword">Italian</a>,
                <a href="search.php?nq=1&query=Spanish+language+Self-instruction&query_type=keyword">Spanish</a>,
                <a href="search.php?nq=1&query=german+language+Self-instruction&query_type=keyword">German</a>
                )</li>
            <li><a href="search.php?nq=1&query=marches&query_type=keyword">Marches</a></li>
            <li><a href="search.php?nq=1&query=minstrel+music&query_type=keyword">Minstrel music</a></li>
            <li><a href="search.php?nq=1&query=musicals&query_type=keyword">Musical theater</a></li>
            <li><a href="search.php?nq=1&query=national+songs&query_type=keyword">National songs/anthems</a></li>
            <li><a href="search.php?nq=1&query=operas&query_type=keyword">Operas</a></li>
            <li><a href="search.php?nq=1&query=orchestral&query_type=keyword">Orchestral music</a></li>
            <li><a href="search.php?nq=1&query=patriotic&query_type=keyword">Patriotic music</a></li>
            <li><a href="search.php?nq=1&query=polkas&query_type=keyword">Polkas</a></li>
            <li>Popular music: 
                <a href="search.php?nq=1&query=popular+music+to+1901&query_type=keyword">to 1901</a>,
                <a href="search.php?nq=1&query=popular+music+1901-1910&query_type=keyword">1901-1910</a>,
                <a href="search.php?nq=1&query=popular+music+1911-1920&query_type=keyword">1911-1920</a>,
                <a href="search.php?nq=1&query=popular+music+1921-1930&query_type=keyword">1921-1930</a></li>
            <li><a href="search.php?nq=1&query=ragtime&query_type=keyword">Ragtime and rag songs</a></li>
            <li><a href="search.php?nq=1&query=sacred&query_type=keyword">Sacred music</a></li>
            <li><a href="search.php?nq=1&query=sermons&query_type=keyword">Sermons</a></li>
            <li><a href="search.php?nq=1&query=speeches&query_type=keyword">Speeches</a></li>
            <li><a href="search.php?nq=1&query=spirituals&query_type=keyword">Spirituals</a></li>
            <li><a href="search.php?nq=1&query=string+quartets&query_type=keyword">String Quartets</a></li>
            <li><a href="search.php?nq=1&query_type=keyword&amp;query=Tamburitza">Tamburitza orchestra</a> </li>
            <li><a href="search.php?nq=1&query=tyroliennes&query_type=subject">Tyroliennes</a></li>
            <li><a href="search.php?nq=1&query=vaudeville&query_type=keyword">Vaudeville</a></li>
            <li><a href="search.php?nq=1&query=venetian+instrumental&query_type=keyword">&quot;Venetian&quot; instrumental groups</a></li>
            <li><a href="search.php?nq=1&query=vocal+duets&query_type=keyword">Vocal duets</a></li>
            <li><a href="search.php?nq=1&query_type=subject&amp;query=vocal+trios">Vocal trios</a> </li>
            <li><a href="search.php?nq=1&query=vocal+quartets&query_type=keyword">Vocal quartets</a></li>
            <li><a href="search.php?nq=1&query=waltzes&query_type=keyword">Waltzes</a></li>
            <li><a href="search.php?nq=1&query=whistling&query_type=keyword">Whistling</a></li>
            <li><a href="search.php?nq=1&query_type=keyword&amp;query=yodel">Yodeling</a></li>
            <li><a href="search.php?nq=1&query=zarzuelas&query_type=keyword">Zarzuelas</a></li>
        </div>
      </div>
      <div class="pure-u-1 pure-u-md-1-2 pure-u-lg-1-2">
        <h3>Browse by instruments</h3>
          <div class="browse-list">
            <li><a href="search.php?nq=1&query=accordion+&query_type=keyword">Accordion</a></li>
            <li><a href="search.php?nq=1&query=Bagpipe&query_type=keyword">Bagpipes</a></li>
            <li><a href="search.php?nq=1&query=Banjo&query_type=keyword">Banjo</a></li>
            <li><a href="search.php?nq=1&query=Bassoon&query_type=keyword">Bassoon</a></li>
            <li><a href="search.php?nq=1&query_type=keyword&amp;query=Bugle">Bugle</a></li>
            <li><a href="search.php?nq=1&query_type=keyword&amp;query=Castanets">Castanets</a></li>
            <li><a href="search.php?nq=1&query_type=keyword&amp;query=Celesta">Celesta</a></li>
            <li><a href="search.php?nq=1&query=Cello&query_type=keyword">Cello</a></li>
            <li><a href="search.php?nq=1&query=cimbalom&query_type=keyword">Cimbalom</a></li>
            <li><a href="search.php?nq=1&query=Clarinet&query_type=keyword">Clarinet</a></li>
            <li><a href="search.php?nq=1&query=concertina&query_type=keyword">Concertina</a></li>
            <li><a href="search.php?nq=1&query=Cornet&query_type=keyword">Cornet</a></li>
            <li><a href="search.php?nq=1&query_type=keyword&amp;query=Dulcimer">Dulcimer</a></li>
            <li><a href="search.php?nq=1&query=fiddle&amp;query_type=keyword">Fiddle</a></li>
            <li><a href="search.php?nq=1&query=Fife+and+drum&query_type=keyword">Fife and drum</a></li>
            <li><a href="search.php?nq=1&query=flute&amp;query_type=keyword">Flute</a></li>
            <li><a href="search.php?nq=1&query=Glockenspiel+&query_type=keyword">Glockenspiel (bells)</a></li>
            <li><a href="search.php?nq=1&query=Guitar&query_type=keyword">Guitar</a></li>
            <li><a href="search.php?nq=1&query_type=keyword&amp;query=Harmonica">Harmonica</a></li>
            <li><a href="search.php?nq=1&query_type=keyword&amp;query=Harp">Harp</a></li>
            <li><a href="search.php?nq=1&query=Hawaiian+guitar+music&query_type=keyword">Hawaiian guitars</a></li>
            <li><a href="search.php?nq=1&query=Mandolin&query_type=keyword">Mandolin</a></li>
            <li><a href="search.php?nq=1&query=Marimba&query_type=keyword">Marimba</a></li>
            <li><a href="search.php?nq=1&query=Oboe&query_type=keyword">Oboe</a></li>
            <li><a href="search.php?nq=1&query=ocarina&query_type=keyword">Ocarina</a></li>
            <li><a href="search.php?nq=1&query_type=keyword&amp;query=penny+whistle">Penny whistle</a> </li>
            <li><a href="search.php?nq=1&query=piano+music&query_type=keyword">Piano</a></li>
            <li><a href="search.php?nq=1&query=Piccolo&query_type=keyword">Piccolo</a></li>
            <li><a href="search.php?nq=1&query_type=keyword&amp;query=organ">Pipe organ</a> </li>
            <li><a href="search.php?nq=1&query=Saxophone&query_type=keyword">Saxophone</a></li>
            <li><a href="search.php?nq=1&query_type=keyword&amp;query=Tambura">Tambura</a></li>
            <li><a href="search.php?nq=1&query=Tarogato&query_type=subject">Tarogato</a></li>
            <li><a href="search.php?nq=1&query=trombone&query_type=keyword">Trombone</a></li>
            <li><a href="search.php?nq=1&query=trumpet&query_type=keyword">Trumpet</a></li>
            <li><a href="search.php?nq=1&query_type=keyword&amp;query=Tuba">Tuba</a></li>
            <li><a href="search.php?nq=1&query=Tubular+bells&query_type=keyword">Tubular bells/chimes</a></li>
            <li><a href="search.php?nq=1&query=ukulele&query_type=keyword">Ukulele</a></li>
            <li><a href="search.php?nq=1&query=Violin&query_type=keyword">Violin</a></li>
            <li><a href="search.php?nq=1&query=Xylophone&query_type=keyword">Xylophone</a></li>
            <li><a href="search.php?nq=1&query=Zither&query_type=keyword">Zither</a></li>
          </div>
        </div>
      <div class="pure-u-1 pure-u-md-1-2 pure-u-lg-1-2">
        <h3>Browse by topical subject</h3>
          <div class="browse-list">
            <li><a href="search.php?nq=1&query=baseball&query_type=keyword">Baseball</a></li>
            <li><a href="search.php?nq=1&query=wreck&query_type=keyword">Disasters</a></li>
            <li><a href="search.php?nq=1&query=prohibition&query_type=keyword">Prohibition</a></li>
            <li><a href="search.php?nq=1&query=spanish+american+war&query_type=keyword">Spanish-American War</a></li>
            <li><a href="search.php?nq=1&query=Transportation&query_type=keyword">Transportation</a></li>
            <li><a href="search.php?nq=1&query=civil+war&query_type=keyword">U.S. Civil War</a></li>
            <li>U.S. States (
              <a href="search.php?nq=1&query=alabama&query_type=keyword">Alabama</a>,
              <a href="search.php?nq=1&query=arkansas&query_type=keyword">Arkansas</a>,
              <a href="search.php?nq=1&query=california&query_type=keyword">California</a>,
              <a href="search.php?nq=1&query=colorado&query_type=keyword">Colorado</a>,
              <a href="search.php?nq=1&query=florida&query_type=keyword">Florida</a>,
              <a href="search.php?nq=1&query=georgia&query_type=keyword">Georgia</a>,
              <a href="search.php?nq=1&query=hawaii+songs+music&query_type=keyword">Hawaii</a>,
              <a href="search.php?nq=1&query=idaho&query_type=keyword">Idaho</a>,
              <a href="search.php?nq=1&query=illinois&query_type=keyword">Illinois</a>,
              <a href="search.php?nq=1&query=indiana&query_type=keyword">Indiana</a>,
              <a href="search.php?nq=1&query=iowa&query_type=keyword">Iowa</a>,
              <a href="search.php?nq=1&query=kentucky&query_type=keyword">Kentucky</a>,
              <a href="search.php?nq=1&query=louisiana&query_type=keyword">Louisiana</a>,
              <a href="search.php?nq=1&query=maine&query_type=keyword">Maine</a>,
              <a href="search.php?nq=1&query=maryland&query_type=keyword">Maryland</a>,
              <a href="search.php?nq=1&query=massachusetts&query_type=keyword">Massachusetts</a>,
              <a href="search.php?nq=1&query=michigan&query_type=keyword">Michigan</a>,
              <a href="search.php?nq=1&query=mississippi&query_type=keyword">Mississippi</a>,
              <a href="search.php?nq=1&query=missouri&query_type=keyword">Missouri</a>,
              <a href="search.php?nq=1&query=new+hampshire&query_type=keyword">New Hampshire</a>,
              <a href="search.php?nq=1&query=new+york&query_type=keyword">New York</a>,
              <a href="search.php?nq=1&query=ohio&query_type=keyword">Ohio</a>,
              <a href="search.php?nq=1&query=oregon&query_type=keyword">Oregon</a>,
              <a href="search.php?nq=1&query=pennsylvania&query_type=keyword">Pennsylvania</a>,
              <a href="search.php?nq=1&query=tennessee&query_type=keyword">Tennessee</a>,
              <a href="search.php?nq=1&query=texas&query_type=keyword">Texas</a>,
              <a href="search.php?nq=1&query=utah&query_type=keyword">Utah</a>,
              <a href="search.php?nq=1&query=virginia&query_type=keyword">Virginia</a>,
              <a href="search.php?nq=1&query=wisconsin&query_type=keyword">Wisconsin</a>,
              <a href="search.php?nq=1&query=wyoming&query_type=keyword">Wyoming</a>
              )</li>
            <li><a href="search.php?nq=1&query=World+War&query_type=keyword">World War I</a></li>
          </div>

      </div>
      <div class="pure-u-1 pure-u-md-1-2 pure-u-lg-1-2">
        <h3>Browse ethnic and foreign cylinders</h3>
          <div class="browse-list">
          <li><a href="search.php?nq=1&query=argentine&query_type=keyword">Argentine</a></li>
            <li><a href="search.php?nq=1&query_type=keyword&amp;query=Austria">Austrian</a></li>
            <li><a href="search.php?nq=1&query_type=keyword&amp;query=Jugoslavian">Balkan (&quot;Jugoslavian&quot;)</a> </li>
            <li><a href="search.php?nq=1&query=bohemian&query_type=keyword">Bohemian (Czech)</a></li>
            <li><a href="search.php?nq=1&query=brazilian&query_type=keyword">Brazilian</a></li>
            <li><a href="search.php?nq=1&query=British&query_type=keyword">British</a></li>
            <li><a href="search.php?nq=1&query_type=subject&query=Music+China">Chinese</a></li>
            <li><a href="search.php?nq=1&query=cuban&query_type=keyword">Cuban</a></li>
            <li><a href="search.php?nq=1&query_type=keyword&query=Holland+series">Dutch</a></li>
            <li><a href="search.php?nq=1&query=finnish&query_type=keyword">Finnish</a></li>
            <li><a href="search.php?nq=1&query=french&query_type=keyword">French</a></li>
            <li><a href="search.php?nq=1&query=german&query_type=keyword">German</a></li>
            <li><a href="search.php?nq=1&query=music+hawaiian&query_type=keyword">Hawaiian</a></li>
            <li><a href="search.php?nq=1&query=hungarian&query_type=keyword">Hungarian</a></li>
            <li><a href="search.php?nq=1&query_type=keyword&amp;query=italian">Italian</a></li>
            <li><a href="search.php?nq=1&query_type=keyword&amp;query=Irish">Irish</a></li>
            <li><a href="search.php?nq=1&query_type=subject&query=music+japan">Japanese</a></li>
            <li><a href="search.php?nq=1&query_type=keyword&query=jewish+series">Jewish</a> (Hebrew and Yiddish)</li>
            <li><a href="search.php?nq=1&query=Mexican&query_type=keyword">Mexican</a></li>
            <li><a href="search.php?nq=1&query=arabic&query_type=keyword">Middle Eastern</a></li>
            <li><a href="search.php?nq=1&query=Norwegian&query_type=keyword">Norwegian</a></li>
            <li><a href="search.php?nq=1&query_type=keyword&amp;query=polish">Polish</a></li>
            <li><a href="search.php?nq=1&query=ukrainian&amp;query_type=keyword">Ukrainian</a></li>
            <li><a href="search.php?nq=1&query=Russian&query_type=keyword">Russian</a></li>
            <li><a href="search.php?nq=1&query=swedish&query_type=keyword">Swedish</a></li>
            <li><a href="search.php?nq=1&query_type=keyword&amp;query=welsh">Welsh</a></li>
            </div>
      </div>
      </div>
</div>
<!-- End of file: content-header.tpl -->
{include file="footer.tpl"}
