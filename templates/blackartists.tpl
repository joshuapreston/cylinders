{include file="header.tpl"}
{include file="navigation.tpl"}
<!-- File: blackartists.tpl -->
<div class="content text">
  <h1>Early Black Artists and Composers</h1>
  <p>
    <a href="thematic_playlists.xml"><img src="/images/rss-podcast.png" alt="Podcast"></a>
    &nbsp;</p>

  <p>
    The early 20th century was a period of extreme segregation and discrimination
    in America&mdash;the era of the &quot;color line.&quot; One of the few arenas
    in which African-Americans were allowed to demonstrate their talents was
    in show business, including the then-new recording industry. In this program
    we hear a selection of popular songs written and/or performed by African-Americans,
    some folk-based, some spirituals, some from vaudeville, Broadway or, the
    minstrel show, and some featuring the exciting new music called &quot;ragtime&quot;
    (which would eventually give birth to jazz). The artists range from George
    W. Johnson, the first black recording star (in the 1890s), to white vaudevillian
    Al Bernard, &quot;The Boy from Dixie,&quot; who popularized the work of
    black composers in the 1920s. Also heard are the world-famous Fisk Jubilee
    Singers in one of their earliest recordings. <em>&mdash;Tim Brooks, author of
    </em><a href="http://www.press.uillinois.edu/books/catalog/82bnq3sk9780252028502.html">Lost Sounds: Blacks and the Birth of the Recording Industry, 1890-1919</a>.<em> (University
    of Illinois Press, 2004).</em>
  </p>
  <ul>
    <li><span>Nobody: Abyssinia / Bert Williams. Bert Williams.
      <a href="search.php?nq=1&query_type=call_number&query=4821">Columbia Phonograph Co.: 33011</a>. 1906.</span></li>
    <li><span>Under the bamboo tree / Bob Cole. Arthur Collins.
      <a href="search.php?nq=1&query_type=call_number&query=4349">Edison Gold Moulded Record: 8215</a>. 1904?</span></li>
    <li><span>Humpty Dumpty rag / Charley Straight. New York Military
      Band. <a href="search.php?nq=1&query_type=call_number&query=0236">Edison Blue Amberol: 2458</a>. 1914.
      </span></li>
    <li><span>He's a cousin of mine / Chris Smith. Bob Roberts.
      <a href="search.php?nq=1&query_type=call_number&query=3141">Edison Gold Moulded Record: 9412</a>. 1906.
      </span></li>
    <li><span>Shame on you. Chris Smith / Tascott. <a href="search.php?nq=1&query_type=call_number&query=2836">Edison Gold Moulded Record: 9033</a>. 1905
      </span></li>
    <li><span>Laughing song. George W. Johnson. George W. Johnson.
      <a href="search.php?nq=1&query_type=call_number&query=4405">Columbia Phonograph Co: 7601</a>. 1904-1909.
      </span></li>
    <li><span>Strut Miss Lizzie / Henry Creamer. Al Bernard. <a href="search.php?nq=1&query_type=call_number&query=1241">Edison Blue Amberol: 4248</a>. 1921.
      </span></li>
    <li><span>In the evening by the moonlight / James Allen Bland.
      Edison Male Quartet. <a href="search.php?nq=1&query_type=call_number&query=4197">Edison Gold Moulded Record: 9069</a>. 1904.
      </span></li>
    <li><span>Roll them cotton bales / John Rosamond Johnson. American
      Quartet. <a href="search.php?nq=1&query_type=call_number&query=0477">Edison Blue Amberol: 2448</a>. 1914.
      </span></li>
    <li><span>Jerusalem mournin'. Polk Miller and his Old South
      Quartet. <a href="search.php?nq=1&query_type=call_number&query=0042">Edison Standard Record: 10334</a>. 1910.
      </span></li>
    <li><span>All over this world. Fisk Jubilee Singers. <a href="search.php?nq=1&query_type=call_number&query=1158">Edison Blue Amberol: 4045</a>. 1920.
      </span></li>
    <li><span>A Christmas basket &amp; Howdy, honey, howdy / Paul
      Laurence Dunbar. Edward Sterling Wright. <a href="search.php?nq=1&query_type=call_number&query=0311">Edison Blue Amberol: 2152</a>. 1913.
      </span></li>
    <li><span>What a time / Polk Miller. Polk Miller and his Old
      South Quartet. <a href="search.php?nq=1&query_type=call_number&query=0010">Edison Amberol Record: 391</a>. 1910.
      </span></li>
    <li><span>Down home rag / Wilbur C. Sweatman. Van Eps Trio.
      <a href="search.php?nq=1&query_type=call_number&query=0849">Edison Blue Amberol: 2377</a>. 1914.
      </span></li>
    <li><span>Bon bon buddie / Will Marion Cook. Billy Murray. <a href="search.php?nq=1&query_type=call_number&query=3930">Indestructible Record: 794</a>. 1908.
      </span></li>
    <li><span>Oh, didn't he ramble / Will Handy (i.e. Cole &amp;
      Johnson). Arthur Collins. <a href="search.php?nq=1&query_type=call_number&query=2642">Edison Gold Moulded Record: 8081</a>. 1902.</span>
    </li>
  </ul>
</div><!-- end .content -->
<!-- End of file: blackartists.tpl -->
{include file="footer.tpl"}
