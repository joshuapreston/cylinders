<!-- File: footer.tpl -->
   <div class="footer">
 			<div class="footer-content">
	      <div class="pure-g">
	      	<div class="pure-u-1 pure-u-md-1-3">
	      		 <ul class="footer-list">
		      		<li>Discover</li>
		      		<li><a href="history.php">Cylinder History</a></li>
		      		<li><a href="playlists.php">Thematic Playlists</a></li>
		      		<li><a href="browse.php">Browse All</a></li>
		      	</ul>
	      	</div>
	      	<div class="pure-u-1 pure-u-md-1-3">
	      		<ul class="footer-list">
		      		<li>Donate</li>
		      		<li><a href="adopt.php">Adopt a Cylinder</a></li>
		      		<li><a href="donate.php">Donating Cylinders</a></li>
		      		<li><a href="donors.php">Project Donors</a> </li>
	      		</ul>
	      	</div>
	      	<div class="pure-u-1 pure-u-md-1-3">
	      		<ul class="footer-list">
		      		<li>About</li>
		      		<li><a href="overview.php">Project Overview</a></li>
		      		<li><a href="news.php">News</a></li>
		      		<li><a href="help.php">Help/FAQ</a></li>
		      		<li><a href="licensing.php">Copyright and Licensing</a></li>
		      		<li><a href="links.php">Links</a></li>
		      		<li><a href="overview.php"></a><a href="contact.php">Contact Us</a></li>
		      	</ul>
	      	</div>
	      </div>
	      <div class="pure-g">
	      	<div class="pure-u-1 pure-u-md-1-3">
	      		<div id="footer-logo">
	      			<a href="http://www.library.ucsb.edu"><img src="/images/ucsb_logo.png"></a>
	      		</div>
	      	</div>
	      	<div class="pure-u-1 pure-u-md-2-3">
	      		<div id="copyright">
	      			An initiative of the UC Santa Barbara Library &bull; (805) 893-5444 &bull; Santa Barbara, CA 93106-9010. Direct questions or comments about the project or this page to the <a href="mailto:seubert@ucsb.edu">project staff</a> or visit the <a href="help.php">help pages</a>. Copyright &copy; 2005-2017 The Regents of the University of California, All Rights Reserved.	      		</div>
	      	</div>
	      </div>
	    </div>
    </div>
  </div><!-- end .main -->
</div><!-- end .layout -->
<script language="javascript">
  var tracks = document.getElementsByTagName('audio');
  var n = tracks.length;
  while (n--) tracks[n].removeAttribute('controls');
</script>
<script async defer src="js/cylinders.js" language="javascript"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
</body>
</html>
<!-- End of file: footer.tpl -->
