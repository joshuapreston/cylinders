{include file="header.tpl"}
{include file="navigation.tpl"}
<!-- File: search_results.tpl -->
	<div class="content">

    {include file="search_nav.tpl"}

    <div class="search-results">

      {foreach $record_array as $num => $record}
        <div class="result-row">
          <div class="result-container">
            <div class="row-link"><a href="detail.php?query_type=mms_id&query={$record.mms_id}&r={$record.record_position}&of={$hits}">{$record.record_position}</a></div>

            <div class="pure-g">
              <div class="pure-u-1-3"><div class="data-label">Title:</div></div>
              <div class="pure-u-2-3"><div class="data-entry">
                <a href="detail.php?query_type=mms_id&query={$record.mms_id}&r={$record.record_position}&of={$hits}">{$record.titles.245a} {$record.titles.245b} {$record.titles.245p} {$record.titles.245n}</a>
              </div></div>
            </div>
            <div class="pure-g">
              <div class="pure-u-1-3"><div class="data-label">Performers:</div></div>
              <div class="pure-u-2-3"><div class="data-entry">{$record.performer}</div></div>
            </div>
{if $record.issue_numbers[0] neq ""}
            <div class="pure-g">
              <div class="pure-u-1-3"><div class="data-label">Issue Number:</div></div>
              <div class="pure-u-2-3"><div class="data-entry">{$record.issue_numbers[0]}</div></div>
            </div>
{/if}
            <div class="pure-g">
              <div class="pure-u-1-3"><div class="data-label">Release Year:</div></div>
              <div class="pure-u-2-3"><div class="data-entry">{$record.release_years[0]|regex_replace:"/[\[\]]/":''}</div></div>
            </div>
          </div>
          <hr />
        </div>
      {/foreach}
    </div>

    {include file="search_nav.tpl"}

	</div>
{include file="footer.tpl"}
<script>
// load links to next/previous page of search results
$(document).ready(function(e) {
  $('#next1').load('next.php');
  $('#next2').load('next.php');
  $('#prev1').load('previous.php');
  $('#prev2').load('previous.php');
});
</script>
<!-- End of file: search_results.tpl -->
