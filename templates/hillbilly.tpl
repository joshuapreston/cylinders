{include file="header.tpl"}
{include file="navigation.tpl"}
<!-- File: hillbilly.tpl -->
<div class="content text">
	<h1 class="center-text">Early Hillbilly &amp; Old Time Music</h1>
	<p>
		<a href="thematic_playlists.xml"><img src="/images/rss-podcast.png" alt="Podcast" border="0"></a>
	&nbsp;</p>
  <p>The origins of country music are often traced to Victor’s 1927  Bristol sessions, during which Jimmie Rodgers and the Carter Family were  discovered and made their first recordings. While this was indeed a watershed  moment for the commercial country genre—as both artists quickly rose to  national fame and contributed to the large-scale exposure of local, rural folk  music—the foundations for this “Big Bang” were laid earlier in the 1920s.  As radio emerged in the early '20s, record sales declined in the face of  competition, and record producers searched for ways to expand their audiences.  In various cities in the South, they began to record and release songs by  popular local folk music artists, targeting lower-class, rural white audiences  that recognized the featured local musicians and music styles. In 1924,  light-opera-singer-turned-country-artist Vernon Dalhart’s double-sided single  “The Wreck of the Old 97” b/w “The Prisoner’s Song,” became an unexpected hit,  proving the commercial viability of “hillbilly” or “old-time” music beyond  small local markets. </p>
  <p>The rise of country music coincided with the decline of the  cylinder, and consequently this playlist is composed almost exclusively of  Edison cylinders, as they were nearly the only cylinder manufacturer that  persisted into the 1920s. Accordingly, this playlist functions more as a grab  bag of early hillbilly music styles and artists, and less as a comprehensive  summary. However, despite this limited scope, these selections provide a  fascinating glimpse into the amazing diversity of white American folk music  before the codification of the commercial country music genre. </p>
  <p>
	<em>&mdash;Chris Warden, Reed College </em>  </p>

  <div class="center-img-horiz"></div>
  <ul>
    <li>Old Joe Clark / Fiddlin' Powers and Family. <a href="search.php?nq=1&query_type=call_number&query=1394">Edison Blue Amberol: 5076</a>. 1925. </li>
    <li>Cripple Creek / Fiddlin’ Powers &amp; Family. <a href="search.php?nq=1&query_type=call_number&query=12650">Edison Blue Amberol: 5219.</a> 1925. </li>
    <li>Wild reckless hobo / Cowen Powers. <a href="search.php?nq=1&query_type=call_number&query=1395">Edison Blue Amberol: 5131</a>. 1926.</li>
    <li>Hand me down my walking cane / Ernest Stoneman. <a href="search.php?nq=1&query_type=call_number&query=7362">Edison Blue Amberol:   5297</a>. 1927. </li>
    <li>When the work’s all done this fall  / Ernest Stoneman. <a href="search.php?nq=1&query_type=call_number&query=8115">Edison Blue Amberol: 5188</a>. 1926. </li>
    <li>McDonald’s reel / Jep Bisbee. <a href="search.php?nq=1&query_type=call_number&query=11451">Edison Blue Amberol: 4916</a>. 1924.</li>
    <li>Wreck of southern Old 97 / Vernon Dalhart. <a href="search.php?nq=1&query_type=call_number&query=6357">Edison Blue Amberol: 4898</a>. 1924.</li>
    <li>The death of Floyd Collins / Vernon Dalhart. <a href="search.php?nq=1&query_type=call_number&query=7370">Edison Blue Amberol: 5049</a>. 1925. </li>
    <li>You will never miss your mother / Blue Ridge Duo. <a href="search.php?nq=1&query_type=call_number&query=8088">Edison Blue Amberol: 4961</a>. 1924. </li>
    <li>Arkansas traveler / The Blue Ridge Duo. <a href="search.php?nq=1&query_type=call_number&query=11439">Edison Blue Amberol: 4936</a>. 1925 </li>
    <li>Life’s railway to heaven / The Blue Ridge Duo. <a href="search.php?nq=1&query_type=call_number&query=13853">Edison Blue Amberol: 4968</a>. 1925.</li>
    <li>Little brown jug / The Blue Ridge Duo. <a href="search.php?nq=1&query_type=call_number&query=11779">Edison Blue Amberol: 4973</a>. 1925.</li>
    <li>Turkey in the straw / The Blue Ridge Duo. <a href="search.php?nq=1&query_type=call_number&query=11448">Edison Blue Amberol: 4977</a>. 1925.</li>
    <li>Walking water / Allen Sisson with John F. Burckhardt. <a href="search.php?nq=1&query_type=call_number&query=12153">Edison Blue Amberol: 4981</a>. 1925. </li>
    <li>Durang hornpipe medley / John Baltzell (violin) and John F. Burckhardt. <a href="search.php?nq=1&query_type=call_number&query=12363">Edison Blue Amberol: 4918</a>. 1923. </li>
    <li>The old red  barn medley quadrille / John Baltzell. <a href="search.php?nq=1&query_type=call_number&query=12367">Edison Blue Amberol: 4914</a>. 1923.</li>
  </ul>
</div><!-- end .content -->
<!-- End of file: hillbilly.tpl -->
{include file="footer.tpl"}
