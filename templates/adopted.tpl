{include file="header.tpl"}
{include file="navigation.tpl"}
<!-- File: adopted.tpl -->
<div class="content text">
<h1>Adopted Cylinders</h1>
<p> The UCSB Library would like to thank the following donors for making a donation to the project and adopting a cylinder. Further information on <a href="adopt.php">adopting cylinders</a> is here, including a <a href="/search.php?query_type=keyword&amp;query=not+digitized&amp;nq=1">list of adoptable cylinders</a>. </p>
<ul>
    <li>Paul Abramson, Purchase, New York</li>
      <ul>
        <li>
        Before the dawn. Berrick Von Norden.
        <a href="search.php?nq=1&query_type=call_number&query=11182">Edison Amberol: 634</a>. 1910.        </li>
      </ul>
    <li>Mark Anderson, New Orleans, Louisiana
      <ul>
        <li>Ain't I no use, Mr. Jackson?
          / C.W. Murphy. Peter G. Hampton. <a href=search.php?nq=1&query_type=call_number&query=10013>Edison Bell Record: 6494</a>. 1905.</li>
        <li>The engineer's child. Vernon Dalhart and Co. <a href=search.php?nq=1&query_type=call_number&query=9245>Edison Blue Amberol: 5135</a>. 1926.</li>
  </ul>
    </li>
    <li>Anonymous</li>
      <ul>
        <li>
        Nobody knows (and nobody seems to care) / Irving Berlin. Louise Terrell.
        <a href="/search.php?nq=1&query_type=call_number&query=6889">Edison Blue Amberol: 3941.</a> 1920.        </li>
        <li>Drowsy head / Irving Berlin. Green Brothers Novelty Band.
        <a href="search.php?nq=1&query_type=call_number&query=9117">Edison Blue Amberol: 4337</a>. 1921.        </li>
        <li>
        Crazy blues. Noble Sissle.
        <a href="search.php?nq=1&query_type=call_number&query=9806">Edison Blue Amberol: 4264</a>. 1921        </li>
      </ul>
    <li>David Arnold, Broomall, Pennsylvania
      <ul>
        <li>Liberty Bell march. Vess Ossman. <a href=search.php?nq=1&query_type=call_number&query=13668>Columbia Phonograph Co: 3807</a>. Between 1896 and 1899.</li>
      </ul>
    </li>
    <li>Ann Bailey, Vashon, Washington</li>
      <ul>
        <li>Will you love me when I'm old? / John Ford. Elizabeth Spencer.
        <a href="search.php?nq=1&query_type=call_number&query=7582">Edison Blue Amberol: 4253</a>. 1920.</li>
      </ul>
      <li>Katherine Beck, Seattle Washington</li>
      <ul>
        <li>Arthur Collins. Ragtime violin  / Irving Berlin. <a href="search.php?nq=1&query_type=call_number&query=11406">Lakeside Indestructible Cylinder Record: 446</a>. 1913 or 1914.</li>
    </ul>
    <li>Titus Belgard, Pineville, Louisiana
      <ul>
        <li>Where we'll never grow old / James C. Moore. Sam Patterson Trio. <a href=search.php?nq=1&query_type=call_number&query=12361>Edison Blue Amberol: 5349</a>. 1927.</li>
      </ul>
    </li>
      <li>Joseph and Lynda Berman, Athens, Ohio</li>
      <ul>
        <li>March Boccaccio/ London Concert Orchestra. <a href="/search.php?nq=1&query_type=call_number&query=8165">Lambert: 948</a>. 190-?. </li>
    </ul>
      <li>Joelle N. Bird, Camden, New Jersey</li>
      <ul>
        <li>Waldvoglein / Guido Gialdini. <a href="/search.php?nq=1&query_type=call_number&query=8435">Indestructible Record: 1500</a>. 1911. </li>
    </ul>
      <li>Doug and Sharon Boilesen, Westminster, Colorado</li>
      <ul>
        <li>It blew! Blew! Blew! / Edison Concert Band. <a href="/search.php?nq=1&query_type=call_number&query=8688">Edison Gold Moulded Record: 9185.</a> 1906. </li>
    </ul>
      <li>Jill Breedon, Santa Barbara, Calif.</li>
      <ul>
        <li>Poet and peasant overture [Dichter und Bauer. Ouvert&uuml;re] / Suppe. National Military Band (London, England). <a href="/search.php?nq=1&query_type=call_number&query=7646">Edison Blue Amberol 23001</a>. 1913. </li>
    </ul>
    <li>Robert C. Bowman, Franklin, Ohio
      <ul>
        <li>You're just as sweet at sixty as you were at sweet sixteen / William A. Heelan. W.H. Thompson. <a href=search.php?nq=1&query_type=call_number&query=11272>Indestructible Record: 3312</a>. 1913.    </li>
      </ul>
    </li>
    <li>Eric Carstensen, Millbury, Ohio
      <ul>
        <li>For this / Reginald De Koven. Charles W. Harrison. <a href=search.php?nq=1&query_type=call_number&query=11966>Edison Blue Amberol: 1546</a>. 1912.    </li>
      </ul>
    </li>
    <li> Joseph Cravo, Kearny, New Jersey
      <ul>
        <li>Galathée / Victor Massé. Unidentified performers. <a href=search.php?nq=1&query_type=call_number&query=14179>Pathé: 774</a>. Approximately 1902.</li>
      </ul>
    </li>
      <li>Ken Davis, Dorchester Masachusettes</li>
      <ul>
        <li>Oh, promise me / Reginald De Koven. Henry Burr. <a href="search.php?nq=1&query_type=call_number&query=11609">Edison Bell Gold Moulded Record: 8929</a>. 1904.</li>
        <li>My wild Irish rose / Chauncey Olcott. George Gaskin. <a href="search.php?nq=1&query_type=call_number&query=11395">Columbia Phonograph Co.: 4188</a>. between 1896 and 1900.</li>
        <li>A bird in a gilded cage / Von Tilzer. Steve Porter. <a href="search.php?nq=1&query_type=call_number&query=12318">Columbia Graphophone Grand: 4608</a>. between 1897 and 1900.</li>
        <li>In dear old Georgia. Frank C. Stanley. <a href="search.php?nq=1&query_type=call_number&query=10927">Columbia: 32805</a>. 1905.</li>
    </ul>
    <li>Rob Deland &amp; Susan Irion</li>
      <ul>
        <li>The old log cabin. Imperial Minstrels. <a href="search.php?nq=1&query_type=call_number&query=9969">Columbia Phonograph Co.: 13002</a>. 1896 or 1897.</li>
    </ul>
    <li>Christopher Dixon, Kokomo, Indiana</li>
      <ul>
        <li>Falstaff. Quand’ ero paggio / Giuseppe Verdi. Antonio Scotti. <a href="search.php?nq=1&query_type=call_number&query=8288">Edison Gold Moulded Record: B. 57</a>. 1907.</li>
        <li>Pagliacci / Ruggiero Leoncavallo. Antonio Scotti. <a href="search.php?nq=1&query_type=call_number&query=8256">Edison Gold Moulded Record: B. 20</a>. 1906.</li>
      </ul>
    <li>Nina R. Dryer, Lagrangeville, New York
      <ul>
        <li>Farintosh & Jenny Dang the Weaver. William Craig. <a href=search.php?nq=1&query_type=call_number&query=10509>Edison Gold Moulded Record: 13057</a>. 1910.</li>
      </ul>
    </li>
    <li>Frederick Ellis, Folson, Louisiana</li>
      <ul>
        <li>For old times sake / Chas. K Harris. Jere Mahoney. <a href="search.php?nq=1&query_type=call_number&query=9964">Edison Record: 7680</a>. 1900.</li>
    </ul>
    <li>Douglas Feldmann, Granby Connecticut.
      <ul>
        <li>Take me out to the ball game. Edward Meeker. <a href=search.php?nq=1&query_type=call_number&query=13785>Edison Gold Moulded Record: 9926</a>. 1908.        </li>
        <li>2nd Regt., Conn. N.G. march / David Wallis Reeves. Edison Grand Concert Band. <a href=search.php?nq=1&query_type=call_number&query=10811>Edison Gold Moulded Record: 8337</a>. 1903.</li></ul>
    </li>
    <li>Leigh Fullmer</li>
      <ul>
        <li>Les myrtes sont flétris : aubade / Jean Faure. E. Soubeyran. <a href="search.php?nq=1&query_type=call_number&query=9926">Cylindres Edison Moulés Sur Or: 17908</a>. 1907.</li>
    </ul>
    <li> Andre Gardner, Wayne, Pennsylvania
      <ul>
        <li>Ain't she sweet / Milton Ager. Johnny Marvin and Andy Sannella. <a href=search.php?nq=1&query_type=call_number&query=15631>Edison Blue Amberol: 5343</a>. 1927.</li>
      </ul>
    </li>
      <li>Rich Gardner</li>
      <ul>
        <li>Ma Tiger Lily / A. Baldwin Sloan. Peerless Orchestra. <a href="search.php?nq=1&query_type=call_number&query=10025">Edison Record: 7617</a>. between 1896 and 1900. </li>
    </ul>
    <li>Heather Good, Oak Harbor, Washington.
      <ul>
        <li>Ammore e meo / N. Di Chiara. [Olimpia d'Avigny ; Berardo Cantalamessa]. <a href=search.php?nq=1&query_type=call_number&query=11077>Edison Gold Moulded Record: 12418</a>. 1901.    </li>
      </ul>
    </li>
    <li>John Graff, Denver, Colo.</li>
      <ul>
        <li>Closing time at a country grocery. Byron G. Harlan and Frank C. Stanley.  <a href="/search.php?nq=1&query_type=call_number&query=6396&sortBy=&sortOrder=id">Edison Gold Moulded Record: 8172</a>. 1902.</li>
        <li>I've got rings on my fingers. Ada Jones. <a href="/search.php?nq=1&query_type=call_number&query=8760&sortBy=&sortOrder=id">Indestructible Record: 1156</a>. 1909. </li>
        <li>Sweetheart days. Male Quartet. <a href="/search.php?nq=1&query_type=call_number&query=9148">Columbia Phonograph Co.: 33230</a>. 1908. </li>
        <li>Whistle. Edison Military Band. <a href="/search.php?nq=1&query_type=call_number&query=8761&sortBy=&sortOrder=id">Edison Gold Moulded Record: 9877</a>. 1908. </li>
    </ul>
    <li>Matthew Hahn</li>
      <ul>
        <li>Nanny (I have never loved another girl but you) / Harry Lauder. <a href="/search.php?nq=1&query_type=call_number&query=7671">Edison Blue Amberol: 2905</a>. 1916. </li>
<li>American Symphony Orchestra. Ride of the Valkyries / Richard Wagner.  <a href="search.php?nq=1&query_type=call_number&query=9878">Edison Blue Amberol: 4802</a>. 1924.</li>
    </ul>
    <li>Larry     Hawes, Seattle Wash.</li>
      <ul>
        <li>The white wash man / Arthur Collins. <a href="/search.php?nq=1&query_type=call_number&query=8508&sortBy=&sortOrder=id">Indestructible Record: 1073</a>. 1909.</li>
        <li>Bear's oil / Billy Golden and Joe Hughes. <a href="/search.php?nq=1&query_type=call_number&query=8506&sortBy=&sortOrder=id">Indestructible Record: 1111</a>. 1909.</li>
        <li>My Sambo / Arthur Collins. <a href="/search.php?nq=1&query_type=call_number&query=6792&sortBy=&sortOrder=id">Edison Gold Moulded Record: 8123</a>. 1902. </li>
    </ul>
    <li>Mark Hopkins and Virginia Anders, Santa Barbara, CA</li>
      <ul>
        <li>Eternal mind the potter is / Louis Sphor. Edison Mixed Quartet. <a href="search.php?nq=1&query_type=call_number&query=7544">Edison Standard Record: 10425</a>. 1910.</li>
    </ul>
    <li>Thomas G. Hutchinson, Portland, Oregon</li>
      <ul>
        <li>Christmas eve in old England. Bransby Williams & Edison Carol Singers. <a href="search.php?nq=1&query_type=call_number&query=10493">Edison Gold Moulded Record: 13345</a>. 1905.</li>
      </ul>
    <li>Steven Jablonoski, Chicago, Illinois
      <ul>
        <li>American billionaire. Frank Stanley. <a href=search.php?nq=1&query_type=call_number&query=13658>Edison Record: 7960</a>. 1901.</li>
        <li>It looks like rain / Wendell W. Hall. Ernest Hare. <a href=search.php?nq=1&query_type=call_number&query=12125>Edison Blue Amberol: 4881</a>. 1924.</li>
        <li>Daisy Belle / Edward M. Favor. Harry Dacre. <a href=search.php?nq=1&query_type=call_number&query=8099>North American Phonograph Co.: 1058</a>. 1894.</li>
        <li>Backyard conversation between two [jealous] Irish washerwomen. Performers not given. <a href=search.php?nq=1&query_type=call_number&query=11089>Columbia Phonograph Co.: 11102</a>. between 1901 and 1904.</li>
        <li>Barney Google / Billy Rose. Billy Jones and Ernest Hare. <a href=search.php?nq=1&query_type=call_number&query=12167>Edison Blue Amberol: 4757</a>. 1923.</li>
      </ul>
    </li>
    <li> Paul Jessop, Kensworth, Great Britain
      <ul>
        <li>Klara stjärna / Wetterlind. Arvid Asplund. <a href=search.php?nq=1&query_type=call_number&query=12749>Edison Goldguss Walze: 20549</a>. 1909.    </li>
      </ul>
    </li>
    <li>Dianne L. Kryter, Santa Barbara, Calif.</li>
      <ul>
        <li>The old rustic bridge by the mill / E. Pike and P. Dawson. <a href="/search.php?nq=1&query_type=call_number&query=7842">Edison Blue Amberol: 23052</a>. 1908. </li>
    </ul>
      <li>Gianluca La Villa, Ferrara, Italy
        <ul>
          <li>Cavatina  / Joachim Raff. Prof. Polo. <a href=search.php?nq=1&query_type=call_number&query=10665>Pathé: 84099</a>. between 1900 and 1909.</li>
          <li>Berceuse de Jocelyn. Enrico Polo. <a href=search.php?nq=1&query_type=call_number&query=14799>Pathé: 84094</a>. between 1905 and 1910.</li>
        </ul>
    </li>
    <li>Liebhaber Family, Santa Barbara, Calif.</li>
      <ul>
        <li><a href="https://www.library.ucsb.edu/OBJID/Cylinder8667">Tahitian Field Recordings, 1923 </a></li>
    </ul>
    <li>Lucas Family, Jim Thorpe, Penna.</li>
      <ul>
        <li>All she gets from the iceman is ice / Ada Jones. <a href="/search.php?nq=1&query_type=call_number&query=7786&sortBy=&sortOrder=id">Edison Record: 9859</a>. 1908. </li>
    </ul>
    <li>Megan MacMillan, Santa Barbara, Calif.</li>
      <ul>
        <li>Valcartier - Canadian march / Frederick J. Pearsall. Sodero's Band. <a href="/search.php?nq=1&query_type=call_number&query=6905">Edison Blue Amberol: 2634</a>. 1915. </li>
      </ul>
    <li>Diane Maxted, Cathedral City, California, in memory of Samuel and Sarah Peterhans
      <ul>
        <li>Beautiful Ohio / Mary Earl. Federal Dance Orchestra. <a href=search.php?nq=1&query_type=call_number&query=4418>Indestructible Record: 3444</a>. 1919.</li>
        <li>Ain't she sweet / Milton Ager. Johnny Marvin and Andy Sannella. <a href=search.php?nq=1&query_type=call_number&query=15631>Edison Blue Amberol: 5343</a>. 1927.</li>
        <li>Romance of Athlone. My wild Irish rose / Chauncey Olcott. Walter Van Brunt. <a href=search.php?nq=1&query_type=call_number&query=0975>Edison Blue Amberol: 2787</a>. 1916.</li>
        <li>It’s nice to get up in the mornin’ / Sir Harry Lauder. Glen Ellison. <a href=search.php?nq=1&query_type=call_number&query=5558>Edison Blue Amberol: 3306</a>. 1917.</li>
        <li>Moonlight and roses / Ben Black. Polla's Clover Gardens Orchestra; Helen Clark; Joseph A. Phillips. <a href=search.php?nq=1&query_type=call_number&query=15606>Edison Blue Amberol: 5018</a>. 1925.</li>
        <li>When the pussywillow whispers to the catnip / Cliff Friend. Vaughn De Leath. <a href=search.php?nq=1&query_type=call_number&query=15591>Edison Blue Amberol: 5368 </a>. 1927.</li>
        <li>Uncle Josh buys an automobile / Cal Stewart. Cal Stewart. <a href=search.php?nq=1&query_type=call_number&query=0661>Edison Blue Amberol: 1583</a>. 1912.</li>
        <li>The trail of the lonesome pine / Harry Carroll. Manuel Romain. <a href=search.php?nq=1&query_type=call_number&query=0717>Edison Blue Amberol: 1743</a>. 1913.</li>
        <li>Uncle Josh in a photograph gallery / Cal Stewart. Cal Stewart. <a href=search.php?nq=1&query_type=call_number&query=5103>Edison Blue Amberol: 2108</a>. 1913.</li>
        <li>Uncle Josh keeps house / Cal Stewart. Cal Stewart. <a href=search.php?nq=1&query_type=call_number&query=0734>Edison Blue Amberol: 1714</a>. 1913.</li>
        <li>It’s a long, long way to Tipperary / Harry Williams. Albert Farrington. <a href=search.php?nq=1&query_type=call_number&query=7167>Edison Blue Amberol: 2487</a>. 1914.</li>
        <li>Silver threads among the gold / Hart Pease Danks. Lou Chiha "Frisco". <a href=search.php?nq=1&query_type=call_number&query=0874>Edison Blue Amberol: 2901</a>. 1916.</li>
        <li>Beautiful Ohio / Mary Earl. Metropolitan Quartet. <a href=search.php?nq=1&query_type=call_number&query=5909>Edison Blue Amberol: 3759</a>. 1919.</li>
        <li>Casey Jones / Eddie Newton. Billy Murray. <a href=search.php?nq=1&query_type=call_number&query=0636>Edison Blue Amberol: 1550</a>. 1912.</li>
        <li>Red Wing / Kerry Mills. Frederic H. Potter and the New York Military Band. <a href=search.php?nq=1&query_type=call_number&query=0600>Edison Blue Amberol: 1543</a>. 1913.</li>
      </ul>
    </li>
<li>Blake Merrifield, Chugiak, Alaska
      <ul>
        <li>The rights of labor / William H. Taft. William H. Taft. <a href=search.php?nq=1&query_type=call_number&query=11261>Edison Gold Moulded Record: 9999</a>. 1908.</li>
      </ul>
    </li>
    <li>Evert Meulie, Klavestadhaugen, Norway
      <ul>
        <li>Het oude Wilhelmus . Performer not identified. <a href=search.php?nq=1&query_type=call_number&query=11812>Pathé: 30419</a>. 190-.    </li>
      </ul>
    </li>
    <li>Werner Moll, Mannheim, Germany</li>
      <ul>
        <li>Columbia Quartette. You're the flower of my heart, sweet Adeline  / Harry Armstrong. <a href="search.php?nq=1&query_type=call_number&query=10500">Columbia Phonograph Co.: 32584</a>. 1904.</li>
    </ul>
      <li>Adrienne Naylor, in dedication to Jessica Hicks.</li>
      <ul>
        <li>George J. Gaskin. Bred in Kentucky / Stanley Carter. <a href="search.php?nq=1&query_type=call_number&query=9827">Columbia Phonograph Co.: 4166</a>. between 1896 and 1900.          </li>
      </ul>
    <li>Sarah Norris, Austin, Texas
      <ul>
        <li>Be mine? Gilmore's Band. <a href=search.php?nq=1&query_type=call_number&query=13646>Columbia Phonograph Co: </a>. Between 1896 and 1901.</li>
        <li>All she gets from the iceman is ice. Edward M. Favor. <a href=search.php?nq=1&query_type=call_number&query=13715>Indestructible Record: 756</a>. 1908.</li>
      </ul>
    </li>
    <li>Sean O'Brenan, Vancouver, British Columbia
      <ul>
        <li> O Canada, mon pay, mes amours / Paul Dufault. <a href=search.php?nq=1&query_type=call_number&query=11911>Edison Blue Amberol: 27132</a>. between 1912 and 1921.</li>
      </ul>
    </li>
      <li>Beth Pitton-August, Santa Barbara, California
        <ul>
          <li>Can you tame wild wimmin? / Harry von Tilzer. Billy Murray. <a href=search.php?nq=1&query_type=call_number&query=13856>Edison Record: 3720</a>. 1919.      </li>
        </ul>
      </li>
      <li>Davíd Rasko, Sydney, Australia
        <ul>
          <li>Caridad. Luis y Simon Ramirez. <a href=search.php?nq=1&query_type=call_number&query=10316>Unknown issue</a>. 1900.</li></ul>
      </li>
      <li>Pamela Richardson, Alpharetta, Georgia
        <ul>
          <li>At a Georgia camp meeting. unidentified performer. <a href=search.php?nq=1&query_type=call_number&query=15111>Pathé: 8921</a>. approximately 1903.      </li></ul>
      <li>James Robertson, Austin Texas</li>
      <ul>
        <li>All that glitters is not gold. Frank Miller [i.e. Stanley Kirkby]
          . <a href="search.php?nq=1&query_type=call_number&query=10010">Edison Bell Record: 10142</a>. 1907.</li>
    </ul>
    <li> James Robertson, in honor of J.D. Robertson, Pocola, Oklahoma</li>
      <ul>
        <li>There's a little white church in the valley / Irving Kaufman and Chorus. <a href="/search.php?nq=1&query_type=call_number&query=8364">Edison Blue Amberol: 2717</a>. 1915.</li>
    </ul>
      <li>Dale Sadler, Steilacoom, Washington
        <ul>
          <li>I aint-en got-en no time to have the blues / Harry Von Tilzer. Billy Murray and Ed Smalle.  <a href=search.php?nq=1&query_type=call_number&query=12218>Edison Blue Amberol: 3795</a>. 1919.</li>
        </ul>
    </li>
    <li>Sunny Sanders, Richmond, Virginia
      <ul>
        <li>I want to see my Tennessee  / Jack Yellen; Milton Ager. Vernon Dalhart and Ed. Smalle. <a href=search.php?nq=1&query_type=call_number&query=11775>Edison Blue Amberol: 4950</a>. 1925.</li>
      </ul>
    </li>
    <li>David Seubert, Santa Barbara, Calif.</li>
      <ul>
        <li>Any rags medley / Thos. S. Allen. Edison Concert Band. <a href="/search.php?nq=1&query_type=call_number&query=9137">Edison Gold Moulded Record: 8573</a>. 1903.  </li>
<li>Performer not identified. Ouverture des noces Jeannette / Victor Massé.  <a href="search.php?nq=1&query_type=call_number&query=11692">Lioret: [no number]</a>. ca. 1898</li>
    </ul>
    <li>Neil Siegal, Glen Cove, New York</li>
      <ul>
        <li>[Ballad sung by Leland [?] female vocal quartet]. Leland Quartet. <a href="search.php?nq=1&query_type=call_number&query=11108">Brown wax home recording: </a>. between 1890 and 1920.</li>
        <li>What a time. Polk Miller's Old South Quartet. <a href="search.php?nq=1&query_type=call_number&query=9857">Edison Blue Amberol: 2177</a>. 1913.</li>
        <li>I'se gwine back to Dixie / C.A. White. Manhansett Quartet. <a href="search.php?nq=1&query_type=call_number&query=10640">Columbia Phonograph Co.: 9010</a>. between 1896 and 1900.</li>
    </ul>
    <li>Victoria Szabo, Durham, North Carolina
      <ul>
        <li>Tyrolean song. Professor D. Wormser. <a href=search.php?nq=1&query_type=call_number&query=13629>Columbia Phonograph Co: 23912</a>.  1898 or 1899.</li>
      </ul>
    </li>
    <li>Rex Stokes, Northcote, New Zealand
      <ul>
        <li>How was I to know. Edward M. Favor. <a href=search.php?nq=1&query_type=call_number&query=15486>Edison Record: 7269 </a>. 1899.    </li>
      </ul>
    </li>
    <li>Bruce Triggs, Vancouver, British Columbia</li>
      <ul>
        <li>The butterfly / Theo Bendix. Alexander Prince. <a href="search.php?nq=1&query_type=call_number&query=7579">Edison Blue Amberol: 23081</a>. 1913.</li>
        <li>Nazareth / Charles Gounod. Alexander Prince. <a href="search.php?nq=1&query_type=call_number&query=7634">Edison Blue Amberol: 23055</a>. 1913.</li>
        <li>Medley of German waltzes. John J. Kimmel. <a href="search.php?nq=1&query_type=call_number&query=7729">Indestructible Record: 995</a>. 1909.</li>
        <li>Haley's fancy: medley of Irish jigs. John J. Kimmel and Joe Linder. <a href=s"earch.php?nq=1&query_type=call_number&query=7811">Edison Blue Amberol: 4076</a>. 1920.</li>
        <li>Oh gee!. John J. Kimmel. <a href="search.php?nq=1&query_type=call_number&query=7812">Edison Blue Amberol: 3985</a>. 1920.</li>
        <li>Oi ya nestchastay (Malo russkaya piesnia) / Mykola Vitaliiovych Lysenko. Alexander Sashko ; A. Iranova. <a href="search.php?nq=1&query_type=call_number&query=8015">Edison Blue Amberol: 11234</a>. 1922.</li>
        <li>Il bacio / Luigi Arditi. Alexander Prince. <a href="search.php?nq=1&query_type=call_number&query=8631">Sterling Record: 399</a>. [n.d.].</li>
        <li>Merry widow waltz / Franz Lehár. Alexander Prince. <a href="search.php?nq=1&query_type=call_number&query=8701">Edison Blue Amberol: 23111</a>. 1913 or 1914.</li>
        <li>The battle song of liberty march / F. E. Bigelow. New York Military Band. <a href="search.php?nq=1&query_type=call_number&query=8802">Edison Blue Amberol: 3460</a>. 1918.</li>
        <li>On the Mississippi / Theodore F. Morse. A. Prince. <a href="search.php?nq=1&query_type=call_number&query=8853">Edison Gold Moulded Record: 13828</a>. 1908.</li>
        <li>Belphegor march / Engebert Brepsant. Concertina, played by A. Prince. <a href="search.php?nq=1&query_type=call_number&query=8860">Edison Gold Moulded Record: 14022</a>. 1910.</li>
        <li>Medley of Irish jigs. John Kimmble [sic] [John Kimmel]. <a href="search.php?nq=1&query_type=call_number&query=9201">Edison Gold Moulded Record: 9881</a>. 1908.</li>
    </ul>
    <li>Michael Troudt, East Troy, Wisconsin
      <ul>
        <li>All that I ask of you is love / Herbert Ingraham. Henry Burr. <a href=search.php?nq=1&query_type=call_number&query=13726>Indestructible Record: 1419</a>. 1910.</li>
      </ul>
    </li>
    <li>Ward Irish Music Archives, Milwaukee, Wisconsin</li>
    <ul>
          <li>Killarney. Miss Marie Narelle. <a href="search.php?nq=1&query_type=call_number&query=6824">Edison Gold Moulded Record: 9081</a>. 1905.</li>
          <li> Bedelia / N/A. George J. Gaskin. <a href="search.php?nq=1&query_type=call_number&query=6844">Columbia Phonograph Co.: 32320</a>. 1903.</li>

      <li>An evening at Mrs. Clancy's boarding house / N/A. Steve Porter and Billy Murray. <a href="search.php?nq=1&query_type=call_number&query=6848">Columbia Phonograph Co.: 33124</a>. 1906.</li>
      <li>The courtship of Barney and Eileen. Ada Jones and Len Spencer. <a href="search.php?nq=1&query_type=call_number&query=7783">Edison Gold Moulded Record: 9143</a>. 1905.</li>
      <li>St. Patrick's Day at Clancy's. Columbia Quartette. <a href="search.php?nq=1&query_type=call_number&query=8232">Columbia Record: 32236</a>. [1903].</li>
      <li>Pepita Maguire / J. B. Mullen. Edward M. Favor. <a href="search.php?nq=1&query_type=call_number&query=8387">Edison Gold Moulded Record: 8836</a>. 1904.</li>
      <li>The girl I left behind me / David Belasco. Columbia Drum, Fife and Bugle Corps. <a href="search.php?nq=1&query_type=call_number&query=8818">Columbia Phonograph Co.: 12800</a>. 1899.</li>
      <li>Ireland's well known melodies no. 2. Edison Grand Concert Band. <a href="search.php?nq=1&query_type=call_number&query=9119">Edison Gold Moulded Record: 128</a>. between 1896 and 1900.</li>
      <li>The minstrel boy / Thomas Moore. William Tuson. <a href="search.php?nq=1&query_type=call_number&query=9199">Edison Gold Moulded Record: 8769</a>. 1904.</li>
      <li>Killarney from "Rogers brothers in Ireland" / Max Hoffmann. Albert Benzler. <a href="search.php?nq=1&query_type=call_number&query=9226">Edison Gold Moulded Record: 9165</a>. 1905.</li>
    </ul>
    <li>Greg Walters, Apopka, Florida
      <ul>
        <li>Stars and stripes forever
          / John Philip Sousa. Indestructible Military Band. <a href=search.php?nq=1&query_type=call_number&query=10003>Indestructible Record: 665</a>. 1907</li>
      </ul>
    </li>
      <li></li>
      Benjamin Warshaw, Carrboro, North Carolina (in honor of his former teacher, Nancy Zorena)
      <ul>
        <li>I'm a running after Nancy. Collins and Harlan. <a href=search.php?nq=1&query_type=call_number&query=11982>Columbia Phonograph Co.: 33202</a>. 1907.</li>
      </ul>
      <li> Dale R. Wein, Cleveland, Ohio
        <ul>
          <li>Washington Post / John Philip Sousa. Performer not identified. <a href="search.php?nq=1&query_type=call_number&query=11723">Pathé: 40104</a>. 190-.</li>
        </ul>
      </li>
      <li>Jeremy Young, Grand Rapids, Michigan
        <ul>
          <li>Crown of Thorns and Cross of Gold. William Jennings Bryan. <a href=search.php?nq=1&query_type=call_number&query=13454>
Columbia Phonograph Co.: 11038</a>. between 1896 and 1900.</li>
        </ul>
      </li>
      </li>
  </ul>
</div>
<!-- End of file: adopted.tpl -->
{include file="footer.tpl"}
