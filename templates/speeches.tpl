{include file="header.tpl"}
{include file="navigation.tpl"}
<!-- File: speeches.tpl -->
<div class="content text">
  <h1 align="center">Historical Speeches on Edison Cylinders</h1>
  <p>
    <a href="thematic_playlists.xml"><img src="/images/rss-podcast.png" alt="Podcast"></a>
    &nbsp;</p>
  <p>
    Thomas Edison originally envisioned sound recording as
    a tool for office dictation and not popular amusement, but eventually
    entertainment&mdash;primarily recorded music&mdash;prevailed. Nonetheless, from
    the earliest days of recorded sound, spoken word was also recorded on
    cylinders and sold for educational purposes such as language instruction and for edification of the people through exposure to historical speeches, as
    well as for entertainment, including comic monologues and vaudeville sketches.
    This program presents examples of the second of these three categories.
  </p>
  <p>
    Speeches by contemporary figures were recorded because of
    their current fame, as with the cylinders of actress <a href="search.php?nq=1&query=Sarah+Bernhardt&amp;query_type=keyword">Sarah Bernhardt,</a> or because
    of their political importance, as with the cylinders of <a href="search.php?nq=1&query=theodore+Roosevelt&amp;query_type=keyword">Roosevelt</a>, <a href="search.php?nq=1&query=William+h+taft&amp;query_type=keyword">Taft</a>,
    and <a href="search.php?nq=1&query=jennings+bryan&amp;query_type=keyword">Bryan</a>. They are significant today because they are still important
    historical figures, but also because their voices are not well known today.
    Edison infrequently recorded, but his famous speech about World War I can be heard here.
    Other cylinders, like the recitations by Harry
    Humphrey, including his dramatization of Patrick Henry's &quot;Give me
    liberty or give me death&quot; speech, show which historical figures were
    admired and how history was viewed in the early 20th century.  </p>
  <p>
    This program begins with a cylinder that is somewhat different, Len Spencer's
    famous &quot;Advertising Record&quot; in which he personifies the phonograph
    in a speech designed to demonstrate and sell phonographs in stores. This
    cylinder, recorded purely for commercial purposes, has become historically
    significant itself, not unlike the famous television commercials created
    by Coca-Cola or Apple Computer.
  </p>
  <p>
    <em>&mdash;David Seubert, UC Santa Barbara</em></p>

  <ul>
    <li><span class="small-text">Advertising record. Len Spencer. <a href="search.php?nq=1&query_type=call_number&query=2500">Edison Gold Moulded Record</a>. 1906</span></li>
    <li><span class="small-text"> Social and industrial justice. Theodore Roosevelt.
      <a href="search.php?nq=1&query_type=call_number&query=2682">Edison Blue Amberol: 3709</a>. 1919.</span></li>
    <li><span class="small-text"> Ph&egrave;dre. Sarah Bernhardt. <a href="search.php?nq=1&query_type=call_number&query=2303">Edison Amberol: 35008</a>. 1910.</span></li>
    <li><span class="small-text"> Guaranty of bank deposits. William Jennings Bryan.
      <a href="search.php?nq=1&query_type=call_number&query=3356">Edison Gold Moulded Record: 9921</a>. 1908.</span></li>
    <li><span class="small-text"> My south polar expedition. Lieutenant E.H. Shackleton.
      <a href="search.php?nq=1&query_type=call_number&query=1859">Edison Amberol: 473</a>. 1910.</span></li>
    <li><span class="small-text"> Republican and Democratic treatment of trusts. William
      H. Taft. <a href="search.php?nq=1&query_type=call_number&query=3414">Edison Gold Moulded Record: 9998</a>. 1908.</span></li>
    <li><span class="small-text"> The right of the people to rule. Theodore Roosevelt.
      <a href="search.php?nq=1&query_type=call_number&query=2683">Edison Blue Amberol: 3707</a>. 1919</span></li>
    <li><span class="small-text"> The tariff question. William J. Bryan. <a href="search.php?nq=1&query_type=call_number&query=3355">Edison Gold Moulded Record: 9918</a>. 1908</span></li>
    <li><span class="small-text"> Patrick Henry's speech. Harry E. Humphrey. <a href="search.php?nq=1&query_type=call_number&query=0713">Edison Blue Amberol: 1652</a>. 1912.</span></li>
    <li><span class="small-text"> Sheridan's ride. Edgar L. Davenport. <a href="search.php?nq=1&query_type=call_number&query=0804">Edison Blue Amberol: 1957</a>. 1913.</span></li>
    <li><span class="small-text">Immortality. William J. Bryan. <a href="search.php?nq=1&query_type=call_number&query=6051">Edison Gold Moulded Record: 9923</a>. 1908.</span></li>
    <li><span class="small-text">The farmer and the business man. Theodore Roosevelt.
      <a href="search.php?nq=1&query_type=call_number&query=6052">Edison Blue Amberol 3708</a>. 1919.</span></li>
    <li><span class="small-text">Let us not forget. Thomas A. Edison. <a href="search.php?nq=1&query_type=call_number&query=5907">Edison Blue Amberol: 3756</a>. 1919.</span></li>
  </ul>
</div><!-- end .content -->
<!-- End of file: speeches.tpl -->
{include file="footer.tpl"}
