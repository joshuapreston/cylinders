      <div class="pagination">
<!--        <div id="sort_order">
          <select onChange="window.location.href=this.value">
            <option>Order by:</option>
            <option id="title_sort" value='{$title_sort}'>Title</option>
            <option id="author_sort" value='{$author_sort}'>Author</option> -->
<!--             <option id="issue_num_sort" value='{$issue_num_sort}'>Issue Number</option>  -->
<!--            <option id="year_sort" value='{$year_sort}'>Year</option>
          </select>
        </div>
 -->
        <div class="results-nav">

        {if isset($prev_page_link)}
          <i class="fa fa-caret-left"></i><span id="prev{counter name=prev}"><img src="images/ui-anim_basic_16x16.gif"></span>
          <i class="fa fa-ellipsis-h"></i>
        {/if}

        {$hits} records

        {if isset($next_page_link)}
          <i class="fa fa-ellipsis-h"></i><span id="next{counter name=next}"><img src="images/ui-anim_basic_16x16.gif"></span>
             <i class="fa fa-caret-right"></i>
        {/if}
        </div>
      </div>
