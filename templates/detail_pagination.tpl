{if $hits gt 1}
  <div class="pagination search-detail">

    {if $show_previous_cylinder_link}
      <i class="fa fa-caret-left"></i><span id="prev{counter name=prev}"><img src="images/ui-anim_basic_16x16.gif"></span>
      <i class="fa fa-ellipsis-h"></i>
    {/if}

    <a href="search.php?query_type={$session.query_type}&query={$session.query_term}&nq=1" class="button-xsmall pure-button">Search Results</a>

    {if $show_next_cylinder_link}
      <i class="fa fa-ellipsis-h"></i>
      <span id="next{counter name=next}"><img src="images/ui-anim_basic_16x16.gif"></span> <i class="fa fa-caret-right"></i>
    {/if}

  </div>
{/if}
