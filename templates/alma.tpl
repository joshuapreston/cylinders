{include file="header.tpl"}
{include file="navigation.tpl"}
<!-- File: alma.tpl -->
  <div class="content text">
      <h2>
        Cylinder Audio Archive Moves To New Back-End Database</h2>
      
      <p>October 24, 2017</p>
      <p>This morning the Cylinder Audio Archive switches over to the  UCSB Library's next generation database, the Alma system from Ex Libris, as our  back-end database. Users should see no change to the search experience or  functionality of the system, though a few interface improvements have been made  such as hot-linking of genre terms and better display of titles. Take numbers  for certain cylinders no longer display, which we hope to have fixed shortly.  If you discovery any broken links or problems, please contact <a href="mailto:seubert@ucsb.edu"> library staff</a>.</p>
      <p>
      The Cylinder Audio Archive has been running continuously  since October 2005 off of the Library's Aleph database, which was retired this  summer. For the technically inclined, the old search interface was built  through Z39.50 queries to the Aleph system which accounts for its 12 years of  continuous up-time. The new system uses the SRU (Search/Retrieve via URL)  system in Alma. By maintaining this live connection to our library catalog, all  updates and additions we make are instantaneous and new content can be added  daily. </p>
    <p>
      Many thanks to the Library technical and metadata staff (Ian  Lessing, Ana Fidler, Catherine Busselen, and Chrissy Rissmeyer) and Special  Collections staff (Dain Lopez and Nadine Turner) for seeing this process  through. </p>
      <p>
      We look forward to the next dozen years with Alma!</p>
</div>
<!-- end .content -->
<!-- End of file: alma.tpl -->   
{include file="footer.tpl"}
<p></p>
<p></p>
