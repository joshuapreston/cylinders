{include file="header.tpl"}
{include file="navigation.tpl"}
<!-- File: empty.tpl -->
<div class="content text">
  <h1>No Results Found</h1>
  <p>Sorry about that. Try a different query, or <a href="/browse.php">browse our collection</a> to find what you're looking for.</p>
</div>
<!-- End of file: empty.tpl -->
{include file="footer.tpl"}
