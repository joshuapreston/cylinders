{include file="header.tpl"}
{include file="navigation.tpl"}
<!-- File: mexico.tpl -->
<div class="content text">
	<h1>Mexican Cylinders: Of National Identity and Sound Recordings </h1>
	<p>
		<a href="thematic_playlists.xml"><img src="/images/rss-podcast.png" alt="Podcast"></a>
	&nbsp;</p>
  <figure>
  <img src="/images/coculense.png" alt="Cuarteto Coculense" width="280" height="406" class="pure-img" /> <figcaption>Cuarteto Coculense. (Photograph from Arhoolie reissue &quot;<a href="https://folkways.si.edu/cuarteto-coculense/the-very-first-recorded-mariachis-1908-1909/latin-world/music/album/smithsonian">Cuarteto Coculense: The Very First Recorded Mariachis: 1908-1909</a>.&quot;)</figcaption>
  </figure>
  <p>Cylinder recording in Mexico first took place in the midst of &quot;El Porfiriato,&quot; (1884&ndash;1911) the three and a half decades in which General Porfirio Díaz led the country as president until the start of the Mexican Revolution. His control was encapsulated by the phrase &quot;Pan, o palo,&quot; or &quot;bread or a beating,&quot; meaning one could either accept his rule, or suffer consequences otherwise. Needless to say, this period was tumultuous in the history of Mexico. Although Porfirio Díaz repressed the country's people, often by privatizing communal landholdings, his reign is also remembered as one in which the country experienced serious economic growth, attracting markets and companies from the United States and Europe to invest in Mexico. One of Díaz's strategies to gain the attention of foreign investors was to do so through the promotion of a national identity, based on Mexico's most rural expressions.</p>
  <p>It was during this time that major labels such as Edison, Columbia, and Victor began producing the first sound recordings in Mexico. These companies dispatched recording teams throughout the world to capture music of local communities on wax cylinders and discs. In Mexico, these companies (along with smaller labels, Zonophone and Odeon), were actively recording between 1902 and 1910. Due to the turmoil of the revolution between 1910 and 1920, however, transnational recording operations ceased in Mexico, and would not continue again until 1921. In this short window of time, these major labels set out to record and preserve &quot;cultural expressions that were considered representative [of Mexico],&quot; but in the process, contributed to the establishment of a level of authenticity that overshadowed other genres (Madrid, 96). The affect of this process was the erasure of many rural genres, and a reproduction through sound of what was deemed authentically Mexican. In short, these companies ultimately bolstered President Díaz's goal to formulate a national identity for Mexico abroad.</p>
  <p>Amongst the collection as part of UCSB's Cylinder Audio Archive are some of the earliest mariachi recordings
  (by <a href="/search.php?nq=1&query_type=keyword&amp;query=Cuarteto+Coculense">Cuarteto Coculense</a>),
  recordings by <a href="/search.php?nq=1&query_type=keyword&amp;query=Octaviano+Yanez">Octaviano Yañez</a>,
  one of the first guitarists ever recorded, an early adaptation of
  &quot;<a href="/search.php?nq=1&query_type=call_number&query=11337">Jarabe Tapatío</a>&quot; (Mexican Hat Dance),
   and various other titles that befell the genre of son abajeño, or &quot;son from the lowlands&quot; (Madrid, 96). The repertoire of recordings in this regional category spans in musical instrumentation and vocal styles as well as several genre variants. These recordings reflect a glimpse into Mexican popular music prior to the revolution, and conversely, the agenda of Porfirio Díaz to concentrate a national identity for Mexico to markets in the western world. Nevertheless, the cylinders of this period give us an idea of how much well-known genres from Mexico have evolved during the past century since they were initially recorded. </p>
  <p>
  	<em>&mdash;<span id=":2lk" title="Javier Garibay">Javier Garibay</span>, UC Santa Barbara Library</em>  </p>

  <ul>
    <li>Aires  Nacionales / Banda. <a href="https://www.library.ucsb.edu/OBJID/Cylinder8614" target="_blank">Edison Gold Moulded Record: 18755</a>. 1905.<br />
      </li>
    <li>Besos y Pesos / Banda de Artilleria. <a href="https://www.library.ucsb.edu/OBJID/Cylinder9122" target="_blank">Edison Blue Amberol: 22038</a>. 1909.</li>
    <li>Jarabe Tapatio / R. H. Robinson. <a href="https://www.library.ucsb.edu/OBJID/Cylinder11337" target="_blank">Edison Gold Moulded Record: 18508</a>. 1904.</li>
    <li> El Amor es la Vida / Quinteto Jordá.&nbsp;<a href="https://www.library.ucsb.edu/OBJID/Cylinder6673" target="_blank">Edison Gold Moulded: 18780</a>. 1905.</li>
    <li>Las Campanitas / Cuarteto Coculense. <a href="https://www.library.ucsb.edu/OBJID/Cylinder14930" target="_blank">Edison Amberol: 6070</a>. n.d. </li>
    <li>Una Noche en La Mar /  Rita Villa. <a href="https://www.library.ucsb.edu/OBJID/Cylinder8856" target="_blank">Edison Gold Moulded Record: 20362</a>.  1911.</li>
    <li>Viva Jalisco! / Banda de Policia de Mexico. <a href="https://www.library.ucsb.edu/OBJID/Cylinder9367" target="_blank">Edison Amberol: 6078</a>. 1910.</li>
    <li>Macario Romero / Herrera Robinson y Picazo. <a href="https://www.library.ucsb.edu/OBJID/Cylinder4668" target="_blank">Edison Gold Moulded Record: 18638</a>. 1905.</li>
    <li>Quien Sabe? / Banda de Artilleria. <a href="https://www.library.ucsb.edu/OBJID/Cylinder9114" target="_blank">Edison Blue Amberol: 22083</a>. 1910. </li>
    <li>Zacatecas Marcha / Banda Española. <a href="https://www.library.ucsb.edu/OBJID/Cylinder4916" target="_blank">Columbia Phonograph Co.: 40322</a>. n.d.</li>
    <li>Anita / Octaviano Yañez. <a href="https://www.library.ucsb.edu/OBJID/Cylinder7751" target="_blank">Edison Gold Moulded Record: 20102</a>. 1907. </li>
  </ul>
  <p>Sources: </p>
  <p>Madrid, Alejandro L. &quot;Sound Recordings.&quot; <a href="http://worldcatlibraries.org/wcpa/oclc/695560230">Transnational Encounters: Music and Performance at the U.S.-Mexico Border</a>. New York: Oxford UP, 2011. 94-97. Print.<br />
    <br />
&quot;<a href="https://en.wikipedia.org/wiki/Porfirio_D%C3%ADaz">Porfirio Díaz</a>.&quot; Wikipedia, The Free Encyclopedia.  18 Nov. 2015. Web. <span tabindex="0" data-term="goog_1911418992">23 Nov. 2015</span>.</p>
</div>
<!-- end .content -->
<!-- End of file: mexico.tpl -->
{include file="footer.tpl"}
