{include file="header.tpl"}
{include file="navigation.tpl"}
<!-- File: incunabula2.tpl -->
<div class="content text">
  <h1>Recorded Incunabula, Part II</h1>
  <p>
    <a href="thematic_playlists.xml"><img src="/images/rss-podcast.png" alt="Podcast"></a>
    &nbsp;</p>

  <h3>Introduction</h3>
  <p>
    I am  pleased to be able to present the second installment of Recorded Incunabula, a follow-up to John Levin's
    <a href="incunabula.php">first installment</a> that he curated for the Cylinder Audio Archive   in 2009.
    John's vast knowledge of the earliest cylinders, especially those pre-dating the widespread sale of cylinders to home
    consumers in the late 1890s, is knowledge that only a handful of collectors have today. He has again generously provided
    digital transfers of some of the world's rarest cylinders. His  commentary puts them in context and provides a
    fascinating insight into the aural landscape of early sound recordings   in the  1890s. We hope you enjoy Part II of Recorded Incunabula.  </p>
  <p>
    <em>&mdash;<a href="mailto:seubert@library.ucsb.edu">David Seubert</a>, UC Santa Barbara Library</em>  </p>
  <h3>Curator's notes</h3>
  <p>
    Because we are drenched in media today and almost every day,  it's difficult to imagine life without it.  Yet that was
    the way it was in the 19th century. Media at home consisted of reading (sometimes  out loud) and, perhaps, playing a parlor
    organ or other instrument. When affordable phonographs started to appear  in quantity in homes in 1897, they were unrivaled
    home entertainment  appliances, and cylinders were played over and over again.
  </p>
  <p>
    The recordings in this program pre-date that time. The first phonographs were too expensive and  finicky for home use so records
    were principally played in arcades where people  could hear them using listening tubes.  Such was the hunger for media experience
    that people paid a nickel ($1.25  today) to listen to 2&frac12; minutes of faintly recorded music.
  </p>
  <p>
    There are few records remaining from the early 1890s, before  rag appeared and before the '90's turned &ldquo;gay.&rdquo;
    Most are gone forever. Further, since  the arcades were considered a bit d&eacute;class&eacute;, the musical repertoire
    they offered  was limited too. Still, from what has  survived, it's clear that 19th-century America was musically diverse
    and that it sounded quite different. Instead  of rock bands, there were concert bands.  Instead of pop music, there were
    sub-categories of popular songs like  comic, sentimental, and &ldquo;coon&rdquo; songs.  Dance genres included quadrilles,
    lanciers, gavottes, and  schottisches. Orchestras performed  musical tableaux called descriptive selections, and audiences
    delighted in  artistic whistlers, ethnic humorists, recitations and banjoists.
  </p>
  <p>
    Despite the differences between those times and now, there is  an important parallel. Even with today's  near-constant
    immersion in sound, we still respond to live music. We recognize its organic quality &ndash; its &ldquo;realness.&rdquo;
    We can tell it's not canned, compressed  sound, and we are drawn to it. Today's  musicians have the creativity and technology
    to build pieces of exquisite complexity  with synthesized sounds and sampled assets.  Yet, it is often the solo singer or
    live band that affects us most  powerfully.
  </p>
  <p>
    This, then, is one of the reasons for the <em>Incunabula </em>series &ndash; and for the UCSB Cylinder Audio Archive. Presented  here are more than the quaint, odd sounds of a bygone era. Unlike the stream of numeric
    codes we usually  listen to, this is the sound of real life, real performance, and real experience  from another time,
    etched into wax for posterity in realtime with unlimited  sampling. Somehow, our primal core  recognizes this,
    knows the difference, responds to it, and cares.  </p>
  <p>
    &mdash;<em> John Levin, Los Angeles, California</em>
  </p>

  <h4>
    Chimes of Normandy selections  / Robert Planquette. Brand's Concert Band.
    <a href="search.php?nq=1&query_type=call_number&query=10470">Ohio Phonograph Co.: [catalog number unknown]</a>. [1896]
  </h4>
  <p>
    It's 1896. You are strolling in a town park on a bright, warm Sunday.  Wooden folding chairs are set up for a midday
    &ldquo;programme&rdquo; in the gazebo by Brand's Concert Band&hellip;  The Chimes of Normandy was the English version
    of the very successful operetta Les Cloches de Corneville, which opened in both Paris and New York in 1877.  It was
    praised for its melodies, rhythmic variety, and orchestral complexity, and it ran (in Paris) for over 700 performances.
    This cylinder of &ldquo;selections&rdquo; is typical of light opera compilations that were performed without singing
    cast members during this early period.  The band leader, Michael Brand, led the Cincinnati Orchestra, which in 1895
    formed the nucleus of the Cincinnati Symphony Orchestra.  It is therefore highly likely that the lyrical quality of
    this performance can be attributed to musicians from the orchestra who &ldquo;moonlighted&rdquo; with Brand's Concert Band.
  </p>

  <h4>
    The Commodore song : ship ahoy. Edward M. Favor.
    <a href="search.php?nq=1&query_type=call_number&query=10471">Edison Phonograph Company: 722</a>. [1893].
  </h4>
  <p>
    Edward M. Favor was one of  the most successful and popular early recording artists, appearing on both  cylinders and
    discs into the early teens.  But he got his start in musical comedy, appearing in 1890 in the play <em>1492 Up To Date</em>,
    by Edward Everett Rice, as  part of its original cast. <em>Ship Ahoy! </em>opened in 1891. The company name was derived from an
    1893  hit, <em>1492 </em>(which Favor also appeared  in), hence the announcement on this cylinder.  This cylinder is probably
    the earliest extant original cast recording.
  </p>

  <h4>
    New York Herald  march and two-step / Monroe Rosenfeld. 23rd Regiment Band.
    <a href="search.php?nq=1&query_type=call_number&query=10480">[unknown manufacturer] </a>. [between 1893 and 1895].
  </h4>
  <p>
    Many of today's promotional marketing schemes &ndash; such  as the limited time offer and the installment plan &ndash;
    were first conceived in  the late 19th century. One of  these &ndash; the gift with purchase &ndash; was widely used
    to sell newspapers. Publishers would have a march composed which  people obtained by purchasing copies of the paper.
    The most famous of these, <em>Washington Post March</em>, was written by  Sousa in 1892. This rarity was written
    in 1893 by Monroe Rosenfeld and was probably recorded at that time before  disappearing into complete obscurity.
    Note how the recording company attempted to give the piece added luster  by adding Sousa's name to the announcement,
    even though he had nothing to do  with it.
  </p>

  <h4>
    Electric light quadrille. Issler's Orchestra.
    <a href="search.php?nq=1&query_type=call_number&query=10475">U.S. Phonograph Co.: [catalog number unknown]</a>. [1894 or 1895].
  </h4>
  <p>
    The quadrille was the precursor of square dancing. It was first developed in Europe as formal  and highly structured
    social dancing.  When it came to the United States, it became less structured and the  social boundaries disappeared
    too, causing the quadrille and its many cousins &ndash;  galops, schottisches, lanciers, etc. &ndash; to become
    immensely popular here in the  late 19th century. Quadrilles  consist of &quot;figures&quot; or step sequences that
    are &quot;called&quot; by  a &quot;prompter.&quot; Recordings of  quadrilles commonly include a light-hearted announcement
    in the middle (also  heard in <em>Gems of Ireland</em>), often  delivered by a different person than the prompter.
    Since listeners were tethered to machines  with listening tubes, these were likely added to entertain people who
    could not  dance. The electric light that is both  the inspiration of this piece and part of its announcement was
    a relative novelty  in the mid-1890s. While the  incandescent bulb was first invented in 1879, technical and manufacturing
    obstacles limited electrification to urban areas and large commercial  structures until the 20th century.  In 1935, 90%
    of American farms were still not electrified.
  </p>

  <h4>
    Daisy Bell / Henry Dacre. Edward M. Favor.
    <a href="search.php?nq=1&query_type=call_number&query=10477">Edison Phonograph Company: 1058</a>. [1894].
  </h4>
  <p>
    It's  interesting to speculate why some songs, such as <em>After The Ball </em>(see previous <em>Incunabula</em> radio program)
    and <em>Daisy Bell</em>, have  stood the test of time while others that were initially more popular have sunk
    into obscurity. This cylinder was  recorded in 1893 when <em>Daisy Bell </em>was  first written. At that time, the
    bicycle  had evolved to a basic design much like today's bikes, and they were enormously  popular. They were also more
    than a new  form of inexpensive transportation, liberating women from the need to rely on  men to help with horses.
    The &ldquo;bicycle  built for two&rdquo; referred to in this song is not the current, two-wheeled  version, which wasn't
    developed until 1898.  This version had three or four wheels with two passengers seated in  tandem, side by side.
  </p>

  <h4>
    Still his whiskers grew / C. W. Murphy. Dan W. Quinn.
    <a href="search.php?nq=1&query_type=call_number&query=10474">Columbia Phonograph Co.: 5065 </a>. [1896].
  </h4>
  <p>
    Comic songs were a staple of phonograph arcades and music halls, and  even during the early days of the phonograph
    certain performers emerged with  the talent to deliver comedic lyrics to the best advantage. One of them was Dan W. Quinn.
    He started his career in New England, at  first performing vaudeville and ultimately enjoying immense success recording
    comic and light-hearted popular fare for over 25 years. Quinn himself estimated making over 2,500  recordings for every major
    American record company. Hair restorers first appeared in the mid-1800s and became increasingly popular through the end of
    the century. This light-hearted promotion of their  efficacy was composed by C.W. Murphy, a prolific British composer of light,
    music-hall fare.
  </p>

  <h4>
    Poor blind boy. R. J. Jose.
    <a href="search.php?nq=1&query_type=call_number&query=10478">New England Phonograph Co.: [catalog number unknown]</a>. [1892].
  </h4>
  <p>
    Countertenors have had a place in popular song for centuries, achieving a  resurgence in recent decades with artists
    as diverse as Frankie Valli, Smokey  Robinson, Timothy B. Schmit (The Eagles), Mark Linkous (Sparklehorse), and, in
    a classical vein, Philippe Jaroussky.  R. J. Jose was the first countertenor to record, achieving great fame in the
    early 1900s with his recording of <em>Silver  Threads Among the Gold. </em>Jose's  repertoire appears to have been
    limited to sentimental ballads, and this  self-composed piece is no exception.  Until this recording surfaced, it was
    believed that Jose began recording  in 1903 exclusively for Victor Talking Machine.  This cylinder was probably recorded
    in 1891 or 1892. It is channel-rimmed with paper label, and  the announcer is Calvin Child, who went on to become a
    prominent member of the  Victor organization. The Spanish  pronunciation that he uses for Jose's name was an
    affectation: Jose was born in  Cornwall, England, where his surname is pronounced, &ldquo;joce.&rdquo;
  </p>

  <h4>
    Gems of Ireland quadrille. Banta's Popular Orchestra.
    <a href="search.php?nq=1&query_type=call_number&query=10472">Chicago Talking Machine Co.: 753</a>. [n.d].
  </h4>
  <p>
    This bouncy medley with its popular Irish tunes and quadrille structure  conjures images of Irish immigrants finding
    diversion from dreary tenement life  in teeming American cities like New York, Philadelphia, and Boston. And that is
    very much the way it was in 1890  America. Irish immigration dates back to  before  the American Revolution but peaked
    during the potato famine of the  mid-1840s, when more than half of American immigrants were Irish. Most Irish
    immigrants remained in the  Northeast and Midwest, moving into skilled and semi-skilled jobs. In 1890, the combined Irish-born and  second-generation population was 4,795,681.  Since early recordings were mostly heard in arcades and
    other venues with  inexpensive entertainment, many titles from this period are conspicuously aimed  at the working-class
    Irish Americans who frequented them.
  </p>

  <h4>
    Anvil chorus / Giuseppe Verdi. John York Atlee.
    <a href="search.php?nq=1&query_type=call_number&query=10476">Columbia Phonograph Co.: [catalog number unknown]</a>. [1893].
  </h4>
  <p>
    With rare exception, classical music was not recorded until the late  1890s. There are many explanations for  this,
    including the preference of arcade patrons for lively, popular selections  and the limitations of 2&frac12; minute
    cylinders. Yet the &ldquo;Coro di zingari&rdquo; &ndash; better known as the Anvil Chorus &ndash; from  Verdi's
    <em>Il Trovatore </em>was an  exception. Starting in the early 1890s,  it appears for decades in record  catalogs.
    Its accessible melody and  frequent inclusion in band concerts probably promoted it from the classical  category to
    that of popular fare. Even  so, it's hard to imagine why people found whistling preferable to a  conventional band
    performance. Perhaps  it's because whistling was a popular vaudeville specialty at the time. It was also well-suited
    for early recording  equipment because of its volume and sharp tone.  John Yorke Atlee was a leading American whistling
    virtuoso and made  dozens of recordings for Columbia and others.  Still, one hears him struggle through the more
    aggressive passages. This is understandable, considering that  early recording technology required him to perform the
    piece over and over to create  multiple copies.
  </p>

  <h4>
    Mill medley. Bison City Quartet.
    <a href="search.php?nq=1&query_type=call_number&query=10473">Ohio Phonograph Co.: [catalog number unknown]</a>. [n.d].
  </h4>
  <p>
    Almost every early record company included male vocal quartets as part of their  repertoire, even though none of
    these groups ever achieved the popularity of  other musical genres. This recording by  the Bison City Quartet is
    typical of its time, with a sentimental subject,  complex harmonies, <em>a cappella</em> singing,  and ensemble performance
    rather than solos designed to evoke images of the  slow, gentle life of rural, 19th-century America. Yet male vocal
    quartets have persisted as  part of popular music during the past century.  In addition to the nostalgia-fueled
    &quot;barbershop&quot; quartet revival  of the mid-century, there have been popular performers like The Ink Spots and
    rock groups like Crosby, Stills, Nash and Young, and The Eagles.
  </p>

  <h4>
    Haul the woodpile down. Charles Asbury.
    <a href="search.php?nq=1&query_type=call_number&query=8103">U.S. Phonograph Co.: [catalog number unknown]</a>. [ca. 1894].
  </h4>
  <p>
    It's hard to  fully grasp the nature of race relations in the United States during the  &ldquo;separate but equal&rdquo;
    era that followed the Civil War and continued until the  1950s.&nbsp; But it's certain that the lyrics  and &ndash; indeed &ndash;
    the categories of &quot;coon songs&quot; and minstrelsy did not widely offend then as  they do today. This music was
    enjoyed &ndash;  and performed &ndash; by whites and blacks. In  fact, the excellent banjoist Charles Asbury was portrayed
    as both white and black in the various record catalogs that were sent to record dealers in the  1890s.&nbsp;
  </p>
  <p>
    UPDATE:&nbsp; Thanks to the musical archaeology  of astute old-time banjo enthusiasts, it appears that my previous assertion
    is  incorrect that this song refers to the Underground Railroad and places of  concealment used by slaves.The song is likely
    a minstrel piece written in 1887 by the musical theater duo Edward  Harrigan and Tony Hart. It was probably derived from a work
    song used on wood-burning steamboats to stoke the boiler. The  lyrics follow in their entirety:
  </p>
  <p><em>De red cow brushing de old blue fly</em><br>
    <em>Away down in Florida</em><br>
    <em>De white man laugh when de coon goes by</em><br>
    <em>I'll haul de wood-pile down</em><br>
    <em>De steamboat ready to burn dat pine</em><br>
    <em>Away down in Florida</em><br>
    <em>De grape am ripe on de old black vine</em><br>
      <em>I'll haul de wood-pile down.</em></p>
  <p><em>CHORUS</em><br>
      <em>Traveling, den traveling</em><br>
      <em>As long as de moon am round</em><br>
      <em>Dat black girl mine on de Georgia line</em><br>
      <em>I'll haul de wood-pile down.</em></p>
  <p><em>De muskrat hide in de old burnt log</em><br>
      <em>Away down in Florida</em><br>
      <em>De chipmunk laugh at de old house dog</em><br>
      <em>I'll haul de wood-pile down</em><br>
      <em>Dar's Captain Jim of de old Bob Lee</em><br>
      <em>Away down in Florida</em><br>
      <em>He drinks more rum den he does hot tea</em><br>
      <em>I'll haul de wood-pile down</em></p>
  <p><em>CHORUS</em></p>
  <p><em>De old roof leaks and de rain comes thro'</em><br>
      <em>Away down in Florida</em><br>
      <em>De nig done die if he touch hoodoo</em><br>
      <em>I'll haul de wood-pile down</em><br>
      <em>When I grow wear den I lay down</em><br>
      <em>Away down in Florida</em><br>
      <em>De wench looks nice in a new clean gown</em><br>
  <em>Now haul de wood-pile down.</em></p>
  <h4>
    The night alarm. Holding's Military Band.
    <a href="search.php?nq=1&query_type=call_number&query=10482">U.S. Phonograph Co.: 355</a>. [1892].
  </h4>
  <p>
    &ldquo;Descriptive selections&rdquo; were sonic tapestries that came into favor in the late  1800s and faded in the early 1910s.
    Sometimes comprising music only and other times a combination of  music, voices, and sound effects, the subject matter of
    descriptive selections varied  widely, from auto races to the destruction of San Francisco in 1906. Far and away the most popular
    selection was <em>The Night Alarm</em>, and it's not hard to  understand why. Fire was the great  scourge of urbanizing America.
    Even  before the Chicago Fire of 1871, 400 large conflagrations struck  30 leading American cities. In 1907  alone,
    it was estimated that U.S. fires inflicted more than $215 million in  damage. <em>The Night Alarm</em> spoke to the fears of many.
    It also packed music, drama, jeopardy,  heroism, and triumph into two minutes of exciting multimedia.  </p>

  <h4>
    Wang's gavotte. Issler's Orchestra.
    <a href="search.php?nq=1&query_type=call_number&query=10481">U.S. Phonograph Co.: [catalog number unknown]</a>. [1893?].
  </h4>
  <p>
    The musical <em>Wang</em> opened in 1891, was revived in 1892, and had its most successful  run in 1904. Even though it was set in
    Siam, the show was basically a blend of comic opera and burlesque. It also included a gavotte, which was  particularly popular in the
    late 19th and early 20th  centuries. The gavotte originated as a  French folk dance. It was performed by  crossing the feet twice for each
    step, followed by a hop. In the United States, the gavotte took many  forms, musically and in terms of steps and formations. Over time,
    it also took on an undeserved air  of pretension as evidenced by the Ascot Gavotte in <em>My Fair Lady </em>and the lyric in the Carly
    Simon song <em>You're So Vain</em>:&ldquo;You had one eye in the mirror as you watched yourself gavotte.&rdquo; As evidenced by this lilting
    confection,  gavottes were informal, up tempo, and fun.  </p>

  <h4>
    The last rose of summer / Thomas Moore. Jules Levy.
    <a href="search.php?nq=1&query_type=call_number&query=10479">Edison Record: 1425</a>. [1894].
  </h4>
  <p>
    Just as many American boys born in the latter half of the 20th  century wanted to play the guitar, in the 1890s that same yearning applied to
    the cornet. Cornetists were the rock  stars of their era because they were often the lead musicians in concert bands. Jules Levy, who
    &ndash; according to legend &ndash;  learned to play with a mouthpiece because he was too poor to buy an instrument &ndash;  was probably
    the most famous of them all.  He was also arguably the first to be recorded, having made tinfoil  recordings for Edison in the late 1870s.
    By the time this cylinder was recorded in 1894, he was past his prime  (56 years old), but still anxious to demonstrate that he had the control
    and  lung power to perform this slow and highly sentimental version of <em>Last Rose of  Summer</em>. The melody dates back to the  early 1800s when it
    was written for an eponymous verse written by Thomas Moore,  a friend of Byron and Shelley. Friedrich  von Flotow used the song in the opera
    <em>Martha,</em> which opened in 1847, and it remained a popular, sentimental song until the  early 1900s.  </p>
</div><!-- end .content -->
<!-- End of file: incunabula2.tpl -->
{include file="footer.tpl"}
