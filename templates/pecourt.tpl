{include file="header.tpl"}
{include file="navigation.tpl"}
<!-- File: pecourt.tpl -->
<div class="content text">
	<h2>Edouard Pecourt Collection</h2>
	<div class="center-img-horiz">
	  <img class="pure-img" src="/images/pecourt.jpg" alt="La Boite a Disque">
	  <p class="img-description center-text">
	  	Edouard Pecourt in front of his  record shop <em>La Bo&icirc;te &agrave; Disques</em> in 1972.
	  </p>
	</div>
  <p>
  	The UCSB Library has acquired the cylinder collection of Edouard Pecourt, a Parisian record 
  	dealer and collector who owned  <em>La Bo&icirc;te &agrave; Disques</em> at 58 bis Rue du 
  	Louvre in Paris from the early 1950s until moving to Portland, Oregon in 1986.
  </p>
  <p>
  	Pecourt's extraordinary collection of over 3,000 cylinders, almost all French repertoire from 
  	the turn of the 20th century, includes hundreds  of cylinders issued by Path&eacute; including 
  	salon and concert (stentor) sizes, hundreds recorded by smaller companies such as Cylindres Dutreih, 
  	Soci&eacute;t&eacute; Francaise des Cylindres Artistiques, Phenix and other French companies, as well 
  	as hundreds of cylinders made by the French divisions of Edison and Columbia records. There are nearly 
  	1,000   brown wax cylinders in the collection.
  </p>
  <p>
  	Paris was one of the major centers of cylinder production and this collection  dramatically 
  	broadens the scope of the UCSB collection to more fully reflect the international nature of the 
  	early phonograph industry. We are delighted that we will eventually be able to bring this significant 
  	collection of French recordings to a wider audience.
  </p>
  <div class="center-img-horiz">
  	<img class="pure-img" src="/images/pecourt2.jpg" alt="Pecourt collection">
  	<p class="img-description center-text">
  		Cylinders after arriving at the UCSB Library's warehouse.
  	</p>
  </div>
  <p>
  	A full inventory and cataloging of the collection has commenced and cylinders will be added to 
  	the website as they are cataloged, at which point digitization of the collection will begin. 
  	Search for &quot;<a href="/search.php?nq=1&query=pecourt&query_type=keyword">Pecourt collection</a>&quot; to see what has been cataloged to date.
  </p>
</div><!-- end .content -->
<!-- End of file: pecourt.tpl -->   
{include file="footer.tpl"}