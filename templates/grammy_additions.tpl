{include file="header.tpl"}
{include file="navigation.tpl"}
<!-- File: grammy_additions.tpl -->
<div class="content text">
	<h2>GRAMMY Foundation funds digitization of 1,000 Cylinders (September 2010)</h2>
   <div class="center-img-horiz">
  	<img class="pure-img" src="/images/grammy_logo_blue.gif" alt="GRAMMY logo">
  </div>
  <p>
  	Thanks to a generous grant from the GRAMMY Foundation, the UCSB Library is pleased to announce an additional 1,000 cylinders have been digitized and made accessible online. A listing of cylinders digitized to date is below.
  </p>
  <ul>
    <li>Friendship gavotte. A. Schmehl. <a href=search.php?nq=1&query_type=call_number&query=8433>Indestructible Record: 978</a>. 1909.</li>
    <li>If I knock the "l" out of Kelly / Bert Grant, Sam M. Lewis and Joe Young. Ada Jones. <a href=search.php?nq=1&query_type=call_number&query=7096>Indestructible Record: 3371</a>. 1915.</li>
    <li>Come out of the kitchen, Mary Ann / James Kendis ; Charles Anthony Bayha. Ada Jones. <a href=search.php?nq=1&query_type=call_number&query=7594>Edison Blue Amberol: 3194</a>. 1917.</li>
    <li>The daughter of Rosie O'Grady / Walter Donaldson. Ada Jones. <a href=search.php?nq=1&query_type=call_number&query=7626>Edison Blue Amberol: 3500</a>. 1918.</li>
    <li>Bedtime at the zoo / Lionel Monkton. Ada Jones. <a href=search.php?nq=1&query_type=call_number&query=7660>Edison Blue Amberol: 2409</a>. 1914.</li>
    <li>All she gets from the iceman is ice / Alfred Solman. Ada Jones. <a href=search.php?nq=1&query_type=call_number&query=7786>Edison Record: 9859</a>. 1908.</li>
    <li>He lost er in the subway / S. R. Henry. Ada Jones. <a href=search.php?nq=1&query_type=call_number&query=7788>Edison Gold Moulded Record: 9678</a>. 1907.</li>
    <li>Henny Klein / William Jerome ; Jean Schwartz. Ada Jones. <a href=search.php?nq=1&query_type=call_number&query=7791>Edison Gold Moulded Record: 9195</a>. 1906.</li>
    <li>That's how I lost him / Raymond A. Browne. Ada Jones. <a href=search.php?nq=1&query_type=call_number&query=7853>Edison Blue Amberol: 2190</a>. 1913.</li>
    <li>Hugo / Ted Snyder. Ada Jones. <a href=search.php?nq=1&query_type=call_number&query=7919>Edison Record: 9928</a>. 1908.</li>
    <li>He never even said good bye / Albert Gumble. Ada Jones. <a href=search.php?nq=1&query_type=call_number&query=7922>Edison Gold Moulded Record: 9603</a>. 1907.</li>
    <li>Smarty  / Albert Von Tilzer. Ada Jones. <a href=search.php?nq=1&query_type=call_number&query=8043>Edison Gold Moulded Record: 9872</a>. 1908.</li>
    <li>Virginia song / George M. Cohan. Ada Jones. <a href=search.php?nq=1&query_type=call_number&query=8046>Edison Gold Moulded Record: 9294</a>. 1906.</li>
    <li>I've got the finest man / James Reese Europe. Ada Jones. <a href=search.php?nq=1&query_type=call_number&query=8418>Indestructible Record: 3287</a>. 1912.</li>
    <li>I've got rings on my fingers / Maurice Scott. Ada Jones. <a href=search.php?nq=1&query_type=call_number&query=8760>Indestructible Record: 1156</a>. 1909.</li>
    <li>Who puts me in my little bed? / Harry Von Tilzer. Ada Jones. <a href=search.php?nq=1&query_type=call_number&query=8935>Edison Blue Amberol: 1611</a>. 1913.</li>
    <li>Put on your slippers, you're in for the night / Seymour Furth. Ada Jones. <a href=search.php?nq=1&query_type=call_number&query=9079>Edison Blue Amberol: 1879</a>. 1913.</li>
    <li>When you steal a kiss or two / Kenneth S. Clark. Ada Jones ; Billy Murray. <a href=search.php?nq=1&query_type=call_number&query=7734>Indestructible Record: 744</a>. 1908.</li>
    <li>Snow deer  / Percy Wenrich. Ada Jones ; Billy Murray. <a href=search.php?nq=1&query_type=call_number&query=8080>Edison Blue Amberol: 2021</a>. 1913.</li>
    <li>Chimmie and Maggie at "The merry widow". Ada Jones ; Len Spencer. <a href=search.php?nq=1&query_type=call_number&query=7915>Edison Gold Moulded Record: 9820</a>. 1908.</li>
    <li>Broncho Bob and his little Cheyenne. Ada Jones ; Len Spencer. <a href=search.php?nq=1&query_type=call_number&query=8000>Edison Gold Moulded Record: 9720</a>. 1907.</li>
    <li>Mandy and her man. Ada Jones ; Len Spencer. <a href=search.php?nq=1&query_type=call_number&query=8002>Edison Gold Moulded Record: 9236</a>. 1906.</li>
    <li>I'll get you. Ada Jones ; Walter Van Brunt. <a href=search.php?nq=1&query_type=call_number&query=7909>U.S. Everlasting Record: 1644</a>. 1912.</li>
    <li>When I get you alone tonight. Ada Jones ; Walter Van Brunt. <a href=search.php?nq=1&query_type=call_number&query=8201>U.S. Everlasting Record: 1602</a>. [1909].</li>
    <li>I'm looking for a sweetheart, and I think you'll do / Manuel Klein and  R. H. Burnside. Ada Jones & Billy Murray. <a href=search.php?nq=1&query_type=call_number&query=7101>Indestructible Record: 1024</a>. 1909.</li>
    <li>Henny and Hilda at the Schotzenfest. Ada Jones & Len Spencer. <a href=search.php?nq=1&query_type=call_number&query=8505>Indestructible Record: 832</a>. 1908.</li>
    <li>Modest manicure. Ada Jones & Len Spencer. <a href=search.php?nq=1&query_type=call_number&query=8507>Indestructible Record: 792</a>. 1908.</li>
    <li>I wish that you belonged to me / Raymond A. Browne. Ada Jones and Billy Murray. <a href=search.php?nq=1&query_type=call_number&query=7145>Edison Blue Amberol: 2069</a>. 1913.</li>
    <li>I'm looking for a sweetheart and I think you'll do / Manuel Klein. Ada Jones and Billy Murray. <a href=search.php?nq=1&query_type=call_number&query=8060>Edison Blue Amberol: Special G</a>. 1913.</li>
    <li>The courtship of Barney and Eileen. Ada Jones and Len Spencer. <a href=search.php?nq=1&query_type=call_number&query=7783>Edison Gold Moulded Record: 9143</a>. 1905.</li>
    <li>A race for a wife. Ada Jones and Len Spencer. <a href=search.php?nq=1&query_type=call_number&query=7824>Edison Blue Amberol: 3857</a>. 1919.</li>
    <li>Cupid's I.O.U. / George Meyer. Ada Jones and Walter Van Brunt. <a href=search.php?nq=1&query_type=call_number&query=7059>Indestructible Record: 1343</a>. 1910.</li>
    <li>Der Balzer Beim Sachenhauser Aebbelwei / Adam Müller. Adam Müller. <a href=search.php?nq=1&query_type=call_number&query=2562>Edison Goldguss Walze: 15581</a>. 1907.</li>
    <li>My song shall be alway Thy mercy - Hymn of praise / Felix Mendelssohn-Bartholdy. Agnes Kimball ; Reed Miller. <a href=search.php?nq=1&query_type=call_number&query=7628>Edison Blue Amberol: 1579</a>. 1912.</li>
    <li>The lyre-bird and the jay / unknown. Agnes Kimball and Harry McClaskey (Henry Burr). <a href=search.php?nq=1&query_type=call_number&query=7030>U.S. Everlasting Record: 1600</a>. 1909.</li>
    <li>She walks in her husband's sleep / Harry Von Tilzer. Aileen Stanley. <a href=search.php?nq=1&query_type=call_number&query=8048>Edison Blue Amberol: 4233</a>. 1921.</li>
    <li>Where-is-my-daddy-now-blues  / Otto Motzan ; Abe Olman. Aileen Stanley. <a href=search.php?nq=1&query_type=call_number&query=8049>Edison Blue Amberol: 4204</a>. 1921.</li>
    <li>I'm nobody's baby / Benny Davis, Milton Ager,  Lester Santley. Aileen Stanley. <a href=search.php?nq=1&query_type=call_number&query=8119>Edison Blue Amberol: 4327</a>. 1921.</li>
    <li>I'm looking for a bluebird / Fred Rich. Aileen Stanley. <a href=search.php?nq=1&query_type=call_number&query=8131>Edison Blue Amberol: 4413</a>. 1922.</li>
    <li>Anna in Indiana / Billy Gorman ; Eddie Gorman ; Harry Rose. Aileen Stanley. <a href=search.php?nq=1&query_type=call_number&query=8134>Edison Blue Amberol: 4349</a>. 1921.</li>
    <li>Chasin' the blues. Al Bernard. <a href=search.php?nq=1&query_type=call_number&query=7799>Edison Blue Amberol: 3949</a>. 1920.</li>
    <li>St. Louis blues / William Christopher Handy. Al Bernard. <a href=search.php?nq=1&query_type=call_number&query=7804>Edison Blue Amberol: 3930</a>. 1920.</li>
    <li>I want to hold you in my arms / J. Russel Robinson. Al Bernard ; Ernest Hare. <a href=search.php?nq=1&query_type=call_number&query=7369>Edison Blue Amberol: 3773</a>. 1919.</li>
    <li>You're my gal / Al Bernard. Al Bernard ; Ernest Hare. <a href=search.php?nq=1&query_type=call_number&query=7588>Edison Blue Amberol: 6936</a>. 1919.</li>
    <li>My lovin' sing song man / Al Bernard . Al Bernard ; Frank M. Kamplain. <a href=search.php?nq=1&query_type=call_number&query=7967>Edison Blue Amberol: 4071</a>. 1920.</li>
    <li>Oh yeedle ay / Fred Fisher ; Irving Maslof. Al Bernard ; Frank M. Kamplain. <a href=search.php?nq=1&query_type=call_number&query=8114>Edison Blue Amberol: 4305</a>. 1921.</li>
    <li>Swanee / George Gershwin. Al Bernard and Frank M. Kamplain. <a href=search.php?nq=1&query_type=call_number&query=7771>Edison Blue Amberol: 3988</a>. 1920.</li>
    <li>Venetian song [Canzone veneziana] / Francesco Paolo Tosti. Alan Turner. <a href=search.php?nq=1&query_type=call_number&query=8149>Edison Blue Amberol: 1640</a>. 1913.</li>
    <li>The chapel in the woods / Gustav Lange. Albert Benzler. <a href=search.php?nq=1&query_type=call_number&query=6968>Edison Gold Moulded Record: 9653</a>. 1907.</li>
    <li>Belle of the east / unknown. Albert Benzler. <a href=search.php?nq=1&query_type=call_number&query=7008>U.S. Everlasting Record: 427</a>. 1912.</li>
    <li>Happy Lena polka / Heidelberg. Albert Benzler. <a href=search.php?nq=1&query_type=call_number&query=7784>Edison Blue Amberol: 9397</a>. 1906.</li>
    <li>Stephanie gavotte / Alphons Czibulka. Albert Benzler. <a href=search.php?nq=1&query_type=call_number&query=7897>U.S. Everlasting Record: 1621</a>. 1912.</li>
    <li>Ideas and ripples. Albert Benzler. <a href=search.php?nq=1&query_type=call_number&query=7899>U.S. Everlasting Record: 1596</a>. between 1909 and 1912.</li>
    <li>Beaumarie . Albert Benzler. <a href=search.php?nq=1&query_type=call_number&query=7989>Edison Gold Moulded Record: 8536</a>. 1903.</li>
    <li>Ancient court whisperings. Albert Benzler. <a href=search.php?nq=1&query_type=call_number&query=8475>U.S. Everlasting Record: 1506</a>. 1909.</li>
    <li>Killarney from "Rogers brothers in Ireland" / Max Hoffmann. Albert Benzler. <a href=search.php?nq=1&query_type=call_number&query=9226>Edison Gold Moulded Record: 9165</a>. 1905.</li>
    <li>Bugle calls. Albert C. Sweet. <a href=search.php?nq=1&query_type=call_number&query=8010>Edison Record: 2435</a>. 1898.</li>
    <li>Don't bite the hand that's feeding you / Jimmie Morgan. Albert Campbell ; Henry Burr. <a href=search.php?nq=1&query_type=call_number&query=8020>Indestructible Record: 3378</a>. 1916.</li>
    <li>Harmony Bay / Terry Sherman. Albert Campbell and Henry Burr. <a href=search.php?nq=1&query_type=call_number&query=8062>Edison Blue Amberol: 2372</a>. 1914.</li>
    <li>It’s a long, long way to Tipperary / Harry Williams. Albert Farrington. <a href=search.php?nq=1&query_type=call_number&query=7167>Edison Blue Amberol: 2487</a>. 1914.</li>
    <li>Waiting down by the Mississippi shore  / David Reed. Albert H. Campbell ; Henry Burr. <a href=search.php?nq=1&query_type=call_number&query=7993>Edison Blue Amberol: 15028</a>. 1913.</li>
    <li>Macushla / Dermot Macmurrough ; Josephine V. Rowe. Albert Lindquest. <a href=search.php?nq=1&query_type=call_number&query=7764>Edison Blue Amberol: 3868</a>. 1919.</li>
    <li>The song that reached my heart / Jordan Jules. Albert Pike. <a href=search.php?nq=1&query_type=call_number&query=7942>Sterling Record: 902</a>. [1906].</li>
    <li>Valse sentimentale / Franz Schubert. Albert Spalding ; André Benoist. <a href=search.php?nq=1&query_type=call_number&query=7954>Edison Royal Purple Amberol: 29058</a>. 1920.</li>
    <li>Elle ne croyait pas / Ambroise Thomas. Albert Vaguet. <a href=search.php?nq=1&query_type=call_number&query=9591>Pathé: 3754</a>. 1905.</li>
    <li>Les prunes / Justin Clérice. Albert Vaguet. <a href=search.php?nq=1&query_type=call_number&query=9648>Pathé: 3692</a>. 1902 or 1903.</li>
    <li>I call you from the shadows / Hughes Macklin. Albert W. Ketèlbey. <a href=search.php?nq=1&query_type=call_number&query=7564>Edison Blue Amberol: 23205</a>. 1913.</li>
    <li>Spanish rhapsody / S. Salvetti. Alessios Mandolin Quartet. <a href=search.php?nq=1&query_type=call_number&query=8126>Edison Blue Amberol: 2876</a>. 1916.</li>
    <li>The butterfly / Theo Bendix. Alexander Prince. <a href=search.php?nq=1&query_type=call_number&query=7579>Edison Blue Amberol: 23081</a>. 1913.</li>
    <li>Nazareth / Charles Gounod. Alexander Prince. <a href=search.php?nq=1&query_type=call_number&query=7634>Edison Blue Amberol: 23055</a>. 1913.</li>
    <li>Hoch Hapsburg: march / Johann Nepomuk Král. Alexander Prince. <a href=search.php?nq=1&query_type=call_number&query=7877>Edison Record: 13791</a>. 1908.</li>
    <li>Forgotten melodies. Alexander Prince. <a href=search.php?nq=1&query_type=call_number&query=7884>Edison Blue Amberol: 23128</a>. 1909.</li>
    <li>Il bacio / Luigi Arditi. Alexander Prince. <a href=search.php?nq=1&query_type=call_number&query=8631>Sterling Record: 399</a>. ?.</li>
    <li>Merry widow waltz / Franz Lehár. Alexander Prince. <a href=search.php?nq=1&query_type=call_number&query=8701>Edison Blue Amberol: 23111</a>. 1913 or 1914.</li>
    <li>The despatch rider / Richard Eilenberg. Alexander Prince. <a href=search.php?nq=1&query_type=call_number&query=8862>Edison Record: 13902</a>. 1909.</li>
    <li>Oi ya nestchastay (Malo russkaya piesnia) / Mykola Vitaliiovych Lysenko. Alexander Sashko ; A. Iranova. <a href=search.php?nq=1&query_type=call_number&query=8015>Edison Blue Amberol: 11234</a>. 1922.</li>
    <li>The sunshine of your smile / Lilian Ray. Alfred D. Shaw. <a href=search.php?nq=1&query_type=call_number&query=7709>Edison Blue Amberol: 23383</a>. 1915.</li>
    <li>The dollar princess waltz / Leo Fall. Alhambra Orchestra. <a href=search.php?nq=1&query_type=call_number&query=7605>Edison Blue Amberol: 23122</a>. 1913.</li>
    <li>In the shadows / Herman Finck. Alhambra Orchestra. <a href=search.php?nq=1&query_type=call_number&query=7632>Edison Blue Amberol: 23048</a>. 1913.</li>
    <li>The choristers waltz / Bernard Phelps. Alhambra Orchestra. <a href=search.php?nq=1&query_type=call_number&query=7696>Edison Blue Amberol: 23025</a>. 1913.</li>
    <li>Stabat Mater : Qui est homo / Gioacchino Rossini. Alice Verlet and Margaret Matzenauer. <a href=search.php?nq=1&query_type=call_number&query=9873>Edison Royal Purple Amberol: 29036</a>. 1915.</li>
    <li>Sand dunes: one step / Byron Gay. All Star Trio. <a href=search.php?nq=1&query_type=call_number&query=7768>Edison Blue Amberol: 3696</a>. 1919.</li>
    <li>Cleo: fox trot / Lee S. Roberts. All Star Trio. <a href=search.php?nq=1&query_type=call_number&query=7822>Edison Blue Amberol: 3902</a>. 1920.</li>
    <li>Lucille: fox trot / F. Wheeler Wadsworth ; Victor Arden. All Star Trio. <a href=search.php?nq=1&query_type=call_number&query=8136>Edison Blue Amberol: 3952</a>. 1920.</li>
    <li>Just blue / Victor Arden. All-Star Trio. <a href=search.php?nq=1&query_type=call_number&query=7773>Edison Blue Amberol: 3624</a>. 1919.</li>
    <li>That old Irish mother of mine / Harry Von Tilzer. Allen McQuhae. <a href=search.php?nq=1&query_type=call_number&query=7670>Edison Blue Amberol: 4236</a>. 1921.</li>
    <li>American war songs - no. 2 / unknown. American Brass Quartet. <a href=search.php?nq=1&query_type=call_number&query=7663>Edison Blue Amberol: 3215</a>. 1917.</li>
    <li>Oh Johnny, oh Johnny, oh / Abe Olman ; Ed Rose. American Quartet. <a href=search.php?nq=1&query_type=call_number&query=7621>Edison Blue Amberol: 3237</a>. 1917.</li>
    <li>I was born in Michigan / Malvin M. Franklin. American Quartet. <a href=search.php?nq=1&query_type=call_number&query=7972>Edison Blue Amberol: 4324</a>. 1921.</li>
    <li>Pocahontas / J. A. MacMeekin. American Quartet. <a href=search.php?nq=1&query_type=call_number&query=8056>Edison Blue Amberol: 4232</a>. 1921.</li>
    <li>Onward Christian soldiers  / Sir Arthur Sullivan. American Quartet. <a href=search.php?nq=1&query_type=call_number&query=8167>Lambert: 646</a>. 190-.</li>
    <li>Zampa overture - part 2 / Ferdinand Hérold . American Symphony Orchestra. <a href=search.php?nq=1&query_type=call_number&query=6960>Edison Blue Amberol: 3686</a>. 1919.</li>
    <li>Barcarolle - Tales of Hoffman / Jacques Offenbach. American Symphony Orchestra. <a href=search.php?nq=1&query_type=call_number&query=7602>Edison Blue Amberol: 3070</a>. 1917.</li>
    <li>Artist's life waltz / Johann Strauss. American Symphony Orchestra. <a href=search.php?nq=1&query_type=call_number&query=7810>Edison Blue Amberol: 3647</a>. 1919.</li>
    <li>The hermit's bell overture / Aimé Maillart. American Symphony Orchestra. <a href=search.php?nq=1&query_type=call_number&query=8117>Edison Blue Amberol: Special J</a>. 1913.</li>
    <li>Ride of the Valkyries / Richard Wagner. American Symphony Orchestra. <a href=search.php?nq=1&query_type=call_number&query=9878>Edison Blue Amberol: 4802</a>. 1924.</li>
    <li>Old folks at home / Stephen Collins Foster. Anna Case. <a href=search.php?nq=1&query_type=call_number&query=9865>Edison Blue Amberol: 28260</a>. 1917.</li>
    <li>Kathleen Mavourneen /  F. Nicholls Crouch. Anna Pinto. <a href=search.php?nq=1&query_type=call_number&query=8132>Edison Blue Amberol: 4699</a>. 1923.</li>
    <li>Bras d’ssus, bras d’ssous / Robert Planquette. Anne Judic. <a href=search.php?nq=1&query_type=call_number&query=6245>Pathé: 3407</a>. 190-.</li>
    <li>She sleeps 'neath the Old Ohio River / Alfred Solman. Anthony & Harrison. <a href=search.php?nq=1&query_type=call_number&query=7974>Edison Blue Amberol: 1789</a>. 1913.</li>
    <li>El Toreador - Carmen / Georges Bizet. Antonio Vargas. <a href=search.php?nq=1&query_type=call_number&query=7754>Edison Gold Moulded Record: 7754</a>. 190-?.</li>
    <li>Annie Laurie / John Douglas Scott. Archie Anderson. <a href=search.php?nq=1&query_type=call_number&query=7745>Indestructible Record: 3148</a>. 1910.</li>
    <li> The flowers that we love ; Mariette / Octave Crémieux. Armand Vecsey and His Hungarian Orchestra. <a href=search.php?nq=1&query_type=call_number&query=7615>Edison Blue Amberol: 28156</a>. 1913.</li>
    <li>Valse pathetique /  Domenico Savino. Armand Vecsey and His Hungarian Orchestra. <a href=search.php?nq=1&query_type=call_number&query=7654>Edison Blue Amberol: 2856</a>. 1916.</li>
    <li>Chaperons. My Sambo / Isidore Witmark. Arthur Collins. <a href=search.php?nq=1&query_type=call_number&query=6792>Edison Gold Moulded Record: 8123</a>. 1902.</li>
    <li>Mother hasn't spoken to father since / Jean Schwartz, William Jerome. Arthur Collins. <a href=search.php?nq=1&query_type=call_number&query=6986>Edison Record: 9898</a>. 1908/1909.</li>
    <li>Down at the huskin' bee / unknown. Arthur Collins. <a href=search.php?nq=1&query_type=call_number&query=7019>Indestructible Record: 1096</a>. 1909.</li>
    <li>I apologize. Arthur Collins. <a href=search.php?nq=1&query_type=call_number&query=8014>Indestructible Record: 3150</a>. 1910.</li>
    <li>Everybody's doin' it now  / Irving Berlin. Arthur Collins. <a href=search.php?nq=1&query_type=call_number&query=8025>Indestructible Record: 3265</a>. 191-.</li>
    <li>Nobody  / Bert Williams. Arthur Collins. <a href=search.php?nq=1&query_type=call_number&query=8075>Edison Gold Moulded Record: 9084</a>. 1905.</li>
    <li>By the Swanee River / W. H. Myddleton. Arthur Collins. <a href=search.php?nq=1&query_type=call_number&query=8461>Indestructible Record: 3079</a>. 1910.</li>
    <li>Take plenty of shoes. Arthur Collins. <a href=search.php?nq=1&query_type=call_number&query=8504>Indestructible Record: 962</a>. 1909.</li>
    <li>The white wash man / Jean Schwartz. Arthur Collins. <a href=search.php?nq=1&query_type=call_number&query=8508>Indestructible Record: 1073</a>. 1909.</li>
    <li>The preacher and the bear / Joe Arzonia. Arthur Collins. <a href=search.php?nq=1&query_type=call_number&query=8510>U.S. Everlasting Record: 1092</a>. 1909.</li>
    <li>All I wants is ma chickens / Jack Wilson. Arthur Collins. <a href=search.php?nq=1&query_type=call_number&query=8650>Edison Record: 5425</a>. 1898 or 1899.</li>
    <li>Cubanola glide / Harry Von Tilzer. Arthur Collins ; Byron G. Harlan. <a href=search.php?nq=1&query_type=call_number&query=7321>Indestructible Record: 1305</a>. 1910.</li>
    <li>The troubles of the Reuben and the maid. Arthur Collins ; Byron G. Harlan. <a href=search.php?nq=1&query_type=call_number&query=7756>Edison Gold Moulded Record: 8239</a>. 1902.</li>
    <li>Hey! Mister Joshua / Lester W. Kieth. Arthur Collins ; Byron G. Harlan. <a href=search.php?nq=1&query_type=call_number&query=8004>Edison Gold Moulded Record: 9007</a>. 1905.</li>
    <li>Moonlight in jungleland / J. E. Dempsey. Arthur Collins ; Byron G. Harlan. <a href=search.php?nq=1&query_type=call_number&query=8008>Indestructible Record: 3071</a>. 191-.</li>
    <li>With his umpah umpah on the Umpah Isle / Harry Von Tilzer. Arthur Collins ; Byron G. Harlan. <a href=search.php?nq=1&query_type=call_number&query=8127>Edison Blue Amberol: 4647</a>. 1922.</li>
    <li>They were all doing the same. Arthur Collins ; Byron G. Harlan. <a href=search.php?nq=1&query_type=call_number&query=8205>U.S. Everlasting Record: 311</a>. [1908].</li>
    <li>Three pickaninnies / unknown. Arthur Collins and Byron G. Harlan. <a href=search.php?nq=1&query_type=call_number&query=6954>Edison Blue Amberol: 3518</a>. 1918.</li>
    <li>Down where the big bananas grow / Ted. S. Barron. Arthur Collins and Byron G. Harlan. <a href=search.php?nq=1&query_type=call_number&query=7011>Indestructible Record: 3011</a>. 1910.</li>
    <li>On a monkey honeymoon / Theodore Morse. Arthur Collins and Byron G. Harlan. <a href=search.php?nq=1&query_type=call_number&query=7077>Indestructible Record: 1089</a>. 1909.</li>
    <li>Some day Melinda / Bert Fitzgibbon. Arthur Collins and Byron G. Harlan. <a href=search.php?nq=1&query_type=call_number&query=8032>Edison Standard Record: 10326</a>. 1910.</li>
    <li>Down in jungletown / Theodore F. Morse. Arthur Collins and Byron G. Harlan. <a href=search.php?nq=1&query_type=call_number&query=8053>Indestructible Record: 816</a>. 1908.</li>
    <li>Mammy Jinny's jubilee / Lewis F. Muir. Arthur Collins, Byron G. Harlan. <a href=search.php?nq=1&query_type=call_number&query=7017>Edison Blue Amberol: 2032</a>. 1913.</li>
    <li>On the Hoko Moko Isle / Harry Von Tilzer. Arthur Collins; Byron G. Harlan. <a href=search.php?nq=1&query_type=call_number&query=7595>Edison Blue Amberol: 2894</a>. 1916.</li>
    <li>Auntie Skinner's Chicken Dinner / Theodore F. Morse ; Harry Carroll. Arthur F. Collins ; Byron G. Harlan. <a href=search.php?nq=1&query_type=call_number&query=8146>Edison Blue Amberol: 2637</a>. 1915.</li>
    <li>Oh Helen! / Carey Morgan ; Chas R. McCarron. Arthur Fields. <a href=search.php?nq=1&query_type=call_number&query=7656>Edison Blue Amberol: 3713</a>. 1919.</li>
    <li>Dinky doo / Alfred Ellerton. Arthur Gilbert. <a href=search.php?nq=1&query_type=call_number&query=8168>Edison Gold Moulded Record: 13691</a>. 1907.</li>
    <li>When you're all dressed up and no place to go / Silvio Hein. Arthur Hawley. <a href=search.php?nq=1&query_type=call_number&query=8420>Indestructible Record: 3347</a>. 1914.</li>
    <li>Ack! i Arkadien : Ur "Gluntarne" / Gunnar Wennerber. Arvid Asplund. <a href=search.php?nq=1&query_type=call_number&query=7965>Edison Blue Amberol: 9425</a>. 1905.</li>
    <li>Här är gudagott att vara  :Gluntarne / Gunnar Wennerber. Arvid Asplund ; Albert Arveschong. <a href=search.php?nq=1&query_type=call_number&query=7948>Edison Record: 18830</a>. 1905.</li>
    <li>Fate - fox trot / Byron Gay. Atlantic Dance Orchestra. <a href=search.php?nq=1&query_type=call_number&query=7001>Edison Blue Amberol: 4709</a>. 1923.</li>
    <li>Street piano medley. August Molinari. <a href=search.php?nq=1&query_type=call_number&query=7787>Edison Gold Moulded Record: 9615</a>. 1907.</li>
    <li>Little charmer. August Schmehl. <a href=search.php?nq=1&query_type=call_number&query=8024>Indestructible Record: 1170</a>. 1909.</li>
    <li>Pass dat possum. August Schmehl. <a href=search.php?nq=1&query_type=call_number&query=8484>Indestructible Record: 1208</a>. 1909.</li>
    <li>Wild cherries / Ted Snyder. August Schmehl. <a href=search.php?nq=1&query_type=call_number&query=8485>Indestructible Record: 1147</a>. 1909.</li>
    <li>Besos y pesos. Banda de Artilleria. <a href=search.php?nq=1&query_type=call_number&query=9122>Edison Blue Amberol: 22038</a>. 191-?.</li>
    <li>Quién sabe? two step / G. Y. Brussel. Banda de Artilléria. <a href=search.php?nq=1&query_type=call_number&query=9114>Edison Blue Amberol: 22083</a>. 191-?.</li>
    <li>La campana de la independencia / Ernesto Elorduy. Banda de Artillería de México. <a href=search.php?nq=1&query_type=call_number&query=9072>Edison Blue Amberol: 22084</a>. 1913.</li>
    <li>El dos de mayo. Banda de Policia de Mexico. <a href=search.php?nq=1&query_type=call_number&query=8873>Edison Blue Amberol: 2218</a>. 191-.</li>
    <li>Serenade des coucous. Bergeret. <a href=search.php?nq=1&query_type=call_number&query=7986>[unknown publisher]</a>. 189-?.</li>
    <li>Timuctoo / Bert Kalmar, Harry Ruby. Bert Kalmar, Harry Ruby. <a href=search.php?nq=1&query_type=call_number&query=8143>Edison Blue Amberol: 4228</a>. 1921.</li>
    <li>Turkey in the straw / Otto Bonnell. Billy Golden. <a href=search.php?nq=1&query_type=call_number&query=6990>Indestructible Record: 941</a>. 1908.</li>
    <li>Turkey in the straw. Billy Golden. <a href=search.php?nq=1&query_type=call_number&query=7997>Edison Gold Moulded Record: 8293</a>. 1903.</li>
    <li>Clamy Green / unknown. Billy Golden ;  Joe Hughes. <a href=search.php?nq=1&query_type=call_number&query=7644>Edison Blue Amberol: 1837</a>. 1913.</li>
    <li>Bear's oil. Billy Golden & Joe Hughes. <a href=search.php?nq=1&query_type=call_number&query=8506>Indestructible Record: 1111</a>. 1909.</li>
    <li>Joining the church. Billy Golden & Joe Hughes. <a href=search.php?nq=1&query_type=call_number&query=8524>U.S. Everlasting Record: 1136</a>. 1909.</li>
    <li>Yes! We have no bananas / Frank Silver ; Irving Cohn. Billy Jones. <a href=search.php?nq=1&query_type=call_number&query=9861>Edison Blue Amberol: 4778</a>. 1923.</li>
    <li>Peggy O'Neil / Ed G. Nelson
      . Billy Jones . <a href=search.php?nq=1&query_type=call_number&query=7645>Edison Blue Amberol: 4328</a>. 1921.</li>
    <li>Ten little fingers and ten little toes (down in Tennessee) / Harry Pease ; Johnny White. Billy Jones ;  Ernest Hare. <a href=search.php?nq=1&query_type=call_number&query=7649>Edison Blue Amberol: 4427</a>. 1922.</li>
    <li>A prairie life for me / Billy Merson. Billy Merson. <a href=search.php?nq=1&query_type=call_number&query=7614>Edison Blue Amberol: 23206</a>. 1914.</li>
    <li>The gay cavalier / Billy Merson. Billy Merson. <a href=search.php?nq=1&query_type=call_number&query=9869>Edison Blue Amberol: 23094</a>. 1913.</li>
    <li>The makin's of the U.S.A / Harry Von Tilzer. Billy Murray. <a href=search.php?nq=1&query_type=call_number&query=6952>Edison Blue Amberol: 3506</a>. 1918.</li>
    <li>We'll have a jubilee in my old Kentucky home / Walter Donaldson. Billy Murray. <a href=search.php?nq=1&query_type=call_number&query=6995>Edison Blue Amberol: 2748</a>. 1915.</li>
    <li>If you can't sing the words, you must whistle the tune / Herman Darewski. Billy Murray. <a href=search.php?nq=1&query_type=call_number&query=7681>Edison Blue Amberol: 2732</a>. 1915.</li>
    <li>I think I oughtn't auto any more. Billy Murray. <a href=search.php?nq=1&query_type=call_number&query=7733>Indestructible Record: 656</a>. 1907.</li>
    <li>Cheyenne ; Shy Ann. Billy Murray. <a href=search.php?nq=1&query_type=call_number&query=7757>Columbia Phonograph Co.: 32944</a>. 1906.</li>
    <li>The little Ford rambled right along / Byron Gay. Billy Murray. <a href=search.php?nq=1&query_type=call_number&query=7776>Edison Blue Amberol: 2556</a>. 1915.</li>
    <li>In Washington / Hoffman. Billy Murray. <a href=search.php?nq=1&query_type=call_number&query=7780>Edison Gold Moulded Record: 9558</a>. 1907.</li>
    <li>How long have you been married? / Rennie Cormack. Billy Murray. <a href=search.php?nq=1&query_type=call_number&query=7855>Edison Blue Amberol: 2162</a>. 1913.</li>
    <li>Sullivan / George M. Cohan. Billy Murray. <a href=search.php?nq=1&query_type=call_number&query=7914>Edison Gold Moulded Record: 10060</a>. 1909.</li>
    <li>You're a grand old flag / George M. Cohan. Billy Murray. <a href=search.php?nq=1&query_type=call_number&query=7920>Edison Gold Moulded Record: 9256</a>. 1906.</li>
    <li> I was a hero too / Egbert Van Alstyne. Billy Murray. <a href=search.php?nq=1&query_type=call_number&query=7924>Edison Gold Moulded Record: 9880</a>. 1908.</li>
    <li>Kiss me good night / Joe Goodwin. Billy Murray. <a href=search.php?nq=1&query_type=call_number&query=9069>Edison Blue Amberol: 2148</a>. 1913.</li>
    <li>You said something - Have a heart / Jerome Kern. Billy Murray ; Gladys Rice. <a href=search.php?nq=1&query_type=call_number&query=7699>Edison Blue Amberol: 3300</a>. 1917.</li>
    <li>The last long mile - Toot-toot! / Emil Breitenfeld. Billy Murray and Chorus. <a href=search.php?nq=1&query_type=call_number&query=7678>Edison Blue Amberol: 3513</a>. 1918.</li>
    <li>Angostura. Belle of Boulter's lock / Billy Whitlock. Billy Whitlock. <a href=search.php?nq=1&query_type=call_number&query=9055>Edison Amberol: 12475</a>. 1912.</li>
    <li>Good-bye ragtime / Fred Godfrey. Billy Williams. <a href=search.php?nq=1&query_type=call_number&query=7565>Edison Blue Amberol: 23289</a>. 1914.</li>
    <li>Jean loves all the jockeys / Fred Godfrey. Billy Williams. <a href=search.php?nq=1&query_type=call_number&query=7572>Edison Blue Amberol: 23153</a>. 1913.</li>
    <li>I must go home tonight / William Hargreaves. Billy Williams. <a href=search.php?nq=1&query_type=call_number&query=7581>Edison Blue Amberol: 23015</a>. 1912 - 1929.</li>
    <li>I'm out for the day today / Fred Godfrey. Billy Williams. <a href=search.php?nq=1&query_type=call_number&query=7590>Edison Blue Amberol: 23236</a>. 1914.</li>
    <li>Blame it on to poor old father / Fred Godfrey. Billy Williams. <a href=search.php?nq=1&query_type=call_number&query=7613>Edison Blue Amberol: 23202</a>. 1914.</li>
    <li>My young man is not the chocolate soldier / Fred Godfrey. Billy Williams. <a href=search.php?nq=1&query_type=call_number&query=7622>Edison Blue Amberol: 23024</a>. 1913.</li>
    <li>Wait till I'm as old as father / Fred Godfrey. Billy Williams. <a href=search.php?nq=1&query_type=call_number&query=7655>Edison Blue Amberol: 23042</a>. 1913.</li>
    <li>Something nice about the Isle of Man / Fred Godfrey. Billy Williams. <a href=search.php?nq=1&query_type=call_number&query=7703>Edison Blue Amberol: 23124</a>. 1913.</li>
    <li>The worst of its I like it / Fred Godfrey. Billy Williams. <a href=search.php?nq=1&query_type=call_number&query=7716>Edison Blue Amberol: 23252</a>. 1914.</li>
    <li>Oh! Molly Macintyre / Fred Godfrey ; Billy Williams. Billy Williams. <a href=search.php?nq=1&query_type=call_number&query=7725>Edison Blue Amberol: 23161</a>. 1913.</li>
    <li>All the ladies fell in love with Sandy / Fred Godfrey ; Billy Williams. Billy Williams. <a href=search.php?nq=1&query_type=call_number&query=7839>Edison Blue Amberol: 23133</a>. 1913.</li>
    <li>Wild woodbines / Michael W. Balfe ; Alfred Lord Tennyson. Billy Williams. <a href=search.php?nq=1&query_type=call_number&query=7859>Edison Blue Amberol: 23027</a>. 1909.</li>
    <li>What time tomorrow night? / Fred Godfrey ; Billy Williams. Billy Williams. <a href=search.php?nq=1&query_type=call_number&query=7864>Edison Blue Amberol: 23279</a>. 1914.</li>
    <li>I must go home tonight / William Hargreaves. Billy Williams. <a href=search.php?nq=1&query_type=call_number&query=7868>Edison Amberol: 12056</a>. 1910.</li>
    <li>She does like a little bit of scotch / Fred Godfrey ; Billy Williams. Billy Williams. <a href=search.php?nq=1&query_type=call_number&query=7881>Edison Blue Amberol: 23113</a>. 1913- 1914.</li>
    <li>Let's all go mad / Fred Godfrey. Billy Williams. <a href=search.php?nq=1&query_type=call_number&query=9049>Edison Amberol: 12405</a>. 1911.</li>
    <li>Molly: fox trot / Ray Miller. Black and White Melody Boys. <a href=search.php?nq=1&query_type=call_number&query=8085>Edison Blue Amberol: 4344</a>. 1921.</li>
    <li>You will never miss your mother etc.. Blue Ridge Duo. <a href=search.php?nq=1&query_type=call_number&query=8088>Edison Blue Amberol: 4961</a>. 1924.</li>
    <li>Dat friend of mine / Egbert Van Alstyne. Bob Roberts. <a href=search.php?nq=1&query_type=call_number&query=7777>Edison Gold Moulded Record: 9740</a>. 1908.</li>
    <li>Language / Worton David ; Shirley Ilton. Bobbie Naish. <a href=search.php?nq=1&query_type=call_number&query=7685>Edison Blue Amberol: 23295</a>. 1914.</li>
    <li>King Carneval polka / Bohumir Kryl. Bohumir Kryl. <a href=search.php?nq=1&query_type=call_number&query=8069>Edison Gold Moulded Record: 8663</a>. 1903.</li>
    <li>Du, du. Bohumir Kryl. <a href=search.php?nq=1&query_type=call_number&query=8476>U.S. Everlasting Record: 1305</a>. 1909.</li>
    <li>Cary waltz. Bohumir Kryl. <a href=search.php?nq=1&query_type=call_number&query=9102>Edison Gold Moulded Record: 8609</a>. 1903.</li>
    <li>Russian fantasia. Bohumir Kryl. <a href=search.php?nq=1&query_type=call_number&query=9232>Edison Gold Moulded Record: 8208</a>. 1902.</li>
    <li>Arbucklenian Polka / J. Hartmann. Bohumir Kryl ; Frank P. Banta. <a href=search.php?nq=1&query_type=call_number&query=8988>Edison Gold Moulded Record: 8327</a>. 1902.</li>
    <li>The street watchman's Christmas / Winter. Bransby Williams. <a href=search.php?nq=1&query_type=call_number&query=9880>Edison Blue Amberol: 23148</a>. 1914.</li>
    <li>Burial of Sir John Moore / Charles Wolfe. Bransby Williams.. <a href=search.php?nq=1&query_type=call_number&query=9867>Edison Blue Amberol: 23031</a>. 1913.</li>
    <li>Sweet and low / Joseph Barnby. Brass Quartett. <a href=search.php?nq=1&query_type=call_number&query=7091>Indestructible Record: 1405</a>. 1910.</li>
    <li>The dawn of light. British Male Quartet. <a href=search.php?nq=1&query_type=call_number&query=8874>Edison Blue Amberol: 23140</a>. 1913.</li>
    <li>You've got to see mamma every night. Broadway Dance Orchestra. <a href=search.php?nq=1&query_type=call_number&query=7789>Edison Blue Amberol: 4745</a>. 1923.</li>
    <li>By the waters of Killarney: waltz / Alma M. Sanders. Broadway Dance Orchestra. <a href=search.php?nq=1&query_type=call_number&query=8094>Edison Blue Amberol: 4406</a>. 1921.</li>
    <li>Sol-o-may / Robert Stolz. Broadway Dance Orchestra. <a href=search.php?nq=1&query_type=call_number&query=8122>Edison Blue Amberol: 4454</a>. 1922.</li>
    <li>Nobody's little girl / Theodore F. Morse. Byron G. Harlan. <a href=search.php?nq=1&query_type=call_number&query=7027>Edison Gold Moulded Record: 9539</a>. 1907.</li>
    <li>Dancing the du da du da dae / unknown. Byron G. Harlan. <a href=search.php?nq=1&query_type=call_number&query=7707>Edison Blue Amberol: 3138</a>. 1917.</li>
    <li>The good old U.S.A. /  Theodore F. Morse. Byron G. Harlan. <a href=search.php?nq=1&query_type=call_number&query=7752>Edison Gold Moulded Record: 9350</a>. 1906.</li>
    <li>I'm a twelve o'clock fellow / H. Von Tilzer. Byron G. Harlan. <a href=search.php?nq=1&query_type=call_number&query=7770>Edison Blue Amberol: 3734</a>. 1919.</li>
    <li>Wait till the sun shines Nellie / Andrew B. Sterling ; Harry Von Tilzer. Byron G. Harlan. <a href=search.php?nq=1&query_type=call_number&query=8070>Columbia Phonograph Co.: 32882</a>. 1906.</li>
    <li>He carved his mother's name upon a tree. Byron G. Harlan ; Addison Dashiell Madeira. <a href=search.php?nq=1&query_type=call_number&query=7996>Edison Record: 7345</a>. [1899].</li>
    <li>Gentle spring is here again. Byron G. Harlan ; Joseph Belmont. <a href=search.php?nq=1&query_type=call_number&query=7742>Indestructible Record: 820</a>. 1908.</li>
    <li>Schooldays / Gus Edwards. Byron G. Harlan & Will D. Cobb. <a href=search.php?nq=1&query_type=call_number&query=8527>Indestructible Record: 692</a>. 1908.</li>
    <li>Two rubes at the vaudeville / unknown. Byron G. Harlan and Frank C. Stanley. <a href=search.php?nq=1&query_type=call_number&query=7037>Edison Gold Moulded Record: 8736</a>. 1904.</li>
    <li>The whistling and singing farmer boys. Byron G. Harlan and Joe Belmont. <a href=search.php?nq=1&query_type=call_number&query=7794>Edison Record: 9871</a>. 1920.</li>
    <li>Stop making faces at me. Byron G. Harlan.. <a href=search.php?nq=1&query_type=call_number&query=7311>Indestructible Record: 843</a>. 1908.</li>
    <li>Uncle Josh's trip to Coney Island / Cal Stewart. Cal Stewart. <a href=search.php?nq=1&query_type=call_number&query=7031>Lakeside Indestructible Cylinder Record: 1630</a>. 1909 - 1911.</li>
    <li>Uncle Josh on a bicycle / Cal Stewart. Cal Stewart. <a href=search.php?nq=1&query_type=call_number&query=7112>Indestructible Record: 915</a>. 1908.</li>
    <li>Opera at Pumpkin Center / Cal Stewart. Cal Stewart. <a href=search.php?nq=1&query_type=call_number&query=7598>Edison Blue Amberol: 3830</a>. 1919.</li>
    <li>Uncle Josh's husking bee dance / Cal Stewart. Cal Stewart. <a href=search.php?nq=1&query_type=call_number&query=7992>Columbia Phonograph Co.: 14031</a>. between 1898 and 1900.</li>
    <li>The show troupe at Pumpkin Center / Cal Stewart. Cal Stewart. <a href=search.php?nq=1&query_type=call_number&query=8148>Edison Blue Amberol: 1912</a>. 1913.</li>
    <li>Uncle Josh and the sailor. Cal Stewart. <a href=search.php?nq=1&query_type=call_number&query=8151>Edison Blue Amberol: 4077</a>. 1920.</li>
    <li>Uncle Josh & the insurance agent. Cal Stewart. <a href=search.php?nq=1&query_type=call_number&query=8511>U.S. Everlasting Record: 1640</a>. 1909-1912.</li>
    <li>Uncle Josh and the osteopath / Cal Stewart. Cal Stewart. <a href=search.php?nq=1&query_type=call_number&query=8525>U.S. Everlasting Record: 1350</a>. 1909.</li>
    <li>Uncle Josh at the dentist's. Cal Stewart ; Len Spencer. <a href=search.php?nq=1&query_type=call_number&query=8031>Edison Gold Moulded Record: 9037</a>. 1909.</li>
    <li>The Lord's prayer, doxology, responses, and hymn / unknown. Calvary Choir. <a href=search.php?nq=1&query_type=call_number&query=7693>Edison Blue Amberol: 3794</a>. 1919.</li>
    <li>Angels from the realms of glory  / Henry Thomas Smart. Carol Singers. <a href=search.php?nq=1&query_type=call_number&query=8702>Edison Blue Amberol: 2771</a>. 1915.</li>
    <li>I loved you more than I knew / Albert W. Ketèlbey. Charles Compton. <a href=search.php?nq=1&query_type=call_number&query=7625>Edison Blue Amberol: 23080</a>. 1913.</li>
    <li>I want you near / Elliott ; Cooke. Charles Compton. <a href=search.php?nq=1&query_type=call_number&query=7701>Edison Blue Amberol: 23155</a>. 1914.</li>
    <li>Sally in our alley / Henry Carey. Charles Compton. <a href=search.php?nq=1&query_type=call_number&query=7710>Edison Blue Amberol: 23062</a>. 1913.</li>
    <li>Tell her I love her so / Philip de Faye. Charles Compton. <a href=search.php?nq=1&query_type=call_number&query=7721>Edison Blue Amberol: 23039</a>. 1912.</li>
    <li>Love, could I only tell thee / Clifton Bingham ;John Mais Capel. Charles Compton. <a href=search.php?nq=1&query_type=call_number&query=7834>Edison Blue Amberol: 23132</a>. 1913.</li>
    <li>If I could only make you care / J. E. Dempsey ; Johann C. Schmid. Charles Compton. <a href=search.php?nq=1&query_type=call_number&query=7846>Edison Blue Amberol: 23201</a>. 1914.</li>
    <li>Come into the garden, Mand / Michael W. Balfe ; Alfred Lord Tennyson. Charles Compton. <a href=search.php?nq=1&query_type=call_number&query=7861>Edison Blue Amberol: 23049</a>. 1912.</li>
    <li>I don't suppose / Henry Trotère. Charles Compton. <a href=search.php?nq=1&query_type=call_number&query=9247>Edison Blue Amberol: 23357</a>. 1915?.</li>
    <li>Chanticleer reels and jigs medley / Charles. Charles D'Almaine. <a href=search.php?nq=1&query_type=call_number&query=7065>Edison Amberol: 584</a>. 1910.</li>
    <li>Blackthorn stick medley of jigs. Charles D'Almaine. <a href=search.php?nq=1&query_type=call_number&query=7852>Edison Blue Amberol: 3434</a>. 1914.</li>
    <li>Listen to the mockingbird / Septimus Winner. Charles D'Almaine. <a href=search.php?nq=1&query_type=call_number&query=7866>Columbia Phonograph Co.: 27000</a>. 1896-1900.</li>
    <li>Shepherds' dance / Sir Edward German. Charles D'Almaine. <a href=search.php?nq=1&query_type=call_number&query=8035>Edison Gold Moulded Record: 8070</a>. 1902.</li>
    <li>Charme d'amour - valse lente / Edwin F. Kendall. Charles Daab. <a href=search.php?nq=1&query_type=call_number&query=7063>Edison Amberol: 861</a>. 1911.</li>
    <li>La Vierge à la crèche / A. Périlhou. Charles Gilibert. <a href=search.php?nq=1&query_type=call_number&query=9881>Cylindres Edison Moulés Sur Or: 17848</a>. between 1906 and 1907.</li>
    <li>Roméo et Juliette : Que l'hymne nuptial succède aux cris d'alarmes / Charles Gounod,. Charles Gilibert. <a href=search.php?nq=1&query_type=call_number&query=9882>Cylindres Edison Moulés Sur Or: 17870</a>. 1909.</li>
    <li>Maritana. Yes, let me like a soldier fall] / William Vincent Wallace. Charles Hackett. <a href=search.php?nq=1&query_type=call_number&query=8081>Edison Blue Amberol: 1724</a>. 1913.</li>
    <li>The prisoner's sweetheart / Art Walsh. Charles Harrison. <a href=search.php?nq=1&query_type=call_number&query=8027>Edison Blue Amberol: 5172</a>. 1926.</li>
    <li>Floating down the old Monongahela / Kerry Mills. Charles Hart. <a href=search.php?nq=1&query_type=call_number&query=7530>Edison Blue Amberol: 3938</a>. 1920.</li>
    <li>Shall you? Shall I? / James McGranahan. Charles Hart, Elliott Shaw, and Calvary Choir. <a href=search.php?nq=1&query_type=call_number&query=7814>Edison Blue Amberol: 3880</a>. 1919.</li>
    <li>Pickaninny polka / unknown. Charles P. Lowe. <a href=search.php?nq=1&query_type=call_number&query=7114>Columbia Phonograph Co.: 15504</a>. ca. 1900.</li>
    <li>When I waltz with you. Charles W. Harrison. <a href=search.php?nq=1&query_type=call_number&query=7177>Edison Blue Amberol: 1556</a>. 1912.</li>
    <li>La leçon de cor / N/A. Charlus. <a href=search.php?nq=1&query_type=call_number&query=6400>Pathé: 1961 (21321)</a>. 190-.</li>
    <li>La fille de Parthenay. Charlus. <a href=search.php?nq=1&query_type=call_number&query=7982>Pathé: 2199</a>. 1909.</li>
    <li>Love's old sweet song / James L. Molloy. Chester Gaylord. <a href=search.php?nq=1&query_type=call_number&query=8145>Edison Blue Amberol: 4282</a>. 1921.</li>
    <li>Soldier songs: no. 2. Chorus of Male Voices. <a href=search.php?nq=1&query_type=call_number&query=7802>Edison Blue Amberol: 3638</a>. 1919.</li>
    <li>My ain countrie / Hanna. Christine Miller. <a href=search.php?nq=1&query_type=call_number&query=7610>Edison Blue Amberol: 28208</a>. 1915.</li>
    <li>Just for to-day / Jane Bingham Abbott. Christine Miller. <a href=search.php?nq=1&query_type=call_number&query=7616>Edison Blue Amberol: 28202</a>. 1914.</li>
    <li>Afton water / Alexander Hume ; Robert Burns. Christine Miller. <a href=search.php?nq=1&query_type=call_number&query=7851>Edison Blue Amberol: 28128</a>. 1912.</li>
    <li>My old Kentucky home / Stephen Collins Foster. Clarinetist. <a href=search.php?nq=1&query_type=call_number&query=4878>Columbia Phonograph Co.: 3409</a>. 1904-1909.</li>
    <li>It must be someone like you / Charles Straight, Roy Bargy ; Jack Frost. Club de Vingt Orchestra. <a href=search.php?nq=1&query_type=call_number&query=7583>Edison Blue Amberol: 4372</a>. 1921.</li>
    <li>Three o'clock in the morning - waltz / Julian Robledo. Club de Vingt Orchestra. <a href=search.php?nq=1&query_type=call_number&query=7608>Edison Blue Amberol: 4426</a>. 1922.</li>
    <li>Rosy cheeks: fox trot / Harry D. Squires. Club de Vingt Orchestra. <a href=search.php?nq=1&query_type=call_number&query=8036>Edison Blue Amberol: 4442</a>. 1922.</li>
    <li>Not long ago: fox trot. Club de Vingt Orchestra. <a href=search.php?nq=1&query_type=call_number&query=8082>Edison Blue Amberol: 4373</a>. 1924.</li>
    <li>Sugar moon / Percy Wenrich and Stan Murphy. Collins & Harlan. <a href=search.php?nq=1&query_type=call_number&query=7092>Indestructible Record: 3121</a>. 1910.</li>
    <li>Under the yum, yum tree / Harry Von Tilzer. Collins and Harlan. <a href=search.php?nq=1&query_type=call_number&query=9262>Edison Amberol: 646</a>. 1911.</li>
    <li>Stadium march. Columbia Band. <a href=search.php?nq=1&query_type=call_number&query=8078>Columbia Phonograph Co.: 32598</a>. 1904.</li>
    <li>Noisy Bill / F. H. Losey. Columbia Band. <a href=search.php?nq=1&query_type=call_number&query=9164>Columbia Phonograph Co.: 32600</a>. 1904.</li>
    <li>The girl I left behind me / David Belasco. Columbia Drum, Fife and Bugle Corps. <a href=search.php?nq=1&query_type=call_number&query=8818>Columbia Phonograph Co.: 12800</a>. 1899.</li>
    <li>The girl I left behind me / David Belasco. Columbia Drum, Fife and Bugle Corps. <a href=search.php?nq=1&query_type=call_number&query=8819>Columbia Phonograph Co.: 12800</a>. 1899.</li>
    <li>Schubert's serenade / Franz Schubert. Columbia Orchestra. <a href=search.php?nq=1&query_type=call_number&query=7931>Columbia Phonograph Co.: 15045</a>. between 1896 and 1900.</li>
    <li>Narcissus  / Ethelbert Woodbridge Nevin. Columbia Orchestra. <a href=search.php?nq=1&query_type=call_number&query=7935>Columbia Phonograph Co.: 15201</a>. between 1896 and 1900.</li>
    <li>Titl's serenade  / Anton Emil Titl. Columbia Orchestra. <a href=search.php?nq=1&query_type=call_number&query=7940>Columbia Phonograph Co.: 15013</a>. between 1896 and 1900.</li>
    <li>On the Midway. Columbia Orchestra. <a href=search.php?nq=1&query_type=call_number&query=7941>Columbia Phonograph Co.: 15143</a>. 1898.</li>
    <li>Kentucky jubilee singers. Columbia Orchestra. <a href=search.php?nq=1&query_type=call_number&query=7968>Columbia Phonograph Co.: 1959</a>. between 1896 and 1900.</li>
    <li>Schubert's serenade / Franz Schubert. Columbia Orchestra. <a href=search.php?nq=1&query_type=call_number&query=7973>Columbia Phonograph Co.: 15045</a>. between 1896 and 1900.</li>
    <li>La serenata waltz. Columbia Orchestra. <a href=search.php?nq=1&query_type=call_number&query=8005>Columbia Phonograph Co.: 15053</a>. between 1896 and 1900.</li>
    <li>Rose from the south waltz. Columbia Orchestra. <a href=search.php?nq=1&query_type=call_number&query=8066>Columbia Phonograph Co.: 15044</a>. between 1904 and 1909.</li>
    <li>Yankee Doodle's come to town [Little Johnny Jones. Yankee doodle boy] / George Michael Cohan. Columbia Quartette. <a href=search.php?nq=1&query_type=call_number&query=4854>Columbia Phonograph Co.: 33281</a>. 1908.</li>
    <li>St. Patrick's Day at Clancy's. Columbia Quartette. <a href=search.php?nq=1&query_type=call_number&query=8232>Columbia Record: 32236</a>. [1903].</li>
    <li>Sweetheart days / Anton J. Daily. Columbia Quartette.. <a href=search.php?nq=1&query_type=call_number&query=9148>Columbia Phonograph Co.: 33230</a>. after 1901.</li>
    <li>Belphegor march / Engebert Brepsant. Concertina, played by A. Prince. <a href=search.php?nq=1&query_type=call_number&query=8860>Edison Gold Moulded Record: 14022</a>. 1910.</li>
    <li>Lisztiana march / Al. Chiaffarelli. Conway's Band. <a href=search.php?nq=1&query_type=call_number&query=7865>Edison Blue Amberol: 3947</a>. 1920.</li>
    <li>The American Legion march / Harry J. Lincoln. Conway's Band. <a href=search.php?nq=1&query_type=call_number&query=8055>Edison Blue Amberol: 4090</a>. 1920.</li>
    <li>Reuben and Cynthia / Charles Hale Hoyt ; Percy Gaunt. Corrine Morgan ; Frank C. Stanley. <a href=search.php?nq=1&query_type=call_number&query=7998>Columbia Phonograph Co.: 31611</a>. 1901.</li>
    <li>Jane / Albert Von Tilzer. Crescent Trio. <a href=search.php?nq=1&query_type=call_number&query=7958>Edison Blue Amberol: 4434</a>. 1922.</li>
    <li>Kentucky home / Abe Brashen, Harold Weeks. Crescent Trio. <a href=search.php?nq=1&query_type=call_number&query=8087>Edison Blue Amberol: 4420</a>. 1921.</li>
    <li>I'll be your rain-beau / J. Fred Helf. Dan W. Quinn. <a href=search.php?nq=1&query_type=call_number&query=7014>Columbia Phonograph Co.: 31874</a>. 1902.</li>
    <li>The handicap race. Dan W. Quinn. <a href=search.php?nq=1&query_type=call_number&query=7960>Columbia Phonograph Co.: 5097</a>. 1896- 1900.</li>
    <li>So you want to be a soldier, little man? / [Henry Trotère ; Leonard Cooke. David Brazell. <a href=search.php?nq=1&query_type=call_number&query=7569>Edison Blue Amberol: 23334</a>. 1912 - 1929.</li>
    <li>Bedouin love song / Ciro Pensuti. David Brazell. <a href=search.php?nq=1&query_type=call_number&query=7669>Edison Blue Amberol: 23171</a>. 1914.</li>
    <li>It takes a little rain with the sunshine / Harry Carroll. DeLos Becker. <a href=search.php?nq=1&query_type=call_number&query=8419>Indestructible Record: 3303</a>. 1913.</li>
    <li>Why did you make me care . Delos Becker ; Solman McGuire. <a href=search.php?nq=1&query_type=call_number&query=7900>U.S. Everlasting Record: 1649</a>. 1912.</li>
    <li>The Nelson touch / Edward Lockton. Donald Chalmers. <a href=search.php?nq=1&query_type=call_number&query=7567>Edison Blue Amberol: 23384</a>. 1915.</li>
    <li>O'er the billowy sea / Earl K. Smith. Donald Chalmers. <a href=search.php?nq=1&query_type=call_number&query=8091>Edison Blue Amberol: 3710</a>. 1918.</li>
    <li>Salut d'amour / Elgar?. Dorfman. <a href=search.php?nq=1&query_type=call_number&query=7103>Indestructible Record: 3009</a>. 1910.</li>
    <li>Move on Mr. Moon / Ed Rose and Synder. Dorothy Kingsley. <a href=search.php?nq=1&query_type=call_number&query=7102>Indestructible Record: 860</a>. 1908.</li>
    <li>Le gardien des ruines / Gabriel Bunel. Dranem. <a href=search.php?nq=1&query_type=call_number&query=7976>Cylindres Edison Moulés Sur Or: 17565</a>. 1906.</li>
    <li>The two grenadiers / Robert Schumann. Earl Cartwright. <a href=search.php?nq=1&query_type=call_number&query=8152>Edison Blue Amberol: 23336</a>. 191-.</li>
    <li>Rigoletto. Selections / Giuseppe Verdi. Edison Concert Band. <a href=search.php?nq=1&query_type=call_number&query=0425>Edison Blue Amberol: 2100</a>. 1913.</li>
    <li>Speedway march. Edison Concert Band. <a href=search.php?nq=1&query_type=call_number&query=7003>Edison Gold Moulded Record: 7763</a>. 1902.</li>
    <li>Rippling waters / Will T. Pierson. Edison Concert Band. <a href=search.php?nq=1&query_type=call_number&query=7651>Edison Blue Amberol: 2656</a>. 1915.</li>
    <li>Manisot march / Thomas Preston Brooke. Edison Concert Band. <a href=search.php?nq=1&query_type=call_number&query=7739>Edison Record: 7425</a>. 1899- 1900.</li>
    <li>Symphia waltzes. Edison Concert Band. <a href=search.php?nq=1&query_type=call_number&query=7761>Edison Gold Moulded Record: 8376</a>. 1903.</li>
    <li>Petite tonkinoise / Vincent Scotto. Edison Concert Band. <a href=search.php?nq=1&query_type=call_number&query=7775>Edison Gold Moulded Record: 9596</a>. 1907.</li>
    <li>I've got my eyes on you  / Theodore F. Morse ; Frederick W. Hager, Justus Ringleben. Edison Concert Band. <a href=search.php?nq=1&query_type=call_number&query=8076>Edison Gold Moulded Record: 8317</a>. 1903 .</li>
    <li>The  beautiful Galatea overture / Franz von Suppé. Edison Concert Band. <a href=search.php?nq=1&query_type=call_number&query=8150>Edison Blue Amberol: 2115</a>. 1913.</li>
    <li>Aïda. Marcia trionfale / Giuseppe Verdi. Edison Concert Band. <a href=search.php?nq=1&query_type=call_number&query=8371>Edison Blue Amberol: 2145 </a>. 1913.</li>
    <li>Y como le va / Joaquin Valverde. Edison Concert Band. <a href=search.php?nq=1&query_type=call_number&query=8440>Edison Amberol: 1077</a>. 1912.</li>
    <li>Lustspiel overture / Béla Kéler. Edison Concert Band. <a href=search.php?nq=1&query_type=call_number&query=8695>Edison Blue Amberol: 1573</a>. 1912.</li>
    <li>Ballet music from Faust / Charles Gounod. Edison Concert Band. <a href=search.php?nq=1&query_type=call_number&query=9001>Edison Gold Moulded Record: 8450</a>. between 1904 and 1908.</li>
    <li>Boston commandery march / Thomas Carter. Edison Concert Band. <a href=search.php?nq=1&query_type=call_number&query=9019>Edison Blue Amberol: 1960</a>. 1913.</li>
    <li>Mignon: polacca / Thomas Ambroise. Edison Concert Band. <a href=search.php?nq=1&query_type=call_number&query=9097>Edison Gold Moulded Record: 8432</a>. 1903.</li>
    <li>American fantasie / Victor Herbert. Edison Concert Band. <a href=search.php?nq=1&query_type=call_number&query=9103>Edison Gold Moulded Record: 9041</a>. 1905.</li>
    <li>Aisha, Indian intermezzo / John Lindsay. Edison Concert Band. <a href=search.php?nq=1&query_type=call_number&query=9156>Edison Blue Amberol: 2084</a>. 1913.</li>
    <li>The death of Custer. Edison Concert Band. <a href=search.php?nq=1&query_type=call_number&query=9271>Edison Amberol: 80</a>. 1908.</li>
    <li>It blew! Blew! Blew!. Edison Concert Band . <a href=search.php?nq=1&query_type=call_number&query=8688>Edison Gold Moulded Record:  9185</a>. 1906.</li>
    <li>The adventurer march (144 rpm). Edison Grand Concert Band. <a href=search.php?nq=1&query_type=call_number&query=8926>Edison Record: 7707</a>. 1901.</li>
    <li>Ireland's well known melodies no. 2. Edison Grand Concert Band. <a href=search.php?nq=1&query_type=call_number&query=9119>Edison Gold Moulded Record: 128</a>. between 1896 and 1900.</li>
    <li>Any rags medley. Edison Grand Concert Band. <a href=search.php?nq=1&query_type=call_number&query=9137>Edison Gold Moulded Record: 8573</a>. 1903.</li>
    <li>Favorite airs from Olivette / Edmond Audran. Edison Light Opera Company. <a href=search.php?nq=1&query_type=call_number&query=6998>Edison Blue Amberol: 2355</a>. 1914.</li>
    <li>Pinafore airs no. 3 / W. S. Gilbert ; Arthur Sullivan. Edison Light Opera Company. <a href=search.php?nq=1&query_type=call_number&query=7697>Edison Blue Amberol: 1892</a>. 1913
      1913.</li>
    <li>Blessed assurance (144 rpm) / Phoebe Palmer Knapp. Edison Male Quartet. <a href=search.php?nq=1&query_type=call_number&query=5250>Edison Record: 10063</a>. 1896-1899.</li>
    <li>I can't do the sum / Victor Herbert. Edison Male Quartet. <a href=search.php?nq=1&query_type=call_number&query=7018>Edison Gold Moulded Record: 8753</a>. 1904.</li>
    <li>The bridge / M. Lindsey. Edison Male Quartet. <a href=search.php?nq=1&query_type=call_number&query=8635>Edison Gold Moulded Record: 2203</a>. 1902-1908.</li>
    <li>Soldier's farewell / Johanna Kinkel. Edison Male Quartette. <a href=search.php?nq=1&query_type=call_number&query=7036>Edison Gold Moulded Record: 7710</a>. 1902.</li>
    <li>Moonlight on the lake / C. A. White. Edison Male Quartette. <a href=search.php?nq=1&query_type=call_number&query=9144>Edison Record: 2238</a>. between 1896 and 1900.</li>
    <li>Good-night waltz / N/A. Edison Military Band. <a href=search.php?nq=1&query_type=call_number&query=0004>Edison Gold Moulded Record: 9164</a>. 1905.</li>
    <li>Everywhere medley / unknown. Edison Military Band. <a href=search.php?nq=1&query_type=call_number&query=6967>Edison Gold Moulded Record: 8768</a>. 1904.</li>
    <li>My Mariuccia take-a steamboat medley / Al Piantadosi. Edison Military Band. <a href=search.php?nq=1&query_type=call_number&query=7032>Edison Gold Moulded Record: 9529</a>. 1907.</li>
    <li>Jovial Joe  / Justin Ring. Edison Military Band. <a href=search.php?nq=1&query_type=call_number&query=8074>Edison Gold Moulded Record: 8838</a>. 1904.</li>
    <li>Whistle: intermezzo / Leon Copeland. Edison Military Band. <a href=search.php?nq=1&query_type=call_number&query=8761>Edison Gold Moulded Record: 9877</a>. 1908.</li>
    <li>The darkies' dream / G. L. Lansing. Edison Military Band. <a href=search.php?nq=1&query_type=call_number&query=8990>Edison Gold Moulded Record: 8878</a>. 1905.</li>
    <li>Hedge roses Lancers / G. Weingarten. Edison Military Band. <a href=search.php?nq=1&query_type=call_number&query=8992>Edison Bell Record: 8881</a>. 1904.</li>
    <li>Good humor quadrille. Edison Military Band. <a href=search.php?nq=1&query_type=call_number&query=8993>Edison Gold Moulded Record: 8886</a>. 1904.</li>
    <li>Good humor quadrille. Edison Military Band. <a href=search.php?nq=1&query_type=call_number&query=8995>Edison Gold Moulded Record: 8887</a>. 1904.</li>
    <li>Light cavalry overture / Franz von Suppé. Edison Symphony Orchestra. <a href=search.php?nq=1&query_type=call_number&query=7120>Edison Gold Moulded Record: 524</a>. 1902.</li>
    <li>Love's dreamland waltz / Otto Roeder. Edison Symphony Orchestra. <a href=search.php?nq=1&query_type=call_number&query=7873>Edison Gold Moulded Record: 574</a>. 1902.</li>
    <li>My little canoe / Leslie Stuart. Edison Symphony Orchestra. <a href=search.php?nq=1&query_type=call_number&query=8987>Edison Gold Moulded Record: 8851</a>. 1904.</li>
    <li>The new era overture / J.C. Heed. Edison Symphony Orchestra.. <a href=search.php?nq=1&query_type=call_number&query=8045>Edison Gold Moulded Record: 9110</a>. 1905.</li>
    <li>All night long / Shelton Brooks. Edna Brown [i.e. Elsie Baker]. <a href=search.php?nq=1&query_type=call_number&query=7094>Indestructible Record: 3295</a>. 1913.</li>
    <li>There's only one pal after all / F. Henri Klickman. Edward Allen. <a href=search.php?nq=1&query_type=call_number&query=7850>Edison Blue Amberol: 4460</a>. 1921.</li>
    <li>Pretty Peggy / Pat Rooney. Edward F. Rubsam. <a href=search.php?nq=1&query_type=call_number&query=6991>Edison Gold Moulded Record: 8379</a>. 1903/1904.</li>
    <li>Dance of the lightning bugs. Edward F. Rubsam. <a href=search.php?nq=1&query_type=call_number&query=8632>Columbia Phonograph Co.: 32590</a>. 1904.</li>
    <li>How can they tell that I'm Irish / Jack Norworth. Edward M. Favor. <a href=search.php?nq=1&query_type=call_number&query=7067>Indestructible Record: 1265</a>. 1910.</li>
    <li>O’Brien has no place to go / George Evans. Edward M. Favor. <a href=search.php?nq=1&query_type=call_number&query=7104>Indestructible Record: 841</a>. 1908.</li>
    <li>Questions / Maurice FitzGerald Scott. Edward M. Favor. <a href=search.php?nq=1&query_type=call_number&query=7732>Indestructible Record: 1245</a>. 1910.</li>
    <li>Pocahontas / Gus Edwards. Edward M. Favor. <a href=search.php?nq=1&query_type=call_number&query=8044>Edison Gold Moulded Record: 9208</a>. 1906.</li>
    <li>I'll not go out with Reilly any more (copy 2). Edward M. Favor. <a href=search.php?nq=1&query_type=call_number&query=8059>Edison Gold Moulded Record: 7508</a>. 1902?.</li>
    <li>Pepita Maguire / J. B. Mullen. Edward M. Favor. <a href=search.php?nq=1&query_type=call_number&query=8387>Edison Gold Moulded Record: 8836</a>. 1904.</li>
    <li>Whistling coon / Sam Devere. Edward Meeker. <a href=search.php?nq=1&query_type=call_number&query=7712>Edison Blue Amberol: 3466</a>. 1918.</li>
    <li>When the band plays "Yankee Doodle" / Joel P. Corin. Edward Meeker. <a href=search.php?nq=1&query_type=call_number&query=7782>Edison Gold Moulded Record: 9696</a>. 1907.</li>
    <li>The Argentines, the Portuguese and the Greeks / Arthur Swanstrom ; Carey Morgan. Edward Meeker. <a href=search.php?nq=1&query_type=call_number&query=7817>Edison Blue Amberol: 4083</a>. 1920
      1920.</li>
    <li>I think I oughtn't ought to any more. Edward Meeker. <a href=search.php?nq=1&query_type=call_number&query=8077>Edison Gold Moulded Record: 9638</a>. 1907.</li>
    <li>Pretty Peggy / unknown. Edward Rubsam. <a href=search.php?nq=1&query_type=call_number&query=7111>Columbia Phonograph Co.: 32728</a>. 1905.</li>
    <li>Star spangled banner / Francis Scott Key. Elaine Gordon. <a href=search.php?nq=1&query_type=call_number&query=8575>Indestructible Record: 3424</a>. 1917.</li>
    <li>Battle hymn of the Republic / Julia Ward Howe. Elise C. Stevenson ; Frank C. Stanley. <a href=search.php?nq=1&query_type=call_number&query=7867>Edison Amberol: 79</a>. 1908.</li>
    <li>Will you love me when I'm old? / John Ford. Elizabeth Spencer. <a href=search.php?nq=1&query_type=call_number&query=7582>Edison Blue Amberol: 4253</a>. 1920.</li>
    <li>Say not love is a dream : the Count of Luxembourg / Franz Lehár. Elizabeth Spencer. <a href=search.php?nq=1&query_type=call_number&query=7600>Edison Blue Amberol: 1610</a>. 1912.</li>
    <li>Little boy blue / Ethelbert Woodbridge Nevin. Elizabeth Spencer. <a href=search.php?nq=1&query_type=call_number&query=7637>Edison Blue Amberol: 1757</a>. 1913.</li>
    <li>Those songs my mother used to sing / H. Wakefield Smith. Elizabeth Spencer. <a href=search.php?nq=1&query_type=call_number&query=7871>Edison Amberol: 625</a>. 1911.</li>
    <li>Call me back, pal, o'mine / Harold Dixon. Elizabeth Spencer ; Charles Hart. <a href=search.php?nq=1&query_type=call_number&query=7666>Edison Blue Amberol: 4666</a>. 1923.</li>
    <li>Delaware / Marvin Smolev. Elizabeth Spencer ; Charles Hart. <a href=search.php?nq=1&query_type=call_number&query=8083>Edison Blue Amberol: 4693</a>. 1922.</li>
    <li>I would that my love / Felix Mendelssohn-Bartholdy. Elizabeth Spencer ; E. Eleanor Patterson. <a href=search.php?nq=1&query_type=call_number&query=7627>Edison Blue Amberol: 1831</a>. 1913.</li>
    <li>Weeping, sad and lonely / Henry Tucker. Elizabeth Spencer and chorus. <a href=search.php?nq=1&query_type=call_number&query=7630>Edison Blue Amberol: 1586</a>. 1912.</li>
    <li>Love's melody / Léo Daniderff. Elizabeth Spencer and Emory B. Randolph.. <a href=search.php?nq=1&query_type=call_number&query=9007>Edison Blue Amberol: 2502</a>. 1914.</li>
    <li>Where the edelweiss is blooming: "Hanky panky" / Alfred Baldwin Sloane. Elizabeth Spencer and Henry Burr. <a href=search.php?nq=1&query_type=call_number&query=7277>Edison Blue Amberol: 1599</a>. 1912.</li>
    <li>Tosti's goodbye / Francesco Paolo Tosti. Elizabeth Wheeler. <a href=search.php?nq=1&query_type=call_number&query=7084>Indestructible Record: 3024</a>. 1910.</li>
    <li>Elk Minstrels`. Elk's Minstrel Co. <a href=search.php?nq=1&query_type=call_number&query=7829>Edison Blue Amberol: 2321</a>. 1914.</li>
    <li>I'll sit right here on the moon / James V. Monaco. Elsie Baker. <a href=search.php?nq=1&query_type=call_number&query=8061>Edison Blue Amberol: 1623</a>. 1913.</li>
    <li>Das kleine fischermadchen. Emil Muench. <a href=search.php?nq=1&query_type=call_number&query=8405>Edison Goldguss Walze: 12734</a>. 1902.</li>
    <li>Bonjour Suzon / Emile Pessard. Emile Mercadier. <a href=search.php?nq=1&query_type=call_number&query=6398>Pathé: 1597 (25771)</a>. 190-.</li>
    <li>Pres des cieux / N/A. Emile Mercadier. <a href=search.php?nq=1&query_type=call_number&query=6401>Pathé: 1694 (18140)</a>. 190-.</li>
    <li>Trottinette. Emile Mercadier. <a href=search.php?nq=1&query_type=call_number&query=9886>Cylindres Edison Moulés Sur Or: 17607</a>. between 1905 and 1906.</li>
    <li>What have I to give? / Gerald Lane. Emory B. Randolph. <a href=search.php?nq=1&query_type=call_number&query=7618>Edison Blue Amberol: 2875</a>. 1916.</li>
    <li>A bullfrog am no nightingale / George Schleiffarth. Ernest Hare. <a href=search.php?nq=1&query_type=call_number&query=7820>Edison Blue Amberol: 3937</a>. 1919.</li>
    <li>The magic mirror waltzes / Fred H. Losey. Ernest L. Stevens Trio. <a href=search.php?nq=1&query_type=call_number&query=7584>Edison Blue Amberol: 4733</a>. 1922.</li>
    <li>Do you remember the last waltz / Bennett Scott ; Arthur J. Mills. Ernest Pike. <a href=search.php?nq=1&query_type=call_number&query=7402>Edison Standard Record: 14088</a>. 1911.</li>
    <li>Take a pair of sparkling eyes / Sir Arthur Sullivan. Ernest Pike. <a href=search.php?nq=1&query_type=call_number&query=7604>Edison Blue Amberol: 23005</a>. 1913.</li>
    <li>Kathleen Mavourneen / Frederick W. N. Crouch ; Julia M. Crawford. Ernest Pike. <a href=search.php?nq=1&query_type=call_number&query=7837>Edison Blue Amberol: 23209</a>. 1911.</li>
    <li>The green isle of Erin / Joseph L. Roeckel. Ernest Pike. <a href=search.php?nq=1&query_type=call_number&query=7870>Edison Amberol: 12231</a>. 1910.</li>
    <li>The pilgrim of love: from the Noble outlaw / Henry R. Bishop. Ernest Pike. <a href=search.php?nq=1&query_type=call_number&query=8155>Edison Amberol: 12094</a>. 1909.</li>
    <li>Margerita / Frederic N. Löhr. Ernest Pike. <a href=search.php?nq=1&query_type=call_number&query=8880>Edison Blue Amberol: 23173</a>. 1911.</li>
    <li>Excelsior / M. W. Balfe. Ernest Pike and Peter Dawson. <a href=search.php?nq=1&query_type=call_number&query=7838>Edison Blue Amberol: 23091</a>. 1910.</li>
    <li>The old rustic bridge by the mill | / J. P. Skelly. Ernest Pike and Peter Dawson. <a href=search.php?nq=1&query_type=call_number&query=7842>Edison Blue Amberol: 23052</a>. 1908.</li>
    <li>All over nothing at all / James S. Rule, Joseph K. Brennan, and Cunningham. Ernest Stevens Trio. <a href=search.php?nq=1&query_type=call_number&query=7121>Edison Gold Moulded Record: 4607</a>. 1922.</li>
    <li>When the work's all done this fall. Ernest V. Stoneman. <a href=search.php?nq=1&query_type=call_number&query=8115>Edison Blue Amberol: 5188</a>. 1926.</li>
    <li>The fatal wedding / Gussie L. Davis. Ernest V. Stoneman (The Blue Ridge Mountaineer).. <a href=search.php?nq=1&query_type=call_number&query=9249>Edison Blue Amberol: 5355</a>. 1927.</li>
    <li>Turkey in the straw. Ernie Golden. <a href=search.php?nq=1&query_type=call_number&query=7903>U.S. Everlasting Record: 307</a>. 1908.</li>
    <li>Alas! Those chimes - Maritana / William Vincent Wallace. Ethel Toms. <a href=search.php?nq=1&query_type=call_number&query=7658>Edison Blue Amberol: 23294</a>. 1914.</li>
    <li>Mary (Our Miss Gibbs) / Lionel Monckton. Ethel Williams and Jack Charman. <a href=search.php?nq=1&query_type=call_number&query=7088>Indestructible Record: 3165</a>. 1910.</li>
    <li>On the banks of the Brandywine / Dave Oppenheim and Anatol Friedland. Eugene Emmett. <a href=search.php?nq=1&query_type=call_number&query=9244>Edison Blue Amberol: 2357</a>. 1914.</li>
    <li>Medley of southern airs / unknown. F. J. Bacon. <a href=search.php?nq=1&query_type=call_number&query=7607>Edison Blue Amberol: 3122</a>. 1917  .</li>
    <li>Selections from "the dollar princess" / Leo Fall. Famous Indestructible Record Band. <a href=search.php?nq=1&query_type=call_number&query=8423>Indestructible Record: 3175</a>. 1910.</li>
    <li>Selections from the Balkan princess / Paul A. Rubens. Famous Indestructible Record Band. <a href=search.php?nq=1&query_type=call_number&query=8453>Indestructible Record: 3145</a>. 1910.</li>
    <li>Darktown strutter's ball / Shelton Brooks. Federal Band. <a href=search.php?nq=1&query_type=call_number&query=7079>Indestructible Record: 3442</a>. 1918.</li>
    <li>Girls study your cookery book / Maurice Scott ; Joe Burley. Florrie Forde. <a href=search.php?nq=1&query_type=call_number&query=7876>Edison Gold Moulded Record: 13715</a>. 1907.</li>
    <li>Keep straight down the road / Lawrence Wright. Florrie Forde. <a href=search.php?nq=1&query_type=call_number&query=9866>Edison Blue Amberol: 23051</a>. 1913.</li>
    <li>Keep on swinging me, Charlie / Long ; Scott. Florrie Forde. <a href=search.php?nq=1&query_type=call_number&query=9870>Edison Blue Amberol: 23006</a>. 1913.</li>
    <li>Kohala march / unknown. Ford Hawaiians. <a href=search.php?nq=1&query_type=call_number&query=7652>Edison Blue Amberol: 3446</a>. 1918.</li>
    <li>Dr. Whackem's Academy. Four Komical Kards. <a href=search.php?nq=1&query_type=call_number&query=7875>Edison Bell Record: 20098</a>. 1908.</li>
    <li>L'hotesse : chansonnette / Georges Fragerolle. Fragson. <a href=search.php?nq=1&query_type=call_number&query=7975>Cylindres Edison Moulés Sur Or: 17701</a>. 1906.</li>
    <li>Le pays des roses. Fragson. <a href=search.php?nq=1&query_type=call_number&query=7981>Mazo: 656</a>. 189-?.</li>
    <li>Alla stella confidente / Vincenzo Robaudi. Francesco Daddi. <a href=search.php?nq=1&query_type=call_number&query=8017>Edison Amberol: 5014</a>. 1909.</li>
    <li>It pays to serve Jesus / Louise Collins Nhare and George E. Nhare. Frank C. Huston. <a href=search.php?nq=1&query_type=call_number&query=8058>Edison Blue Amberol: 4122</a>. 1920.</li>
    <li>Stella / Edward Paulton. Frank C. Stanley. <a href=search.php?nq=1&query_type=call_number&query=7477>Indestructible Record: 953</a>. 1909.</li>
    <li>When the snowbirds cross the valley / Alfred Solman ; Monroe H. Rosenfeld. Frank C. Stanley. <a href=search.php?nq=1&query_type=call_number&query=8164>Columbia Phonograph Co.: 33078</a>. 1907.</li>
    <li>Two rubes in an eating house . Frank C. Stanley ; Byron G. Harlan. <a href=search.php?nq=1&query_type=call_number&query=8065>Columbia Phonograph Co.: 32267</a>. 1903.</li>
    <li>If I had a thousand lives to live / Alfred Solman. Frank C. Stanley ; Henry Burr. <a href=search.php?nq=1&query_type=call_number&query=7603>Indestructible Record: 3013</a>. 1910.</li>
    <li>Bell Brandon / Francis Woolcott. Frank Coombs. <a href=search.php?nq=1&query_type=call_number&query=7746>U.S. Everlasting Record: 1081</a>. 1909.</li>
    <li>Silver threads among the gold / H. P. Danks. Frank Coombs. <a href=search.php?nq=1&query_type=call_number&query=7912>Lakeside Indestructible Cylinder Record: 1065</a>. 1910.</li>
    <li>I really can't reach that top note / Arthurs. Frank Lombard. <a href=search.php?nq=1&query_type=call_number&query=7083>Indestructible Record: 1338</a>. 1910.</li>
    <li>Roll on silver moon / Charles M. Ernest. Frank M. Kamplain. <a href=search.php?nq=1&query_type=call_number&query=7806>Edison Blue Amberol: 3979</a>. 1920.</li>
    <li>Always in the way  / Chas. K. Harris. Frank Morgan. <a href=search.php?nq=1&query_type=call_number&query=7863>Lambert: 874</a>. 190-.</li>
    <li>Red wing / Kerry Mills. Frank Stanley and Henry Burr. <a href=search.php?nq=1&query_type=call_number&query=7075>Indestructible Record: 1366</a>. 1910.</li>
    <li>Silver bell / Edward Madden and Percy Wenrich. Frank Stanley and Henry Burr. <a href=search.php?nq=1&query_type=call_number&query=7082>Indestructible Record: 1413</a>. 1910.</li>
    <li>Killarney, my home o'er the sea / Frederick Knight Logan. Frank X. Doyle. <a href=search.php?nq=1&query_type=call_number&query=7676>Edison Blue Amberol: 1958</a>. 1913.</li>
    <li>Way down Barcelona way / Harry Jentes. Fred Hillebrand. <a href=search.php?nq=1&query_type=call_number&query=8135>Edison Blue Amberol: 4260</a>. 1921.</li>
    <li>My Sumurun girl : medley. Fred Van Eps. <a href=search.php?nq=1&query_type=call_number&query=8050>Edison Blue Amberol: 1549</a>. 1912.</li>
    <li>Thora / Fred Wheeler. Fred Wheeler. <a href=search.php?nq=1&query_type=call_number&query=7078>Indestructible Record: 3072</a>. 1910.</li>
    <li>Here we are! Here we are! etc. / Kenneth Lyle. Frederick J. Wheeler. <a href=search.php?nq=1&query_type=call_number&query=7562>Edison Blue Amberol: 23376</a>. 1915.</li>
    <li>Boys in khaki, boys in blue / Bennett Scott ; Arthur J. Mills. Frederick J. Wheeler. <a href=search.php?nq=1&query_type=call_number&query=7580>Edison Blue Amberol: 23367</a>. 1914.</li>
    <li>Tant pis pour elle / Henri Christiné. Fréjol. <a href=search.php?nq=1&query_type=call_number&query=9443>Cylindres Edison Moulés Sur Or: 17990</a>. 1907.</li>
    <li>Cohen on the telephone / G. L. Thompson. G. L. Thompson. <a href=search.php?nq=1&query_type=call_number&query=8030>Indestructible Record: 3370</a>. 191-.</li>
    <li>Des petits pierrots. Garde républicaine. <a href=search.php?nq=1&query_type=call_number&query=7980>Columbia Phonograph Co.: [unknown catalog number]</a>. 1898.</li>
    <li>Marseillaise / Rouget de Lisle, Claude Joseph. Garde Républicaine. <a href=search.php?nq=1&query_type=call_number&query=9781>Pathé: 4000</a>. 1900?.</li>
    <li>Marseillaise / Rouget de Lisle, Claude Joseph. Garde Républicaine. <a href=search.php?nq=1&query_type=call_number&query=9782>Pathé: 4000</a>. 1900.</li>
    <li>Marche originale / V. Destrost. Garde républicaine de Paris. <a href=search.php?nq=1&query_type=call_number&query=9862>Edison Blue Amberol: 27066</a>. 1913.</li>
    <li>The palms  / J. Faure. George Alexander. <a href=search.php?nq=1&query_type=call_number&query=7990>Columbia Phonograph Co.: 24005</a>. between 1904 and 1909.</li>
    <li>Till the sands of the desert grow cold / Ernest R. Ball. George Baker. <a href=search.php?nq=1&query_type=call_number&query=7314>Indestructible Record: 3308</a>. 1913.</li>
    <li>Comrade o' mine / Thomas. George Ballard. <a href=search.php?nq=1&query_type=call_number&query=6961>Edison Blue Amberol: 3995</a>. 1920.</li>
    <li>When father said he'd pay the rent / William Hargreaves. George Formby. <a href=search.php?nq=1&query_type=call_number&query=9879>Edison Blue Amberol: 23177</a>. 1913.</li>
    <li>Triplets: fox trot / George Hamilton Green. George Hamilton Green. <a href=search.php?nq=1&query_type=call_number&query=7790>Edison Blue Amberol: 3968</a>. 1920.</li>
    <li>Bedelia / N/A. George J. Gaskin. <a href=search.php?nq=1&query_type=call_number&query=6844>Columbia Phonograph Co.: 32320</a>. 1903.</li>
    <li>Let me call you sweetheart / Friedman. George W. Ballard. <a href=search.php?nq=1&query_type=call_number&query=7009>U.S. Everlasting Record: 1448</a>. 1912.</li>
    <li>Red wing / Kerry Mills. George W. Ballard. <a href=search.php?nq=1&query_type=call_number&query=7898>Lakeside Indestructible Cylinder Record: 1150</a>. 1909.</li>
    <li>I lost my heart in Honolulu / Will D. Cobb. George Wilton Ballard. <a href=search.php?nq=1&query_type=call_number&query=6977>Edison Blue Amberol: 2978</a>. 1916.</li>
    <li>Take me back to your heart / Fred Godfrey. George Wilton Ballard. <a href=search.php?nq=1&query_type=call_number&query=7563>Edison Blue Amberol: 23389</a>. 1915.</li>
    <li>Ages and ages / James Kendis ; James Brockman. George Wilton Ballard. <a href=search.php?nq=1&query_type=call_number&query=7705>Edison Blue Amberol: 4060</a>. 1920.</li>
    <li>When I was a dreamer (and you were my dream) / Egbert van Alstyne. George Wilton Ballard. <a href=search.php?nq=1&query_type=call_number&query=7731>Edison Blue Amberol: 2680</a>. 1915.</li>
    <li>Lonesome that's all / Roberts Bradley. George Wilton Ballard. <a href=search.php?nq=1&query_type=call_number&query=7815>Edison Blue Amberol: 3832</a>. 1919.</li>
    <li>When honey sings an old-time song / Joseph B. Carey. George Wilton Ballard. <a href=search.php?nq=1&query_type=call_number&query=7816>Edison Blue Amberol: 3976</a>. 1919.</li>
    <li>Was there ever a pal like you? / Irving Berlin. George Wilton Ballard. <a href=search.php?nq=1&query_type=call_number&query=7953>Edison Blue Amberol: 3971</a>. 1920.</li>
    <li>C'est une valse populaire / Paul Fauchey. Georges Elval. <a href=search.php?nq=1&query_type=call_number&query=7978>Cylindres Edison Moulés Sur Or: 18124</a>. 1909.</li>
    <li>The anvil chorus from il trovatore / Giuseppe Verdi. Gilmore's Band. <a href=search.php?nq=1&query_type=call_number&query=7113>Columbia Phonograph Co.: 1582</a>. 1896 - 1900.</li>
    <li>Bridal march from Lohengrin / Richard Wagner. Gilmore's Band. <a href=search.php?nq=1&query_type=call_number&query=7921>Columbia Phonograph Co.: 1519</a>. 1896- 1900.</li>
    <li>Mr. Thomas Cat. Gilmore's Band. <a href=search.php?nq=1&query_type=call_number&query=7946>Columbia Phonograph Co.: 31538</a>. 1901.</li>
    <li>Die Wacht am Rhein  / Carl Wilhelm. Gilmore's Band. <a href=search.php?nq=1&query_type=call_number&query=7991>Columbia Phonograph Co.: 1518</a>. between 1896 and 1900.</li>
    <li>Hearts and flowers  / Theo M. Tobani. Gilmore's Band. <a href=search.php?nq=1&query_type=call_number&query=8071>Columbia Phonograph Co.: 31597</a>. 1901.</li>
    <li>America. Gilmore's Band. <a href=search.php?nq=1&query_type=call_number&query=8602>Columbia Phonograph Co.: 1514</a>. 1896-1900.</li>
    <li>Our director march. Gilmore's Band. <a href=search.php?nq=1&query_type=call_number&query=9045>Columbia Phonograph Co.: 31848</a>. Between 1890 and 1902.</li>
    <li>La marseillaise / Claude Joseph Rouget de Lisle. Gilmore's Famous Band. <a href=search.php?nq=1&query_type=call_number&query=9153>Columbia Phonograph Co.: 1526</a>. between 1896 and 1900.</li>
    <li>Flora bella / Milton Schwarzwald. Gladys Rice. <a href=search.php?nq=1&query_type=call_number&query=6963>Edison Blue Amberol: 3076</a>. 1917.</li>
    <li>Sunnyside Sal / James Kendis ; Jame Brockman. Gladys Rice ; Vernon Dalhart . <a href=search.php?nq=1&query_type=call_number&query=8139>Edison Blue Amberol: 4353</a>. 1921.</li>
    <li>Go to sleep my little pickaninny / Fred Heltman. Gladys Rice and Chorus.. <a href=search.php?nq=1&query_type=call_number&query=7528>Edison Blue Amberol: 2857</a>. 1916.</li>
    <li>The wee little house that you live in / Tom Mellor. Glen Ellison ; Harry Gifford. <a href=search.php?nq=1&query_type=call_number&query=7573>Edison Blue Amberol: 23403</a>. 1915.</li>
    <li>Roses bloom for lovers - "The rose maid" / Bruno Granichstaedten. Grace Kerns. <a href=search.php?nq=1&query_type=call_number&query=7691>Edison Blue Amberol: 1504</a>. 1912.</li>
    <li>Has your mother any more like you / Robert A. Keiser. Grace Nelson, Frank Stanley. <a href=search.php?nq=1&query_type=call_number&query=6981>Edison Gold Moulded Record: 8831</a>. 1904.</li>
    <li>When we are married. Grace Spencer ; Harry MacDonough. <a href=search.php?nq=1&query_type=call_number&query=7916>Edison Record: 7558</a>. 1900.</li>
    <li>The blue and the grey. Greater New York Quartette. <a href=search.php?nq=1&query_type=call_number&query=7926>Columbia Phonograph Co.: 9071</a>. between 1896 and 1900.</li>
    <li>Marie - fox trot / Otto Motzan ; Henry Stantly. Green Brothers Novelty Band. <a href=search.php?nq=1&query_type=call_number&query=7641>Edison Blue Amberol: 4501</a>. 1922.</li>
    <li>Kiss a miss - medley waltz / Cal De Voll ; Jack Yellen ; Maurice Baron ; Abe Olman. Green Brothers Novelty Band. <a href=search.php?nq=1&query_type=call_number&query=7665>Edison Blue Amberol: 4311</a>. 1921.</li>
    <li>Siren of a southern sea - fox trot / Harold Weeks. Green Brothers Novelty Band. <a href=search.php?nq=1&query_type=call_number&query=7668>Edison Blue Amberol: 4272</a>. 1921.</li>
    <li>Fancies: fox trot / Herbert Spencer. Green Brothers Novelty Band. <a href=search.php?nq=1&query_type=call_number&query=8144>Edison Blue Amberol: 4429</a>. 1921.</li>
    <li>Drowsy head / Irving Berlin. Green Brothers Novelty Band. <a href=search.php?nq=1&query_type=call_number&query=9117>Edison Blue Amberol: 4337</a>. 1921.</li>
    <li>The vamp oriental: one step / Byron Gay. Green Brothers Novelty Orchestra. <a href=search.php?nq=1&query_type=call_number&query=7821>Edison Blue Amberol: 3850</a>. 1919.</li>
    <li>Bugle union march. Grenadier Fife, Drum and Bugle Band. <a href=search.php?nq=1&query_type=call_number&query=9058>Edison Bell Record: 6700</a>. either 1900 or 1901.</li>
    <li>La spagnola / Vincenzo Di Chiara. Guido Ciccolini. <a href=search.php?nq=1&query_type=call_number&query=9871>Edison Royal Purple Amberol: 29029</a>. 1918.</li>
    <li>Mignon: polonaise / Ambroise Thomas. Guido Gialdini. <a href=search.php?nq=1&query_type=call_number&query=7962>Edison Blue Amberol: 2434</a>. 1914.</li>
    <li>Waldvoglein. Guido Gialdini. <a href=search.php?nq=1&query_type=call_number&query=8435>Indestructible Record: 1500</a>. 1911.</li>
    <li>When the bell in the lighthouse rings / Alfred Solman. Gus Reed. <a href=search.php?nq=1&query_type=call_number&query=7845>Edison Blue Amberol: 1956</a>. 1913.</li>
    <li>Laverne: waltz caprice / H. Benne Henton. H. Benne Henton. <a href=search.php?nq=1&query_type=call_number&query=7823>Edison Blue Amberol: 3737</a>. 1919.</li>
    <li>A dream / James Carol Bartlett. Hardy Williamson. <a href=search.php?nq=1&query_type=call_number&query=6983>Edison Blue Amberol: 3046</a>. 1917.</li>
    <li>Roses / Stephen Adams. Hardy Williamson. <a href=search.php?nq=1&query_type=call_number&query=7662>Edison Blue Amberol: 23159</a>. 1914.</li>
    <li>Hark! Hark!  My soul /  Frederick William Faber ; Henry Thomas Smart. Hardy Williamson and T. F. Kinniburgh. <a href=search.php?nq=1&query_type=call_number&query=7847>Edison Blue Amberol: 23050</a>. 1913.</li>
    <li>Dew drops. Harry A. Yerkes. <a href=search.php?nq=1&query_type=call_number&query=8543>Indestructible Record: 1434</a>. 1910.</li>
    <li>All for love of you / Ernest Ball. Harry Anthony. <a href=search.php?nq=1&query_type=call_number&query=6964>Edison Record: 9906</a>. 1908.</li>
    <li>Won't you love me Molly darling / unknown. Harry Anthony. <a href=search.php?nq=1&query_type=call_number&query=7109>Indestructible Record: 1479</a>. 1911?.</li>
    <li>I'll love you forevermore / Henry Frantzen. Harry Anthony. <a href=search.php?nq=1&query_type=call_number&query=7889>Edison Blue Amberol: 1629</a>. 1913.</li>
    <li>Baby's eyes. Harry Anthony. <a href=search.php?nq=1&query_type=call_number&query=9253>Edison Blue Amberol: 22544</a>. 191-?.</li>
    <li>The Valley of Peace. Harry Anthony ; James F. Harrison. <a href=search.php?nq=1&query_type=call_number&query=7480>U.S. Everlasting Record: 1233</a>. 1910?.</li>
    <li>Let the lower lights be burning / P. P. Bliss. Harry Anthony ; James F. Harrison. <a href=search.php?nq=1&query_type=call_number&query=7747>U.S. Everlasting Record: 234</a>. 1908.</li>
    <li>Rescue the perishing / W. Howard Doane ; Fanny J. Crosby. Harry Anthony ; James F. Harrison. <a href=search.php?nq=1&query_type=call_number&query=7869>Edison Gold Moulded Record: 9711</a>. 1907.</li>
    <li>Tarry with me. Harry Anthony ; James F. Harrison. <a href=search.php?nq=1&query_type=call_number&query=8021>Indestructible Record: 3183</a>. 1911.</li>
    <li>He lifted me /  Charles Hutchinson Gabriel. Harry Anthony ; James F. Harrison. <a href=search.php?nq=1&query_type=call_number&query=8042>Edison Gold Moulded Record: 9759</a>. 1908.</li>
    <li>That funny little bob-tail coat / Robert Gorman ; Tom Lowan. Harry Champion. <a href=search.php?nq=1&query_type=call_number&query=8162>Edison Gold Moulded Record: 13573</a>. 1904.</li>
    <li>Luke / Bret Harte. Harry E. Humphrey. <a href=search.php?nq=1&query_type=call_number&query=9073>Edison Blue Amberol: 1608</a>. 1912.</li>
    <li>Amours Fragiles / Harry Fragson. Harry Fragson. <a href=search.php?nq=1&query_type=call_number&query=9490>Cylindres Edison Moulés Sur Or: 17248</a>. 1904.</li>
    <li>L'anglais triste : monologue comique / Harry Fragson. Harry Fragson. <a href=search.php?nq=1&query_type=call_number&query=9495>Cylindres Edison Moulés Sur Or: 17341</a>. 1904 or 1905.</li>
    <li>My little Coney Isle. Harry L. Tally. <a href=search.php?nq=1&query_type=call_number&query=7759>Edison Gold Moulded Record: 8483</a>. 1903.</li>
    <li>The wee hoose 'mang the heather / Fred Elton. Harry Lauder. <a href=search.php?nq=1&query_type=call_number&query=7629>Edison Blue Amberol: 3654</a>. 1919.</li>
    <li>Weddin' of Sandy Macnab / Harry Lauder. Harry Lauder. <a href=search.php?nq=1&query_type=call_number&query=7750>Edison Blue Amberol: 5243</a>. 1926.</li>
    <li>Queen amang the heather / Harry Lauder. Harry Lauder. <a href=search.php?nq=1&query_type=call_number&query=8401>Edison Blue Amberol: 5506</a>. 1928.</li>
    <li>Absence makes the heart grow fonder / Herbert Dillea. Harry MacDonough. <a href=search.php?nq=1&query_type=call_number&query=7455>Edison Gold Moulded Record: 7870</a>. 1902?.</li>
    <li>Only me. Harry MacDonough. <a href=search.php?nq=1&query_type=call_number&query=7939>Edison Record: 2041</a>. [ca. 1897].</li>
    <li>In the valley of the sunny San Joaquin / James G. Dewey. Harry MacDonough. <a href=search.php?nq=1&query_type=call_number&query=8040>Edison Gold Moulded Record: 8860</a>. 1904.</li>
    <li>Pretty Kitty Kelly / William Bonner. Harry Pease ; Ed G. Nelson. <a href=search.php?nq=1&query_type=call_number&query=8052>Edison Blue Amberol: 4085</a>. 1920.</li>
    <li>Toddle: medley toddle / Paul Biese. Harry Raderman's Jazz Orchestra. <a href=search.php?nq=1&query_type=call_number&query=8089>Edison Blue Amberol: 4292</a>. 1921.</li>
    <li>Broadway Rose  / Otis Spencer. Harry Raderman's Jazz Orchestra. <a href=search.php?nq=1&query_type=call_number&query=8092>Edison Blue Amberol: 4224</a>. 1920.</li>
    <li>Can't you see I'm lonely / Harry Armstrong. Harry Tally. <a href=search.php?nq=1&query_type=call_number&query=8067>Columbia Phonograph Co.: 32946</a>. 1906.</li>
    <li>Good-bye Betty Brown. Harry Tally.. <a href=search.php?nq=1&query_type=call_number&query=7301>U.S. Everlasting Record: 1128</a>. 1909.</li>
    <li>When a peach in Georgia weds a rose in Alabam' / Clyde Hager. Hart & James. <a href=search.php?nq=1&query_type=call_number&query=6962>Edison Blue Amberol: 4088</a>. 1920.</li>
    <li>Roses, roses everywhere / H.(Henry) Trotère. Harvey Hindermyer. <a href=search.php?nq=1&query_type=call_number&query=7231>Edison Blue Amberol: 1554</a>. 1912.</li>
    <li>In the evening by the moonlight, dear Louise / Harry Von Tilzer. Harvey Hindermyer. <a href=search.php?nq=1&query_type=call_number&query=7240>Edison Blue Amberol: 2457</a>. 1914.</li>
    <li>When the robin calls his mate / Jacob Henry Ellis. Harvey Hindermyer. <a href=search.php?nq=1&query_type=call_number&query=8376>Edison Blue Amberol: 4619</a>. 1922.</li>
    <li>If I'm not at the roll-call, kiss mother "good-bye" for me /  George L. Boyden. Harvey Hindermyer . <a href=search.php?nq=1&query_type=call_number&query=8123>Edison Blue Amberol: 3630</a>. 1919.</li>
    <li> Höchstes Vertrauen / Richard Wagner. Heinrich Knote. <a href=search.php?nq=1&query_type=call_number&query=8003>Edison Gold Moulded Record: 1</a>. 1906.</li>
    <li>Ev'rybody calls me Honey / Charley Straight. Helen Clark. <a href=search.php?nq=1&query_type=call_number&query=7587>Edison Blue Amberol: 4019</a>. 1919.</li>
    <li>Mother's dear old chair / Genevieve Scott. Helen Clark ; Harvey Hindermyer. <a href=search.php?nq=1&query_type=call_number&query=7657>Edison Blue Amberol: 2316</a>. 1914.</li>
    <li>When you're away / Bert Grant. Helen Clark ; Harvey Hindermyer. <a href=search.php?nq=1&query_type=call_number&query=7659>Edison Blue Amberol: 1505</a>. 1912.</li>
    <li>Just suppose / Lyn Udall ; Karl Kennett. Helen Clark ; Joseph A. Phillips. <a href=search.php?nq=1&query_type=call_number&query=7586>Edison Blue Amberol: 4544</a>. 1921.</li>
    <li>My bugler boy / Herman Darewski. Helen Clark ; R. P. Weston. <a href=search.php?nq=1&query_type=call_number&query=7568>Edison Blue Amberol: 23372</a>. 1915.</li>
    <li>Mrs. Sippi, you're a grand old lady / Belle Ashlyn. Helen Clark and Billy Murray. <a href=search.php?nq=1&query_type=call_number&query=9041>Edison Blue Amberol: 2495</a>. 1914.</li>
    <li>When I met you / F. Henri Klickmann. Helen Clark and George Wilton Ballard. <a href=search.php?nq=1&query_type=call_number&query=7808>Edison Blue Amberol: 3815</a>. 1919.</li>
    <li>My bird of paradise / Irving Berlin. Helen Louise ; Frank Ferera. <a href=search.php?nq=1&query_type=call_number&query=8001>Indestructible Record: 3427</a>. 191-.</li>
    <li>Lulu, and her la, la, la / Harry Von Tilzer. Helen Trix. <a href=search.php?nq=1&query_type=call_number&query=8039>Edison Gold Moulded Record: 9574</a>. 1907.</li>
    <li>Le cœur et la main : L'adjudant et sa monture / Charles Lecocq. Henri Wéber. <a href=search.php?nq=1&query_type=call_number&query=9518>Columbia Record: 37033</a>. 1902 or 1903.</li>
    <li>Is there still room for me / unknown. Henry Burr. <a href=search.php?nq=1&query_type=call_number&query=7028>Indestructible Record: 1526</a>. 1916.</li>
    <li>When it's apple blossom time in Normandy / Mellor, Gifford & Trevor. Henry Burr. <a href=search.php?nq=1&query_type=call_number&query=7095>Indestructible Record: 3319</a>. 1914.</li>
    <li>Good-night, little girl, good-night / James Cartwright Macy. Henry Burr. <a href=search.php?nq=1&query_type=call_number&query=7589>Indestructible Record: 3052</a>. 1910.</li>
    <li>Saviour, lead me lest I stray. Henry Burr. <a href=search.php?nq=1&query_type=call_number&query=8064>Columbia Phonograph Co.: 32768</a>. 1905.</li>
    <li>In the golden afterwhile. Henry Burr. <a href=search.php?nq=1&query_type=call_number&query=8522>U.S. Everlasting Record: 1372</a>. 1909.</li>
    <li>Just a baby's prayer at twilight / M. K. Jerome. Henry Burr. <a href=search.php?nq=1&query_type=call_number&query=8583>Indestructible Record: 3436</a>. 1918.</li>
    <li>Your King and Country need you / Trevor Huntley. Henry E. Pether ; Hughes Macklin. <a href=search.php?nq=1&query_type=call_number&query=7577>Edison Blue Amberol: 23347</a>. 1914.</li>
    <li>The friendly rivals / Charles Godfrey. Herbert Lincoln Clarke ; John Hazel. <a href=search.php?nq=1&query_type=call_number&query=8047>Edison Gold Moulded Record: 9125</a>. 1905.</li>
    <li>I used to sigh for the silvery moon / Herman Darewski. Herbert Payne. <a href=search.php?nq=1&query_type=call_number&query=8156>Edison Amberol: 12032</a>. 1909.</li>
    <li>Gypsy John / Frederic Clay. Herbert Stuart. <a href=search.php?nq=1&query_type=call_number&query=7708>Edison Blue Amberol: 1649</a>. 1913.</li>
    <li>The Athol highlanders march / unknown. Highlanders Bagpipe Band. <a href=search.php?nq=1&query_type=call_number&query=7593>Edison Blue Amberol: 23079</a>. 1913.</li>
    <li>Selection of Moore's Irish melodies /  C. H. Hassell. His Majesty's Irish Guards Band. <a href=search.php?nq=1&query_type=call_number&query=8159>Edison Amberol: 12392</a>. 1911.</li>
    <li>A dusky lullaby / Hallett Gilberte. Homestead Trio. <a href=search.php?nq=1&query_type=call_number&query=7805>Edison Blue Amberol: 3684</a>. 1919.</li>
    <li>The little tin soldier or the little rag doll / James F. Hanley.  Homestead Trio. <a href=search.php?nq=1&query_type=call_number&query=8016>Edison Blue Amberol: 4567</a>. 1922.</li>
    <li>Keep the home fires burning  / Ivor Novello. Homestead Trio. <a href=search.php?nq=1&query_type=call_number&query=8093>Edison Blue Amberol: 3646</a>. 1918.</li>
    <li>Little grey home in the west / D. Eardley-Wilmot. Hughes Macklin. <a href=search.php?nq=1&query_type=call_number&query=7611>Edison Blue Amberol: 23282</a>. 1914.</li>
    <li>A loved voice / Ruth Rutherford. Hughes Macklin. <a href=search.php?nq=1&query_type=call_number&query=7683>Edison Blue Amberol: 23239</a>. 1914.</li>
    <li>Somewhere a voice is calling / Arthur F. Tate. Hughes Macklin. <a href=search.php?nq=1&query_type=call_number&query=7684>Edison Blue Amberol: 23227</a>. 1914.</li>
    <li>When love creeps in your heart / A. J. Mills ; Bennett Scott. Hughes Macklin. <a href=search.php?nq=1&query_type=call_number&query=7830>Edison Blue Amberol: 23212</a>. 1914.</li>
    <li>Mary, kind and gentle is she / Thomas Richardson. Hughes Macklin ; David Brazell. <a href=search.php?nq=1&query_type=call_number&query=7571>Edison Blue Amberol: 23278</a>. 1914.</li>
    <li>Her bright smile haunts me still / J.E. Carpenter. Hughes Macklin ; David Brazell. <a href=search.php?nq=1&query_type=call_number&query=7620>Edison Blue Amberol: 23216</a>. 1914.</li>
    <li>On the banks of Allan Water / unknown. Hughes Macklin ; David Brazell. <a href=search.php?nq=1&query_type=call_number&query=7694>Edison Blue Amberol: 23156</a>. 1914.</li>
    <li>The dear little shamrock / Cherry, A.. Hughes Macklin ; David Brazell. <a href=search.php?nq=1&query_type=call_number&query=7700>Edison Blue Amberol: 23230</a>. 1914.</li>
    <li>Mary of Argyle / S. Nelson. Hughes Macklin ; David Brazell. <a href=search.php?nq=1&query_type=call_number&query=7835>Edison Blue Amberol: 23203</a>. 1913.</li>
    <li>Eileen Alannah / E.S. Marble ; J.R. Thomas. Hughes Macklin ; David Brazell. <a href=search.php?nq=1&query_type=call_number&query=7836>Edison Blue Amberol: 23125</a>. 1913.</li>
    <li>Blue bird inspiration  / Milbury H. Ryder. Imperial Marimba Band
      . <a href=search.php?nq=1&query_type=call_number&query=7833>Edison Blue Amberol: 4499</a>. 1922.</li>
    <li>Jack Tar / John Philip Sousa. Indestructible Band. <a href=search.php?nq=1&query_type=call_number&query=8490>Indestructible Record: 1444</a>. 1911.</li>
    <li>The free lance / John Philip Sousa. Indestructible Band. <a href=search.php?nq=1&query_type=call_number&query=8577>Indestructible Record: 1456</a>. 1911-1914.</li>
    <li>Dreaming / Archibald Joyce. Indestructible Band. <a href=search.php?nq=1&query_type=call_number&query=8581>Indestructible Record: 3326</a>. 3326.</li>
    <li>Where do we go from here ; Over there / Percy Wenrich. Indestructible Band. <a href=search.php?nq=1&query_type=call_number&query=8584>Indestructible Record: 3432</a>. 1918.</li>
    <li>Pearla. Indestructible Bohemian Orchestra. <a href=search.php?nq=1&query_type=call_number&query=7743>Indestructible Record: 663</a>. 1907.</li>
    <li>Rainbow medley / Percy Wenrich. Indestructible Concert Band. <a href=search.php?nq=1&query_type=call_number&query=8012>Indestructible Record: 986</a>. 1909.</li>
    <li>Snyder successes. Indestructible Concert Band. <a href=search.php?nq=1&query_type=call_number&query=8424>Indestructible Record: 3262</a>. 1912.</li>
    <li>Remick's 1912 hits. Indestructible Concert Band. <a href=search.php?nq=1&query_type=call_number&query=8426>Indestructible Record: 3256</a>. 1912.</li>
    <li>Danza delle ore / Amilcare Ponchielli. Indestructible Concert Band. <a href=search.php?nq=1&query_type=call_number&query=8481>Indestructible Record: 1005</a>. 1909.</li>
    <li>Genee Waltz / Maurice Levi. Indestructible Concert Band. <a href=search.php?nq=1&query_type=call_number&query=8531>Indestructible Record: 935</a>. 1908.</li>
    <li>Medley from "The merry widow" /  Franz Lehár. Indestructible Concert Band. <a href=search.php?nq=1&query_type=call_number&query=8554>Indestructible Record: 706</a>. 1908.</li>
    <li>Herd girl's dream / August Labitsky. Indestructible Instrumental Trio. <a href=search.php?nq=1&query_type=call_number&query=7035>Indestructible Record: 1229</a>. 1909.</li>
    <li>Old Faithful / Holzmann. Indestructible Military Band. <a href=search.php?nq=1&query_type=call_number&query=6975>Indestructible Record: 640</a>. 1907.</li>
    <li>23rd regiment march / Gilmore. Indestructible Military Band. <a href=search.php?nq=1&query_type=call_number&query=6987>Indestructible Record: 684</a>. 1908.</li>
    <li>Softly unawares / Paul Lincke. Indestructible Military Band. <a href=search.php?nq=1&query_type=call_number&query=7012>Indestructible Record: 3137</a>. 1910.</li>
    <li>Blue and gray patrol / unknown. Indestructible Military Band. <a href=search.php?nq=1&query_type=call_number&query=7070>Indestructible Record: 1238</a>. 1910.</li>
    <li>Siamese patrol / Paul Lincke. Indestructible Military Band. <a href=search.php?nq=1&query_type=call_number&query=7089>Indestructible Record: 1412</a>. 1910.</li>
    <li>Aubade printaniere / Paul Lacombe. Indestructible Military Band. <a href=search.php?nq=1&query_type=call_number&query=7097>Indestructible Record: 3123</a>. 1910.</li>
    <li>Southern roses waltz / Johann Strauss. Indestructible Military Band. <a href=search.php?nq=1&query_type=call_number&query=7325>Indestructible Record: 3110</a>. 1910.</li>
    <li>Around the Christmas tree. Indestructible Military Band. <a href=search.php?nq=1&query_type=call_number&query=7728>Indestructible Record: 1440</a>. 1910.</li>
    <li>New colonial march / Robert Browne Hall. Indestructible Military Band. <a href=search.php?nq=1&query_type=call_number&query=7971>Indestructible Record: 1372</a>. 1910.</li>
    <li>Anvil chorus / Giuseppe Verdi. Indestructible Military Band. <a href=search.php?nq=1&query_type=call_number&query=8038>Indestructible Record: 666</a>. 1907.</li>
    <li>March indienne / Adolphe Valentin Sellenick. Indestructible Military Band. <a href=search.php?nq=1&query_type=call_number&query=8422>Indestructible Record: 1281</a>. 1910.</li>
    <li>American spirit march. Indestructible Military Band. <a href=search.php?nq=1&query_type=call_number&query=8431>Indestructible Record: 1495</a>. 1911.</li>
    <li>The mill in the forest / Richard Eilenberg. Indestructible Military Band. <a href=search.php?nq=1&query_type=call_number&query=8432>Indestructible Record: 1401</a>. 1910.</li>
    <li>The old homestead / William H. Penn. Indestructible Military Band. <a href=search.php?nq=1&query_type=call_number&query=8445>Indestructible Record: 3330</a>. 1914.</li>
    <li>Irresistible / L. Logatti. Indestructible Military Band. <a href=search.php?nq=1&query_type=call_number&query=8448>Indestructible Record: 3321</a>. 1913.</li>
    <li>Georgia barn dance / Kerry Mills. Indestructible Military Band. <a href=search.php?nq=1&query_type=call_number&query=8454>Indestructible Record: 3190</a>. 1911.</li>
    <li>Medley of Indian hits. Indestructible Military Band. <a href=search.php?nq=1&query_type=call_number&query=8455>Indestructible Record: 3205</a>. 1911.</li>
    <li>Girls of Baden /  Karel Komzák. Indestructible Military Band. <a href=search.php?nq=1&query_type=call_number&query=8458>Indestructible Record: 3113</a>. 1910.</li>
    <li>Orpheus / Jacques Offenbach. Indestructible Military Band. <a href=search.php?nq=1&query_type=call_number&query=8459>Indestructible Record: 3115</a>. 1910.</li>
    <li>Pilgrims chorus / Richard Wagner. Indestructible Military Band. <a href=search.php?nq=1&query_type=call_number&query=8462>Indestructible Record: 3109</a>. 1910.</li>
    <li>Reminiscences of Scotland. Indestructible Military Band. <a href=search.php?nq=1&query_type=call_number&query=8478>Indestructible Record: 3214</a>. 1910.</li>
    <li>Barn yard galop (kikiriki). Indestructible Military Band. <a href=search.php?nq=1&query_type=call_number&query=8479>Indestructible Record: 1165</a>. 1909.</li>
    <li>Royal Italian march. Indestructible Military Band. <a href=search.php?nq=1&query_type=call_number&query=8480>Indestructible Record: 1185</a>. 1909.</li>
    <li>El capitan march / John Philip Sousa. Indestructible Military Band. <a href=search.php?nq=1&query_type=call_number&query=8482>Indestructible Record: 1176</a>. 1909.</li>
    <li>Kerry Mill's barn dance / Kerry Mills. Indestructible Military Band. <a href=search.php?nq=1&query_type=call_number&query=8487>Indestructible Record: 1141</a>. 1909.</li>
    <li>Tambour der garde / Anton Emil Titl. Indestructible Military Band. <a href=search.php?nq=1&query_type=call_number&query=8489>Indestructible Record: 766</a>. 1908.</li>
    <li>Coronation march / Giacomo Meyerbeer. Indestructible Military Band. <a href=search.php?nq=1&query_type=call_number&query=8502>Indestructible Record: 1272</a>. 1909.</li>
    <li>The Grasshoppers' dance / Ernest  Bucalossi. Indestructible Military Band. <a href=search.php?nq=1&query_type=call_number&query=8503>Indestructible Record: 1382</a>. 1910.</li>
    <li>Washington Post / John Philip Sousa. Indestructible Military Band. <a href=search.php?nq=1&query_type=call_number&query=8528>Indestructible Record: 997</a>. 1909.</li>
    <li>In darkest Africa. Indestructible Military Band. <a href=search.php?nq=1&query_type=call_number&query=8530>Indestructible Record: 785</a>. 1908.</li>
    <li>March de Molay Commandery. Indestructible Military Band. <a href=search.php?nq=1&query_type=call_number&query=8545>Indestructible Record: 1257</a>. 1910.</li>
    <li>The coquette / John Philip Sousa. Indestructible Military Band. <a href=search.php?nq=1&query_type=call_number&query=8546>Indestructible Record: 1431</a>. 1910.</li>
    <li>The whistlers / Indestructible Military Band. Indestructible Military Band. <a href=search.php?nq=1&query_type=call_number&query=8547>Indestructible Record: 1213</a>. 1909.</li>
    <li>Broncho Bill. Indestructible Military Band. <a href=search.php?nq=1&query_type=call_number&query=8574>Indestructible Record: 1201</a>. 1909.</li>
    <li>The bride elect / John Philip Sousa. Indestructible Military Band. <a href=search.php?nq=1&query_type=call_number&query=8576>Indestructible Record: 1371</a>. 1910.</li>
    <li>High school cadets march / John Philip Sousa. Indestructible Military Band. <a href=search.php?nq=1&query_type=call_number&query=8579>Indestructible Record: 949</a>. 1908.</li>
    <li>Warbler's serenade / J. Perry. Indestructible Military Band. <a href=search.php?nq=1&query_type=call_number&query=8580>Indestructible Record: 1115</a>. 1909.</li>
    <li>Near my God to Thee / Lowell Mason. Indestructible Military Band. <a href=search.php?nq=1&query_type=call_number&query=8582>Indestructible Record: 868</a>. 1908.</li>
    <li>Christmas echoes. Indestructible Military Band ; Peerless Quartet. <a href=search.php?nq=1&query_type=call_number&query=7308>Indestructible Record: 3160</a>. 1910.</li>
    <li>Washington grays / Claudio S. Grafulla. Indestructible Record Band. <a href=search.php?nq=1&query_type=call_number&query=8428>Indestructible Record: 3468 </a>. 1919.</li>
    <li>Hearts and flowers / Theo M. Tobani. Indestructible Symphony Indestructible Symphony Orchestra. <a href=search.php?nq=1&query_type=call_number&query=8023>Indestructible Record: 3197</a>. 1911.</li>
    <li>Everybody's doin' it / Irving Berlin. Indestructible Symphony Orchestra. <a href=search.php?nq=1&query_type=call_number&query=7025>Indestructible Record: 3250</a>. 1911.</li>
    <li>Charme d'amour / Edwin F. Kendall. Indestructible Symphony Orchestra. <a href=search.php?nq=1&query_type=call_number&query=7081>Indestructible Record: 3120</a>. 1910.</li>
    <li>Flower girl intermezzo / Perry Wenrich. Indestructible Symphony Orchestra. <a href=search.php?nq=1&query_type=call_number&query=7478>Indestructible Record: 1061</a>. 1909.</li>
    <li>Simple confession / Francis Thomé. Indestructible Symphony Orchestra. <a href=search.php?nq=1&query_type=call_number&query=8029>Indestructible Record: 1349</a>. 1910.</li>
    <li>Meet me where the lanterns glow / Manuel Klein. Indestructible Symphony Orchestra. <a href=search.php?nq=1&query_type=call_number&query=8477>Indestructible Record: 3105</a>. 1910.</li>
    <li>Country dance & merrymakers dance. Indestructible Symphony Orchestra. <a href=search.php?nq=1&query_type=call_number&query=8495>Indestructible Record: 3170</a>. 1910.</li>
    <li>Under the tent / Neil Moret. Indestuctible Military Band. <a href=search.php?nq=1&query_type=call_number&query=8496>Indestructible Record: 1416</a>. 1910.</li>
    <li>I bostonvalsen lefver jag än / N/A. Ingeborg Laudon. <a href=search.php?nq=1&query_type=call_number&query=7252>Edison Blue Amberol: 9431</a>. 1913.</li>
    <li>Kentucky Babe / Invincible Quartet. Invincible Quartet. <a href=search.php?nq=1&query_type=call_number&query=7945>Columbia Phonograph Co.: 9054</a>. between 1896 and 1900.</li>
    <li>Sunshine and roses / Egbert Van Alstyne. Irving Gillette. <a href=search.php?nq=1&query_type=call_number&query=7673>Edison Blue Amberol: 2023</a>. 1913.</li>
    <li>Sailing on the good ship Sunshine / David Reed. Irving Kaufman. <a href=search.php?nq=1&query_type=call_number&query=7561>Edison Blue Amberol: 23399</a>. 1915.</li>
    <li>She lives down in our alley / Chas R. McCarron and Charles Anthony Bayha. Irving Kaufman. <a href=search.php?nq=1&query_type=call_number&query=7785>Edison Blue Amberol: 2694</a>. 1915.</li>
    <li>I found the end of the rainbow / John Mears ; Harry Tierney ; Joseph McCarthy. Irving Kaufman. <a href=search.php?nq=1&query_type=call_number&query=7801>Edison Blue Amberol: 3714</a>. 1919.</li>
    <li>Some little girl named Mary / Fred Godfrey and Warton David. Irving Kaufman. <a href=search.php?nq=1&query_type=call_number&query=7841>Edison Blue Amberol: 23387</a>. 1915.</li>
    <li>There's a little white church in the valley / Arthur A. Penn. Irving Kaufman. <a href=search.php?nq=1&query_type=call_number&query=8364>Edison Blue Amberol: 2717</a>. 1915.</li>
    <li>Everybody loves my girl / Nat D. Ayer. Irving Kaufman. <a href=search.php?nq=1&query_type=call_number&query=9040>Edison Blue Amberol: 2390</a>. 1914.</li>
    <li>When the bees make honey: down in sunny Alabam' / Walter Donaldson. Irving Kaufman ; Jack Kaufman. <a href=search.php?nq=1&query_type=call_number&query=8137>Edison Blue Amberol: 3834</a>. 1919.</li>
    <li>Titl's serenade / Anton Titl. Issler's orchestra. <a href=search.php?nq=1&query_type=call_number&query=7947>Columbia Phonograph Co.: 2520</a>. 1896 or 1897.</li>
    <li>Mazurka. Issler's Orchestra. <a href=search.php?nq=1&query_type=call_number&query=8658>U.S. Phonograph Co.: [unknown catalog number]</a>. 1893 or 1894.</li>
    <li>Delightful women . J. Charman ; Ida Hamilton. <a href=search.php?nq=1&query_type=call_number&query=8033>Indestructible Record: 3142</a>. 191-.</li>
    <li>The old church hymns and bells / W. E. Naunton ; Chrystal. J. F. Harrison ; Edison Mixed Quartet. <a href=search.php?nq=1&query_type=call_number&query=7862>Edison Blue Amberol: 22543</a>. 1911.</li>
    <li>Rocked in the cradle of the deep / Joseph Philip Knight ; Emma Willard. J. W. Myers. <a href=search.php?nq=1&query_type=call_number&query=7917>North American Phonograph Co.: [unknown catalog number]</a>. [ca. 1894].</li>
    <li>Wait at the gate for me  / Theodore F. Morse. J. W. Myers. <a href=search.php?nq=1&query_type=call_number&query=7995>Columbia Phonograph Co.: 31953</a>. ca 1901?.</li>
    <li>On a moonlight night. J. W. Myers. <a href=search.php?nq=1&query_type=call_number&query=8006>Columbia Phonograph Co.: 32245</a>. ca 1901?.</li>
    <li>The Midshipmite  / Frederic Edward Weatherly ; Stephen Adams. J. W. Myers. <a href=search.php?nq=1&query_type=call_number&query=8007>Columbia Phonograph Co.: 5609</a>. between 1898 and 1900.</li>
    <li>The matrimonial handicap / Robert P. Weston ; Fred J. Barnes. Jack Charman. <a href=search.php?nq=1&query_type=call_number&query=7878>Edison Blue Amberol: 23158</a>. 1913.</li>
    <li>All the girls are lovely by the seaside / Fragson. Jack Charman. <a href=search.php?nq=1&query_type=call_number&query=8881>Edison Blue Amberol: 23108</a>. 1913.</li>
    <li>Beautiful isle of somewhere / Jessie Brown Pounds and J.S.Fearis. James F. Harrison. <a href=search.php?nq=1&query_type=call_number&query=7085>Indestructible Record: 3365</a>. 1915.</li>
    <li>Hosanna / Jules Granier. James F. Harrison. <a href=search.php?nq=1&query_type=call_number&query=7099>Indestructible Record: 3054</a>. 1910.</li>
    <li>Some day / Charles Hutchinson Gabriel. James F. Harrison. <a href=search.php?nq=1&query_type=call_number&query=7591>Indestructible Record: 3089</a>. 1910.</li>
    <li>For all eternity / Angelo Mascheroni. James Harrod. <a href=search.php?nq=1&query_type=call_number&query=6997>Edison Blue Amberol: 2983</a>. 1916.</li>
    <li>Once in a while / Gustav Luders. James J. Harrison.. <a href=search.php?nq=1&query_type=call_number&query=9270>Edison Amberol: 104</a>. 1909.</li>
    <li>The ragtime drummer / James I. Lent. James Lent. <a href=search.php?nq=1&query_type=call_number&query=8492>Indestructible Record: 689</a>. 1908.</li>
    <li>Casey at the telephone / Russell Hunting. James White. <a href=search.php?nq=1&query_type=call_number&query=7923>Edison Gold Moulded Record: 8069</a>. 1902.</li>
    <li>Waters of Venice waltz / Albert Von Tilzer. Jaudas' Society Orchestra. <a href=search.php?nq=1&query_type=call_number&query=7680>Edison Blue Amberol: 2966</a>. 1916.</li>
    <li>Destiny waltz / Sydney Baynes. Jaudas' Society Orchestra. <a href=search.php?nq=1&query_type=call_number&query=7726>Edison Blue Amberol: 23400</a>. 1915?.</li>
    <li>Shadowland: fox trot / Lawrence B. Gilbert. Jaudas' Society Orchestra. <a href=search.php?nq=1&query_type=call_number&query=8121>Edison Blue Amberol: 2666</a>. 1915.</li>
    <li>Broken melody / Auguste Van Biene. Jean Schwiller. <a href=search.php?nq=1&query_type=call_number&query=7832>Edison Blue Amberol: 23016</a>. 1913.</li>
    <li>The little old red shawl my mother wore. Jere Mahoney. <a href=search.php?nq=1&query_type=call_number&query=7938>Edison Record: 1534</a>. [ca. 1897].</li>
    <li>Just a girl that men forget / unknown. Jim Doherty. <a href=search.php?nq=1&query_type=call_number&query=7713>Edison Blue Amberol: 4812</a>. 1923.</li>
    <li>Mary O'Brien /  William C. Polla. Jim Doherty. <a href=search.php?nq=1&query_type=call_number&query=8133>Edison Blue Amberol: 4446</a>. 1922.</li>
    <li>Sally won't you come back? / Dave Stamper; Rudolf Friml. Jim Doherty and Girls Chorus. <a href=search.php?nq=1&query_type=call_number&query=8142>Edison Blue Amberol: 4374</a>. 1921.</li>
    <li>Bad'ner Mad'ln / Karel Komzák. Johann Strauss Orchester. <a href=search.php?nq=1&query_type=call_number&query=8597>Edison Amberol: 15091</a>. 1910.</li>
    <li>Nachtschwärmer / Carl Michael Ziehrer. Johann Strauss Orchester. <a href=search.php?nq=1&query_type=call_number&query=8623>Edison Amberol: 15059</a>. 1909.</li>
    <li>Der Graf von Luxemburg / Franz Lehár. Johann Strauss Orchester. <a href=search.php?nq=1&query_type=call_number&query=9071>Edison Blue Amberol: 26043</a>. 1913.</li>
    <li>Handwerkerleben / C. Hellmann. Johann Strauss Orchester (Berlin). <a href=search.php?nq=1&query_type=call_number&query=8622>Edison Amberol: 15129</a>. 1910.</li>
    <li>Fra Diavolo / D.F.E. Auber (Daniel François Esprit). Johann Strauss Orchester.. <a href=search.php?nq=1&query_type=call_number&query=9082>Edison Blue Amberol: 26015</a>. 1913.</li>
    <li>Ein Abend in Toledo / Martin Schmeling. Johann Strauss Orchestra. <a href=search.php?nq=1&query_type=call_number&query=9067>Edison Blue Amberol: 26007</a>. 1913.</li>
    <li>Am elterngrab / Emil Winter-Tymian. Johannes Semke. <a href=search.php?nq=1&query_type=call_number&query=8629>Edison Goldguss Walze: 15040</a>. 1904.</li>
    <li>Everybody loves an Irish song / William McKenna. John Finnegan. <a href=search.php?nq=1&query_type=call_number&query=7119>Edison Blue Amberol: 3107</a>. 1917.</li>
    <li>Pretty Peggy. John Fletcher. <a href=search.php?nq=1&query_type=call_number&query=8549>Indestructible Record: 940</a>. 1908.</li>
    <li>Hear me, Norma / Vincenzo Bellini. John Hazel and William Tuson. <a href=search.php?nq=1&query_type=call_number&query=7781>Edison Gold Moulded Record: 8915</a>. 1905.</li>
    <li>Medley of German waltzes. John J. Kimmel. <a href=search.php?nq=1&query_type=call_number&query=7729>Indestructible Record: 995</a>. 1909.</li>
    <li>Oh gee!. John J. Kimmel. <a href=search.php?nq=1&query_type=call_number&query=7812>Edison Blue Amberol: 3985</a>. 1920.</li>
    <li>Haley's fancy: medley of Irish jigs. John J. Kimmel and Joe Linder. <a href=search.php?nq=1&query_type=call_number&query=7811>Edison Blue Amberol: 4076</a>. 1920.</li>
    <li>Medley of Irish jigs. John Kimmble [sic] [John Kimmel]. <a href=search.php?nq=1&query_type=call_number&query=9201>Edison Gold Moulded Record: 9881</a>. 1908.</li>
    <li>Violets / Ellen Wright . John Lacalle's Band. <a href=search.php?nq=1&query_type=call_number&query=7066>Indestructible Record: 3095</a>. 1910.</li>
    <li>American patrol / F. W. Meacham. John Lacalle's Band. <a href=search.php?nq=1&query_type=call_number&query=7758>Indestructible Record: 3001</a>. 1910.</li>
    <li>Fantasia on my old Kentucky home / Stephen Foster Collins. John Lacalle's Band. <a href=search.php?nq=1&query_type=call_number&query=8537>Indestructible Record: 3070</a>. 1910.</li>
    <li>Songe d'automne / Archibald Joyce. John Lacalle's Orchestra. <a href=search.php?nq=1&query_type=call_number&query=7038>Indestructible Record: 3043</a>. 1910.</li>
    <li>The two beggars / H. Lane Wilson. John Young ; Fredrick J. Wheeler. <a href=search.php?nq=1&query_type=call_number&query=7711>Edison Blue Amberol: 1728</a>. 1913.</li>
    <li>The American flag march. Joseph Belmont. <a href=search.php?nq=1&query_type=call_number&query=7999>Edison Gold Moulded Record: 8612</a>. 1904.</li>
    <li> Menuett G? major ;  Valse bluette / Ludwig van Beethoven ; Riccardo Drigo . Kathleen Parlow. <a href=search.php?nq=1&query_type=call_number&query=7635>Edison Blue Amberol: 28192</a>. 1914.</li>
    <li>Henry and Hank at the levee / Kaufman Brothers. Kaufman Brothers. <a href=search.php?nq=1&query_type=call_number&query=7766>Edison Blue Amberol: 3951</a>. 1916.</li>
    <li>Rose of no-man's land / Jack Caddigan and James Brennan. Kaufman Brothers (Irving Kaufman and Jack Kaufman). <a href=search.php?nq=1&query_type=call_number&query=7074>Indestructible Record: 3435</a>. 1918.</li>
    <li>America. Knickerbocker Mixed Quartet. <a href=search.php?nq=1&query_type=call_number&query=7918>Columbia Phonograph Co.: 31637</a>. 1901.</li>
    <li>Altbayrischer Original Laendler. Königlich Bayerisches 1 Infanterie-Regiment. <a href=search.php?nq=1&query_type=call_number&query=7465>Edison Goldguss Walze: 15481</a>. 1906.</li>
    <li>The nightingale and the frogs / Richard Eilenberg. L.F. Fritze. <a href=search.php?nq=1&query_type=call_number&query=7160>Indestructible Record: 931 </a>. 1908.</li>
    <li>Selection from Carmen / Georges Bizet.  Lacalle's Band. <a href=search.php?nq=1&query_type=call_number&query=8022>Indestructible Record: 3002</a>. 1910.</li>
    <li>Selections from the midnight sons. Lacalle's Band. <a href=search.php?nq=1&query_type=call_number&query=8441>Indestructible Record: 3069</a>. 1910.</li>
    <li>La Gitana waltz / Ernest Bucalossi. Lacalle's Band. <a href=search.php?nq=1&query_type=call_number&query=8443>Indestructible Record: 3082</a>. 1910.</li>
    <li>Morning, Cy!. Lacalle's Band. <a href=search.php?nq=1&query_type=call_number&query=8446>Indestructible Record: 3081</a>. 1910.</li>
    <li>Selection from Cavalleria rusticana / Pietro Mascagni. Lacalle's Band. <a href=search.php?nq=1&query_type=call_number&query=8460>Indestructible Record: 3007</a>. 1910.</li>
    <li>At a Georgia camp meeting / Kerry Mills.  Lacalle's Band. <a href=search.php?nq=1&query_type=call_number&query=8463>Indestructible Record: 3097</a>. 1910.</li>
    <li>Blue Danube / Johann Strauss. Lacalle's Band. <a href=search.php?nq=1&query_type=call_number&query=8493>Indestructible Record: 3084</a>. 1910.</li>
    <li>My old Kentucky home / Stephen Collins Foster. Lacalle's Band. <a href=search.php?nq=1&query_type=call_number&query=8494>Indestructible Record: 3070</a>. 1910.</li>
    <li>Vision of Salome / Archibald Joyce. Lacalle's Orchestra. <a href=search.php?nq=1&query_type=call_number&query=7970>Indestructible Record: 3076</a>. 1910.</li>
    <li>The whispering of the flowers / Franz von Blon. Lacalle's Orchestra. <a href=search.php?nq=1&query_type=call_number&query=8456>Indestructible Record: 3059</a>. 1910.</li>
    <li>La serenata / F. Paolo Tosti. Lacalle's Orchestra. <a href=search.php?nq=1&query_type=call_number&query=8457>Indestructible Record: 3055</a>. 1910.</li>
    <li>Moss rose. Lacalle’s Orchestra. <a href=search.php?nq=1&query_type=call_number&query=8427>Indestructible Record: 827</a>. 1908.</li>
    <li>A la militaire. Lambert Military Band. <a href=search.php?nq=1&query_type=call_number&query=8617>Lambert: 180</a>. [190-.</li>
    <li>Satisfied / Al Bernard ; Ernest Hare. Larry Briers. <a href=search.php?nq=1&query_type=call_number&query=8141>Edison Blue Amberol: 4218</a>. 1921.</li>
    <li>Kol nidrei part 1 / Max Bruch. Lauri Kennedy and Dorothy Kennedy. <a href=search.php?nq=1&query_type=call_number&query=7774>Edison Blue Amberol: 4128</a>. 1920.</li>
    <li>Pompernickel's silver wedding. Len Spencer. <a href=search.php?nq=1&query_type=call_number&query=7753>Columbia Record: 32585</a>. 1904.</li>
    <li>The Arkansas traveler. Len Spencer. <a href=search.php?nq=1&query_type=call_number&query=7767>Edison Blue Amberol: 3745</a>. 1919.</li>
    <li>My black bird. Len Spencer. <a href=search.php?nq=1&query_type=call_number&query=7950>Columbia Phonograph Co.: 7474</a>. 1896- 1900.</li>
    <li>Typical tune of Zanzibar: from El Capitan . Len Spencer. <a href=search.php?nq=1&query_type=call_number&query=8009>Columbia Phonograph Co.: 7206</a>. between 1896 and 1900.</li>
    <li>Auction sale of household goods. Len Spencer ; Gilbert Girard. <a href=search.php?nq=1&query_type=call_number&query=8057>Edison Gold Moulded Record: 8089</a>. 1902?.</li>
    <li>The banjo evangelist  / Len Spencer. Len Spencer ; Parke Hunter. <a href=search.php?nq=1&query_type=call_number&query=8068>Columbia Phonograph Co.: 32269</a>. 1903.</li>
    <li>Reuben Haskins' trip on his airship / Len Spencer. Len Spencer ; Parke Hunter. <a href=search.php?nq=1&query_type=call_number&query=8079>Edison Gold Moulded Record: 8704</a>. 1904.</li>
    <li>Bye and bye you will forget me. Len Spencer ; Roger Harding. <a href=search.php?nq=1&query_type=call_number&query=7957>Columbia Phonograph Co.: 8404</a>. 1897- 1899.</li>
    <li>Musical wizard and the bell boy / Len Spencer. Len Spencer and Albert H. Campbell. <a href=search.php?nq=1&query_type=call_number&query=9027>Edison Blue Amberol: 2093</a>. 1913.</li>
    <li>Musical wizard and the bell boy. Len Spencer and Albert H. Campbell. <a href=search.php?nq=1&query_type=call_number&query=9052>Edison Amberol: 586</a>. 1911.</li>
    <li>The two jolly Irishmen / Len Spencer. Len Spencer and Steve Porter. <a href=search.php?nq=1&query_type=call_number&query=6992>Edison Gold Moulded Record: 9349</a>. 1906.</li>
    <li>Buddies - Waltz / unknown. Lenzberg's Riverside Orchestra. <a href=search.php?nq=1&query_type=call_number&query=6959>Edison Blue Amberol: 3998</a>. 1920.</li>
    <li>A young man's fancy / Milton Ager ; Anderson, Jack Yellen. Lenzberg's Riverside Orchestra. <a href=search.php?nq=1&query_type=call_number&query=7585>Edison Blue Amberol: 4095</a>. 1920.</li>
    <li>Swanee - One-step / George Gershwin. Lenzberg's Riverside Orchestra. <a href=search.php?nq=1&query_type=call_number&query=7818>Edison Blue Amberol: 4034</a>. 1920.</li>
    <li>Pretty little rainbow: waltz / Vincent Plunkett. Lenzberg's Riverside Orchestra. <a href=search.php?nq=1&query_type=call_number&query=8129>Edison Blue Amberol: 3954</a>. 1920.</li>
    <li>Who wants a baby? / Abe Olman. Lenzberg’s Riverside Orchestra. <a href=search.php?nq=1&query_type=call_number&query=7772>Edison Blue Amberol: 3944</a>. 1920.</li>
    <li>Jealous moon / J.S. Zamecnik. Leola Lucey. <a href=search.php?nq=1&query_type=call_number&query=7819>Edison Blue Amberol: 3657</a>. 1919.</li>
    <li>Kentucky dream / S.R. Henry. Leola Lucey and Charles Hart. <a href=search.php?nq=1&query_type=call_number&query=6993>Edison Blue Amberol: 6537</a>. 1919.</li>
    <li>Shadows / Howard Lutter. Leola Lucey and Charles Hart. <a href=search.php?nq=1&query_type=call_number&query=7765>Edison Blue Amberol: 3867</a>. 1919.</li>
    <li>Dragons de Villars : Ne parle pas, Rose, je t'en supplie / Aimé Maillart. Léon Beyle. <a href=search.php?nq=1&query_type=call_number&query=9646>Pathé: 3240</a>. 1903 or 1904.</li>
    <li>Way down in Borneo-o-o-o / Al Piantadosi. Leonard Chick. <a href=search.php?nq=1&query_type=call_number&query=7661>Edison Blue Amberol: 2973</a>. 1916.</li>
    <li>March Boccacio  / Franz von Suppé. London Concert Orchestra. <a href=search.php?nq=1&query_type=call_number&query=8165>Lambert: 948</a>. 190-.</li>
    <li>Sword and lance. London Regimental Band. <a href=search.php?nq=1&query_type=call_number&query=9094>Edison Bell Record: 168</a>. 190-?.</li>
    <li>Valse caprice / F. H. Losey. Losey's Orchestra. <a href=search.php?nq=1&query_type=call_number&query=9076>Edison Blue Amberol: 4370</a>. 1921.</li>
    <li>In my heart, on my mind all day long & I wonder if you still care for me / Bert Kalmar ; Harry Ruby ; Ted Snyder. Lou Chiha. <a href=search.php?nq=1&query_type=call_number&query=7698>Edison Blue Amberol: 4458</a>. 1922.</li>
    <li>Nobody knows (and nobody seems to care) / Irving Berlin. Louise Terrell. <a href=search.php?nq=1&query_type=call_number&query=6889>Edison Blue Amberol: 3941</a>. 1920.</li>
    <li>B-hap-e: one step / Anton Lada ; Alcide Nunez ; Carl Burger. Louisiana Five. <a href=search.php?nq=1&query_type=call_number&query=7803>Edison Blue Amberol: 3789</a>. 1919.</li>
    <li>Lovable eyes: fox trot /  Harold Richard Atteridge ; Jean Schwartz ; Bryan. Lovable eyes. <a href=search.php?nq=1&query_type=call_number&query=7828>Edison Blue Amberol: 4631</a>. 1922.</li>
    <li>When I come home to you / Frank H. Grey. Lyric Quartet. <a href=search.php?nq=1&query_type=call_number&query=8140>Edison Blue Amberol: 3664</a>. 1919.</li>
    <li>La Mule de Pedro : opérette / Victor Massé. M. Dathané. <a href=search.php?nq=1&query_type=call_number&query=9884>Cylindres Edison Moulés Sur Or: 18007</a>. 1907.</li>
    <li>Otello. Ora e per sempre addio / Giuseppe Verdi. M. Gluck. <a href=search.php?nq=1&query_type=call_number&query=9887>Cylindres Edison Moulés Sur Or: 17305</a>. between 1904 and 1905.</li>
    <li>Le Pardon de Ploœrmel / Giacomo Meyerbeer. M. Ragneau. <a href=search.php?nq=1&query_type=call_number&query=9883>Cylindres Edison Moulés Sur Or: 17928</a>. 1909.</li>
    <li>Elks minstrels  / N/A. Male quartet. <a href=search.php?nq=1&query_type=call_number&query=1610>Edison Amberol: 64</a>. 1908.</li>
    <li>Forget-me-not / James Kendis. Manuel Romain. <a href=search.php?nq=1&query_type=call_number&query=6971>Edison Blue Amberol: 3786</a>. 1919.</li>
    <li>The girl you can't forget / W. R. Williams. Manuel Romain. <a href=search.php?nq=1&query_type=call_number&query=6984>Edison Blue Amberol: 3281</a>. 1917.</li>
    <li>I will love you when the silver threads are shining among the gold / F. Henri Klickmann. Manuel Romain. <a href=search.php?nq=1&query_type=call_number&query=7039>Edison Blue Amberol: 1538</a>. 1912.</li>
    <li>Would you take me back again? / Alfred Solman. Manuel Romain. <a href=search.php?nq=1&query_type=call_number&query=7664>Edison Blue Amberol: 2155</a>. 1913.</li>
    <li>I will love you etc. / F. Henri Klickmann. Manuel Romain. <a href=search.php?nq=1&query_type=call_number&query=7910>Lakeside Indestructible Cylinder Record: 1557</a>. 1912.</li>
    <li>Let me crown you queen of May with orange blossoms / J. Fred Helf. Manuel Romain. <a href=search.php?nq=1&query_type=call_number&query=8011>Edison Record: 9925</a>. 1908.</li>
    <li>I miss you most of all / James V.  Monaco. Manuel Romain. <a href=search.php?nq=1&query_type=call_number&query=9074>Edison Blue Amberol: 2258</a>. 1914.</li>
    <li>A mother's croon / Edward J. Walt. Margaret A. Freer. <a href=search.php?nq=1&query_type=call_number&query=7831>Edison Blue Amberol: 4462</a>. 1922.</li>
    <li>To the strains of that wedding march / Grace Le Boy. Marguerite E. Farrell. <a href=search.php?nq=1&query_type=call_number&query=6996>Edison Blue Amberol: 4280</a>. 1921.</li>
    <li>Arrah go 'long with you, or: Do you see any green in my eyes?  / Harry Von Tilzer. Marguerite E. Farrell. <a href=search.php?nq=1&query_type=call_number&query=8120>Edison Blue Amberol: 4338</a>. 1921.</li>
    <li>Noces de Figaro : Mon cœur soupire /  Wolfgang Amadeus Mozart. Marguerite Revel. <a href=search.php?nq=1&query_type=call_number&query=9482>Cylindres Edison Moulés Sur Or: 17467</a>. .</li>
    <li>Non la sospiri : Tosca / Giacomo Puccini. Maria Labia. <a href=search.php?nq=1&query_type=call_number&query=9868>Edison Blue Amberol: 28153</a>. 1913.</li>
    <li>Villanelle - Oft have I seen the swift swallow / Eva Dell' Acqua. Marie Kaiser. <a href=search.php?nq=1&query_type=call_number&query=7692>Edison Blue Amberol: 2015</a>. 1913.</li>
    <li>Last night when you said good-bye / Irving M. Wilson. Marie Morrisey. <a href=search.php?nq=1&query_type=call_number&query=7648>Edison Blue Amberol: 2519</a>. 1915.</li>
    <li>The Bonnie banks o'Loch Lomond / unknown. Marie Narelle. <a href=search.php?nq=1&query_type=call_number&query=7020>Edison Gold Moulded Record: 9325</a>. 1906.</li>
    <li>[I can't give up my rough and rowdish ways] (150 rpm) / George Graham. Marle Palmer?. <a href=search.php?nq=1&query_type=call_number&query=5203>[unknown publisher]: rown wax home recording</a>. 1890-1929.</li>
    <li>The lover and the bird / Pasquale D. Guglielmo.. Mary Carson ; Joseph Belmont. <a href=search.php?nq=1&query_type=call_number&query=7677>Edison Blue Amberol: 2418</a>. 1914.</li>
    <li>Won't you come and waltz with me / Sirmay, Albert. . Mary Carson and Harvey Hindermeyer. <a href=search.php?nq=1&query_type=call_number&query=9251>Edison Blue Amberol: 2260</a>. 1914.</li>
    <li>Rose: fox trot / Frank Magine ; Paul Biese. Max Fells' Della Robbia Orchestra. <a href=search.php?nq=1&query_type=call_number&query=7963>Edison Blue Amberol: 4303</a>. 1921.</li>
    <li>Good as gold: waltz / James Kendis ; James Brockman ; Al Hoffman ; Al Sherman. Max Fells' Della Robbia Orchestra. <a href=search.php?nq=1&query_type=call_number&query=8130>Edison Blue Amberol: 4341</a>. 1921.</li>
    <li>Romance: waltz / David Lee. Max Fells' Della Robbia Orchestra. <a href=search.php?nq=1&query_type=call_number&query=8138>Edison Blue Amberol: 4203</a>. 1921.</li>
    <li>The Arabian yogi man / James Kendis. Max Fells' Della Robbia Orchestra. <a href=search.php?nq=1&query_type=call_number&query=8403>Edison Blue Amberol: 4290</a>. 1921.</li>
    <li>No me olvides / Gerardo Metallo. Maximiano Rosales ; R. H. Robinson. <a href=search.php?nq=1&query_type=call_number&query=8090>Edison Blue Amberol: 22103</a>. 1910-1919.</li>
    <li>C'était un rêve. Mercadier. <a href=search.php?nq=1&query_type=call_number&query=7985>[unknown publisher]</a>. 1897.</li>
    <li>Vocal solo: "tyrolienne comedie" by Messieur Bravo. Messieur Bravo. <a href=search.php?nq=1&query_type=call_number&query=7988>[unknown publisher]</a>. 189u.</li>
    <li>All Hail the Power of Jesus' Name / unknown. Metropolitan Quartet. <a href=search.php?nq=1&query_type=call_number&query=6958>Edison Blue Amberol: 3248</a>. 1917.</li>
    <li>Dear Spirit, lead Thou me / Charles Austin Miles. Metropolitan Quartet. <a href=search.php?nq=1&query_type=call_number&query=6980>Edison Blue Amberol: 3644</a>. 1919.</li>
    <li>When the mists have rolled away / Ira David Sankey. Metropolitan Quartet. <a href=search.php?nq=1&query_type=call_number&query=7723>Edison Blue Amberol: 4582</a>. 1922.</li>
    <li>Love blossom / Lucien Denni. Metropolitan Quartet. <a href=search.php?nq=1&query_type=call_number&query=7763>Edison Blue Amberol: 3908</a>. 1920.</li>
    <li>Work, for the night is coming / Lowell Mason. Metropolitan Quartet. <a href=search.php?nq=1&query_type=call_number&query=7792>Edison Gold Moulded Record: 9607</a>. 1907.</li>
    <li>Love blossom / Lucien Denni. Metropolitan Quartet. <a href=search.php?nq=1&query_type=call_number&query=8034>Edison Blue Amberol: 3908</a>. 1920.</li>
    <li>In the heart of dear old Italy / Jack Glogau. Metropolitan Quartet. <a href=search.php?nq=1&query_type=call_number&query=8118>Edison Blue Amberol: 4281</a>. 1921.</li>
    <li>Killarney. Miss Marie Narelle. <a href=search.php?nq=1&query_type=call_number&query=6824>Edison Gold Moulded Record: 9081</a>. 1905.</li>
    <li>Tell mother I'll be there / Charles Millard Fillmore. Mixed Quartette. <a href=search.php?nq=1&query_type=call_number&query=7259>Indestructible Record: 1351</a>. 1910.</li>
    <li>He'd have to go under / Maurice Abrahams. Moss-Squire Celeste Orchestra. <a href=search.php?nq=1&query_type=call_number&query=9075>Edison Blue Amberol: 23275</a>. 1914?.</li>
    <li>Il trovatore / Giuseppe Verdi. Mr. Hicks. <a href=search.php?nq=1&query_type=call_number&query=9056>Edison Bell Record: 6034</a>. between 1896 and 1900.</li>
    <li>Queen of the earth / Ciro Pinsuti. Mr. Hicks. <a href=search.php?nq=1&query_type=call_number&query=9059>Edison Bell Record: 6005</a>. between 1896 and 1900.</li>
    <li>Kathleen Mavourneen / F. Nicholls Crouch. Mrs. Clarence Eddy. <a href=search.php?nq=1&query_type=call_number&query=9010>Edison Blue Amberol: 1828</a>. 1913.</li>
    <li>She was bred in old Kentucky  / Harry Braisted ; Stanley Carter. Mssrs. Clarke & Morgan. <a href=search.php?nq=1&query_type=call_number&query=7933>Columbia Phonograph Co.: 2815</a>. ca. 1898.</li>
    <li>A bunch of nonsense . Murray K. Hill. <a href=search.php?nq=1&query_type=call_number&query=8019>Indestructible Record: 3230</a>. 191-.</li>
    <li>Married life. Murray K. Hill. <a href=search.php?nq=1&query_type=call_number&query=8200>U.S. Everlasting Record: 1178</a>. ca. 1910.</li>
    <li>The tale of the cheese / Murry K. Hill. Murry K. Hill. <a href=search.php?nq=1&query_type=call_number&query=7741>Indestructible Record: 3249</a>. 1911?.</li>
    <li>Until we meet again. Name of performer unknown.. <a href=search.php?nq=1&query_type=call_number&query=7827>Brown wax commercial recording: [unknown catalog number]</a>. 1890-1902.</li>
    <li>Passing review - patrol / unknown. National Guard Fife and Drum Corps. <a href=search.php?nq=1&query_type=call_number&query=7714>Edison Blue Amberol: 23339</a>. 1914 or1915.</li>
    <li>Sons of the brave march / Thomas Bidgood. National Military Band. <a href=search.php?nq=1&query_type=call_number&query=7575>Edison Blue Amberol: 23331</a>. 1912 - 1929.</li>
    <li>Our troops. National Military Band. <a href=search.php?nq=1&query_type=call_number&query=7578>Edison Blue Amberol: 23340</a>. 1912 - 1929.</li>
    <li>Salome intermezzo / William Loraine. National Military Band. <a href=search.php?nq=1&query_type=call_number&query=7601>Edison Blue Amberol: 23105</a>. 1913.</li>
    <li>Gems of Scotland / unknown. National Military Band. <a href=search.php?nq=1&query_type=call_number&query=7606>Edison Blue Amberol: 23343</a>. 1915.</li>
    <li>Overture Oberon /  Carl Maria von Weber. National Military Band. <a href=search.php?nq=1&query_type=call_number&query=7631>Edison Blue Amberol: 23116</a>. 1913 - 1914.</li>
    <li>Poet and peasant overture / Franz von Suppé. National Military Band. <a href=search.php?nq=1&query_type=call_number&query=7646>Edison Blue Amberol: 23001</a>. 1913.</li>
    <li>Casse noisette / Peter Ilich Tchaikovsky. National Military Band. <a href=search.php?nq=1&query_type=call_number&query=7653>Edison Blue Amberol: 23131</a>. 1913.</li>
    <li>Gems of Ireland / unknown. National Military Band. <a href=search.php?nq=1&query_type=call_number&query=7682>Edison Blue Amberol: 23342</a>. 1915.</li>
    <li>Merry widow lancers, figs. 1 and 2 / Franz Lehár. National Military Band. <a href=search.php?nq=1&query_type=call_number&query=7688>Edison Blue Amberol: 23178</a>. 1914.</li>
    <li>Here, there and everywhere march / Auguste Bosc. National Military Band. <a href=search.php?nq=1&query_type=call_number&query=7702>Edison Blue Amberol: 23060</a>. 1913.</li>
    <li>With sword and lance / Hermann Starke. National Military Band. <a href=search.php?nq=1&query_type=call_number&query=7704>Edison Blue Amberol: 23074</a>. 1913.</li>
    <li>Merry widow lancers, figs. 3 and 4 / Franz Lehár. National Military Band. <a href=search.php?nq=1&query_type=call_number&query=7719>Edison Blue Amberol: 23179</a>. 1912.</li>
    <li>Regimental marches no. 2 / Seymour. National Military Band. <a href=search.php?nq=1&query_type=call_number&query=7724>Edison Blue Amberol: 23329</a>. 1912.</li>
    <li>The bells of St. Malo / William Rimmer. National Military Band. <a href=search.php?nq=1&query_type=call_number&query=7858>Edison Blue Amberol: 23013</a>. 1913.</li>
    <li>H.M.S. Pinafore: Selection / Arthur Sullivan ; William S. Gilbert. National Military Band. <a href=search.php?nq=1&query_type=call_number&query=7879>Edison Amberol: 12170</a>. 1910.</li>
    <li>Selection from 'The Gondoliers' / Arthur Sullivan ; William S. Gilbert. National Military Band. <a href=search.php?nq=1&query_type=call_number&query=7885>Edison Blue Amberol: 23170</a>. 1911.</li>
    <li>Laughing eyes: Intermezzo / Herman Finck. National Military Band. <a href=search.php?nq=1&query_type=call_number&query=7886>Edison Blue Amberol: 23290</a>. 1914.</li>
    <li>Gems of Ireland. National Military Band. <a href=search.php?nq=1&query_type=call_number&query=8154>Edison Amberol: 12160</a>. 1909.</li>
    <li>Gems of England. National Military Band. <a href=search.php?nq=1&query_type=call_number&query=8157>Edison Amberol: 12128</a>. 1909.</li>
    <li>Christmas at sea . National Military Band. <a href=search.php?nq=1&query_type=call_number&query=8696>Edison Blue Amberol: 23150</a>. 1914.</li>
    <li>Bonjour Chichinettes! march / Charles Borel-Clerc. National Military Band. <a href=search.php?nq=1&query_type=call_number&query=8877>Edison Blue Amberol: 23238</a>. 1914.</li>
    <li>Selection from Veronique /  André Messager. National Military Band. <a href=search.php?nq=1&query_type=call_number&query=8879>Edison Blue Amberol: 23061</a>. 1913.</li>
    <li>Illery march. National Military Band. <a href=search.php?nq=1&query_type=call_number&query=9060>Edison Gold Moulded Record: 13661</a>. 1907.</li>
    <li>British phalanx march / Feliks Nowowiejski. National Military Band. <a href=search.php?nq=1&query_type=call_number&query=9070>Edison Blue Amberol: 23327</a>. 1914 or 1915.</li>
    <li>Youth and vigour march / Henry Lautenschlager. National Military Band. <a href=search.php?nq=1&query_type=call_number&query=9112>Edison Blue Amberol: 23260</a>. 1914.</li>
    <li>Ballet egyptien no. 1 / A. Luigini. National Military Band. <a href=search.php?nq=1&query_type=call_number&query=9115>Edison Blue Amberol: 23237</a>. 1913.</li>
    <li>The bullfighters march / Celian Kottaun. National Military Band . <a href=search.php?nq=1&query_type=call_number&query=7574>Edison Blue Amberol: 23195</a>. 1913.</li>
    <li>You're my girl medley / Bobby Heath. National Promenade Band. <a href=search.php?nq=1&query_type=call_number&query=7016>Edison Blue Amberol: 2234</a>. 1914.</li>
    <li>L'estudiantina - waltz hesitation / Emil Waldteufel. National Promenade Band. <a href=search.php?nq=1&query_type=call_number&query=7617>Edison Blue Amberol: 2467</a>. 1914.</li>
    <li>Hesitation - waltz / James M. Shaw. National Promenade Band. <a href=search.php?nq=1&query_type=call_number&query=7647>Edison Blue Amberol: 2292</a>. 1914.</li>
    <li>He'd have to get under get out and get under medley / Maurice Abrahams. National Promenade Band. <a href=search.php?nq=1&query_type=call_number&query=7796>Edison Blue Amberol: 2346</a>. 1914
      1914.</li>
    <li>There's a girl in the heart of Maryland medley. National Promenade Band. <a href=search.php?nq=1&query_type=call_number&query=9011>Edison Blue Amberol: 2271</a>. 1914.</li>
    <li>Virginia reel. National Promenade Band. <a href=search.php?nq=1&query_type=call_number&query=9012>Edison Blue Amberol: 2063</a>. 1913.</li>
    <li>Valse June / Lionel Baxter. National Promenade Band. <a href=search.php?nq=1&query_type=call_number&query=9142>Edison Blue Amberol: 2423</a>. 1914.</li>
    <li>Valse Septembre / Felix Godin. National Promenade Band. <a href=search.php?nq=1&query_type=call_number&query=9266>Edison Amberol: 868</a>. 1912.</li>
    <li>A broken heart / Ernest Gillett. National String Quartet. <a href=search.php?nq=1&query_type=call_number&query=7623>Edison Blue Amberol: 23168</a>. 1914.</li>
    <li>Evening Breeze / Otto Langey. National String Quartet. <a href=search.php?nq=1&query_type=call_number&query=7706>Edison Blue Amberol: 23043</a>. 1913.</li>
    <li>Hands across the sea / unknown. New York Military Band. <a href=search.php?nq=1&query_type=call_number&query=6953>Edison Blue Amberol: 3490</a>. 1918.</li>
    <li>Good night waltz / unknown. New York Military Band. <a href=search.php?nq=1&query_type=call_number&query=7015>Edison Blue Amberol: 2013</a>. 1913.</li>
    <li>Animation schottische / John C. Heed. New York Military Band. <a href=search.php?nq=1&query_type=call_number&query=7058>Edison Amberol: 276</a>. 1909.</li>
    <li>Ange d'amour / E. Waldteufel. New York Military Band. <a href=search.php?nq=1&query_type=call_number&query=7062>Edison Amberol: 260</a>. 1909.</li>
    <li>The boomerang march / unknown. New York Military Band. <a href=search.php?nq=1&query_type=call_number&query=7069>Edison Blue Amberol: 3008</a>. 1916.</li>
    <li>All alone medley / Harry Von Tilzer. New York Military Band. <a href=search.php?nq=1&query_type=call_number&query=7872>Edison Amberol: 814</a>. 1911.</li>
    <li>Galvini march / Alex Lithgow. New York Military Band. <a href=search.php?nq=1&query_type=call_number&query=8876>Edison Blue Amberol: 22542</a>. 1913.</li>
    <li>Fourth of July patrol. New York Military Band. <a href=search.php?nq=1&query_type=call_number&query=9157>Edison Blue Amberol: 2325</a>. 1914.</li>
    <li>Over the waves waltz [Sobre las olas] / Juventino Rosas. New York Military Band. <a href=search.php?nq=1&query_type=call_number&query=9205>Edison Amberol: 513</a>. 1908.</li>
    <li>How'dy Hiram barn dance / Leo Friedman. New York Military Band. <a href=search.php?nq=1&query_type=call_number&query=9264>Edison Amberol: 277</a>. 1909.</li>
    <li>A day at West Point / Theo (Theodore) Bendix. New York Military Band. <a href=search.php?nq=1&query_type=call_number&query=9267>Edison Amberol: 412</a>. 1910.</li>
    <li>Motor king march / Heinrich Frantzen. New York Military Band. <a href=search.php?nq=1&query_type=call_number&query=9268>Edison Amberol: 449</a>. 1910.</li>
    <li>Superba lancers / T. H. (Thomas H.) Rollinnson. New York Military Band. <a href=search.php?nq=1&query_type=call_number&query=9269>Edison Amberol: 266</a>. 1909.</li>
    <li>A Georgia barn dance / Kerry Mills. New York Military Band. <a href=search.php?nq=1&query_type=call_number&query=9280>Edison Amberol: 304</a>. 1909.</li>
    <li>Spanish fandango. New York Military Band.. <a href=search.php?nq=1&query_type=call_number&query=9100>Edison Blue Amberol: 3400</a>. 1918.</li>
    <li>Idol mine: fox trot / Lew Pollack. Nicholas Orlando's Orchestra. <a href=search.php?nq=1&query_type=call_number&query=8086>Edison Blue Amberol: 4333</a>. 1921.</li>
    <li>Reit im Winkel / Ländler. Oberbayerische Bauernkapelle. <a href=search.php?nq=1&query_type=call_number&query=7755>Edison Blue Amberol: 26046</a>. 1913.</li>
    <li>Mittenwalder Ländler. Oberbayerische Bauernkapelle. <a href=search.php?nq=1&query_type=call_number&query=7961>Edison Amberol: 15081</a>. 1909.</li>
    <li>Anita. Octavio Yanez. <a href=search.php?nq=1&query_type=call_number&query=7751>Edison Gold Moulded Record: 20102</a>. 1907?.</li>
    <li>Waltz of the swallow. Orchestra. <a href=search.php?nq=1&query_type=call_number&query=7951>Busy-Bee Record: 226</a>. [190-].</li>
    <li>Chiming bells. Orchestra bells solo. <a href=search.php?nq=1&query_type=call_number&query=7955>Columbia Phonograph Co.: 12516</a>. 1898- 1899.</li>
    <li>Humming: fox trot / Louis Breau ; Ray Henderson. Orlando's Orchestra. <a href=search.php?nq=1&query_type=call_number&query=8041>Edison Blue Amberol: 4245</a>. 1920.</li>
    <li>Dixieland memories, no. 1 / unknown. Orpheus Male Chorus. <a href=search.php?nq=1&query_type=call_number&query=6955>Edison Blue Amberol: 3412</a>. 1918.</li>
    <li>The secret. Orville Harrold. <a href=search.php?nq=1&query_type=call_number&query=8437>Edison Blue Amberol: 28191</a>. 1914.</li>
    <li>Queen of my heart / Alfred Cellier. P. Dawson. <a href=search.php?nq=1&query_type=call_number&query=8878>Edison Blue Amberol: 23194</a>. 1911.</li>
    <li>Flower of Hawaii / Gravelle ; Harling. Palakiko's Hawaiian Orchestra. <a href=search.php?nq=1&query_type=call_number&query=7860>Edison Blue Amberol: 4634</a>. 1922.</li>
    <li>Hawaiian nightingale / Vaughn De Leath. Palakiko's Hawaiian Orchestra . <a href=search.php?nq=1&query_type=call_number&query=7640>Edison Blue Amberol: 4613</a>. 1922.</li>
    <li>My old Hawaiian home / Harry J. Lincoln. Palakiko's Hawaiian Orchestra . <a href=search.php?nq=1&query_type=call_number&query=7672>Edison Blue Amberol: 4649</a>. 1922.</li>
    <li>Nora / G. Hubi-Newcombe. Patrick Hughes. <a href=search.php?nq=1&query_type=call_number&query=7674>Edison Blue Amberol: 23182</a>. 1914.</li>
    <li>Retraite faubourienne / R. Desmoulins. Paul Lack. <a href=search.php?nq=1&query_type=call_number&query=7977>Cylindres Edison Moulés Sur Or: 18206</a>. 1912.</li>
    <li>Impassioned dream / unknown. Peerless Orchestra. <a href=search.php?nq=1&query_type=call_number&query=6956>Edison Blue Amberol: 3414</a>. 1918.</li>
    <li>Florodora march / Leslie Stuart. Peerless Orchestra. <a href=search.php?nq=1&query_type=call_number&query=6985>Edison Gold Moulded Record: 7725</a>. 1902?.</li>
    <li>Abandonado: Mexican waltz / Guillermo Posadas. Peerless Orchestra. <a href=search.php?nq=1&query_type=call_number&query=7807>Edison Blue Amberol: 3918</a>. 1920.</li>
    <li>The butterfly. Peerless Orchestra. <a href=search.php?nq=1&query_type=call_number&query=7894>U.S. Everlasting Record: 1620</a>. 1913.</li>
    <li>Down in Louisiana. Peerless Orchestra. <a href=search.php?nq=1&query_type=call_number&query=8991>Edison Gold Moulded Record: 8067</a>. 1902.</li>
    <li>The merry widow waltz / Franz Lehár. Peerless Orchestra.. <a href=search.php?nq=1&query_type=call_number&query=9066>Edison Blue Amberol: 4401</a>. 1921.</li>
    <li>Night trip to Buffalo / N/A. Peerless Quartet. <a href=search.php?nq=1&query_type=call_number&query=7076>Indestructible Record: 872</a>. 1908.</li>
    <li>A call to arms / unknown. Peerless Quartet. <a href=search.php?nq=1&query_type=call_number&query=7612>Edison Blue Amberol: 23363</a>. 1915.</li>
    <li>The bells / Frank Stilwell. Peerless Quartet. <a href=search.php?nq=1&query_type=call_number&query=7638>Edison Blue Amberol: 2229</a>. 1914.</li>
    <li>Dixie minstrels. Peerless Quartet. <a href=search.php?nq=1&query_type=call_number&query=7727>Indestructible Record: 863</a>. 1908.</li>
    <li>Come where my love lies dreaming / Stephen Collins Foster. Peerless Quartet. <a href=search.php?nq=1&query_type=call_number&query=7749>Indestructible Record: 3062</a>. 1910.</li>
    <li>Old Black Joe / Stephen Collins Foster. Peerless Quartet. <a href=search.php?nq=1&query_type=call_number&query=7969>Lakeside Indestructible Cylinder Record: 287</a>. [ca. 1910].</li>
    <li>Farmyard medley. Peerless Quartet. <a href=search.php?nq=1&query_type=call_number&query=8509>Indestructible Record: 1274</a>. 1910.</li>
    <li>Did he run / Albert Von Tilzer. Peerless Quartette. <a href=search.php?nq=1&query_type=call_number&query=7024>Indestructible Record: 1063</a>. 1909.</li>
    <li>The little old cabin in the lane / William Hays. Peerless Quartette. <a href=search.php?nq=1&query_type=call_number&query=7072>Indestructible Record: 3066</a>. 1910.</li>
    <li>Carolina minstrels / unknown. Peerless Quartette. <a href=search.php?nq=1&query_type=call_number&query=7086>Indestructible Record: 3138</a>. 1910.</li>
    <li>In the golden afterwhile / Frank Stanley Grinsted. Peerless Quartette. <a href=search.php?nq=1&query_type=call_number&query=7874>Edison Amberol: 947</a>. 1912.</li>
    <li>Whistling Jim / Theodore F. Morse. Peerless Quartette. <a href=search.php?nq=1&query_type=call_number&query=7907>U.S. Everlasting Record: 1606</a>. 1909.</li>
    <li>Brown wax home recording (140 and 160 rpm) / N/A. Performer and speaker not given. <a href=search.php?nq=1&query_type=call_number&query=6473>Brown wax home recording: rown wax home recording</a>. 1900-1921.</li>
    <li>[Title unknown] (120 rpm) / N/A. Performer not audible. <a href=search.php?nq=1&query_type=call_number&query=6716>[unknown publisher]: Indie BW?</a>. 1890-1902.</li>
    <li>Lucia di Lammermoor. Cruda, funesta smania / Gaetano Donizetti. Performer not given. <a href=search.php?nq=1&query_type=call_number&query=5147>Pathé: 84033 (21446)</a>. 190-?.</li>
    <li>Lucia di Lammermoor. Sulla tomba che rinserra / Gaetano Donizetti. Performer not given. <a href=search.php?nq=1&query_type=call_number&query=5153>Pathé: 80171 (10418)</a>. 190-?.</li>
    <li>Dixie minstrels / unknown. Performer not given. <a href=search.php?nq=1&query_type=call_number&query=7013>Indestructible Record: 1231</a>. 1909.</li>
    <li>[Brown wax home recording]. Performer not given. <a href=search.php?nq=1&query_type=call_number&query=7928>Brown wax home recording</a>. [between 1890 and 1929].</li>
    <li>[Brown wax home recording]. Performer not given. <a href=search.php?nq=1&query_type=call_number&query=7932>Brown wax home recording</a>. [between 1890 and 1929].</li>
    <li>Sambre et Meuse [Régiment de Sambre et Meuse] / Robert Planquette. Performer not given. <a href=search.php?nq=1&query_type=call_number&query=7983>[unknown publisher]</a>. 189-?.</li>
    <li>Fille du régiment  [Fille du régiment. arr. Selections;] / Gaetano Donizetti. Performer not given. <a href=search.php?nq=1&query_type=call_number&query=7984>[unknown publisher]</a>. 189-?.</li>
    <li>La fille de Madame Ohngo. Performer not given. <a href=search.php?nq=1&query_type=call_number&query=7987>[unknown publisher]</a>. 189-?.</li>
    <li>[Title unknown]. Performer not given.. <a href=search.php?nq=1&query_type=call_number&query=8018>[unknown publisher]</a>. [ca. 1900].</li>
    <li>Funiculì, funiculà / Luigi Denza. Performers not indicated. <a href=search.php?nq=1&query_type=call_number&query=8344>Pathé: 81128</a>. 1902 or 1903.</li>
    <li>Waikiki : mermaid medley. Performers unknown. <a href=search.php?nq=1&query_type=call_number&query=8028>Indestructible Record: 1552</a>. 191-.</li>
    <li>I am a roamer / Felix Mendelssohn. Peter Dawson. <a href=search.php?nq=1&query_type=call_number&query=7570>Edison Blue Amberol: 23164</a>. 1912 - 1929.</li>
    <li>The singer was Irish / Clarence W. Murphy ; Harry Castling. Peter Dawson. <a href=search.php?nq=1&query_type=call_number&query=7597>Edison Blue Amberol: 23014</a>. 1913.</li>
    <li>Hearts of oak / Boyer. Peter Dawson. <a href=search.php?nq=1&query_type=call_number&query=7633>Edison Blue Amberol: 23333</a>. 1914.</li>
    <li>The miner's dream of home / Will Godwin and Leo Dryden. Peter Dawson. <a href=search.php?nq=1&query_type=call_number&query=7679>Edison Blue Amberol: 23044</a>. 1913.</li>
    <li>The bandelero / Leslie Stuart. Peter Dawson. <a href=search.php?nq=1&query_type=call_number&query=7717>Edison Blue Amberol: 23084</a>. 1910.</li>
    <li>Thora / Stephen Adams ; Fred E. Weatherly. Peter Dawson. <a href=search.php?nq=1&query_type=call_number&query=7854>Edison Blue Amberol: 23002</a>. 1913.</li>
    <li>Queen of the earth / Ciro Pinsuti. Peter Dawson. <a href=search.php?nq=1&query_type=call_number&query=7882>Edison Blue Amberol: 23127</a>. 1909.</li>
    <li>Simon the cellarer / John L. Hatton ; W. H. Bellamy. Peter Dawson. <a href=search.php?nq=1&query_type=call_number&query=8158>Edison Amberol: 12207</a>. 1910.</li>
    <li>Some day when dreams come true / Henry Burr. Phil Staats. <a href=search.php?nq=1&query_type=call_number&query=7740>Indestructible Record: 677</a>. 1907.</li>
    <li>Brotherly Love / Blamphin. Phillip Ritte and Harry Thornton. <a href=search.php?nq=1&query_type=call_number&query=7071>Indestructible Record: 3181</a>. 1911.</li>
    <li>Ô souverain, ô juge, ô père : Le Cid / Jules Massenet. Pierre A. Asselin. <a href=search.php?nq=1&query_type=call_number&query=9864>Edison Royal Purple Amberol: 29047</a>. 1920.</li>
    <li>La dernière carotte : chansonnette. Polin. <a href=search.php?nq=1&query_type=call_number&query=7979>Pathé: 3810</a>. 1901.</li>
    <li>That syncopated boogie-boo / Geoorge W. Meyer. Premier Quartet. <a href=search.php?nq=1&query_type=call_number&query=7599>Edison Blue Amberol: 1646</a>. 1913.</li>
    <li>Another rag / Theodore F. Morse. Premier Quartette. <a href=search.php?nq=1&query_type=call_number&query=9265>Edison Amberol: 937</a>. 1912.</li>
    <li>Honor and glory march. Prince's Military Band. <a href=search.php?nq=1&query_type=call_number&query=8599>Columbia Phonograph Co.: 33095</a>. 1907.</li>
    <li>Tenting tonight on the old camp ground / Walter Kittredge. Quartet?. <a href=search.php?nq=1&query_type=call_number&query=6682>U.S. Everlasting Record: 1079</a>. 191-??.</li>
    <li>Marion: you'll soonbe marryin' me / Nelson Ingham ;  Geo.
      B. McConnell. Rachael Grant [i.e. Glady Rice] and Billy Murray. <a href=search.php?nq=1&query_type=call_number&query=7809>Edison Blue Amberol: 4065</a>. 1920.</li>
    <li>Jealous / Jack Little. Radio Franks. <a href=search.php?nq=1&query_type=call_number&query=7856>Edison Blue Amberol: 4924</a>. 1924.</li>
    <li>Blue Danube blues & Ka-lu-a / Jerome Kern. Ray Perkins. <a href=search.php?nq=1&query_type=call_number&query=7619>Edison Blue Amberol: 4532</a>. 1922.</li>
    <li>Love will find the way / Harry Von Tilzer. Reed Miller. <a href=search.php?nq=1&query_type=call_number&query=6965>Edison Blue Amberol: 4100</a>. 1920.</li>
    <li>Love, here is my heart / Lao Silesu. Reed Miller. <a href=search.php?nq=1&query_type=call_number&query=6970>Edison Blue Amberol: 4140</a>. 1920.</li>
    <li>Messiah. Comfort ye, my people / George Frideric Handel. Reed Miller. <a href=search.php?nq=1&query_type=call_number&query=8147>Edison Blue Amberol: 2498</a>. 1914.</li>
    <li>Asthore / H. (Henry) Trotère. Reinald Werrenrath. <a href=search.php?nq=1&query_type=call_number&query=8372>Edison Blue Amberol: 2055</a>. 1913.</li>
    <li>Zwei dunkle Augen / Carl Heins. Robert Leonhardt. <a href=search.php?nq=1&query_type=call_number&query=7760>Edison Goldguss Walze: 12262</a>. 1904- 1906.</li>
    <li>Siciliana :Cavalleria rusticana  / Pietro Mascagni. Romeo Berti. <a href=search.php?nq=1&query_type=call_number&query=7943>Edison Gold Moulded Record: B17</a>. 1906.</li>
    <li>Keep the home fires burning / Ivor Novello. Royal Dadmun. <a href=search.php?nq=1&query_type=call_number&query=7080>Indestructible Record: 3360</a>. 1915.</li>
    <li>What could a girl do more?. Russell Hunting. <a href=search.php?nq=1&query_type=call_number&query=9325>Eastern Talking Machine Co.: [unknown catalog number]</a>. between 1896 and 1901.</li>
    <li>Girl wanted / unknown. S.H. Dudley. <a href=search.php?nq=1&query_type=call_number&query=6969>Edison Gold Moulded Record: 1027</a>. 1902.</li>
    <li>La bonita waltz. Samuel Siegel. <a href=search.php?nq=1&query_type=call_number&query=7937>Columbia Phonograph Co.: 31388</a>. 1901.</li>
    <li>Sérénade à Marinette. Sardet. <a href=search.php?nq=1&query_type=call_number&query=9885>Cylindres Edison Moulés Sur Or: 18055</a>. 1909 or 1910.</li>
    <li>Come sing to me / Jack Thompson. Sergeant C. Leggett. <a href=search.php?nq=1&query_type=call_number&query=7592>Edison Blue Amberol: 23134</a>. 1913.</li>
    <li>Dixie girl  / J. B. Lampe. Shannon's 23rd Regiment Orchestra. <a href=search.php?nq=1&query_type=call_number&query=8166>Lambert: 866</a>. 190-.</li>
    <li>Nanny (I have never loved another girl but you) / Glen Ellison. Sir Harry Lauder. <a href=search.php?nq=1&query_type=call_number&query=7671>Edison Blue Amberol: 2905</a>. 1916.</li>
    <li>Hey, Donal!. Sir Harry Lauder . <a href=search.php?nq=1&query_type=call_number&query=8161>Edison Gold Moulded Record: 13202</a>. 1904.</li>
    <li>Moon has raised her lamp above / Ernest Pike ; Peter Dawson. Sir Julius Benedict. <a href=search.php?nq=1&query_type=call_number&query=7857>Edison Blue Amberol: 23072</a>. 1913.</li>
    <li>The last waltz / Ford T. Dabney. Sisty and Seitz's Banjo Orchestra. <a href=search.php?nq=1&query_type=call_number&query=8125>Edison Blue Amberol: 2765</a>. 1915.</li>
    <li>Valcartier / Frederick J. Pearsall. Sodero’s Band. <a href=search.php?nq=1&query_type=call_number&query=6905>Edison Blue Amberol: 2634</a>. 1915.</li>
    <li>Glory of the Yankee Navy / John Philip Sousa. Sousa Band. <a href=search.php?nq=1&query_type=call_number&query=9170>Edison Blue Amberol: 5211</a>. 1926.</li>
    <li>La gipsy / Louis Ganne. Sousa's band. <a href=search.php?nq=1&query_type=call_number&query=9261>Edison Amberol: 413</a>. 1910.</li>
    <li>Off to camp march. Sousa's Grand Concert Band. <a href=search.php?nq=1&query_type=call_number&query=8169>Columbia Phonograph Co.: 512</a>. between 1896 and 1900.</li>
    <li>Dancing in the dark. Sousa's Grand Concert Band. <a href=search.php?nq=1&query_type=call_number&query=9061>Columbia Phonograph Co.: 529</a>. between 1896 and 1900.</li>
    <li>[Advice by unknown speaker on how to make best use of one's days].. Speaker unknown. <a href=search.php?nq=1&query_type=call_number&query=7736>Brown wax home recording</a>. 189-?.</li>
    <li>Episodio historico batalla del 5 de Mayo 1a. Speaker unknown.. <a href=search.php?nq=1&query_type=call_number&query=7748>Edison Blue Amberol: 1261</a>. 191-.</li>
    <li>It's a long way to Tipperary / Jack Judge. Stanley Kirkby. <a href=search.php?nq=1&query_type=call_number&query=7023>Indestructible Record: 3342</a>. 1914.</li>
    <li>Yip-i-addy-i-ay / John H. Flynn. Stanley Kirkby. <a href=search.php?nq=1&query_type=call_number&query=7174>Edison Amberol: 12250</a>. 1910.</li>
    <li>Sing us a song of bonnie Scotland / Bennett Scott ; Arthur J. Mills. Stanley Kirkby. <a href=search.php?nq=1&query_type=call_number&query=7566>Edison Blue Amberol: 23135</a>. 1913.</li>
    <li>'Tis a story that shall live forever / Paul Pelham and Lawrence Wright. Stanley Kirkby. <a href=search.php?nq=1&query_type=call_number&query=7636>Edison Blue Amberol: 23093</a>. 1913.</li>
    <li>Don't go down in the mine, Dad / Will Geddes. Stanley Kirkby. <a href=search.php?nq=1&query_type=call_number&query=8160>Edison Amberol: 12290</a>. 1910.</li>
    <li>Come along, be one of the boys / Wright. Stanley Kirkby. <a href=search.php?nq=1&query_type=call_number&query=9877>Edison Blue Amberol: 23009</a>. 1913.</li>
    <li>My Southern maid / Herman Darewski. Stanley Kirkby ; Lester Barrett. <a href=search.php?nq=1&query_type=call_number&query=7576>Edison Blue Amberol: 23244</a>. 1914.</li>
    <li>Oh! You circus day / Edith Maida Lessing. Stella Mayhew. <a href=search.php?nq=1&query_type=call_number&query=7175>Edison Amberol: 1122</a>. 1912.</li>
    <li>Will he answer "Goo goo?". Stella Tobin. <a href=search.php?nq=1&query_type=call_number&query=8013>Edison Gold Moulded Record: 9758</a>. 1908.</li>
    <li>My sunny Southern home  / Will S. Hays. Steve Porter. <a href=search.php?nq=1&query_type=call_number&query=7927>Columbia Phonograph Co.: 4595</a>. between 1897 and 1900.</li>
    <li>Flanagan and his money / Steve Porter. Steve Porter. <a href=search.php?nq=1&query_type=call_number&query=9057>Edison Gold Moulded Record: 9608</a>. 1907.</li>
    <li>Flanagan's troubles in a restaurant. Steve Porter. <a href=search.php?nq=1&query_type=call_number&query=9068>Edison Blue Amberol: 3969</a>. 1920.</li>
    <li>An evening at Mrs. Clancy's boarding house / N/A. Steve Porter and Billy Murray. <a href=search.php?nq=1&query_type=call_number&query=6848>Columbia Phonograph Co.: 33124</a>. 1906.</li>
    <li>Two rubes swapping horses. Steve Porter and Byron G. Harlan. <a href=search.php?nq=1&query_type=call_number&query=7890>U.S. Everlasting Record: 1395</a>. 1909.</li>
    <li>The village barber. Steve Porter and Byron G. Harlan. <a href=search.php?nq=1&query_type=call_number&query=7905>U.S. Everlasting Record: 1275</a>. between 1909 and 1911.</li>
    <li>Sidewalk conversation. Steve Porter and Edward Meeker. <a href=search.php?nq=1&query_type=call_number&query=7779>Edison Gold Moulded Record: 9840</a>. 1908.</li>
    <li>Träumerei / Robert Schumann. String Quartette. <a href=search.php?nq=1&query_type=call_number&query=8026>Indestructible Record: 1346</a>. 1910.</li>
    <li>The ould plaid shawl / Battison Haynes ; Francis A. Fahy. T. F. Kinniburgh. <a href=search.php?nq=1&query_type=call_number&query=7844>Edison Blue Amberol: 23160</a>. 1913.</li>
    <li>Shipmates o' mine / Wilfrid E. Sanderson ; Edward Teschemacher. T. F. Kinniburgh. <a href=search.php?nq=1&query_type=call_number&query=7883>Edison Blue Amberol: 23120</a>. 1913.</li>
    <li>Lead kindly light. T. F. Kinniburgh. <a href=search.php?nq=1&query_type=call_number&query=7944>Edison Bell Record: 20033</a>. 1907.</li>
    <li>March of the Cameron Men / Mary M. Campbell. T. F. Kinniburgh. <a href=search.php?nq=1&query_type=call_number&query=9046>Edison Blue Amberol: 23337</a>. 1912.</li>
    <li>The deathless army / Henry Trotère ; Fred E. Weatherly. T.F. Kinniburgh. <a href=search.php?nq=1&query_type=call_number&query=7840>Edison Blue Amberol: 23057</a>. 1912.</li>
    <li>That old fashioned mother of mine / unknown. Talbot O'Farrell. <a href=search.php?nq=1&query_type=call_number&query=6979>Edison Blue Amberol: 4104</a>. 1920.</li>
    <li>The kingdom within your eyes / Horatio Nicholls. Talbot O'Farrell. <a href=search.php?nq=1&query_type=call_number&query=7002>Edison Blue Amberol: 4139</a>. 1920.</li>
    <li>Who'll take the place of Mary?. Talbot O'Farrell. <a href=search.php?nq=1&query_type=call_number&query=7813>Edison Blue Amberol: 4064</a>. 1920.</li>
    <li>Once in royal David's city / Henry J. Gauntlett. The Carol Singers. <a href=search.php?nq=1&query_type=call_number&query=7609>Edison Blue Amberol: 2768</a>. 1915.</li>
    <li>Memory isle / unknown. The Homestead Trio. <a href=search.php?nq=1&query_type=call_number&query=6972>Edison Blue Amberol: 4831</a>. 1924.</li>
    <li>When dreams come true / Silvio Hein ; Roy Webb. Thomas Chalmers. <a href=search.php?nq=1&query_type=call_number&query=7675>Edison Blue Amberol: 2068</a>. 1913.</li>
    <li>Kathleen Mavourneen / Frederick W. N. Crouch ; Julia M. Crawford. Thomas Chalmers. <a href=search.php?nq=1&query_type=call_number&query=7848>Edison Blue Amberol: 28164</a>. 1913.</li>
    <li>The evening star - Tannhäuser / Richard Wagner. Thomas Chalmers. <a href=search.php?nq=1&query_type=call_number&query=8707>Edison Blue Amberol: 28196</a>. 1914.</li>
    <li>Prologue from Pagliacci / Ruggiero Leoncavallo. Thomas Chalmers. <a href=search.php?nq=1&query_type=call_number&query=9872>Edison Blue Amberol: 28174</a>. 1913.</li>
    <li>Dixie blossoms / Percy Wenrich. Thomas Mills. <a href=search.php?nq=1&query_type=call_number&query=8598>Columbia Phonograph Co.: 33134</a>. 1907.</li>
    <li>Mary of Argyle / Sidney Nelson and Charles Jefferys. Thomas Reid. <a href=search.php?nq=1&query_type=call_number&query=7004>Edison Gold Moulded Record: 12850</a>. 1903.</li>
    <li>Only a man / Fred J. Barnes ; Charles Collins. Tom Woottwell. <a href=search.php?nq=1&query_type=call_number&query=8163>Edison Gold Moulded Record: 13702</a>. 1907.</li>
    <li>One, two, three, four / W. E. Reynolds. Toots Paka's Hawaiians. <a href=search.php?nq=1&query_type=call_number&query=7849>Edison Blue Amberol: 22538</a>. 1910-1911.</li>
    <li>Aloha oe / Liliuokalani, Queen of Hawaii. Toots Paka's Hawaiians. <a href=search.php?nq=1&query_type=call_number&query=9022>Edison Blue Amberol: 1812</a>. 1913.</li>
    <li>Uncle Sammy's army / Jack Dolph. Troxell. <a href=search.php?nq=1&query_type=call_number&query=8429>Indestructible Record: 1549</a>. 1917.</li>
    <li>Poet and peasant / Franz von Suppé. U. S. Concert Band. <a href=search.php?nq=1&query_type=call_number&query=7913>U.S. Everlasting Record: 1087</a>. 1909.</li>
    <li>2nd Regiment National Guard of New Jersey. U. S. Marine Drum and Fife Corps. <a href=search.php?nq=1&query_type=call_number&query=7826>Edison Record: 8207</a>. 1902.</li>
    <li>Selection of Scotch melodies . U.S. Concert Band. <a href=search.php?nq=1&query_type=call_number&query=7895>U.S. Everlasting Record: 1604</a>. 1909.</li>
    <li>The forge in the forest / Theodor Michaelis. U.S. Concert Band. <a href=search.php?nq=1&query_type=call_number&query=8421>U.S. Everlasting Record: 1289</a>. 1909.</li>
    <li>Sweetest story ever told / R. M. Stults. U.S. Instrumental Trio. <a href=search.php?nq=1&query_type=call_number&query=7007>U.S. Everlasting Record: 1417</a>. ca. 1912.</li>
    <li>Simple aveu / Francis Thomé. U.S. Instrumental Trio. <a href=search.php?nq=1&query_type=call_number&query=7904>U.S. Everlasting Record: 1122</a>. 1909.</li>
    <li>Old Eph's vision. U.S. Military Band. <a href=search.php?nq=1&query_type=call_number&query=7891>U.S. Everlasting Record: 359</a>. 1908.</li>
    <li>Lord Baltimore marches. U.S. Military Band. <a href=search.php?nq=1&query_type=call_number&query=7892>U.S. Everlasting Record: 1003</a>. 1909.</li>
    <li>Swedish guard / O.J. Lovander. U.S. Military Band. <a href=search.php?nq=1&query_type=call_number&query=7896>U.S. Everlasting Record: 430</a>. between 1908 and 1912.</li>
    <li>Lord Baltimore marches. U.S. Military Band. <a href=search.php?nq=1&query_type=call_number&query=8464>U.S. Everlasting Record: 1003</a>. 1909.</li>
    <li>Bombardment & Lord Baltimore. U.S. Military Band. <a href=search.php?nq=1&query_type=call_number&query=8465>Lakeside Indestructible Cylinder Record: 1003</a>. 1909.</li>
    <li>Raymond / Ambroise Thomas. U.S. Military Band. <a href=search.php?nq=1&query_type=call_number&query=8473>U.S. Everlasting Record: 1478</a>. 1909.</li>
    <li>Favorite; and Victorious eagle marches. U.S. Military Band. <a href=search.php?nq=1&query_type=call_number&query=8474>U.S. Everlasting Record: 1001</a>. 1909.</li>
    <li>Die wacht am Rhein / Carl Wilhelm. U.S. Military Band. <a href=search.php?nq=1&query_type=call_number&query=8620>U.S. Everlasting Record: 397</a>. 1908-1911.</li>
    <li>The canary and the cuckoo. U.S. Symphony Orchestra. <a href=search.php?nq=1&query_type=call_number&query=7529>U.S. Everlasting Record: 1293</a>. 1909.</li>
    <li>Lady bugs' review / Neil Moret. U.S. Symphony Orchestra. <a href=search.php?nq=1&query_type=call_number&query=7901>U.S. Everlasting Record: 366</a>. 1908.</li>
    <li>Call them in. Unidentified performer. <a href=search.php?nq=1&query_type=call_number&query=7936>Edison Record: 3202</a>. [ca. 1897].</li>
    <li>Way down yonder in the cornfields. Unidentified performer. <a href=search.php?nq=1&query_type=call_number&query=9341>Columbia Record: [unknown catalog number]</a>. before 1901.</li>
    <li>Eglantine caprice / Edward Van Loock. United States Marine Band. <a href=search.php?nq=1&query_type=call_number&query=7064>Edison Amberol: 293</a>. 1909.</li>
    <li>Manhattan Beach and Gladiator March / John Philip Sousa. United States Marine Band. <a href=search.php?nq=1&query_type=call_number&query=7893>U.S. Everlasting Record: 1421</a>. between 1909 and 1912.</li>
    <li>Marsovia waltz. United States Marine Band. <a href=search.php?nq=1&query_type=call_number&query=7925>Edison Gold Moulded Record: 10188</a>. 1909.</li>
    <li>True to the flag / Franz von Blon. United States Marine Band. <a href=search.php?nq=1&query_type=call_number&query=9013>Edison Blue Amberol: 2933</a>. 1916.</li>
    <li>My Rosary of Dreams. Unknown performer. <a href=search.php?nq=1&query_type=call_number&query=8202>U.S. Everlasting Record: 1628</a>. [1913].</li>
    <li>My tango maid. Unknown performer. <a href=search.php?nq=1&query_type=call_number&query=8203>U.S. Everlasting Record: 1636</a>. [1913].</li>
    <li>Guard of honor march. Unknown performer. <a href=search.php?nq=1&query_type=call_number&query=8204>U.S. Everlasting Record: 526</a>. [1913].</li>
    <li>[Speaking and singing by unknown, possibly inebriated male speaker]. Unknown speaker. <a href=search.php?nq=1&query_type=call_number&query=7735>Brown wax home recording</a>.</li>
    <li>[Statements by members of [inaudible] yacht club into horn on a day of foul weather]. Unknown speakers. <a href=search.php?nq=1&query_type=call_number&query=7737>Brown wax home recording</a>. 189-?.</li>
    <li>How you gonna' keep em down on the farm / Sam M. Lewis, Joe Young ; Walter Donaldson. Van Eps Banjo Orchestra. <a href=search.php?nq=1&query_type=call_number&query=7744>Indestructible Record: 3475</a>. 1919?.</li>
    <li>Leaf by leaf the roses fall / T. Brigham Bishop. Vernon Archibald ; Lewis James. <a href=search.php?nq=1&query_type=call_number&query=8124>Edison Blue Amberol: 3956</a>. 1920.</li>
    <li>Mickey / Neil Moret. Vernon Dalhart. <a href=search.php?nq=1&query_type=call_number&query=7642>Edison Blue Amberol: 3739</a>. 1919.</li>
    <li>The Miami storm / Carson Robison. Vernon Dalhart. <a href=search.php?nq=1&query_type=call_number&query=8116>Edison Blue Amberol: 5237</a>. 1926.</li>
    <li>My Blue Ridge Mountain home / Carson Robison. Vernon Dalhart ; Carson Robison. <a href=search.php?nq=1&query_type=call_number&query=7527>Edison Blue Amberol: 5414</a>. 1927
      1927.</li>
    <li>The letter edged in black / Hattie Nevada. Vernon Dalhart ; Carson Robison ; Murray Kellner. <a href=search.php?nq=1&query_type=call_number&query=7956>Edison Blue Amberol: 5088</a>. 1925.</li>
    <li>The engineer's child. Vernon Dalhart and Co.. <a href=search.php?nq=1&query_type=call_number&query=9245>Edison Blue Amberol: 5135</a>. 1926.</li>
    <li>Drowsy Dempsey / unknown. Vess L. Ossman. <a href=search.php?nq=1&query_type=call_number&query=6988>Indestructible Record: 989</a>. 1909.</li>
    <li>Old folks at home / Stephen Collins Foster. Vess L. Ossman. <a href=search.php?nq=1&query_type=call_number&query=7762>Columbia Phonograph Co.: 3860</a>. 190-.</li>
    <li>Tell me, pretty maiden: from Florodora / Leslie Stewart. Vess Ossman ; Parke Hunter. <a href=search.php?nq=1&query_type=call_number&query=7994>Columbia Phonograph Co.: 31592</a>. 1901.</li>
    <li>Dream melody intermezzo - Naughty Marietta. Victor Herbert. <a href=search.php?nq=1&query_type=call_number&query=8373>Edison Blue Amberol: 1775</a>. 1913.</li>
    <li>Kiss me again / Victor Herbert. Vienese trio. <a href=search.php?nq=1&query_type=call_number&query=7100>Indestructible Record: 3484</a>. 1919.</li>
    <li>Good-bye beloved, good-bye / J.E. Sampson. Virginia Rea. <a href=search.php?nq=1&query_type=call_number&query=6976>Edison Blue Amberol: 3919</a>. 1920.</li>
    <li>Down by the old mill stream / Tell Taylor. W. H. Thompson. <a href=search.php?nq=1&query_type=call_number&query=7026>Indestructible Record: 3285</a>. 1912.</li>
    <li>My Waikiki mermaid / unknown. Waikiki Hawaiian Orchestra. <a href=search.php?nq=1&query_type=call_number&query=7166>Edison Blue Amberol: 3227</a>. 1917.</li>
    <li>Coral sands of my Hawaii / Billy Heagney. Waikiki Hawaiian Orchestra. <a href=search.php?nq=1&query_type=call_number&query=7695>Edison Blue Amberol: 4988</a>. 1925.</li>
    <li>Memory lane / Larry Spier. Walter Scanlan . <a href=search.php?nq=1&query_type=call_number&query=7000>Edison Blue Amberol: 4937</a>. 1925.</li>
    <li>Any little girl, that's a nice little girl, is the right little girl for me / Fred Fischer. Walter Van Brunt. <a href=search.php?nq=1&query_type=call_number&query=6989>Indestructible Record: 1407</a>. 1910.</li>
    <li>In the shade of the old apple tree / Egbert Van Alstyne. Walter Van Brunt. <a href=search.php?nq=1&query_type=call_number&query=7033>Indestructible Record: 3206</a>. 1911.</li>
    <li>Come with me to the bungalow - The little millionaire / George Cohan. Walter Van Brunt. <a href=search.php?nq=1&query_type=call_number&query=7056>Edison Amberol: 874</a>. 1911.</li>
    <li>When I was twenty one and you were sweet sixteen / Egbert Van Alstyne and Harry Williams. Walter Van Brunt. <a href=search.php?nq=1&query_type=call_number&query=7098>Indestructible Record: 3268</a>. 1912.</li>
    <li>Mother in Ireland / Gerald Griffin ; Herman Kahn ; Tommy Lyman . Walter Van Brunt. <a href=search.php?nq=1&query_type=call_number&query=7667>Edison Blue Amberol: 4762</a>. 1923.</li>
    <li>Somebody's coming to my house / Irving Berlin. Walter Van Brunt. <a href=search.php?nq=1&query_type=call_number&query=7690>Edison Blue Amberol: 1941</a>. 1913.</li>
    <li>If a picture I could paint / Neat. Walter van Brunt. <a href=search.php?nq=1&query_type=call_number&query=7718>Edison Blue Amberol: 4617</a>. 1922.</li>
    <li>Everything's at home, etc / Ivan Caryll ; C. M. Mc Lellan. Walter Van Brunt. <a href=search.php?nq=1&query_type=call_number&query=7902>U.S. Everlasting Record: 1611</a>. 1912.</li>
    <li>The red, white and blue / George Arthurs. Walter Van Brunt. <a href=search.php?nq=1&query_type=call_number&query=8697>Edison Blue Amberol: 23392</a>. 1915.</li>
    <li>My sweet little Colleen / A. C. Maurice. Walter Van Brunt . <a href=search.php?nq=1&query_type=call_number&query=7639>Edison Blue Amberol: 2816</a>. 1916.</li>
    <li>It ain't gonna' rain no mo'. Wendell Hall (The Red-headed Music Maker). <a href=search.php?nq=1&query_type=call_number&query=7887>Edison Blue Amberol: 4824</a>. 1896 - 1930.</li>
    <li>Through the air / August Damm. Weyert A. Moor. <a href=search.php?nq=1&query_type=call_number&query=7686>Edison Blue Amberol: 2618</a>. 1915.</li>
    <li>Echo: (duet for flute and cornet) / Ernesto Kohler. Weyert A. Moor and Pietro Capodiferro. <a href=search.php?nq=1&query_type=call_number&query=7769>Edison Blue Amberol: 3859</a>. 1919.</li>
    <li>Yachting / Will Evans. Will Evans. <a href=search.php?nq=1&query_type=call_number&query=7596>Edison Blue Amberol: 23167</a>. 1914.</li>
    <li>My word! What a lot of it  / Reed. Will F. Denny. <a href=search.php?nq=1&query_type=call_number&query=7778>Edison Gold Moulded Record: 9620</a>. 1907.</li>
    <li>Take this letter to my mother / Will S. Hays. Will Oakland. <a href=search.php?nq=1&query_type=call_number&query=7650>Edison Blue Amberol: 3097</a>. 1917.</li>
    <li>The girl I'll call my sweetheart [must look like you] / Chauncey Olcott. Will Oakland. <a href=search.php?nq=1&query_type=call_number&query=7906>U.S. Everlasting Record: 1519</a>. 1912.</li>
    <li>You have changed the winter in my heart to glad springtime. Will Oakland. <a href=search.php?nq=1&query_type=call_number&query=7908>U.S. Everlasting Record: 1159</a>. 1909.</li>
    <li>With all her faults, I love her still . Will Oakland. <a href=search.php?nq=1&query_type=call_number&query=7911>U.S. Everlasting Record: 1446</a>. 1909
      .</li>
    <li>You can't make me stop loving you. Will Oakland. <a href=search.php?nq=1&query_type=call_number&query=8523>U.S. Everlasting Record: 1232</a>. 1909.</li>
    <li>My sweetheart's the man in the moon / James Thornton. Will Oakland & W. H. Thompson. <a href=search.php?nq=1&query_type=call_number&query=8526>U.S. Everlasting Record: 1485</a>. 1909.</li>
    <li>Seaside trippers. Will Terry. <a href=search.php?nq=1&query_type=call_number&query=7880>Edison Blue Amberol: 23262</a>. 1914.</li>
    <li>County Kerry Mary / Harry Pease. William Bonner. <a href=search.php?nq=1&query_type=call_number&query=7966>Edison Blue Amberol: 4322</a>. 1921.</li>
    <li>Visions of sunny Italy. William Edward Foster. <a href=search.php?nq=1&query_type=call_number&query=7888>U.S. Everlasting Record: 1618</a>. 1912.</li>
    <li>Schubert's serenade / Franz Schubert. William Paris Chambers. <a href=search.php?nq=1&query_type=call_number&query=7959>Columbia Phonograph Co.: 3700</a>. 1897- 1898.</li>
    <li>The minstrel boy / Thomas Moore. William Tuson. <a href=search.php?nq=1&query_type=call_number&query=9199>Edison Gold Moulded Record: 8769</a>. 1904.</li>
    <li>Turkey in the straw / Otto Bonnell. William Tuson &  Indestructible Military Band. <a href=search.php?nq=1&query_type=call_number&query=8447>Indestructible Record: 3189</a>. 1911.</li>
    <li>Maritana.  There is a flower that bloometh / William V. Wallace ; Edward Fitzball. William V. Wallace ; Edward Fitzball. <a href=search.php?nq=1&query_type=call_number&query=7843>Edison Blue Amberol: 23267</a>. 1914.</li>
  </ul>
</div><!-- end .content -->
<!-- End of file: grammy_additions.tpl -->   
{include file="footer.tpl"}