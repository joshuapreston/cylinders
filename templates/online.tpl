{include file="header.tpl"}
{include file="navigation.tpl"}
<!-- File: online.tpl -->
<div class="content text">
  <h1>Why aren't certain recordings online?</h1>
  <p>
    The Cylinder Project contains bibliographic information on over 13,000 cylinder titles
    representing nearly 16,000 individual cylinders held by the UCSB Library. Over 10,000 of
    these cylinders have been transferred and are available for listening. There are two main 
    reasons why some cylinders have not been transferred.
  </p>
  <h3>Backlog</h3>
  <p>
    Some cylinders are awaiting transfer and will be added at a future date.
    It costs the library  $60 to catalog, digitize, rehouse, and preserve a single cylinder. the library has an ongoing digitization program and is continually adding new cylinders to the collection. Thanks to
    grants from the <a href="http://www.imls.gov/">Institute of Museum and Library Services</a>, the 
    <a href="http://www.grammy.org/grammy-foundation/preservation">GRAMMY Foundation</a>, 
    the Ann and Gordon Getty Foundation, and support from individual donors, we add cylinders 
    incrementally as they are digitized, but, but  are still hundreds more to be preserved and digitized.  </p>
  <h3 align="center">&quot;Adopt a Cylinder&quot; Program </h3>
  <p> If you would like to support our digitization efforts, consider adopting a cylinder or a group of cylinders. We  will prioritize the digitization of those cylinders
    and put it online for you and others to enjoy. <a href="adopt.php">[More information]</a>  </p>
  <h3>Damaged Cylinders </h3>
  <p>
    Some cylinders are split, moldy, or damaged and our staff decided not to, or was unable to transfer them. 
    Present technology wasn't able to  recover the sound from these cylinders, but we keep at least one copy of every unique title in the collection, regardless of condition.  There are no plans right now to
    make further attempts to digitize these cylinders, though we have preserved them as artifacts.  </p>
</div><!-- end .content -->
<!-- End of file: online.tpl -->   
{include file="footer.tpl"}
