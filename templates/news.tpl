{include file="header.tpl"}
{include file="navigation.tpl"}
<!-- File: news.tpl -->
  <div class="content text">
    <h2>News and Updates</h2>
    {if isset($news_items)}
      {for $i=0 to (count($news_items) - 1)}
        {$news_items[$i]}
      {/for}
    {else}
      <p>Couldn't retrieve news.</p>
    {/if}

  </div><!-- end .content -->
<!-- End of file: news.tpl -->
{include file="footer.tpl"}
