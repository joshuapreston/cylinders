{include file="header.tpl"}
{include file="navigation.tpl"}
<!-- File: getty.tpl -->
<div class="content text">
	<h1>Central European Mix Tape </h1>
	<p>
		<a href="thematic_playlists.xml"><img src="/images/rss-podcast.png" alt="Podcast"></a>
	&nbsp;</p>
  <p>Straddling the Danube and located in the center of Europe, Austria-Hungary was one of the major  powers at the beginning of the 20th century. It was a melting pot of over 50 million Germans, Hungarians, Czechs, Slovaks, Poles, Ukrainians, and others. Edison recordings of Austro-Hungarian performers were released abroad on <a href="history-edisonforeign.php">Goldguss Walze cylinders</a> and also marketed to immigrant communities in the United States as part of various foreign <a href="history-blueamberol.php">Blue Amberol</a> series, mainly German and Bohemian. Additionally, foreign artists recorded works by Austro-Hungarian composers, most frequently Johann Strauss II, but also  Jenő Hubay and Karel Komzák. Although this playlist includes a Strauss waltz, it goes beyond the standard Viennese bonbons of waltzes and galops, offering instead a broader sampling of the musical diversity of Austria-Hungary as captured by Edison. </p>
  <p>
	<em>&mdash;Chris Warden, Reed College; Dain Lopez, UC Santa Barbara </em></p>

  <div class="center-img-horiz">
    <h3>Hungarian </h3>
    <ul>
      <li>Biró uram panaszom van / Emil Delley. <a href="search.php?nq=1&query_type=call_number&query=4660">Edison Goldguss Walze: 12665</a>. ca. 1901.</li>
      <li>Deres a fü édes / Emil Delley. <a href="search.php?nq=1&query_type=call_number&query=6225">Edison Goldguss Walze: 12606</a>. 1902.</li>
      <li>Tambourin chinois  / Mary Zentay. <a href="search.php?nq=1&query_type=call_number&query=0559">Edison Blue Amberol: 28246</a>. 1916. </li>
      <li>Ha úgy látok falumbélit / Izsó Sajó.<a href="search.php?nq=1&query_type=call_number&query=1354"> Edison Blue Amberol: 11025</a>. 1911.</li>
      <li>Hejre Kati                    [Szenen aus der Czárda, no. 4] / Moss-Squire Celeste Orchestra. <a href="search.php?nq=1&query_type=call_number&query=1337">Edison Blue Amberol: 23231</a>. 1914.</li>
    </ul>
    <h3>Austrian</h3>
    <ul>
      <li>Wiener Ländler / Lanner Quartett. <a href="search.php?nq=1&query_type=call_number&query=7464">Edison Goldguss Walze: 15720</a>. 1907.</li>
      <li> Bad'ner Mad'ln / Johann Strauss Orchester. <a href="search.php?nq=1&query_type=call_number&query=8597">Edison Amberol Record: 15091</a>. 1910</li>
    </ul>
    <h3>Bohemian (Czech)</h3>
    <ul>
      <li>Jednou v Neděli / Ceské Trio z Prahy.<a href="search.php?nq=1&query_type=call_number&query=1302"> Edison Blue Amberol: 9853</a>. 1913.</li>
      <li>Neštastný safařuz dvoreček / Bohumil Pták. <a href="search.php?nq=1&query_type=call_number&query=1492">Edison Blue Amberol: 9865</a>. 1912 or 1913.</li>
      <li>Národnich pisni / Bohemian Instrumental Dueto. <a href="search.php?nq=1&query_type=call_number&query=1304">Edison Blue Amberol: 9855</a>. 1913. </li>
      <li>Opustena [Opuštená] / Frances Masopust. <a href="search.php?nq=1&query_type=call_number&query=1488">Edison Blue Amberol: 9850</a>. 1912 or 1913.</li>
      <li>Povidky s. Vidensky lesu [G'schichten aus dem Wienerwald] / Bohumir Kryl and his Band. <a href="search.php?nq=1&query_type=call_number&query=1308">Edison Blue Amberol: 9863</a>. 1913.</li>
      <li>Slovenske a Ceska pisne / Milan Lusk. <a href="search.php?nq=1&query_type=call_number&query=1309">Edison Blue Amberol: 9869</a>. 1921</li>
      <li>U zvonu / Alois Tich. <a href="search.php?nq=1&query_type=call_number&query=6810">Edison Goldguss Walze: 15881</a>. 1908.</li>
      <li>Posviceni na Zlychově / Ceské Trio z Prahy. <a href="search.php?nq=1&query_type=call_number&query=1303">Edison Blue Amberol: 9854</a>. 1913.</li>
    </ul>
    <h3>Polish</h3>
    <ul>
      <li>Ujrzalem raz / Stanisław Bolewski. <a href="search.php?nq=1&query_type=call_number&query=6668">Edison Goldguss Walze: 12636</a>. 1901.</li>
    </ul>
  </div>
  <p>&nbsp;</p>
</div><!-- end .content -->
<!-- End of file: getty.tpl -->
{include file="footer.tpl"}