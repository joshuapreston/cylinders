{include file="header.tpl"}
{include file="navigation.tpl"}
<!-- File: wwi.tpl -->
<div class="content text">
  <h1>Popular Songs of World War I</h1>

  <figure>
    <img src="/images/columbia1917.jpg" title="Columbia 1917 catalog" alt="Columbia Records Catalog, November 1917*" /><figcaption>Columbia Records Catalog, November 1917*</figcaption>
  </figure>

  <ul>
    <li><p><a href="wwi-radio.php">WWI Thematic Playlist</a></p></li>
    <li><p><a href="/search.php?nq=1&query=World+War&amp;query_type=subject">Find All World War I Cylinders</a></p></li>
    <li><p><a href="wwi-sources.php">Sources on WWI Songs</a></p></li>
  </ul>

    <p>
    The year 2007 marked the 90th anniversary of the beginning of American involvement in World War I.
    While the events of that era took place nearly a century ago, much of the music of the time has
    remained an important part of American popular culture.
  </p>
  <p>
    Popular song reflects the culture of the moment. In times of war, songs try to boost soldier morale,
    mourn losses, generate homefront support, or, as in so much of the music the of  Vietnam War era,
    question the value of the conflict and call for its end.
  </p>
  <p>
    World War I resulted in the creation of a remarkably large body of popular songs devoted to wartime themes.
    Thousands of songs were written about the war, some of which are still familiar nearly a century later. Songs 
    such as &quot;<a href="/search.php?nq=1&query=over+there+cohan&query_type=keyword">Over There</a>&quot;
    and &quot;<a href="/search.php?nq=1&query=it's+a+Long+Way+to+Tipperary&query_type=title">It's a Long Way to 
    Tipperary</a>&quot; were hits in their day and are still known by many people today.
  </p>
  <p>
    The UCSB Cylinder Audio Archive  includes more than <a href="/search.php?nq=1&query=World+War&query_type=subject">100 recordings</a>
    related to World War I. We have compiled a <a href="wwi-radio.php">thematic playlist   </a> of some of the most important
    and interesting songs from the time of the Great War, as well as a <a href="wwi-sources.php">list of resources</a>
    on music during World War I.  </p>
  <p>
    To learn more about the music and listen to the program, click <a href="wwi-radio.php">here</a>.
  </p>
  <hr>
  <p>*Columbia Records was not issuing cylinders in 1917, but we liked the image anyway.</p>
</div><!-- end .content -->
<!-- End of file: wwi.tpl -->
{include file="footer.tpl"}
