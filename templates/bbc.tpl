{include file="header.tpl"}
{include file="navigation.tpl"}
<!-- File: bbc.tpl -->
<div class="content text">
  <h2>Listen to recordings featured on BBC Newsday.   </h2>
  <p>
    UCSB Cylinder Audio Archive project director David Seubert was interviewed on <a href="http://www.bbc.co.uk/programmes/p037tf3v">BBC Newsday</a> on Nov 12, 2015 discussing three recordings from the collection. The recordings can be listened to in their entirety online: </p>
  <p><a href="/search.php?nq=1&query_type=call_number&query=5171">Gondolier/Temptation rag / Albert Benzler and Fred Van Eps. U.S. Everlasting 1260, 1909.</a> </p>
  <p>This 1909 cylinder is a medley juxtaposing the songs &quot;Gondolier&quot; and &quot;Temptation Rag&quot; and was recorded by the U.S. Everlasting Phonograph Co of Cleveland, Ohio. While we typically associate ragtime with the piano, the earliest known piano ragtime recording was not made until 1911. Earlier ragtime recordings are often banjo like this recording, or band.</p>
  <p><a href="/search.php?nq=1&query_type=call_number&query=8330">Forza del destino. Minaccie, i fieri accenti (Verdi) / Performers unknown. Cylindres Path&eacute; / 80406. 1902 or 1903.</a>  </p>
  <p>A wild and wooly performance by two unknown Italian opera singers, recorded in Milan ca. 1902 for the Anglo-Italian Commerce Company, a joint venture that also recorded the first Enrico Caruso recordings. This performance showcases the raw energy in these early recording sessions. </p>
  <p><a href="/search.php?nq=1&query_type=call_number&query=2599">Hey, Donal! / Harry Lauder. Edison Gold Moulded Record 13741. 1908.</a></p>
  <p>A classic performance by early 20th century Scottish superstar, Harry Lauder, featuring his trademark bawdy humor. </p>
  <p><em>- David Seubert </em></p>
  <div>
    <div>
      <div>
      </div>
    </div>
  </div>
</div><!-- end .content -->
<!-- End of file: bbc.tpl -->
{include file="footer.tpl"}
