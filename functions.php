<?php

function extract_record_info($record){
  /* function to extract all the display (and navigation) info from a SimpleXML object.
   $record argument should be simpleXMLObject with a single record in it.
   $record should be MARC XML retrieved from an SRU query
   returned value is associative array of record information.
   */

  //  Test if record passed in is query-able with Xpath and fail appropriatly if not
  $xpath_query_succeeded = xpath_query_succeeds($record);
  if($xpath_query_succeeded === FALSE){
    error_log(" ".__FILE__." ".__LINE__." record is not valid marcxml or may contain a default xml name space. failed near line ". __LINE__ . " with: " .curl_error($curl_handle));

    //header ( string $string [, bool $replace = true [, int $http_response_code ]] )
    header($_SERVER["SERVER_PROTOCOL"]." 500 Internal Server Error");
    $datetime = date("Y-m-d H:i:s");
    $thishost = $_SERVER['HTTP_HOST'];

    $error_body = <<<EOL
<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
<html><head>
<title>500 Server Error</title>
</head><body>
<h1>Data format error</h1>
<h4>500 Server Error for $thishost</h3>
<p>The server was unable to parse data from the remote data source.</p>
<p>An administrator should inspect the web server log for details.</p>
<p>Error occured at: $datetime </p>
</body></html>
EOL;
    echo $error_body;
    exit;
  }else{

  $fields_to_extract = array(
//    'title'                => array('field' => '245', 'subfield' => 'a'),
/*
    'uniform_title'        => array('field' => '240', 'subfield'  =>'a'),
    'uniform_title2'       => array('field' => '240', 'subfield'  =>'k'),
    'uniform_title3'       => array('field' => '130', 'subfield'  =>'a'),
*/
    'performer'            => array('field' => '511', 'subfield' => 'a'),
    'collection'           => array('field' => '561', 'subfield' => 'a'),
    'language'             => array('field' => '546', 'subfield' => 'a'),
    'subject'              => array('field' => '650'),
    'geographic_subject'   => array('field' => '651', 'subfield' => 'a'),
    'geographic_subject_v' => array('field' => '651', 'subfield' => 'v'),
    'author'               => array('field' => '100', 'subfield' => 'a'),
    'imprint'              => array('field' => '260', 'subfield' => 'b'),
    'imprint2'             => array('field' => '264', 'subfield' => 'b'),
    'alternate_title'      => array('field' => '246', 'subfield' => 'a'),
/*
    'other_title2'         => array('field' => '700', 'subfield' => 't'),
    'other_title3'         => array('field' => '700', 'subfield' => 'p'),
    'other_title4'         => array('field' => '700', 'subfield' => 'm'),
    'other_title5'         => array('field' => '700', 'subfield' => 'n'),
    'other_title6'         => array('field' => '700', 'subfield' => 'r'),
    'other_title7'         => array('field' => '700', 'subfield' => 'o'),
    'other_title8'         => array('field' => '700', 'subfield' => 'k'),
*/
    'call_number'          => array('field' => 'AVA', 'subfield' => 'd'),
    'AVA-c'                => array('field' => 'AVA', 'subfield' => 'c'),
    'AVA-d'                => array('field' => 'AVA', 'subfield' => 'd'),
    'total_items'          => array('field' => 'AVA', 'subfield' => 'f'),
    'group_name'           => array('field' => '710', 'subfield' => 'a'),
    'issue_number'         => array('field' => '500', 'subfield' => 'a'),
    '999j'                 => array('field' => '999', 'subfield' => 'j'),
    '999z'                 => array('field' => '999', 'subfield' => 'z')

    );


  foreach ($fields_to_extract as $key => $value) {
//  descendant::datafield[@tag='028']/subfield[@code='a']";
    $xpath = "descendant::datafield[@tag='".$value['field']."']";
    if(isset($value['subfield'])){
      $xpath .= "/subfield[@code='".$value['subfield']."']";
      }
    $xpath_results = $record->xpath($xpath);
    $record_array[$key] = (string) $xpath_results[0];
  }

// we also need the MMS_ID which is not in a datafield in the XML like the other fields
// MMS_ID is in a controlfield
  $xpath = "descendant::controlfield[@tag='001']";
  $xpath_results = $record->xpath($xpath);
  $record_array['mms_id'] = (string) $xpath_results[0];

// set a "digitized" flag true/false
  $record_array['is_digitized'] = get_digitization_status($record);
  $record_array['marc_856U'] = get_856U_values($record);
  $record_array['marc_856Z'] = get_856Z_values($record);

// Extract the cylinder number from the call_number
  preg_match_all('!\d+!', $record_array['call_number'], $matches);
  $record_array['cylinder_group_dir'] = get_mp3_subdir($record_array['cylinder_number']);

//  $record_array['release_years'] = format_release_year($record_array['release_year']);
//  $record_array['release_year2'] = format_release_year($record_array['release_year2']);

// some fields have multiple values
  $record_array['collections']          = get_multiple_collections($record);
  $record_array['notes']                = get_multiple_notes($record);
  $record_array['issue_numbers']        = get_multiple_issue_numbers($record);
  $record_array['release_years']        = get_multiple_release_years($record);
  $record_array['personal_names']       = get_multiple_personal_names($record);
  $record_array['subjects']             = get_multiple_subjects($record);
  $record_array['genres']               = get_multiple_genres_655($record);
  $record_array['uniform_titles']       = get_multiple_uniform_titles_240($record);
  $record_array['cylinder_numbers']     = get_cylinder_numbers($record);
  $record_array['base_cylinder_number'] = get_base_cylinder_number($record);
  $record_array['titles']               = get_multiple_titles_245($record);
  $record_array['other_titles']         = get_multiple_other_titles_740a($record);
  $record_array['record_position']      = get_record_position($record);
  $record_array['original_item_info']   = get_multiple_999j_and_z($record);

// apply formatting transformations on data before returning it
  array_walk($record_array['release_years'], 'format_release_year');
return $record_array;
} // end conditioal - xpath is working
} // close function

//  ==========================================================================================
//      need to pass a count of multiple cylinder digital objects to smarty

function get_record_position($xml_record){
  //  pass in a simple xml record object from an SRU query result
  //  returns a whole number
  // from the XML  <searchRetrieveResponse><records><record><recordPosition>
  $record_position = (integer)$xml_record->recordPosition;
  return $record_position;
}

function get_holdings($xml_record){
  /*
  some cylinder have single holdings.  Some have multiple holdings.
  The holdings are reflected in the ALMA AVA field

  pass in a simple xml record object from an SRU query result
  returns an indexed array of cylinder numbers
  */
    $xquery_cylinder_AVA_D = "descendant::datafield[@tag='AVA']/subfield[@code='d']";
    $info_cylinder_AVAd = $xml_record->xpath($xquery_cylinder_AVA_D);

    if (($info_cylinder_AVAd !== false) AND (!empty($info_cylinder_AVAd))) {
      foreach($info_cylinder_AVAd as $datafield){
        $temp = (string)$datafield[0];
        $cylinder_numbers[] = str_replace('Cylinder ', '', $temp);
      }
    }
    return $cylinder_numbers;
}

function get_base_cylinder_number($xml_record){
  /*
     --- DEPRICATED ---
  SOMETIMES there is a cylinder in the MARC 099-a field.
  <datafield tag="099" ind1=" " ind2=" ">
    <subfield code="a">Cylinder 12831</subfield>
  </datafield>
  */
    $xquery_cylinder_number = "descendant::datafield[@tag='099']/subfield[@code='a']";
    $info_cylinder_numbers99a = $xml_record->xpath($xquery_cylinder_number);

    if (($info_cylinder_numbers99a !== false) AND (!empty($info_cylinder_numbers99a))) {
      foreach($info_cylinder_numbers99a as $datafield){
        $temp = (string)$datafield[0];
        $cylinder_number[] = str_replace('Cylinder ', '', $temp);
      }
    }else{

  /*
  SOMETIMES cylinder number is stored in ALMA AVA-d
<searchRetrieveResponse xmlns="http://www.loc.gov/zing/srw/">
  <records>
    <record>
      <recordData>
        <record xmlns="">
            <datafield tag="AVA" ind1=" " ind2=" ">
            <subfield code="d">Cylinder 9034</subfield>
  */
    $xquery_cylinder_number = "descendant::datafield[@tag='AVA']/subfield[@code='d']";
    $info_cylinder_numbersAVAd = $xml_record->xpath($xquery_cylinder_number);
    if ($info_cylinder_numbersAVAd) {
      foreach($info_cylinder_numbersAVAd as $datafield){
        $temp = (string)$datafield[0];
        $cylinder_number[] = str_replace('Cylinder ', '', $temp);
      }
    }
  }

//  $cylinder_numbers = array('12831','12831-1');//debug

  return $cylinder_number;
}

function get_multiple_999j_and_z($xml_record){
  /*
  extract multiple MARC 999j and 999z values from XML
  pass in a simple XML object of MARC record
  returns a multidimensional array like:
    $mock['cylinder234'] = array('j' => 'cylinder 234', 'z' => array('copy 1','take 5'));
    $mock['cylinder678'] = array('j' => 'cylinder 678', 'z' => array('copy 1'));
  */

  $xquery = "descendant::datafield[@tag='999']/subfield[@code='z']|descendant::datafield[@tag='999']/subfield[@code='j']";
  $xpath_return = $xml_record->xpath($xquery);
  foreach ($xpath_return as $marc999){
    $subfield_code = (string)$marc999['code'];
    $subfield_value = (string)$marc999[0];

    if($subfield_code == 'j'){
      $cylnum = strtolower(str_replace(' ', '', $subfield_value));
      $return[$cylnum] = array('j' => $subfield_value);
      $temp = array();
    }else{
      array_push($temp, $subfield_value);
    }
    $return[$cylnum]['z'] = $temp;
  }
  return $return;
}

function get_multiple_titles_245($xml_record){
  /*
  extract multiple MARC 245 sub-fields from XML
  pass in simple XML object of MARC record
  returns a multidimensional array
  */
    $xquery = "descendant::datafield[@tag='245']/subfield[@code='a']";
    $xpath_return = $xml_record->xpath($xquery);
    foreach ($xpath_return as $values){
      $marc245a = (string)$values[0];
      }
    $xquery = "descendant::datafield[@tag='245']/subfield[@code='b']";
    $xpath_return = $xml_record->xpath($xquery);
    foreach ($xpath_return as $values){
      $marc245b = (string)$values[0];
      }
    $xquery = "descendant::datafield[@tag='245']/subfield[@code='p']";
    $xpath_return = $xml_record->xpath($xquery);
    foreach ($xpath_return as $values){
      $marc245p = (string)$values[0];
      }
    $xquery = "descendant::datafield[@tag='245']/subfield[@code='n']";
    $xpath_return = $xml_record->xpath($xquery);
    foreach ($xpath_return as $values){
      $marc245n = (string)$values[0];
      }

    $marc245a = rtrim($marc245a, ' ./');
    $marc245b = rtrim($marc245b, '/' );

    $titles = array(
      '245a' => $marc245a ,
      '245b' => $marc245b ,
      '245p' => $marc245p ,
      '245n' => $marc245n ,
      );


  return $titles;
}
function get_multiple_uniform_titles_240($xml_record){
  /*
  extract multiple MARC 240 sub-fields from XML
  pass in simple XML object of MARC record
  returns a multidimensional array
  */
    $xquery = "descendant::datafield[@tag='240']/subfield[@code='a']";
    $xpath_return = $xml_record->xpath($xquery);
    foreach ($xpath_return as $values){
      $marc240a = (string)$values[0];
      }
    $xquery = "descendant::datafield[@tag='240']/subfield[@code='k']";
    $xpath_return = $xml_record->xpath($xquery);
    foreach ($xpath_return as $values){
      $marc240k = (string)$values[0];
      }
    $xquery = "descendant::datafield[@tag='240']/subfield[@code='l']";
    $xpath_return = $xml_record->xpath($xquery);
    foreach ($xpath_return as $values){
      $marc240l = (string)$values[0];
      }
    $xquery = "descendant::datafield[@tag='240']/subfield[@code='o']";
    $xpath_return = $xml_record->xpath($xquery);
    foreach ($xpath_return as $values){
      $marc240o = (string)$values[0];
      }
    $xquery = "descendant::datafield[@tag='240']/subfield[@code='p']";
    $xpath_return = $xml_record->xpath($xquery);
    foreach ($xpath_return as $values){
      $marc240p = (string)$values[0];
      }
    $xquery = "descendant::datafield[@tag='240']/subfield[@code='r']";
    $xpath_return = $xml_record->xpath($xquery);
    foreach ($xpath_return as $values){
      $marc240r = (string)$values[0];
      }
    $xquery = "descendant::datafield[@tag='240']/subfield[@code='n']";
    $xpath_return = $xml_record->xpath($xquery);
    foreach ($xpath_return as $values){
      $marc240n = (string)$values[0];
      }
    $xquery = "descendant::datafield[@tag='240']/subfield[@code='m']";
    $xpath_return = $xml_record->xpath($xquery);
    foreach ($xpath_return as $values){
      $marc240m = (string)$values[0];
      }

    $xquery = "descendant::datafield[@tag='130']/subfield[@code='a']";
    $xpath_return = $xml_record->xpath($xquery);
    foreach ($xpath_return as $values){
      $marc130a = (string)$values[0] ;
      }
    $un_trimmed_uniform_titles = array(
      '240a' => $marc240a ,
      '240n' => $marc240n ,
      '240p' => $marc240p ,
      '240r' => $marc240r ,
      '240m' => $marc240m ,
      '240k' => $marc240k ,
      '240o' => $marc240o ,
      '240l' => $marc240l ,
      '130a' => $marc130a ,
      );
    $uniform_titles = array_map('trim', $un_trimmed_uniform_titles);
    $display_brackets = false;
    foreach ($uniform_titles as $key => $value) {
      if(strlen(trim($value)) > 1){$display_brackets = true;}
    }
  $uniform_titles['imploded'] = trim(implode(' ', $uniform_titles));
  $uniform_titles['display_brackets'] =  $display_brackets;
  return $uniform_titles;
}

function get_multiple_subjects($xml_record){
  /*
   extract MARC 650 field data
   pass in simple XML object of MARC record
   returns indexed array of strings
   */
    $xquery = "descendant::datafield[@tag='650']";
    $xpath_return = $xml_record->xpath($xquery);
    foreach ($xpath_return as $values){

      // skip 650 fields where indicator 2 is 7
      $zud = $values->attributes();
      if($zud['ind2'] == '7'){
        continue;
        }
      foreach($values as $value){
          $buster[] = (string)$value ;
        }
      $subjects[] = implode('&mdash;', $buster);
      $subject = '';
      $buster = array();
    }
  return $subjects;
}

function get_multiple_genres_655($xml_record){
  /*
   extract MARC 655 field data
   pass in simple XML object of MARC record
   returns indexed array of strings
  */
    $xquery = "descendant::datafield[@tag='655']/subfield[@code='a']";
    $xpath_return = $xml_record->xpath($xquery);
    $ct = 0;
    foreach ($xpath_return as $values){

    $genres[] = (string)$values[0];
    }
  return $genres;
}

function get_multiple_personal_names($xml_record){
  /*
  extract multiple MARC 100 & 700 fields from XML
  pass in simple XML object of MARC record
  returns a multidimensional array that looks like this:
  Array(
    [0] => Array
        (
            [name] => Parry, Joseph,
            [dates] => 1841-1903.
            [href] => /search.php?nq=1&query_type=author&query=Parry,+Joseph,
        )

    [1] => Array
        (
            [name] => Edwards, Powell.
            [href] => /search.php?nq=1&query_type=author&query=Edwards,+Powell.
        )

    [2] => Array
        (
            [name] => Roberts, John
            [href] => /search.php?nq=1&query_type=author&query=Roberts,+John
        )
      )
*/

/*
  <datafield tag="100" ind1="1" ind2=" ">
    <subfield code="a">Parry, Joseph,</subfield>
    <subfield code="d">1841-1903.</subfield>
  </datafield>
*/
    $xquery = "descendant::datafield[@tag='100']";
    $xpath_return = $xml_record->xpath($xquery);
    foreach ($xpath_return as $values) {
      foreach($values as $value){
        if($value['code'] == 'a'){$name = (string)$value[0];}
        if($value['code'] == 'd'){$dates = (string)$value[0];}
        $href = "/search.php?nq=1&query_type=author&query=".str_replace('é','e',str_replace(' ', '+', $name));
        if(strlen($dates > 1) ){
          $href .= '+'.urlencode($dates);
        }
      }
    $personal_names[] = array(
      'name'  => $name,
      'dates' => $dates,
      'href'  => $href
      );
    }

/*
  <datafield tag="700" ind1="1" ind2=" ">
    <subfield code="a">Lescot, Edmée,</subfield>
    <subfield code="e">singer.</subfield>
  </datafield>
*/
    $xquery_personal_name = "descendant::datafield[@tag='700']/subfield[@code='a']";
    $info_personal_name = $xml_record->xpath($xquery_personal_name);
    if ($info_personal_name){
      foreach($info_personal_name as $datafield){
        $name = (string)$datafield[0];
        $href = "/search.php?nq=1&query_type=author&query=".str_replace('é','e',str_replace(' ', '+', $name));
        $personal_names[] = array('name' => $name, 'href' => $href);
      }
    }
  foreach ($personal_names as $key => $value) {
    $personal_names[$key]['name'] = trim($value['name'], ',.');
    $personal_names[$key]['dates'] = trim($value['dates'], ',.');
    $personal_names[$key]['href'] = trim($value['href'], ',.');
  }
  return $personal_names;
}

function trim_commas_and_periods(&$value, $key){
  /*
  to be utilized as a callback function
  example:  array_walk(&$array, 'trim_commas_and_periods')
  */
  $value = trim($value, ',.');
}

function format_release_year(&$ry){
  // remove square brackets from release year
  // pass in a string, returns a string
  $ry = trim($ry, '[]');
  return $ry;
  }

function get_multiple_collections($xml_record){
  // extract multiple MARC 561 fields from XML
  // pass in simple XML object of MARC record
  // returns indexed array of strings
    $xquery_collection = "descendant::datafield[@tag='561']/subfield[@code='a']";
    $info_collection = $xml_record->xpath($xquery_collection);
    if ($info_collection) {
      foreach($info_collection as $datafield){
        $collections[] = (string)$datafield[0];
      }
    }
  return $collections;
  }

function get_multiple_other_titles_740a($xml_record){
  /*
  extracts multiple MARC 740-a fields from XML
  pass in a simple XML object of MARC record data
  returns an indexed array of strings
  */
  $xquery_other_title = "descendant::datafield[@tag='740']/subfield[@code='a']";
  $info_other_title = $xml_record->xpath($xquery_other_title);
  if ($info_other_title){
    foreach($info_other_title as $datafield){
      $other_titles[] = (string)$datafield[0];
    }
  return $other_titles;
  }
}

function get_multiple_notes($xml_record){
  /*
  extract multiple MARC 500 & 520 fields from XML
  pass in simple XML object of MARC record
  returns indexed array of strings
  */
    $xquery_notes = "descendant::datafield[@tag='500']/subfield[@code='a']";
    $info_notes = $xml_record->xpath($xquery_notes);
    if ($info_notes) {
      foreach($info_notes as $datafield){
        $notes[] = (string)$datafield[0];
      }
    }
    $xquery_notes520 = "descendant::datafield[@tag='520']/subfield[@code='a']";
    $info_notes520 = $xml_record->xpath($xquery_notes520);
    if ($info_notes520) {
      foreach($info_notes520 as $datafield){
        $notes[] = (string)$datafield[0];
      }
    }
    $xquery_notes590 = "descendant::datafield[@tag='590']/subfield[@code='a']";
    $info_notes590 = $xml_record->xpath($xquery_notes590);
    if ($info_notes590) {
      foreach($info_notes590 as $datafield){
        $notes[] = (string)$datafield[0];
      }
    }
  return $notes;
  }

function get_multiple_issue_numbers($xml_record){
  // extract multiple MARC 500 fields from XML
  // pass in simple XML object of MARC record
  // returns indexed array of strings
    $xquery = "descendant::datafield[@tag='028']";
    $xpath_return = $xml_record->xpath($xquery);
    if($xpath_return){
      foreach($xpath_return as $datafield){
        // they have the data stored oddly in MARC 028 subfields. The values are stored in the opposite order than desired for display.
        $issue_numbers[] = (string)$datafield->subfield[1].": ".(string)$datafield->subfield[0].'.';
      }
    }
  return $issue_numbers;
  }

function get_multiple_release_years($xml_record){
  // extract multiple MARC 260 or 264 fields from XML
  // pass in simple XML object of MARC record
  // returns indexed array of strings
    $xquery = "descendant::datafield[@tag='260']/subfield[@code='c']";
    $xpath_return = $xml_record->xpath($xquery);
    if($xpath_return){
      foreach($xpath_return as $datafield){
        $release_years[] = (string)$datafield[0];
      }
    }else{
      $xquery = "descendant::datafield[@tag='264']/subfield[@code='c']";
      $xpath_return = $xml_record->xpath($xquery);
      if($xpath_return){
        foreach($xpath_return as $datafield){
          // they have the data stored oddly in MARC 028 subfields. The values are stored in the opposite order than desired for display.
          $release_years[] = (string)$datafield[0];
        }
      }else{
        $release_years = false;
      }
    }
  return $release_years;
  }

function get_856U_values($xml_record){
  // get the values of the MARC 856 field.  Most cylinders have a value in the 856-Z or 865-U subfields
  // pass in simple XML object of MARC record
  // returns indexed array of strings or boolean FALSE if 856-U was not set
    $xquery = "descendant::datafield[@tag='856']/subfield[@code='u']";
    $xpath_return = $xml_record->xpath($xquery);

    if($xpath_return){
      foreach($xpath_return as $datafield){
        if(stripos((string)$datafield[0], 'ark:')){continue;}
        $marc_856U[] = (string)$datafield[0];
      }
    }else{
      $marc_856U = false;
    }
  return $marc_856U;
  }

function build_digitized_subdirectory_name($cylinder_number){
  /*
    digitized cylinders are stored in numbered subdirectories
    http://cylinders.library.ucsb.edu/mp3s/0000/0989/cusb-cyl0989d.mp3
    http://cylinders.library.ucsb.edu/mp3s/0000/0001/cusb-cyl0001d.mp3
  */

    // strip trailing '-1' off cylinder numbers if any
    $cylinder_number = preg_replace("/-[[:digit:]]+/" , "" , $cylinder_number);

    // cylinders numbered from 1-999 are stored in directory named 0000
    if(strlen($cylinder_number) < 4){
      $cylinder_group_dir = "0000";
    }
    // cylinders numbered from 1000-9999 are stored in directories with 4 character names 0000 through 9000
    if(strlen($cylinder_number) == 4){
      $cylinder_group_dir = substr($cylinder_number,0,1)."000";
    }
    // cylinders numbered from 10000-99999 are stored in directories with 5 character names 10000 through 99000
    if(strlen($cylinder_number) > 4){
      $cylinder_group_dir = substr($cylinder_number,0,2)."000";
    }
  return $cylinder_group_dir;
}

function get_recordings($xml_record){
  /*
  for some cylinders we have more than one recording for which we need to display player widgets on the detail page.
  pass in a simple xml record object returned from an SRU query.
  returns an indexed multi-dimensional array with all the data needed for the player widget

  pass in simple XML object of MARC record
  */
  $cylinder_856U = get_856U_values($xml_record);
  $pattern = "/https?:\/\/www\.library\.ucsb\.edu\/OBJID\/Cylinder/i";
  foreach ($cylinder_856U as $eight_five_six_u_value) {
    $cylinder_number =  preg_replace($pattern, "", $eight_five_six_u_value);
    $digitized_cylinder_subdirectory_name = build_digitized_subdirectory_name($cylinder_number);
    $recordings_data[] = array ('number' => $cylinder_number, 'subdirectory' => $digitized_cylinder_subdirectory_name);
  }

  return $recordings_data ;

}

function get_cylinder_numbers($xml_record){
  /*
  pass in simple XML object of MARC record
  */
  $cylinder_856U = get_856U_values($xml_record);
  $pattern = "/https?:\/\/www\.library\.ucsb\.edu\/OBJID\/Cylinder/i";
  foreach ($cylinder_856U as $eight_five_six_u_value) {
    $cylinder_numbers[] =  preg_replace($pattern, "", $eight_five_six_u_value);
  }
}

function get_856Z_values($xml_record){
  /*
   gets the value of the MARC 856-Z field.
   Most cylinders have a value in the 856-Z
   pass in simple XML object of MARC record
   returns indexed array of strings or boolean FALSE if 856-Z was not set
   MARC 856-z should contain digitization status.  Example: "Cylinder has not been digitized for online access."
  */
    $xquery = "descendant::datafield[@tag='856']/subfield[@code='z']";
    $xpath_return = $xml_record->xpath($xquery);
    if($xpath_return){
      foreach($xpath_return as $datafield){
        $marc_856Z[] = (string)$datafield[0];
//        if(DEVELOPMENT){error_log(__FILE__." ".__line__." 856-z value: ".(string)$datafield[0]);}// debug
      }
    }else{
      $marc_856Z = false;
    }
  return $marc_856Z;
  }

function get_digitization_status($xml_record){
  $is_digitized = true;
  // determine if a cylinder has or has not been digitized
  // pass in simple XML object of MARC record
  // returns a boolean
  /*
  This Cylinder not digitized: 12147
    <datafield tag="856" ind1=" " ind2=" ">
    <subfield code="z">Cylinder has not been digitized for online access.</subfield>
    </datafield>

  This cylinder IS digitized: 1407
    <datafield tag="856" ind1="4" ind2="1">
    <subfield code="3">Audio files:</subfield>
    <subfield code="u">https://www.library.ucsb.edu/OBJID/Cylinder1407</subfield>
    </datafield>

marc_856Z should have digitization status.  Example: "Cylinder has not been digitized for online access."
marc_856U should have durable URL. Example: "https://www.library.ucsb.edu/OBJID/Cylinder12425"
  */
  $marc856U = get_856U_values($xml_record);
  if($marc_856U === false){
    $is_digitized = false;
  }else{
    $is_digitized = true;
  }

  $marc856Z = get_856Z_values($xml_record);
  if($marc856Z === false){
    $is_digitized = true;
  }else{
    // condition is:  there is something in the 856-Z
    if($marc856Z[0] == "Cylinder has not been digitzed for online access."){
      // handle mis-spelling in 856-Z
      error_log("cylinder MMS_ID: ".get_mms_id($xml_record)." has a mis-spelling in the 856-Z field.");
      $is_digitized = false;
    }
    if($marc856Z[0] == "Cylinder has not been digitized for online access."){
      $is_digitized = false;
    }
  }
  return $is_digitized;
 }

// source: eric http://php.net/manual/en/ref.simplexml.php
function xml2array ( $xmlObject, $out = array () ){
  foreach ( (array) $xmlObject as $index => $node ){
    if( is_object($node) ||  is_array($node)){
      xml2array ( $node );
    }else{
      $out[$index] = $node;
    }
  }
  return $out;
}

function get_mp3_subdir($cylinder_number){
  // cylinder group directory (detailed view)
  // digitized cylinders are stored in numbered subdirectories

      // cylinders numbered from 0-9999 are stored in directories with 4 character names 0000 through 9000
      if(strlen($cylinder_number) == 4){
        $cylinder_group_dir = substr($cylinder_number,0,1)."000";
      }
      // cylinders numbered from 10000-99999 are stored in directories with 5 character names 10000 through 99000
      if(strlen($cylinder_number) > 4){
        $cylinder_group_dir = substr($cylinder_number,0,2)."000";
      }
    return $cylinder_group_dir;
  }
function get_mms_id($record){
  // function to extract an mms_id from a simpleXML object of a single MARC record
  // Pass in a record
  // get back a string of the mms_id
  $xquery = "descendant::controlfield[@tag='001']";
  $xpath_return = $record->xpath($xquery);
  foreach($xpath_return as $array){
    $mms_id = (string)($array[0]);
  }
  return $mms_id;
}

function get_all_mms_ids($searchRetrieveResponse){
  // pass in a result set from an SRU call as an simpleXML object
  // returns an associative array of mms_ids with recordPositions as the keys
  $records = $searchRetrieveResponse->records->record;
  foreach($records as $record){
    //echo"<pre>";var_dump($record);echo"</pre><hr>";
    $key = (string)$record->recordPosition;
    $value = get_mms_id($record);
    $mms_ids[$key] = $value;
    }
  return $mms_ids;
  }

function query_string_without_start($get_params){
  // this function used in building pagination
  unset($get_params['start']);
//  unset($get_params['nq']);
  foreach ($get_params as $key => $value) {
    $string .= '&' . $key . "=" . $value;
  }
  $string = trim($string, '&');
  return '?' .$string;
}

function get_start_record($get_parrams){
  /* to avoid passing a zero as the startRecord query param
   this function used in building pagination  */
  if(isset($_GET['start'])){
    $get_int = intval($_GET['start']);// need to sanitize
  }else{
    $get_int = 0;
  }
  if($get_int == 0){$get_int = 1;}
  return $get_int;
}

function fetch_sru_results($SRUquery){
  /*     get's SRU query results either by...
     pulling them from Memcache
     querying the configured remote SRU server
     constants are stored in config/main.php
     $SRUquery should be the output from build_sru_query()
     returns XML  of  MARC record(s)

     */
  $query_results= "error";
  $memcache = new Memcached();
  $cacheAvailable = $memcache->addServer(MEMCACHED_HOST, MEMCACHED_PORT);
  if($cacheAvailable){
    // memcache service is available

    // set the key for this query
    $memcachekey = 'key' . md5($SRUquery);
    if (!$results = $memcache->get($memcachekey)) {

      // query results have not yet been stored in memcache so query SRU and store the results in the cache
      $SRUresults = fetch_from_sru($SRUquery, $maximumRecords);
//      if(DEVELOPMENT === true){error_log(" ".__FILE__." ".__LINE__." fetching results from Alma");} //debug
      $memcache->set($memcachekey, $SRUresults);

//      if(DEVELOPMENT === true){error_log(" ".__FILE__." ".__LINE__." Caching search results from Alma. memcachekey: ".$memcachekey );} //debug
      $query_results = $SRUresults;
    }else{
      // grab the results from memcache
      $query_results = $memcache->get($memcachekey);
//      if(DEVELOPMENT === true){error_log(" ". __FILE__." ".__LINE__." fetching results from Memcache");} //debug
    }
  }else{
    // memcache service is NOT available so we fall back to SRU
    error_log(" ".__FILE__." ".__LINE__.'memcache service not available');
    $query_results = fetch_from_sru($SRUquery, $maximumRecords);
  }
$query_results = str_replace('xmlns=', 'ns=', $query_results);// fix for Jira issue DEV-1261
return $query_results;
}

function fetch_from_sru($SRUquery){
/*  This function retrieves results from an SRU server.
It does not look for results in Memcache.
For that see function above: fetch_sru_results
pass in your query from build_sru_query()
returns a string of XML from the SRU server.
This function will most often be called from function 'fetch_sru_results'
when the same info is not available from memcache
*/
// initialize connection to SRU Server and fetch query results
// returns a string of XML
if(isset($maximumRecords)){
  // this conditional handles situation where we need to make an SRU query
  // for more than the configured results_per_page limit.
  $SRUserver_path = SRU_PATH;
  $fud = strpos($SRUserver_path, '&maximumRecords=');
  $SRUserver_path = substr_replace($SRUserver_path, '&maximumRecords=', $fud);
  error_log("SRUserver_path: ". $SRUserver_path);
  return __FILE__." ".__LINE__." ".__FUNCTION__." error ";//debug


}else{
    $curlopt_url = SRU_PROTOCOL . '://' . SRU_HOST . SRU_PATH . $SRUquery;
}
    $curl_handle = curl_init();

    $curl_options = array(
      CURLOPT_URL => $curlopt_url,
      CURLOPT_FAILONERROR => TRUE,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_CONNECTTIMEOUT_MS => CURLCONNECTTIMEOUT,
      );

    $_SESSION['url_fetched'] = $curlopt_url; //debug

    $result_of_curlsetops = curl_setopt_array($curl_handle, $curl_options);
    if($result_of_curlsetops == TRUE){
    //  curl options set OK.  will now execute curl call...
        $result_of_curl_exec = curl_exec($curl_handle);
      }elseif($result_of_curlsetops == FALSE){
        error_log(" ".__FILE__." ".__LINE__."curl_setopt_array failed");
        echo "was unable to set options.  Please report this error to system developers.";
        exit;
      }
    //  Test if the curl_exec() failed
    if($result_of_curl_exec === FALSE){
      error_log(" ".__FILE__." ".__LINE__." curl_exec failed near line ". __LINE__ . " with: " .curl_error($curl_handle));
      error_log(" ".__FILE__." ".__LINE__." curlopt_url ".$curlopt_url); //debug

      //header ( string $string [, bool $replace = true [, int $http_response_code ]] )
      header($_SERVER["SERVER_PROTOCOL"]." 500 Internal Server Error");
      $datetime = date("Y-m-d H:i:s");
      $thishost = $_SERVER['HTTP_HOST'];

      $error_body = <<<EOL
<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
<html><head>
<title>500 Server Error</title>
</head><body>
<h1>Data source unavailable</h1>
<h4>500 Server Error for $thishost</h3>
<p>The server was unable to connect to the remote data source.</p>
<p>An administrator should inspect the web server log for details.</p>
<p>Error occured at: $datetime </p>
</body></html>
EOL;
      echo $error_body;
      exit;
    }else{
    // curl call successful. next step is to cache results
    return $result_of_curl_exec;
    }
  }

function fetch_cylinder_record_from_memcache($key){
  // furnish a key and get back whatever was stored for a particular cylinder
  // if nothing was stored function will return false.  This may happen if the record was stored a long time ago and the cache lifetime was reached
  // key should be an Alma mms_id number
  $memcache = new Memcached();
  $cacheAvailable = $memcache->addServer(MEMCACHED_HOST, MEMCACHED_PORT);
  if($cacheAvailable){
    // memcache service is available
    if (!$results = $memcache->get($key)) {
      // this cylinder record is not in memcache
      return false;
    }else{
     if(DEVELOPMENT === true){error_log(" ". __FILE__." ".__LINE__." fetching cyl record from Memcache");} //debug
      //$results['source'] = 'cache';//debug
      return $results;
    }
  }else{
    // memcache service is NOT available
    error_log(" ".__FILE__." ".__LINE__.' memcache service not available. record not cached');
    // may want to add routine here to fetch the data from Alma via SRU which will be slow but at least it should work.
    return false;
  }
}
function build_alma_query_portion($query_term, $alma_field){
  /*  breaks words in a query into separate SRU query terms
  $query_term should be a user submitted query
  $alma_field is the indexed field in which Alma should search
  returns a string suitable for sending to the SRU server
  */
  $return = '';
  $cylinder_special_characters = "éàâäåçéèêëíìîïñóòöúùûüÀÁÃÄÂÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖÙÚÛÜÝàáâãäåæçèéêëìíîïðñòóôõöùúûüýÿ";
  $words = str_word_count($query_term, 1, $cylinder_special_characters.'0123456789');

  //$return = implode(' '.$alma_field.'=', $words);
  foreach ($words as $key => $word) {
    $return .= $alma_field."=".$word.' ';
  }
  return $return;
}

function get_sort_index(){
  /*
  returns a string of any possible sort indexes: creator, main_pub_date, title
  based on the URL query params
  Default is 'title'
  */
  if(isset($_GET['sortBy'])){
    switch($_GET['sortBy']){
      case 'author':
        $return = 'creator';
        break;
      case 'a':
        $return = 'creator';
        break;
      case 'year':
        $return = 'main_pub_date';
        break;
      case 'y':
        $return = 'main_pub_date';
        break;
      default:
        $return = 'title';
    }
  }else{
    $return = 'title';
  }
  return $return;
}

function get_sort_order(){
  /*
  returns a string of selected sort order
  Possible values: ascending, descending
  based on the URL query params
  default is ascending
  */
  if(isset($_GET['sortOrder'])){
    switch($_GET['sortOrder']){
      case 'd':
        $return = 'descending';
        break;
      default:
        $return = 'ascending';
    }
  }else{
    $return = 'ascending';
  }
  return $return;
}

function build_sru_query($query_type = NULL, $query_term = NULL, $direction = FALSE, $sort_index = 'title', $sort_order = 'ascending'){
  /*     $direction passed in should be 'previous' or 'next'
         $query_type should be one of: title, author, keyword, year, subject, call_number, mms_id
         $sort_index possible values: creator, main_pub_date, title
         $sord_order possible values: ascending, descending
         Documentation: https://developers.exlibrisgroup.com/alma/integrations/SRU
         */
  $sru_query_cylinders_limiter_clause = "and alma.mms_tagSuppressed=false and alma.elocation=Cylinder)";
  $sru_query_sort_order_clause = " sortBy alma.". get_sort_index() ."/sort.". get_sort_order() ;

  if(!isset($query_type)){
    $query_type = $_SESSION['query_type'];
  }
  if(!isset($query_term)){
    $query_term = $_SESSION['query_term'];
  }

  switch($query_type){
    case 'title':
//      $SRUquery = '&query=' . urlencode("alma.title=$query_term and alma.mms_tagSuppressed=false and alma.elocation=Cylinder)");
      $SRUquery = '&query=' . urlencode( build_alma_query_portion($query_term, 'alma.title').              $sru_query_cylinders_limiter_clause . $sru_query_sort_order_clause );
      break;
    case 'author':
      $SRUquery = '&query=' . urlencode( build_alma_query_portion($query_term, 'alma.creator').            $sru_query_cylinders_limiter_clause . $sru_query_sort_order_clause );
      break;
    case 'keyword':
      $SRUquery = '&query=' . urlencode( build_alma_query_portion($query_term, 'alma.all_for_ui').         $sru_query_cylinders_limiter_clause . $sru_query_sort_order_clause );
      break;
    case 'year':
      $SRUquery = '&query=' . urlencode( build_alma_query_portion($query_term, 'alma.date_of_publication').$sru_query_cylinders_limiter_clause . $sru_query_sort_order_clause );
      break;
    case 'subject':
      $SRUquery = '&query=' . urlencode( build_alma_query_portion($query_term, 'alma.subjects').           $sru_query_cylinders_limiter_clause . $sru_query_sort_order_clause );
      break;
    case 'call_number':
      $query_term = str_replace('cylinder', '', strtolower($query_term));
      if(is_numeric($query_term)){
        // A query for cylinder 9 or 99 or 999 doesn't work.  The query needs zeros padded on the left to make it 4 digits
        $query_term = str_pad($query_term, 4, '0', STR_PAD_LEFT);
      }
      $SRUquery = '&query=' . urlencode( build_alma_query_portion($query_term, 'alma.PermanentCallNumber').$sru_query_cylinders_limiter_clause . $sru_query_sort_order_clause );
      break;
    case 'mms_id':
      $SRUquery = '&query=' . urlencode( build_alma_query_portion($query_term, 'alma.mms_id').             $sru_query_cylinders_limiter_clause . $sru_query_sort_order_clause );
      break;
    case 'OBJID':
      // this case handles queries forwarded/redirected from the PURL/OBJID with urls like: https://www.library.ucsb.edu/OBJID/Cylinder8317
      $SRUquery = '&query=' . urlencode( build_alma_query_portion($query_term, 'alma.PermanentCallNumber').$sru_query_cylinders_limiter_clause . $sru_query_sort_order_clause );
      break;

    default:
      // default is a keyword search
      $SRUquery = '&query=' . urlencode( build_alma_query_portion($query_term, 'alma.all_for_ui').         $sru_query_cylinders_limiter_clause . $sru_query_sort_order_clause );
  }
    if(isset($_GET['start'])){
        $SRUquery .= '&startRecord=' . get_start_record($_GET);
      }elseif($direction == 'next'){
        $SRUquery .= '&startRecord=' . $_SESSION['nextRecordPosition'];
      }elseif($direction == 'previous'){
        if($_SESSION['nextRecordPosition'] != 0 ){
          $SRUquery .= '&startRecord=' . ($_SESSION['nextRecordPosition'] - ((RESULTS_PER_PAGE * 2) + 1));
        }else{
          // nextRecordPosition equals 0.  Like when the query is for an individual cylinder
          $result_set_keys = array_keys($_SESSION['result_set']);
          $first_key_of_result_set = array_shift($result_set_keys);
          $SRUquery .= '&startRecord=' . ($first_key_of_result_set - RESULTS_PER_PAGE);
          return $SRUquery;
        }
      }
//  error_log(" ".__FILE__." ".__LINE__." SRUquery: ".$SRUquery); //debug
  return $SRUquery;
}

function cache_individual_cylinder_records($searchRetrieveResponse){
/* now to loop over searchRetrieveResponse->records->record extracting and caching the data we're interested in
   Returns an associative array of pagination info and record info.
*/
  $currentRecordNumber = 0;
  foreach($searchRetrieveResponse->records->record as $record){
    $currentRecordNumber++;
    $recordPosition = (integer)$record->recordPosition; // which record of all the records found in the search.
    $pagination[] = $recordPosition;
    $current_record = extract_record_info($record);
    $current_record['recordings'] = get_recordings($record);
    $mms_id = $current_record['mms_id'];
    $_SESSION['result_set'][$recordPosition] = $mms_id; // we store the mms_ids in the session for use by next/prev-cylinder.php scripts

  // Cache each record individually
    $memcache = new Memcached();
    $cacheAvailable = $memcache->addServer(MEMCACHED_HOST, MEMCACHED_PORT);
    // $cacheAvailable = false; //debug turning off memcache for troubleshooting.
    if($cacheAvailable){
      // memcache service is available

      if (!$results = $memcache->get($mms_id)) {
        // this cylinder record has not yet been stored in memcache so we'll do so now
        $memcache->set($mms_id, $current_record);
        error_log('storing record in memcache for '.$mms_id);//debug

      }
    }else{
      // memcache service is NOT available so we fall back to SRU
      error_log('memcache service not available. record not cached');
    }
    $record_array[$currentRecordNumber] = $current_record;

  //echo "<h2>current_record</h2><pre>"; print_r(fetch_cylinder_record_from_memcache($mms_id)); echo "</pre><hr>";//debug
  } // end foreach record returned by SRU
  $return = array();

  array_push($return, $pagination);
  array_push($return, $record_array);

  return $return;
}

function next_mms_id($current_mms_id){
  /*
  looks in the SESSION[result_set]
  and returns a string with the next mms_id in the result set.
  or queries for the next set of results
  */
  $result_set = $_SESSION['result_set'];
  $query_type = $_SESSION['query_type'];
  $query_term = $_SESSION['query_term'];
  $search_results_count = $_SESSION['search_results_count'];
  $current_record_position = array_search($current_mms_id, $result_set);
//  echo "current_mms_id: ".$current_mms_id."<br>";
  //var_dump($current_mms_id); //debug
  //var_dump($current_record_position); //debug
  //var_dump($current_record_position); //debug
  //var_dump($current_record_position); //debug
  //var_dump($current_record_position); //debug
  if($current_record_position === false){
    // this conditional is reached when the current_mms_id is not in the result set
    // conduct a query using the type and term stored in the session
      $SRUquery = build_sru_query($query_type, $query_term);
//      echo "\n<br>SRUquery: ".$SRUquery . "<br> \n";//debug
      fetch_sru_results($SRUquery);
    if(DEVELOPMENT == true){
      error_log("error in file:". __FILE__." ".__FUNCTION__." at line: ".__LINE__);
    }else{
      return $current_mms_id; //temp solution 2018-05-30 IL
    }

  }elseif($current_record_position == $search_results_count){
    // there are no more records in this search result
    return false;
  }else{
    $next_record_position = $current_record_position + 1 ;
    //echo "next_record_position: " . $next_record_position; //debug
    if(isset($result_set[$next_record_position])){
      $return = $result_set[$next_record_position];
      return $return;
    }else{
      // This condition handles the case where we are at the last record in the result_set
      // need to query SRU for the next in the set
      $SRUquery = build_sru_query($query_type, $query_term, 'next');
      $mySRUquery = str_replace('&startRecord='.$_SESSION['nextRecordPosition'], '&startRecord='.$next_record_position, $SRUquery);
      $SRU_query_response = fetch_sru_results($mySRUquery);
      $SRU_query_response_obj = simplexml_load_string($SRU_query_response);
      $currentRecordNumber = 0;
      foreach($SRU_query_response_obj->records->record as $record){
        $currentRecordNumber++;
        $recordPosition = (integer)$record->recordPosition; // which record of all the records found in the search.
        $current_record = extract_record_info($record);
        $mms_id = $current_record['mms_id'];
        $_SESSION['result_set'][$recordPosition] = $mms_id;
        if($currentRecordNumber == 1){
          $next_mms_id = $mms_id;
        }
      }
    return $next_mms_id;
    }
  }
}
function previous_mms_id($current_mms_id){
  /*
  looks in the SESSION[result_set]
  and returns a string with the previous mms_id in the result set.
  or queries for the previous set of results
  */
  $result_set = $_SESSION['result_set'];
  $current_record_position = array_search($current_mms_id, $result_set); // current_record_position is the key or index numeral from the result_set
  //var_dump($current_record_position); //debug
  $previous_record_position = $current_record_position - 1 ;
  //echo "previous_record_position: " . $previous_record_position . "<br>\n"; //debug

  if($current_record_position == 1){
    // the current record is the first of the current result set so there is no previous record
    return false;
  }

  if(isset($result_set[$previous_record_position])){
    // previous mms_id is in the current result set
    $return = $result_set[$previous_record_position];
    return $return;
  }else{
    // This condition handles the case where we are at the first record in the
    // current_result_set available in the _SESSION but not the very first of the entire result set
    // need to query SRU for the previous in the set
    $query_type = $_SESSION['query_type'];
    $query_term = $_SESSION['query_term'];
    $SRUquery = build_sru_query($query_type, $query_term, 'previous');
//    echo $SRUquery."<br>";
//   $mySRUquery = str_replace('&startRecord='.$_SESSION['previousRecordPosition'], '&startRecord='.$previous_record_position, $SRUquery);
    $SRU_query_response = fetch_sru_results($SRUquery);
    $SRU_query_response_obj = simplexml_load_string($SRU_query_response);
    $currentRecordNumber = 0;
    foreach($SRU_query_response_obj->records->record as $record){
      $currentRecordNumber++;
      $recordPosition = (integer)$record->recordPosition; // which record of all the records found in the search.
      $current_record = extract_record_info($record);
      $mms_id = $current_record['mms_id'];
      $_SESSION['result_set'][$recordPosition] = $mms_id;
      if($currentRecordNumber == 1){
        $previous_mms_id = $mms_id;
      }
    }
  return $previous_mms_id;
  }
   error_log('previous_mms_id function failed at '. __LINE__ . 'in file:' . __FILE__);
   return false;
  }

function sanitize($user_input, $output_type = ''){
  switch ($output_type) {
    case 'js':
  // Sanitize for JavaScript output
      return json_encode($user_input);
      break;

    case 'url':
    // Sanitize for use in a URL
      return urlencode($user_input);
      break;

    default:
    // Sanitize for HTML output
      return htmlspecialchars($user_input);
      break;
  }
  return "error funcs line 1204";
}

function xpath_query_succeeds($record_object){
/* pass in a simple XML object and get back a booleen
returns true if the simpleXML method works properly and the object is of the schema 'marcxml'
  xml like:      <recordSchema>marcxml</recordSchema>
returns false if xpath method fails or returns some odd results.

This is to avoid a problem where xpath fails due to a default name space issue.
*/

//  descendant::datafield[@tag='028']/subfield[@code='a']";
  $xpath = "descendant::recordSchema";
  $xpath_results = $record_object->xpath($xpath);
  if(is_null($xpath_results[0])){
  //echo "xpath_results are null\n";
    return false;
  }

  $string_xpath_results = (string)$xpath_results[0];
  if(is_string($string_xpath_results) AND ($string_xpath_results == 'marcxml') ){
  // echo __LINE__."string_xpath_results is a string and the value is 'marcxml' \n";
    return true;
  }
}
