<?php
/*
This script is not meant to be called directly from the browser but rather 
it is meant to be called from a jQuery load() function on the search results page.
Based on params stored in the session it fetches SRU query results, loading them 
into memcache as needed, and returns a link to the next page of search results.
*/
require_once('config/main.php');
require_once('config/smarty.php');
require_once('functions.php');

$SRUquery = build_sru_query($_SESSION['query_type'], $_SESSION['query_term'], 'next');
if(DEVELOPMENT === true){error_log(" ".__FILE__." ".__LINE__." NEXT SRU Query: ".$SRUquery);} //debug

$results = fetch_sru_results($SRUquery, SRU_HOST, $SRUserver_path);

$nextRecordPosition = $_SESSION['nextRecordPosition'];
$current_records = array_keys($_SESSION['result_set']);
$maxCurrentRecord = array_pop($current_records);

if(($_SESSION['search_results_count'] -1 ) == $maxCurrentRecord){
// There is a flaw or bug in Alma's SRU server such that when there are 80 records that match a search result and you ask for maximumRecords=6 and startRecord=74 it does NOT return <nextRecordPosition>80</nextRecordPosition> as I would expect.
	$nextRecordPosition = $_SESSION['search_results_count'];
}

if($results !== false){
  echo '<a href="search.php?query_type='.$_SESSION['query_type'].'&query='.$_SESSION['query_term'].'&start='. $nextRecordPosition .'&nq=1">Next</a>';
}else{
  echo " ";
}

/* ==============================   Debug ==================== */
if($_GET['debug'] ==1 ){
 echo "<br><hr> _SESSION:<pre>"; print_r($_SESSION); echo "</pre><hr>"; 
// $_SESSION['search_results_count']
// echo "maxCurrentRecord:<pre>"; print_r($maxCurrentRecord); echo "</pre><hr>"; 
// echo "_GET:<pre>"; print_r($_GET); echo "</pre><hr>"; 
// echo "SRUquery:<pre>"; print_r($SRUquery); echo "</pre><hr>"; 
// echo "SRUserver:<pre>"; print_r($SRUserver); echo "</pre><hr>"; 
// echo "SRUserver_path:<pre>"; print_r($SRUserver_path); echo "</pre><hr>"; 

// echo $results;

}