stages:
  - maintanence
  - build
  - review
  - staging
  - production
  - cleanup
  - failure_notification

include:
  - ci/base.yml
  - ci/cylinders.yml

# Build with Kaniko. When building on `trunk`, tag a new stable image.
.build_helpers: &build_helpers |
  set -x
  build_image_for() {
    if test $# -ne 2 ; then
        echo "You must specify a project path to a Dockerfile and an image name. example: build_image_for 'cylinders' 'cylinders_web'"
      exit 1
    fi
    project=$1
    image_name=$2
    destination="--destination $CI_REGISTRY_IMAGE/$image_name:$CI_COMMIT_SHA"
    if [ "$CI_COMMIT_BRANCH" = "trunk" ]; then
      destination="$destination --destination $CI_REGISTRY_IMAGE/$image_name:stable"
    fi
    /kaniko/executor --context "$CI_PROJECT_DIR"/$project --cache=true --dockerfile "$CI_PROJECT_DIR"/"$project"/Dockerfile $destination
  }


# Use Kaniko for building container images
.kaniko-build:
  stage: build
  image:
    name: gcr.io/kaniko-project/executor:debug-v0.22.0
    entrypoint: [""]
  before_script:
    - *build_helpers
    - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json

# Default only -> refs settings for all jobs
# see: https://docs.gitlab.com/ee/ci/yaml/#onlyrefsexceptrefs
.only-refs-default:
  only:
    refs:
      - trunk
      - merge_requests
      - tags

# Helm 3 deployment helper functions
.helm3_deploy_helpers: &helm3_deploy_helpers |
  function deploy() {
    download_chart_dependencies

    if helm ls --namespace="$KUBE_NAMESPACE" -q | grep -q "$HELM_RELEASE_NAME"; then
      echo "Deploying new release: $HELM_RELEASE_NAME..."
    else
      first_release="yes"
      echo "Deploying initial release: $HELM_RELEASE_NAME..."
    fi
    helm upgrade \
        --install \
        --atomic \
        --set image.repository="$DEPLOY_IMAGE" \
        --set image.tag="$DEPLOY_TAG" \
        $HELM_UPGRADE_EXTRA_ARGS \
        --namespace="$KUBE_NAMESPACE" \
        "$HELM_RELEASE_NAME" \
        chart/
  }

  # Install chart dependencies
  function download_chart_dependencies(){
    echo "Downloading Helm chart dependencies.."
    # rebuild the charts/ directory based on the Chart.lock file
    helm dependency update chart/
  }

  function delete() {
    echo "Deleting release: $HELM_RELEASE_NAME..."
    helm delete --namespace "$KUBE_NAMESPACE" "$HELM_RELEASE_NAME"
  }

  function rollback() {
    echo "Rolling back to previous release of: $HELM_RELEASE_NAME..."
    helm rollback --wait --namespace "$KUBE_NAMESPACE" "$HELM_RELEASE_NAME"
  }

  function ensure_namespace() {
    kubectl get namespace "$KUBE_NAMESPACE" || kubectl create namespace "$KUBE_NAMESPACE"
  }

  # A GitLab-integrated Kubernetes cluster should already have a KUBECONFIG created
  # Otherwise, we assume the KUBECONFIG is setup as a base64-encoded environment variable
  function setup_kube_config(){
    if [ -z "${KUBECONFIG}" ]; then
      echo "A GitLab KUBECONFIG file was not detected, using KUBE_ROLE_CONFIG env var.."
      mkdir -p /etc/deploy
      export KUBECONFIG=/etc/deploy/config
      echo ${KUBE_ROLE_CONFIG} | base64 -d > $KUBECONFIG
    fi
  }

# Helm3-based image for tiller-less deploys
.helm3-deploy:
  image: "dtzar/helm-kubectl:3.2.1"
  stage: deploy
  before_script:
    - *helm3_deploy_helpers
    - setup_kube_config
    - ensure_namespace
