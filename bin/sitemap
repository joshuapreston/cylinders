#!/usr/bin/env php

<?php
require_once(dirname(__FILE__) . "/../config/main.php");

function cyl_sort($a, $b) {
  // extract the cylinder number, convert to an integer and sort
  $a = (int)preg_replace('/[^0-9]*/', '', $a);
  $b = (int)preg_replace('/[^0-9]*/', '', $b);

  if ($a == $b) {
    return 0;
  }
  return ($a < $b) ? -1 : 1;
}

if ($xml_stat = filemtime(dirname(__FILE__) . "/../sitemap.xml")) {
  $render = "The sitemap was last generated on "
          . date("Y-m-d\TH:i:sP", $xml_stat) . "\n\n";
}
else {
  $render = "Couldn't determine the last time the sitemap was generated.\n\n";
}

// constants SITEMAP_API_SERVER and SITEMAP_API_KEY set in config/main.php
$peg_connection = curl_init(SITEMAP_API_SERVER . "/records_api.php?type=cylinders&key=" . SITEMAP_API_KEY);

// return the result instead of just spitting it onto the screen
curl_setopt($peg_connection, CURLOPT_RETURNTRANSFER, 1);

$cylinders_json = curl_exec($peg_connection);

//
// Bail on error
if (!$cylinders_json) {
  echo curl_error($peg_connection);
}
//
// Success!
else {
  $cylinders = json_decode($cylinders_json);

  if (!$cylinders) {
    $render .= date("Y-m-d\TH:i:sP")
            . ": Received no data from the server.\n";
  }
  else {
    //
    // Prepare to write new sitemap.xml
    //
    $sitemap_xml_file = fopen(dirname(__FILE__) . "/../sitemap.xml", "w");
    if (!$sitemap_xml_file) {
      $render .= date("Y-m-d\TH:i:sP")
              . ": Sitemap regeneration failed—couldn't open <code>sitemap.xml</code>.\n\n";
    }
    else {
      $sitemap_xml = '<?xml version="1.0" encoding="UTF-8"?>'
                   . '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';

      $pages = array(
        "accordion",
        "adopt",
        "adopted",
        "americanvaudeville",
        "audiotheater",
        "blackartists",
        "browse",
        "cakewalks",
        "centraleurope",
        "contact",
        "december1908",
        "deutschekomische",
        "donate",
        "donors",
        "featured",
        "getty",
        "grammy",
        "grammy_additions",
        "help",
        "hillbilly",
        "history",
        "history-amberol",
        "history-bibliography",
        "history-blueamberol",
        "history-brownwax",
        "history-busybee",
        "history-chinese",
        "history-columbia",
        "history-concerts",
        "history-early",
        "history-edisonbell",
        "history-edisonforeign",
        "history-everlasting",
        "history-goldmoulded",
        "history-grandopera",
        "history-indestructible",
        "history-lambert",
        "history-lioret",
        "history-musicalstyles",
        "history-pathe",
        "history-sterling",
        "history-tinfoil",
        "history-wax",
        "homewax",
        "incunabula",
        "incunabula2",
        "index",
        "licensing",
        "links",
        "moran",
        "newadditions-june2008",
        "newadditions-march2007",
        "news",
        "online",
        "overview",
        "pecourt",
        "pilot",
        "pilotintro",
        "pilottech",
        "playlists",
        "press",
        "radio",
        "relaunch",
        "search",
        "speeches",
        "tahiti",
        "wav",
        "wwi-radio",
        "wwi-sources",
        "wwi",
      );

      foreach($pages as $page) {
        $sitemap_xml .= "<url>";
        $sitemap_xml .= "<loc>"
                     . "http://cylinders.library.ucsb.edu/"
                     . $page . ".php" . "</loc>";
        $sitemap_xml .= "<lastmod>"
                     // format the time according to http://www.w3.org/TR/NOTE-datetime
                     . date("Y-m-d\TH:i:sP",
                            filemtime("templates/". $page . ".tpl"))
                     . "</lastmod>";
        $sitemap_xml .= "</url>";
      }

      usort($cylinders, 'cyl_sort');

      foreach($cylinders as $cylinder) {
        $sitemap_xml .= "<url>";
        $sitemap_xml .= "<loc>"
                     . "http://cylinders.library.ucsb.edu/search.php?queryType=@attr+1=1020&amp;num=1&amp;start=1&amp;query="
                     . strtolower(str_replace(" ", "", $cylinder))
                     . "</loc>";
        $sitemap_xml .= "</url>";
      }
      $sitemap_xml .= "</urlset>";

      $write_return = fwrite($sitemap_xml_file, $sitemap_xml);
      if ($write_return) {
        $render .= date("Y-m-d\TH:i:sP")
                . ": Sitemap regeneration succeeded.\n\n";
      }
      else {
        $render .= date("Y-m-d\TH:i:sP")
                . ": Sitemap regeneration failed—couldn't write to <code>sitemap.xml</code>.\n\n";
      }
    }
  }
}

curl_close($peg_connection);

echo $render;
