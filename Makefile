.PHONY: clean test test-stage test-prod

sitemap.xml:
	bin/sitemap

test-stage:
	npm run s

test-prod:
	npm run p

test: test-stage test-prod

clean:
	rm *~
